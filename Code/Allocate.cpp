//! \file
//!
//! \brief Implementation of Functions for allocation and
//! initialisation of memory regions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Allocate.hpp"
#include <stdlib.h>  //for aligned_alloc, _aligned_malloc

namespace frao
{
void* AllocateSpaceOnly(natural64 bytes, natural64 alignment) noexcept
{
	runtime_assert(bytes != 0, "Cannot allocate zero bytes!");
	runtime_assert(alignment != 0, "Cannot have zero alignment!");
	runtime_assert((alignment & (alignment - 1)) == 0,
				   "Alignment must always be a power of two! Anything else is ill-formed");
	runtime_assert((bytes % alignment) == 0, "Allocated amount must be a multiple of alignment!");

	void* result = nullptr;

#if defined(FRAO_LIBVER_MICROSOFT)
	// Microsoft has decided not to offer _aligned_alloc, so we
	// have to use _aligned_malloc instead
	result = _aligned_malloc(bytes, alignment);
#else
	result = aligned_alloc(alignment, bytes);
#endif

	// TODO: throw, if we don't have a power of two
	return result;
}
}  // namespace frao
