//! \file
//!
//! \brief Experimental full implementation of unicode codepoint
//! clases
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/Experimental/CodePointIMPL_Expr.hpp"


#if defined(FRAO_EXPERIMENTAL_STRINGS_ALLOWED) \
	&& defined(NUCLEUS_HEADER_ONLY)
// frao::Strings::Experimental::CodePointIMPL instantiations
template class NUCLEUS_LIB_API
	Experimental::CodePointIMPL_UTF8<charU8>;
#ifdef FRAO_WCHAR_SMALL
template class NUCLEUS_LIB_API
	Experimental::CodePointIMPL_UTF16<wchar_t>;
#else
template class NUCLEUS_LIB_API
	Experimental::CodePointIMPL_UTF32<wchar_t>;
#endif
template class NUCLEUS_LIB_API
	Experimental::CodePointIMPL_UTF16<char16_t>;
template class NUCLEUS_LIB_API
	Experimental::CodePointIMPL_UTF32<char32_t>;
#endif