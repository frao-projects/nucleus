//! \file
//!
//! \brief Implementation of the frao unicode CodePoint class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/CodePoint.hpp"

// If we're not exporting or importing a dll interface, then we need
// to instantiate the templates
#if defined(NUCLEUS_HEADER_ONLY)

template class frao::Strings::CodePoint<frao::charU8>;
template class frao::Strings::CodePoint<char16_t>;
template class frao::Strings::CodePoint<wchar_t>;
template class frao::Strings::CodePoint<char32_t>;

#endif