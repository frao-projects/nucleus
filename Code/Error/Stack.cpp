//! \file
//!
//! \brief Implementation of Error Stack structures
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Error/Stack.hpp"

namespace frao
{
inline namespace ErrorLogging
{
namespace IMPL
{
// ResolveKernel function
ResolveKernel::ResolveKernel(
	std::function<ErrorAcceptance(BaseError&)> filter,
	std::function<ErrorResolution(BaseError&)> handler)
	: m_Filter(std::move(filter)), m_Handler(std::move(handler))
{}
//
ErrorAcceptance ResolveKernel::errorAccepted(BaseError& error)
{
	return m_Filter(error);
}
ErrorResolution ResolveKernel::tryResolveError(BaseError& error)
{
	return m_Handler(error);
}

// ResolveCount functions
ResolveCount::ResolveCount(void* ownerLoc) noexcept
	: m_WeakRefs(0), m_Owner(ownerLoc)
{
	runtime_assert(
		ownerLoc != nullptr,
		"Tried to create ResolveCount without a primary owner");
}
//
ResolveCount* ResolveCount::acquireWeakRef() noexcept
{
	// TODO: make into increment operator?
	++m_WeakRefs;

	return this;
}
//
std::unique_lock<std::shared_mutex> ResolveCount::getOwnerLock()
{
	return std::unique_lock<std::shared_mutex>(m_KernelMutex);
}
std::shared_lock<std::shared_mutex> ResolveCount::getAccessLock()
{
	return std::shared_lock<std::shared_mutex>(m_KernelMutex);
}
//
void ResolveCount::transferOwnership(
	const void* const priorOwner, const void* const newOwner) noexcept
{
	if (m_Owner.load() == priorOwner)
	{
		m_Owner.store(newOwner);
	}
}
//
natural64 ResolveCount::decrementCount() noexcept
{
	natural64 afterRefs = --m_WeakRefs;

	if (hasOwner())
	{
		++afterRefs;
	}

	return afterRefs;
}
//
bool ResolveCount::hasOwner() const noexcept
{
	return (m_Owner != nullptr);
}
natural64 ResolveCount::refCount() const noexcept
{
	natural64 result = m_WeakRefs.load();

	if (hasOwner())
	{
		++result;
	}

	return result;
}
}  // namespace IMPL

// ErrorResolver functions
void ErrorResolver::destroy(void* const			  owner,
							IMPL::ResolveKernel*& kernel,
							IMPL::ResolveCount*&  count) noexcept
{
	// make sure that noone is using the m_Kernel
	auto ownerLock = count->getOwnerLock();

	if (kernel != nullptr)
	{
		// if m_Kernel is non-null, then count must be

		runtime_assert(count != nullptr);
		count->transferOwnership(owner, nullptr);
		DestroyObject(kernel);

		if (count->refCount() == 0)
		{
			DestroyObject(count);
		}
	}

	kernel = nullptr;
	count  = nullptr;
}
//
ErrorResolver::ErrorResolver(
	std::function<ErrorAcceptance(BaseError&)> filter,
	std::function<ErrorResolution(BaseError&)> handler)
	: m_Kernel(nullptr), m_Count(Allocate<IMPL::ResolveCount>(this))
{
	if (m_Count == nullptr)
	{
		// TODO: remove this, when we move to exceptions in
		// Allocate
		throw create_type<MemAlloc>(
			FILELOC_UTF8, "Failed to allocate ResolveCount");
	}

	// create kernel after we create count, in case creating count
	// fails.
	auto ownerLock = m_Count->getOwnerLock();
	m_Kernel	   = Allocate<IMPL::ResolveKernel>(std::move(filter),
											   std::move(handler));

	if (m_Kernel == nullptr)
	{
		// TODO: turn into try catch around m_Kernal alloc,
		// instead
		DestroyObject(m_Kernel);
		throw create_type<MemAlloc>(
			FILELOC_UTF8, "Failed to allocate ResolveKernel");
	}
}
//
ErrorResolver::ErrorResolver(ErrorResolver&& rhs) noexcept
	: m_Kernel(nullptr), m_Count(nullptr)
{
	// make sure we're moving from a valid object, because moving
	// from an invalid one is an error
	runtime_assert(rhs.m_Kernel != nullptr,
				   "Attempted to move from an invalid object!");
	runtime_assert(rhs.m_Count != nullptr,
				   "Attempted to move from an invalid object!");

	// no lock needed for move, as ErrorResolver should be directly
	// used by only thread only, and we're not changing anything
	// that should be noticed by weak reference objects
	rhs.m_Count->transferOwnership(&rhs, this);
	m_Count  = rhs.m_Count;
	m_Kernel = rhs.m_Kernel;

	// invalidate rhs, now we have its insides
	rhs.m_Count  = nullptr;
	rhs.m_Kernel = nullptr;
}
ErrorResolver& ErrorResolver::operator=(ErrorResolver&& rhs) noexcept
{
	// make sure we're moving from a valid object, because moving
	// from an invalid one is an error
	runtime_assert(rhs.m_Kernel != nullptr,
				   "Attempted to move from an invalid object!");
	runtime_assert(rhs.m_Count != nullptr,
				   "Attempted to move from an invalid object!");

	destroy(this, m_Kernel, m_Count);

	// no lock needed for move, as ErrorResolver should be directly
	// used by only thread only, and we're not changing anything
	// that should be noticed by weak reference objects
	rhs.m_Count->transferOwnership(&rhs, this);
	m_Count  = rhs.m_Count;
	m_Kernel = rhs.m_Kernel;

	// invalidate rhs, now we have its insides
	rhs.m_Count  = nullptr;
	rhs.m_Kernel = nullptr;

	return *this;
}
ErrorResolver::~ErrorResolver() noexcept
{
	destroy(this, m_Kernel, m_Count);
}
//
ErrorAcceptance ErrorResolver::errorAccepted(BaseError& error)
{
	auto accessLock = m_Count->getAccessLock();

	return m_Kernel->errorAccepted(error);
}
ErrorResolution ErrorResolver::tryResolveError(BaseError& error)
{
	auto accessLock = m_Count->getAccessLock();

	return m_Kernel->tryResolveError(error);
}

// ResolveReferrer functions
void ResolveReferrer::destroy(IMPL::ResolveCount*& count) noexcept
{
	runtime_assert(count != nullptr,
				   "ResolveReferrer must always have a count. This "
				   "invariant has been violated somehow");

	// decrement, and get the real count after the operation.
	if (count->decrementCount() == 0)
	{
		// we know that this is fine, because we were the last
		// object which referred to m_Count, and copying us during
		// our destruction is a violation of our copy constructor/
		// assignment operator preconditions
		DestroyObject(count);
	}

	count = nullptr;
}
//
ResolveReferrer::ResolveReferrer(
	const ErrorResolver& errStack) noexcept
	: m_Stack(nullptr), m_Count(errStack.m_Count->acquireWeakRef())
{
	m_Stack = errStack.m_Kernel;
}
//
ResolveReferrer::ResolveReferrer(
	const ResolveReferrer& ResolveReferrer) noexcept
	: m_Stack(nullptr),
	  m_Count(ResolveReferrer.m_Count->acquireWeakRef())
{
	m_Stack = ResolveReferrer.m_Stack;
}
ResolveReferrer& ResolveReferrer::operator=(
	const ResolveReferrer& rhs) noexcept
{
	destroy(m_Count);

	m_Count = rhs.m_Count->acquireWeakRef();
	m_Stack = rhs.m_Stack;

	return *this;
}
ResolveReferrer::~ResolveReferrer() noexcept
{
	destroy(m_Count);
}
//
ErrorAcceptance ResolveReferrer::errorAccepted(BaseError& error)
{
	auto accessLock = m_Count->getAccessLock();

	// once we've locked the kernel for normal use, see if it exists
	if (!m_Count->hasOwner())
	{
		// then kernel no longer exists
		m_Stack = nullptr;
		return ErrorAcceptance::NoFunctor;
	}

	// we know that the kernel exists, so now we can do stuff!
	return m_Stack->errorAccepted(error);
}
ErrorResolution ResolveReferrer::tryResolveError(BaseError& error)
{
	auto accessLock = m_Count->getAccessLock();

	// once we've locked the kernel for normal use, see if it exists
	if (!m_Count->hasOwner())
	{
		// then kernel no longer exists
		m_Stack = nullptr;
		return ErrorResolution::NoFunctor;
	}

	// we know that the kernel exists, so now we can do stuff!
	return m_Stack->tryResolveError(error);
}
}  // namespace ErrorLogging
}  // namespace frao