#ifndef FRAO_NUCLEUS_BIT_UTILITIES
#define FRAO_NUCLEUS_BIT_UTILITIES

//! \file
//!
//! \brief Header of basic bit manipulation functions (setters etc.)
//! some of which are necessary for the basic function of nucleus core
//! functions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <limits>
// include static.hpp directly, since this file is used by the runtime
// portion of environment, so including those files would result in
// cyclical includes. ifndef's would stop catastrophy, but still seems
// like a bad idea
#include "Environment/Static.hpp"

namespace frao
{
//! \brief get the position of the highest set bit, or 0 for 0
//!
//! \warning With the release of c++20, and the <bit> header, this
//! function is no longer neccesary. Behaviour is equivalent to
//! (return std::numeric_limits<T>::digits - std::countl_zero(x) - 1)
//!
//! \note This function is not really efficient, but it's mostly
//! just a placeholder until std::floor2 can be used. Or failing
//! that, until we need the efficiency, in which case we can use a
//! better algorithm (at the expense of readablity). The behaviour
//! wrt zero is designed to replicate the specified design of
//! std::floor2
//!
//! \todo not ideal behaviour for 1 and 0 inputs. Change, if we
//! decide against using floor2, later
//!
//! \todo Think about using per-compiler intrinsics, instead of
//! floor2. We'd lose constexpr, though
//!
//! \returns the highest value of k, s.t 2^k <= value. Equivalent
//! to 63-leading_zeroes(value)
//!
//! \param[in] value The number that we wish to test
[[deprecated(
	"Use Bit header directly")]] NUCLEUS_LIB_API constexpr natural64
Log2Floor(natural64 value) noexcept
{
	natural64 result = 63;
	natural64 mask	 = 1ULL << 63ULL;

	// just iterate down the bits, to check for first 1-bit set
	// NOTE: this is not efficient, but it's super-readable and in
	// a constexpr function, so this should be a decent compromise
	// for now
	while ((result > 0) && ((value & mask) == 0))
	{
		// we just iterate from the highest bit down, and count
		// the leading zeros. result will be set to 63-leading
		// zeroes at the end
		--result;
		mask = mask >> 1ULL;
	}

	return result;
}

//! \defgroup BitManipulationFunctions Bit Manipulation Functions
//!
//! \brief functions for manipulating the bits of functions
//!
//! {

//! \brief Will determine whether a given bit in a bitfield is set.
//!
//! \note Obviously can be used for standard numbers too, and not just
//! bitfields.
//!
//! \returns boolean with value true iff the specified bit was set
//!
//! \param[in] bitfield The bitfield or number that we wish to examine
//! the bits of
//!
//! \param[in] bitNum The number of the bit to check, as offset from
//! low valued end. range of [0, 31]. Values outside range may result
//! in undefined behviour
NUCLEUS_LIB_API constexpr bool BitIsSet(small_nat32 bitfield,
										small_nat8	bitNum) noexcept
{
	return (bitfield & (1 << bitNum)) != 0;
}

//! \brief Extract contiguous bits from a given source
//!
//! \note equivalent to Bit field extract (BEXTR) Instruction from
//! BMI1
//!
//! \returns The required bits, suitably shifted as desired
//!
//! \param[in] source The number from which to extract the bits
//!
//! \param[in] length The number of consecutive bits to extract, from
//! the source. Must not be greater than 31
//!
//! \param[in] lowPos The shift down, of the extracted bits. The bit
//! position, of the bit that shall be, at the end, in the 0 position.
//! eg: if 0b101100 was extracted, then a value of 2 would ensure a
//! result of 0b001011 was returned. Length must be less than 64!
NUCLEUS_LIB_API constexpr small_nat64 ExtractBits(
	small_nat64 source, small_nat8 length, small_nat8 lowPos) noexcept
{
	// if not for constexpr, this function could just use BEXTR
	// instruction, from BMI. Visual c++ (actually intel) intrinsic of
	// this is _bextr_u64

	return (source >> lowPos)
		   & ((1ULL << static_cast<small_nat64>(length)) - 1ULL);
}

//! \brief Set specified bits in sink parameter to specified values.
//!
//! \result The sink parameter, with bits suitably set as required
//!
//! \param[in] sink The recipient of the result of this operation.
//! Bits that are not set, will be carried through to the result,
//! although input parameter will remaing unchanged
//!
//! \param[in] source The number / bitfield from which the set bits
//! will be extracted, according to the mask.
//!
//! \param[in] mask Bitfield that details the bits the select from
//! source param. eg: in a source abcd, a mask of 1101 would result in
//! ab0d, which would then be shifted by the amount specified in
//! lowPos
//!
//! \param[in] lowPos The shift up, of the bits to be set. The
//! position of the lowest set bit after shift
NUCLEUS_LIB_API constexpr small_nat64 SetBits(
	small_nat64 sink, small_nat64 source, small_nat64 mask,
	small_nat8 lowPos) noexcept
{
	mask <<= lowPos;

	return (mask & (source << lowPos)) | (sink & (~mask));
}

// turn off clang format, because it screws up the deprecation
// declarations

// clang format off

//! \brief Get the high bits of an unsigned 16-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 8, 8)),
//! instead
//!
//! \returns high half of input, in an unsigned 8-bit  integer
//!
//! \param[in] num unsigned 16-bit integer
[[deprecated(
	"Use static_cast<small_nat8>(ExtractBits(num, 8, "
	"8)), instead")]] NUCLEUS_LIB_API inline small_nat8
topHalf(small_nat16 num)
{
	return static_cast<small_nat8>(num >> 8);
}

//! \brief Get the high bits of an unsigned 32-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 16, 16)),
//! instead
//!
//! \returns high half of input, in an unsigned 16-bit integer
//!
//! \param[in] num unsigned 32-bit integer
[[deprecated(
	"Use static_cast<small_nat16>(ExtractBits(num, 16, "
	"16)), instead")]] NUCLEUS_LIB_API inline small_nat16
topHalf(small_nat32 num)
{
	return static_cast<small_nat16>(num >> 16);
}

//! \brief Get the high bits of an unsigned 64-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 32, 32)),
//! instead
//!
//! \returns high half of input, in an unsigned 32-bit integer
//!
//! \param[in] num unsigned 64-bit integer
[[deprecated(
	"Use static_cast<small_nat32>(ExtractBits(num, 32, "
	"32)), instead")]] NUCLEUS_LIB_API inline small_nat32
topHalf(small_nat64 num)
{
	return static_cast<small_nat32>(num >> 32);
}

//! \brief Get the low bits of an unsigned 16-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 8, 0)),
//! instead
//!
//! \returns low half of input, in an unsigned 8-bit integer
//!
//! \param[in] num unsigned 16-bit integer
[[deprecated(
	"Use static_cast<small_nat8>(ExtractBits(num, 8, "
	"0)), instead")]] NUCLEUS_LIB_API inline small_nat8
bottomHalf(small_nat16 num)
{
	return static_cast<small_nat8>(num & 0xff);
}

//! \brief Get the low bits of an unsigned 32-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 16, 0)),
//! instead
//!
//! \returns low half of input, in an unsigned 16-bit
//! integer
//!
//! \param[in] num unsigned 32-bit integer
[[deprecated(
	"Use static_cast<small_nat16>(ExtractBits(num, 16, "
	"0)), instead")]] NUCLEUS_LIB_API inline small_nat16
bottomHalf(small_nat32 num)
{
	return static_cast<small_nat16>(num & 0xffff);
}

//! \brief Get the low bits of an unsigned 64-bit integer
//!
//! \deprecated Use static_cast<small_nat8>(ExtractBits(num, 32, 0)),
//! instead
//!
//! \returns low half of input, in an unsigned 32-bit
//! integer
//!
//! \param[in] num unsigned 64-bit integer
[[deprecated(
	"Use static_cast<small_nat32>(ExtractBits(num, 32, "
	"0)), instead")]] NUCLEUS_LIB_API inline small_nat32
bottomHalf(small_nat64 num)
{
	return static_cast<small_nat32>(num & 0xffffffff);
}

//! \brief Combine low and high halves of an unsigned 8-bit integer
//! into one 16-bit unsigned integer
//!
//! \deprecated Use static_cast<small_nat16>(SetBits(low, high, 0xFF,
//! 8)), instead
//!
//! \returns unsigned 16-bit integer
//!
//! \param[in] low unsigned 8-bit integer, to be put in low half of
//! result
//!
//! \param[in] high unsigned 8-bit integer, to be put in high half of
//! result
[[deprecated(
	"Use static_cast<small_nat16>(SetBits(low, high, "
	"0xFF, 8)), instead")]] NUCLEUS_LIB_API inline small_nat16
combine(small_nat8 low, small_nat8 high)
{
	return static_cast<small_nat16>(low)
		   | (static_cast<small_nat16>(high) << 8);
}

//! \brief Combine low and high halves of an unsigned 16-bit
//! integer into one 32-bit unsigned integer
//!
//! \deprecated Use static_cast<small_nat16>(SetBits(low, high,
//! 0xFFFF, 16)), instead
//!
//! \returns unsigned 32-bit integer
//!
//! \param[in] low unsigned 16-bit integer, to be put in low
//! half of result
//!
//! \param[in] high unsigned 16-bit integer,
//! to be put in high half of result
[[deprecated(
	"Use static_cast<small_nat32>(SetBits(low, high, "
	"0xFFFF, 16)), instead")]] NUCLEUS_LIB_API inline small_nat32
combine(small_nat16 low, small_nat16 high)
{
	return static_cast<small_nat32>(low)
		   | (static_cast<small_nat32>(high) << 16);
}

//! \brief Combine low and high halves of an unsigned 32-bit integer
//! into one 64-bit unsigned integer
//!
//! \deprecated Use static_cast<small_nat16>(SetBits(low, high,
//! 0xFFFFFFFF, 32)), instead
//!
//! \returns unsigned 64-bit integer
//!
//! \param[in] low unsigned 32-bit integer, to be put in low half of
//! result
//!
//! \param[in] high unsigned 32-bit integer, to be put in high half of
//! result
[[deprecated(
	"Use SetBits(low, high, 0xFFFFFFFF, 32), "
	"instead")]] NUCLEUS_LIB_API inline small_nat64
combine(small_nat32 low, small_nat32 high)
{
	return static_cast<small_nat64>(low)
		   | (static_cast<small_nat64>(high) << 32);
}

// clang format on

//! @}


//! \brief Union for ensuring that bit-preserving conversion between
//! float and natural (unsigned integer) types
//!
//! \tparam FloatType the float type in the union. \pre Must be the
//! same size as NaturalType
//!
//! \tparam FloatType the natural (unsigned integer) type in the
//! union. \pre Must be the same size as FloatType
template<typename FloatType, typename NaturalType>
union BitFloat
{
	//! \brief The first union member, which can be used to access
	//! the data in float format
	FloatType num_f;
	//! \brief Union member, which can be used to access the data
	//! in natural (unsigned integer) format
	NaturalType num_n;

	static_assert(sizeof(FloatType) == sizeof(NaturalType),
				  "We need the sizes of the types to be the "
				  "same, or we could get some weird results");

	//! \brief Construct, using a floating point number
	//!
	//! \param[in] number The floating point number to store
	BitFloat(FloatType number) : num_f(number)
	{}
	//! \brief Construct, using a natural number
	//!
	//! \param[in] number The natural number to store
	BitFloat(NaturalType number) : num_n(number)
	{}

	//! \brief Get the maximum binary value that can be stored in the
	//! mantissa, for this FloatType
	//!
	//! \returns The maximum (binary) value that be be stored in the
	//! mantissa
	static constexpr NaturalType maxMantissa() noexcept
	{
		constexpr const NaturalType
			digits = std::numeric_limits<FloatType>::digits,
			one	   = 1;

		// we have 1 as a variable so that it's a 1 of the right type,
		// without casting
		return (one << (digits - one)) - one;
	}

	//! \brief Get the maximum binary value that can be stored in the
	//! exponent, after unbiasing, for this FloatType
	//!
	//! \note the exponent returned corresponds to the binary value
	//! that will indicate +/- Inf, or NaN, and cannot be used as a
	//! 'real' exponent. The maximum 'real' exponent is one less than
	//! the returned value
	//!
	//! \returns The maximum (binary) value that be be stored in the
	//! exponent, once the bias is removed
	static constexpr NaturalType maxExponent() noexcept
	{
		return std::numeric_limits<FloatType>::max_exponent;
	}

	//! \brief Get the maximum binary value that can be stored in the
	//! biased exponent, for this FloatType
	//!
	//! \note the exponent returned corresponds to the binary value
	//! that will indicate +/- Inf, or NaN, and cannot be used as a
	//! 'real' exponent. The maximum 'real' exponent is one less than
	//! the returned value
	//!
	//! \returns The maximum (binary) value that be be stored in the
	//! exponent, before the bias is removed
	static constexpr NaturalType maxBiasedExponent() noexcept
	{
		return ((maxExponent() - 1) << 1) | 1;
	}

	//! \brief The shift applied to an exponent, to get or set it from
	//! a normal number.
	//!
	//! \note This must be applied before reteiving a value, with a
	//! masking operation
	//!
	//! \returns The shift that should be applied to the number, to
	//! ensure the exponent is in the lower order bits
	static constexpr NaturalType exponentShift() noexcept
	{
		return std::numeric_limits<FloatType>::digits - 1;
	}

	//! \brief determine if two BitFloats are equal
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is equal to rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this
	bool operator==(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f == rhs.num_f;
	}
	//! \brief determine if two BitFloats are unequal
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is unequal to rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this
	bool operator!=(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f != rhs.num_f;
	}

	//! \brief determine if this is less than rhs
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is less than rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this, on the
	//! right hand side
	bool operator<(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f < rhs.num_f;
	}
	//! \brief determine if this is greater than rhs
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is greater than rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this, on the
	//! right hand side
	bool operator>(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f > rhs.num_f;
	}

	//! \brief determine if this is less than or equal to rhs
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is less than rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this, on the
	//! right hand side
	bool operator<=(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f <= rhs.num_f;
	}
	//! \brief determine if this is greater than or equal to rhs
	//!
	//! \note will compare using floating point comparison, as
	//! that has more rules than integer comparison
	//!
	//! \returns The value true if this is greater than rhs
	//!
	//! \param[in] rhs The BitFloat to compare, to this, on the
	//! right hand side
	bool operator>=(const BitFloat<FloatType, NaturalType>& rhs) const
	{
		return num_f >= rhs.num_f;
	}
};

}  // namespace frao


#endif