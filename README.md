# Introduction and Setup

*Nucleus* is the frao basic facilities API for environment, strings, timing and error handling. These basic facilties are used in a range (eg: all) of other frao projects

## Structure
The error handling section uses the features of all other sections on nucleus, and
unsurprisingly, deals with handling, logging, and reporting errors.

The timing section used the features of all other sections, except error handling, and
deals with both calender timings, as well as high-performance millisecond (and below)
timing.

The strings section handles utf8/16/32 strings, converting between them, and related
features, such as conversion to and from numbers.

Then, the environment section deals with ascertaining the circumstances of compilation,
as well as those of OS and runtime CPU.

## Setup
Nucleus, as an API, is designed to be used as a meson subproject. In that case, one needs simply to use as any other meson subproject. If one wishes to use nucleus as a primary project however, and use its scripts, extra care must be taken.

First, one should download/clone the Nucleus gitlab repo, to a suitable location. Since Nucleus uses out of source builds, as is typical with meson, the source file should be located next to a seperate build directory.

There are three ways of setting up nucleus, as a primary project (ie: not a subproject) with the ability to run scripts. They are:
1. [With VSCode](#with-vscode), running setup scripts pre-configured, using vscode tasks
2. [With Powershell Scripts](#with-powershell-scripts), running the powershell scripts yourself, outside of vscode
3. [Manually](#manually), setting up meson build directory yourself, and building your own paths

### With VSCode
For VSCode, simply open the nucleus source folder in VSCode, and run the 'Generate Build folders' task. It will setup a default meson build structure, in ../Build/, as well as writing required build and script data to the .frao directory. The default build structure will be based on current operating system, and installed compilers. Assuming it runs successfully, you will now be setup for running any nucleus scripts, via the other tasks. 

### With Powershell Scripts
In this case, you will have to run the SetupBuildSystem.ps1 powershell script, from the .frao directory. For ths script, the following parameters are mandatory:
- 'SourceFolder'; this is the location of repo folder itself
- 'BuildFolder'; this is the location of the build folder, into which all build artifacts, libraries, and documentation will be built
- 'InstallType'; one of 'NoInstall' (for using meson install defaults), 'Install' (for installing to root/projectname/os/compiler/config), or 'NoOSInstall' (for installing to root/projectname/compiler/config). If you do not intend to install the API anywhere, just pass NoInstall.
- 'DefaultLibrary'; This is the default type of library to build. Either shared (.so/.dll), static (.lib/.a), or both.

### Manually
If you setup vscode manually, you simply set up the meson build folders as you would for any other project, except that after you have, you should compile the ./Build_Paths target. This will create the proper script information in the .frao directory