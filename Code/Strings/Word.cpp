//! \file
//!
//! \brief Implementation of the frao unicode string class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/Word.hpp"

// If we're not exporting or importing a dll interface, then we need
// to instantiate the templates
#if defined(NUCLEUS_HEADER_ONLY)
// frao::Strings::Transitional::WordIMPL instantiations
template class frao::Strings::Transitional::WordIMPL<
	frao::charU8,
	frao::Strings::AsciiNumericalPunctuation<frao::charU8, false>>;
template class frao::Strings::Transitional::WordIMPL<
	wchar_t,
	frao::Strings::AsciiNumericalPunctuation<wchar_t, false>>;
template class frao::Strings::Transitional::WordIMPL<
	char16_t,
	frao::Strings::AsciiNumericalPunctuation<char16_t, false>>;
template class frao::Strings::Transitional::WordIMPL<
	char32_t,
	frao::Strings::AsciiNumericalPunctuation<char32_t, false>>;

#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
template class frao::Strings::Experimental::WordIMPL<
	frao::charU8,
	frao::Strings::AsciiNumericalPunctuation<frao::charU8, true>>;
template class frao::Strings::Experimental::WordIMPL<
	wchar_t,
	frao::Strings::AsciiNumericalPunctuation<wchar_t, true>>;
template class frao::Strings::Experimental::WordIMPL<
	char16_t,
	frao::Strings::AsciiNumericalPunctuation<char16_t, true>>;
template class frao::Strings::Experimental::WordIMPL<
	char32_t,
	frao::Strings::AsciiNumericalPunctuation<char32_t, true>>;
#endif
#endif

// If we're not exporting or importing a dll interface, then we need
// to instantiate the templates
#if defined(NUCLEUS_HEADER_ONLY)

template class frao::Strings::Word<frao::charU8>;
template class frao::Strings::Word<char16_t>;
template class frao::Strings::Word<wchar_t>;
template class frao::Strings::Word<char32_t>;

#endif