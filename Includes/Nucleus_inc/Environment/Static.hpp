#ifndef FRAO_NUCLEUS_ENVIRONMENT_STATIC
#define FRAO_NUCLEUS_ENVIRONMENT_STATIC

//! \file
//!
//! \brief Header of all compile-time constant defintions, functions,
//! and classes, relating to the environment of the software (OS env,
//! Compiler env, API env, etc)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Static/APIDefines.hpp"
#include "Static/Checking.hpp"

#endif