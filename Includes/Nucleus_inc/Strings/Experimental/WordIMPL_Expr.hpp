#ifndef FRAO_NUCLEUS_STRINGS_WORD_IMPLEMENTATION_EXPERIMENTAL
#define FRAO_NUCLEUS_STRINGS_WORD_IMPLEMENTATION_EXPERIMENTAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the experimental implementation of unicode
//! word class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <initializer_list>	 //for initializer_list constructor
#include <vector>			 //for internal m_DataArray implementation
#include "../../Environment.hpp"
#include "../GraphemeCluster.hpp"
#include "StringStore.hpp"

namespace frao
{
inline namespace Strings
{
namespace Experimental
{
//! \brief Experimental implementation class, for words
//!
//! \tparam CharType The underlying type of the word [charU8,
//! char16_t, char32_t, wchar_t].
template<typename CharType, typename NumericalPunctuation>
class NUCLEUS_LIB_API WordIMPL
{
	//! \brief Stores the actual word data
	IMPL::StringStore<CharType> m_WordData;

   public:
	//! \brief Type for referring to our own type, where appropriate
	using OurType =
		Experimental::WordIMPL<CharType, NumericalPunctuation>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = GraphemeCluster<CharType, true>;

	//! \brief construct empty word
	WordIMPL();

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_nat8 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat16 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat32 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat64 number);

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int8 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int16 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int32 number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int64 number);

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to a word
	explicit WordIMPL(float number);
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to a word
	explicit WordIMPL(double number);

	//! \brief construct word from the first word in the
	//! specified string
	//!
	//! \param[in] string pointer to a buffer containing a
	//! string. This string has at least one word
	//!
	//! \param[in] buffersize unsigned 64-bit integer than
	//! contains the size of the buffer pointed to in
	//! 'string'
	//!
	//! \param[in] getLastWord iff true, will scan for the last valid
	//! word in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct word to read. If null, will not be written to
	WordIMPL(const CharType* string, frao::natural64 buffersize,
			 bool getLastWord, natural64* readStart);

	//! \brief Construct a word from an ascii string
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	WordIMPL(const charA* string);

	//! \brief construct word from the first word in the
	//! specified string
	//!
	//! \param[in] clusters a list of clusters to make into a word
	//!
	//! \note they must actually consitute a [single] word, as far as
	//! unicode algorithms are concerned
	WordIMPL(std::initializer_list<ClusterType> clusters);

	//! \brief Copy construct a word, from another word of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other word, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The word to copy from
	template<typename OtherType, typename OtherNumericalPunctuation>
	explicit WordIMPL(
		const WordIMPL<OtherType, OtherNumericalPunctuation>& rhs);
	//! \brief Copy construct a word, from another word of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The word to copy from
	WordIMPL(const OurType& rhs);
	//! \brief Move construct a word, from another word of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The word to move from
	WordIMPL(OurType&& rhs);
	//! \brief destroy the word, and call any derived destructors
	virtual ~WordIMPL() = default;

	//! \brief Copy assign a word, from another word of the same
	//! underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The word to copy from
	OurType& operator=(const OurType& rhs) = default;
	//! \brief Move assign a word, from another word of the same
	//! underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The word to move from
	OurType& operator=(OurType&& rhs) = default;

	//! \brief add a grapheme cluster to the end of this
	//! word
	//!
	//! \param[in] cluster to append to word. Should be a
	//! cluster that makes sense to append to this word.
	//! (ie: don't try and append a space to 'dog', for
	//! example)
	void appendIMPL(const ClusterType& cluster);
	//! \brief remove all grapheme clusters from this word,
	//! rendering it empty
	void clearIMPL();

	//! \brief returns memory size of word, in bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSizeIMPL() const noexcept;
	//! \brief returns logical size of word, in grapheme
	//! clusters
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of grapheme clusters
	frao::natural64 sizeIMPL() const noexcept;

	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A Grapheme cluster reference of the same
	//! underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	ClusterType& getIMPL(frao::natural64 index) noexcept;
	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A const Grapheme cluster reference of the
	//! same underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	const ClusterType& getIMPL(frao::natural64 index) const noexcept;

	//! \brief converts this word to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[out] number reference to where the result should be
	//! held
	template<typename NumberType>
	bool toNumberIMPL(NumberType& number) const noexcept;

	//! \brief Will determine the ordering of this, against a
	//! word, of any valid underlying type
	//!
	//! \tparam OtherType The type of the other word, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t,
	//! char32_t]
	//!
	//! \details Compares the words, with this being on the left
	//! hand side, and the other words on the right. Will return <
	//! 0 for *this < other, will return > 0 for *this > other,
	//! and will return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than,
	//! and > 0 meaning greater than
	//!
	//! \param[in] otherWord The word to compare this with, on the
	//! rhs.
	template<typename OtherType, typename OtherNumericalPunctuation>
	frao::integer64 getOrderIMPL(
		const WordIMPL<OtherType, OtherNumericalPunctuation>&
			otherWord) const noexcept;
};
}  // namespace Experimental
}  // namespace Strings
}  // namespace frao

#endif