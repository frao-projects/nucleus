#ifndef FRAO_NUCLEUS_TIME_CLOCK_TIME
#define FRAO_NUCLEUS_TIME_CLOCK_TIME

//! \file
//!
//! \brief Header of clock time structures, used for recording times
//! of day, and days of the year, etc. That is, non-performance timing
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <optional>  //for safely returning contained object, from union
#include <type_traits>  //for std::is_same
#include "../Environment.hpp"
#include "../Strings.hpp"
#include "TimeStrings.hpp"

namespace frao
{
inline namespace Time
{
//! \brief simple structure that holds clock data, for the classes
//! that require it, since it's required in several places
struct NUCLEUS_LIB_API RawTimeData
{
	//! \brief Year, in the range [1601, 30827]
	natural16 m_Year;
	//! \brief Milliseconds, in the range [0, 999]
	natural16 m_MilliSeconds;
	//! \brief Month, in the range [1, 12] (jan = 1)
	natural8 m_Month;
	//! \brief Day of the month, in the range [1, 31]
	natural8 m_DayOfMonth;
	//! \brief Day of the week, in the range [0, 6] (Sun = 0)
	natural8 m_DayOfWeek;
	//! \brief Hour, in the range [0, 23]
	natural8 m_Hour;
	//! \brief Minute, in the range [0, 59]
	natural8 m_Minute;
	//! \brief Second, in the range [0, 59]
	natural8 m_Second;

	//! \brief Default construct raw time data, with current time
	RawTimeData() noexcept;

	//! \brief update the stored time data, with the current time
	void updateTime() noexcept;

	//! \brief equality comparison. returns true iff all members are
	//! the same (From year down to second)
	//!
	//! \returns true iff the time data is equal, between the two
	//! objects
	//!
	//! \param[in] rhs Time to compare to this
	bool operator==(const RawTimeData& rhs) const noexcept;

	//! \brief inequality comparison. Equivalent to !((*this) == rhs).
	//! That is, returns true if any member(s) differ, between *this
	//! and rhs
	//!
	//! \returns true iff the time data is unequal, between the two
	//! objects
	//!
	//! \param[in] rhs Time to compare to this
	bool operator!=(const RawTimeData& rhs) const noexcept;
};

//! \brief Enum detailing the types of time data that can be held/used
enum class TimeType
{
	//! \brief Data pertaining to the current year
	Year,
	//! \brief Data pertaining to the current Month
	Month,
	//! \brief Data pertaining to the current day of the month (1st,
	//! 2nd etc)
	DayOfMonth,
	//! \brief Data pertaining to the current day of the week (monday,
	//! tuesday, etc)
	DayOfWeek,
	//! \brief Data pertaining to the current hour
	Hour,
	//! \brief Data pertaining to the current minute
	Minute,
	//! \brief Data pertaining to the current second
	Second,
	//! \brief Data pertaining to the current millsecond
	Millisecond
};

//! \brief struct holding information of what data to use, from the
//! raw time data, and how to print it
//!
//! \tparam CharType The underlying character type, of the string to
//! be built
template<typename CharType>
struct NUCLEUS_LIB_API TimeDataDesc
{
	//! \brief The specification of how to print the acquired data
	TimePrintSpec<CharType> m_PrintSpec;
	//! \brief The specification of what data, from the raw data,
	//! should be sent for printing
	TimeType m_DataType;

	//! \brief Construct a description of what data to acquire, and
	//! how to print it
	//!
	//! \param[in] dataType The type of data (year, month, hour etc)
	//! to be acquired, from the raw data
	//!
	//! \param[in] style The standard style, with which the data
	//! should be printed
	TimeDataDesc(TimeType dataType, TimePrintStyle style) noexcept
		: m_PrintSpec(style), m_DataType(dataType)
	{}
	//! \brief Construct a description of what data to acquire, and
	//! how to print it
	//!
	//! \param[in] dataType The type of data (year, month, hour etc)
	//! to be acquired, from the raw data
	//!
	//! \param[in] customArray The array to be used, instead of the
	//! standard printing styles. The data will be used as an index to
	//! the array % size of array
	TimeDataDesc(
		TimeType							dataType,
		std::vector<Basic_String<CharType>> customArray) noexcept
		: m_PrintSpec(std::move(customArray)), m_DataType(dataType)
	{}

	//! \brief Construct a string, as previously described, from the
	//! supplied raw time data
	//!
	//! \returns The constructed string, of the supplied data
	//!
	//! \param[in] data A const reference to the raw time data
	Basic_String<CharType> parse(const RawTimeData& data) const
	{
		switch (m_DataType)
		{
			case TimeType::Month:
				return m_PrintSpec.parse(data.m_Month);
			case TimeType::DayOfMonth:
				return m_PrintSpec.parse(data.m_DayOfMonth);
			case TimeType::DayOfWeek:
				return m_PrintSpec.parse(data.m_DayOfWeek);
			case TimeType::Hour:
				return m_PrintSpec.parse(data.m_Hour);
			case TimeType::Minute:
				return m_PrintSpec.parse(data.m_Minute);
			case TimeType::Second:
				return m_PrintSpec.parse(data.m_Second);
			case TimeType::Millisecond:
				return m_PrintSpec.parse(data.m_MilliSeconds);
			default:
				runtime_assert(m_DataType == TimeType::Year,
							   "Unrecognised TimeType!");
				return m_PrintSpec.parse(data.m_Year);
		}
	}
};

//! \brief class which can optionally hold either a String, or a
//! description of how to parse time raw data. Used to hold a logical
//! section of a TimeFormat
//!
//! \tparam CharType The underlying character type of the String and
//! Desciption
template<typename CharType>
class NUCLEUS_LIB_API TimeSegmentDesc final
{
   public:
	//! \brief Enum to describe what type of segments this is
	enum class SegmentType
	{
		//! \brief Used to describe string segments, which merely
		//! print the provided string
		String,
		//! \brief Used to describe data segments, which take time
		//! data, and provide a string representation of it
		Data
	};

   private:
	union
	{
		//! \brief A string, that isn't dependent on any time data
		Basic_String<CharType> m_String;
		//! \brief A description of how to produce a string, given
		//! some time data
		TimeDataDesc<CharType> m_Data;
	};
	//! \brief The type of data currently held. A string, or a
	//! description of how to produce one, given some time data
	SegmentType m_SegmentType;

	//! \brief Helper function, which destroys the currently active
	//! member of the union, since this must be done manually. Used
	//! for destructor, but also assignment operators
	void destroyCurrentUnionMember() noexcept
	{
		if (m_SegmentType == SegmentType::String)
		{
			m_String.~Basic_String<CharType>();
		} else
		{
			runtime_assert(
				m_SegmentType == SegmentType::Data,
				"Unrecognised segment type! TimeSegmentDesc will be "
				"unsafe, unless this is unaccounted for!");

			m_Data.~TimeDataDesc<CharType>();
		}
	}

   public:
	//! \brief Construct a data segment, of some data type, to be
	//! parsed in one of the standard ways
	//!
	//! \param[in] dataType The type of data that the segment will
	//! parse
	//!
	//! \param[in] style The standard way, in which parsed data will
	//! be parsed
	TimeSegmentDesc(TimeType dataType, TimePrintStyle style) noexcept
		: m_Data(dataType, style), m_SegmentType(SegmentType::Data)
	{}
	//! \brief Construct a data segment, of some data type, to be
	//! parsed using a custom array
	//!
	//! \param[in] dataType The type of data that the segment will
	//! parse
	//!
	//! \param[in] customArray The array of strings, which will be
	//! indexed into by parsed data
	TimeSegmentDesc(
		TimeType							dataType,
		std::vector<Basic_String<CharType>> customArray) noexcept
		: m_Data(dataType, std::move(customArray)),
		  m_SegmentType(SegmentType::Data)
	{}
	//! \brief Construct a string segment, that is not dependent on
	//! parsed data
	//!
	//! \param[in] string The string to be stored
	TimeSegmentDesc(Basic_String<CharType> string) noexcept
		: m_String(std::move(string)),
		  m_SegmentType(SegmentType::String)
	{}
	//! \brief Copy contruct a segment, from another segment
	//!
	//! \param[in] rhs The other segment, whose string or data we
	//! should copy
	TimeSegmentDesc(const TimeSegmentDesc<CharType>& rhs) noexcept
		: m_String(), m_SegmentType(SegmentType::String)
	{
		if (rhs.m_SegmentType == SegmentType::String)
		{
			m_String = rhs.m_String;
			return;
		}

		// if we gete here, then we guessed wrongly, on string. Must
		// therefore habe a data description
		runtime_assert(rhs.m_SegmentType == SegmentType::Data,
					   "Unrecognised segment type!");

		// destroy the string, and make way for the Data description
		destroyCurrentUnionMember();
		new (&m_Data) TimeDataDesc<CharType>(rhs.m_Data);
		m_SegmentType = rhs.m_SegmentType;
	}
	//! \brief Move contruct a segment, from another segment
	//!
	//! \param[in] rhs The other segment, whose string or data we
	//! should move from
	TimeSegmentDesc(TimeSegmentDesc<CharType>&& rhs) noexcept
		: m_String(), m_SegmentType(SegmentType::String)
	{
		// we had to guess, on intialisation, so see whether we
		// guessed right
		if (rhs.m_SegmentType == SegmentType::String)
		{
			m_String = std::move(rhs.m_String);
			return;
		}

		// if we gete here, then we guessed wrongly, on string. Must
		// therefore habe a data description
		runtime_assert(rhs.m_SegmentType == SegmentType::Data,
					   "Unrecognised segment type!");

		// destroy the string, and make way for the Data description
		destroyCurrentUnionMember();
		new (&m_Data) TimeDataDesc<CharType>(std::move(rhs.m_Data));
		m_SegmentType = rhs.m_SegmentType;
	}
	//! \brief Destroy the segment
	~TimeSegmentDesc() noexcept
	{
		destroyCurrentUnionMember();
	}

	//! \brief Copy assign a segment, from another segment
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The other segment, whose string or data we
	//! should copy
	TimeSegmentDesc<CharType>& operator=(
		const TimeSegmentDesc<CharType>& rhs) noexcept
	{
		destroyCurrentUnionMember();

		if (rhs.m_SegmentType == SegmentType::Data)
		{
			new (&m_Data) TimeDataDesc<CharType>(rhs.m_Data);
			m_SegmentType = rhs.m_SegmentType;
		} else
		{
			runtime_assert(rhs.m_SegmentType == SegmentType::String,
						   "Unrecognised segment type!");

			new (&m_String) Basic_String<CharType>(rhs.m_String);
			m_SegmentType = rhs.m_SegmentType;
		}

		return *this;
	}
	//! \brief Move assign a segment, from another segment
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The other segment, whose string or data we
	//! should move from
	TimeSegmentDesc<CharType>& operator=(
		TimeSegmentDesc<CharType>&& rhs) noexcept
	{
		destroyCurrentUnionMember();

		if (rhs.m_SegmentType == SegmentType::Data)
		{
			new (&m_Data)
				TimeDataDesc<CharType>(std::move(rhs.m_Data));
			m_SegmentType = rhs.m_SegmentType;
		} else
		{
			runtime_assert(rhs.m_SegmentType == SegmentType::String,
						   "Unrecognised segment type!");

			new (&m_String)
				Basic_String<CharType>(std::move(rhs.m_String));
			m_SegmentType = rhs.m_SegmentType;
		}
	}

	//! \brief Get the currently stored type, in the array
	SegmentType getType() const noexcept
	{
		return m_SegmentType;
	}

	//! \brief Get the stored string, if possible
	//!
	//! \returns An optional, which will hold a copy of the stored
	//! string, iff the object currently holds a string
	auto getString() const noexcept
		-> std::optional<Basic_String<CharType>>
	{
		std::optional<Basic_String<CharType>> result;

		if (getType() == SegmentType::String)
		{
			result = m_String;
		}

		return result;
	}
	//! \brief Get the stored data, if possible
	//!
	//! \returns An optional, which will hold a copy of the stored
	//! data, iff the object currently holds data
	auto getData() const noexcept -> std::optional<
		std::reference_wrapper<const TimeDataDesc<CharType>>>
	{
		if (getType() == SegmentType::Data)
		{
			return std::optional<
				std::reference_wrapper<const TimeDataDesc<CharType>>>(
				std::cref(m_Data));
		}

		return std::nullopt;
	}
};

// So the compiler won't complain about using STL containers in the interface. Obviously
// doing this can cause problems, esp. if we use a different compiler / flags for compilation
// of dll / consumer of dll, but it should mostly work, if we can keep those straight. Which
// our build system should anyway
DISABLE_VC_WARNING(4251)

//! \brief Formatting class, which transforms raw time data, into a
//! string, of the specified format
//!
//! \tparam CharType The underlying character type, of the string to
//! be built
template<typename CharType>
class NUCLEUS_LIB_API TimeFormat
{
	//! \brief The array of stored segments, stored in the order of
	//! their printing
	std::vector<TimeSegmentDesc<CharType>> m_SegmentSpecs;

   public:
	//! \brief Construct a timeformat, from a list of segments, in
	//! order
	//!
	//! \param[in] segments The segments from which to construct the
	//! TimeFormat, in order
	TimeFormat(
		std::initializer_list<TimeSegmentDesc<CharType>> segments)
		: m_SegmentSpecs(std::move(segments))
	{}
	//! \brief Defaulted copy constructor
	//!
	//! \param[in] rhs The object to copy from
	TimeFormat(const TimeFormat<CharType>& rhs) = default;
	//! \brief Defaulted move constructor
	//!
	//! \param[in] rhs The object to move from
	TimeFormat(TimeFormat<CharType>&& rhs) noexcept = default;
	//! \brief defaulted destructor
	~TimeFormat() noexcept = default;

	//! \brief Defaulted copy assignment operator
	//!
	//! \returns reference to this, for chaining
	//!
	//! \param[in] rhs The object to copy from
	TimeFormat<CharType>& operator=(const TimeFormat<CharType>& rhs) =
		default;
	//! \brief Defaulted move assignment operator
	//!
	//! \returns reference to this, for chaining
	//!
	//! \param[in] rhs The object to move from
	TimeFormat<CharType>& operator			 =(
		   TimeFormat<CharType>&& rhs) noexcept = default;

	//! \brief Parse the specified raw time data, and turn it into a
	//! string, of this format
	//!
	//! \returns A string, in the format that has been specified, in
	//! this object, using the time data
	//!
	//! \param[in] timeData The raw time data, whcih should be used to
	//! construct the string
	Basic_String<CharType> parse(RawTimeData timeData) const
	{
		Basic_String<CharType> result;

		for (const auto& segment : m_SegmentSpecs)
		{
			switch (segment.getType())
			{
				case TimeSegmentDesc<CharType>::SegmentType::String:
				{
					auto string = segment.getString();
					runtime_assert(
						string,
						"TimeSegmentDesc was not as described, and "
						"string could nto be retreived");

					result += string.value();
				}
				break;
				case TimeSegmentDesc<CharType>::SegmentType::Data:
				{
					auto data = segment.getData();
					runtime_assert(
						data,
						"TimeSegmentDesc was not as described, and "
						"data could nto be retreived");

					result += data.value().get().parse(timeData);
				}
				break;
				default:
					runtime_assert(false, "unrecognised SegmentType");
					break;
			}
		}

		return result;
	}

	//! \brief Append this time format, with the contents of another
	//!
	//! \returns A reference to this, chaining. This will hold its
	//! previous contents, as well as those appended
	//!
	//! \param[in] rhs The TimeFormat the should be added to the end
	//! of this
	TimeFormat<CharType>& operator+=(const TimeFormat<CharType>& rhs)
	{
		m_SegmentSpecs.reserve(m_SegmentSpecs.size()
							   + rhs.m_SegmentSpecs.size());

		for (auto& segment : rhs.m_SegmentSpecs)
		{
			m_SegmentSpecs.push_back(segment);
		}

		return *this;
	}
	//! \brief Add two time formats together, and return a new one
	//!
	//! \returns A new time format, which shall consist of this
	//! format, followed by the format specified in rhs
	//!
	//! \param[in] rhs The time format to have at the end of the new
	//! format
	TimeFormat<CharType> operator+(
		const TimeFormat<CharType>& rhs) const
	{
		TimeFormat<CharType> result(*this);

		result += rhs;

		return result;
	}
};

//retore disabled c4251 warning, for using non-exporting class in interface
RESTORE_VC_WARNING(4251)

//! \brief Constrct a time format, that will transform time data to
//! ISO 8601 format (yyyy-mm-dd)
//!
//! \tparam CharType The underlying type of the string that should be
//! produced by the format
//!
//! \returns A TimeFormat, which can be used to produce ISO 8601 data
//! strings, from time data
template<typename CharType>
TimeFormat<CharType> getSortDateFormat()
{
	Basic_String<CharType> dateDivider("-");

	using Seg = TimeSegmentDesc<CharType>;

	return TimeFormat<CharType>{
		Seg(TimeType::Year, TimePrintStyle::Number), Seg(dateDivider),
		Seg(TimeType::Month, TimePrintStyle::Month), Seg(dateDivider),
		Seg(TimeType::DayOfMonth, TimePrintStyle::Number)};
}

//! \brief Constrct a time format, that will transform time data to
//! a short date format. UK (dd/mm/yyyy) or US (mm/dd/yyyy)
//!
//! \tparam CharType The underlying type of the string that should be
//! produced by the format
//!
//! \returns A TimeFormat, which can be used to produce short date
//! strings, from time data
//!
//! \param[in] UK Should the date format use UK order (dd/mm)? If
//! true, will use UK order. If false, will use American (mm/dd) order
template<typename CharType>
TimeFormat<CharType> getShortDateFormat(bool UK)
{
	// this is pretty inefficient, and not ideal, because we should
	// really have an ascii constructor, but it works for now
	Basic_String<CharType> dateDivider("/");

	using Seg = TimeSegmentDesc<CharType>;

	if (UK)
	{
		return TimeFormat<CharType>{
			Seg(TimeType::DayOfMonth, TimePrintStyle::Number),
			Seg(dateDivider),
			Seg(TimeType::Month, TimePrintStyle::Number),
			Seg(dateDivider),
			Seg(TimeType::Year, TimePrintStyle::Number)};
	} else
	{
		return TimeFormat<CharType>{
			Seg(TimeType::Month, TimePrintStyle::Number),
			Seg(dateDivider),
			Seg(TimeType::DayOfMonth, TimePrintStyle::Number),
			Seg(dateDivider),
			Seg(TimeType::Year, TimePrintStyle::Number)};
	}
}

//! \brief Constrct a time format, that will transform time data to
//! a long date format. UK "1st of January 1999" or US "January 1st
//! 1999". Numbers may also be printed as words
//!
//! \tparam CharType The underlying type of the string that should be
//! produced by the format
//!
//! \returns A TimeFormat, which can be used to produce long date
//! strings, from time data
//!
//! \param[in] UK Should the date format use UK order (dd/mm)? If
//! true, will use UK order. If false, will use American (mm/dd) order
//!
//! \param[in] text Should the numbers be printed textually? If true,
//! they will be
template<typename CharType>
TimeFormat<CharType> getLongDateFormat(bool UK, bool text)
{
	Basic_String<CharType> monthYearDivider;
	TimePrintStyle		   ordinalNumberStyle, numberStyle;

	if (text)
	{
		ordinalNumberStyle = TimePrintStyle::OrdinalText;
		numberStyle		   = TimePrintStyle::Text;
		monthYearDivider   = ", ";
	} else
	{
		ordinalNumberStyle = TimePrintStyle::OrdinalNumber;
		numberStyle		   = TimePrintStyle::Number;
		monthYearDivider   = " ";
	}

	using Seg = TimeSegmentDesc<CharType>;

	if (UK)
	{
		return TimeFormat<CharType>{
			Seg(TimeType::DayOfMonth, ordinalNumberStyle),
			Seg(" of "), Seg(TimeType::Month, TimePrintStyle::Month),
			Seg(monthYearDivider), Seg(TimeType::Year, numberStyle)};
	} else
	{
		return TimeFormat<CharType>{
			Seg(TimeType::Month, TimePrintStyle::Month), Seg(" "),
			Seg(TimeType::DayOfMonth, ordinalNumberStyle),
			Seg(monthYearDivider), Seg(TimeType::Year, numberStyle)};
	}
}

//! \brief Constrct a time format, that will transform time data to
//! a 24Hr time format. "18:59" or "18:59:34" or "18:59:34 123ms"
//!
//! \tparam CharType The underlying type of the string that should be
//! produced by the format
//!
//! \returns A TimeFormat, which can be used to produce 24Hr time
//! strings, from time data
//!
//! \param[in] includeSec Should the number of seconds, also be
//! printed? True for yes
//!
//! \param[in] includeMS Should the number of millseconds, also be
//! printed? True for yes
template<typename CharType>
TimeFormat<CharType> get24HrTimeFormat(bool includeSec,
									   bool includeMS)
{
	// this is pretty inefficient, and not ideal, because we should
	// really have an ascii constructor, but it works for now
	Basic_String<CharType> timeDivider(":");

	using Seg = TimeSegmentDesc<CharType>;

	TimeFormat<CharType> result{
		Seg(TimeType::Hour, TimePrintStyle::Number), Seg(timeDivider),
		Seg(TimeType::Minute, TimePrintStyle::Number)};

	if (includeSec)
	{
		result += TimeFormat<CharType>{
			Seg(timeDivider),
			Seg(TimeType::Second, TimePrintStyle::Number)};
	}

	if (includeMS)
	{
		result += TimeFormat<CharType>{
			Seg(" "),
			Seg(TimeType::Millisecond, TimePrintStyle::Number),
			Seg("ms")};
	}

	return result;
}

//! \brief Constrct a time format, that will transform time data to
//! a 12Hr time format. "6:59 pm" or "6:59:34 pm" or "6:59:34 pm
//! 123ms"
//!
//! \tparam CharType The underlying type of the string that should be
//! produced by the format
//!
//! \returns A TimeFormat, which can be used to produce 12Hr time
//! strings, from time data
//!
//! \param[in] includeSec Should the number of seconds, also be
//! printed? True for yes
//!
//! \param[in] includeMS Should the number of millseconds, also be
//! printed? True for yes
template<typename CharType>
TimeFormat<CharType> get12HrTimeFormat(bool includeSec,
									   bool includeMS)
{
	Basic_String<CharType> dateDivider(":"), space(" ");

	using Seg = TimeSegmentDesc<CharType>;

	// set the hour so that [0, 23] -> 12, [1, 11]
	Seg hourSeg(TimeType::Hour, TimePrintStyle::Number);

	// get the actual data part of the segment, and make sure it
	auto hourData = hourSeg.getData();
	runtime_assert(
		hourData,
		"A data segment was no created, when it should have been");

	// set the function to call before parsing the data
	hourData.value().m_PrintSpec.getFunc() =
		[](natural16 hour) -> natural16 {
		hour %= 12;

		return (hour == 0) ? 12 : hour;
	};

	// create standard hh:mm part
	TimeFormat<CharType> result{
		std::move(hourSeg), Seg(dateDivider),
		Seg(TimeType::Minute, TimePrintStyle::Number)};

	// add optional :ss part, if required
	if (includeSec)
	{
		result += TimeFormat<CharType>{
			Seg(dateDivider),
			Seg(TimeType::Second, TimePrintStyle::Number)};
	}

	// add am/pm section
	result += TimeFormat<CharType>{
		Seg(space),
		Seg(TimeType::Hour,
			[](natural16 hour) -> natural16 { return hour / 12; },
			{"am", "pm"})};

	// add optional lllms part, if required
	if (includeMS)
	{
		result += TimeFormat<CharType>{
			Seg(space),
			Seg(TimeType::Millisecond, TimePrintStyle::Number),
			Seg("ms")};
	}

	return result;
}

//! \brief A struct for normal-scale time, on the order of at least
//! seconds (up to years). Not for high-frequency millisecond or lower
//! time
struct NUCLEUS_LIB_API ClockTime
{
	//! \brief The raw data of the time, in detail
	RawTimeData m_Data;

	//! \brief Intialise with current time
	ClockTime() noexcept;

	//! \brief standard (default) copy constructor
	ClockTime(const ClockTime&) noexcept = default;

	//! \brief standard (default) move constructor
	ClockTime(ClockTime&&) noexcept = default;

	//! \brief standard (default) destructor
	~ClockTime() noexcept = default;

	//! \brief standard (default) copy assignment operator
	ClockTime& operator=(const ClockTime&) noexcept = default;

	//! \brief standard (default) move assignment operator
	ClockTime& operator=(ClockTime&&) noexcept = default;

	//! \brief Update with current time
	void updateTime() noexcept;

	//! \brief Get the current time, in 24Hr format
	//!
	//! \tparam CharType One of [charU8, wchar_t, char16_t, char32_t]
	//!
	//! \tparam precise Should the number of millseconds be printed?
	//!
	//! \returns A string, of the specified CharType, which hold the
	//! string representation of this time, in a 24Hr format
	template<class CharType, bool precise = false>
	Basic_String<CharType> time24hr() const
	{
		static auto format =
			get24HrTimeFormat<CharType>(true, precise);

		return format.parse(m_Data);
	}
	//! \brief Get the current time, in 12Hr format
	//!
	//! \tparam CharType One of [charU8, wchar_t, char16_t, char32_t]
	//!
	//! \tparam precise Should the number of millseconds be printed?
	//!
	//! \returns A string, of the specified CharType, which hold the
	//! string representation of this time, in a 12Hr format
	template<class CharType, bool precise = false>
	Basic_String<CharType> time12hr() const
	{
		static auto format =
			get12HrTimeFormat<CharType>(true, precise);

		return format.parse(m_Data);
	}

	//! \brief Get the current date, in a sorting format (ISO 8601,
	//! yyyy-mm-dd)
	//!
	//! \tparam CharType One of [charU8, wchar_t, char16_t, char32_t]
	//!
	//! \returns A string, of the specified CharType, which hold the
	//! string representation of this time, in a sorting format
	template<class CharType>
	Basic_String<CharType> dateSort() const
	{
		static auto format = getSortDateFormat<CharType>();

		return format.parse(m_Data);
	}
	//! \brief Get the current date, in a short format (dd/mm/yyyy or
	//! mm/dd/yyyy)
	//!
	//! \tparam CharType One of [charU8, wchar_t, char16_t, char32_t]
	//!
	//! \tparam UK Should we use dd/mm ordering? If true, we will. If
	//! false, we'll use mm/dd
	//!
	//! \returns A string, of the specified CharType, which hold the
	//! string representation of this time, in a short format
	template<class CharType, bool UK = true>
	Basic_String<CharType> dateShort() const
	{
		static auto format = getShortDateFormat<CharType>(UK);

		return format.parse(m_Data);
	}
	//! \brief Get the current date, in a long format (ddth of
	//! monthname yyyy or monthname ddth yyyy)
	//!
	//! \tparam CharType One of [charU8, wchar_t, char16_t, char32_t]
	//!
	//! \tparam UK Should we use "ddth of month" ordering? If true, we
	//! will. If false, we'll use "month ddth"
	//!
	//! \tparam Textual Should the numbers in the string be printed as
	//! words? If true, they will be
	//!
	//! \returns A string, of the specified CharType, which hold the
	//! string representation of this time, in a long format
	template<class CharType, bool UK = true, bool Textual = false>
	Basic_String<CharType> dateLong() const
	{
		static auto format = getLongDateFormat<CharType>(UK, Textual);

		return format.parse(m_Data);
	}

	//! \brief equality comparison. returns true iff all members are
	//! the same (From year down to second)
	//!
	//! \returns True iff all members are the same (From year down to second)
	//!
	//! \param[in] rhs Time to compare to this
	bool operator==(const ClockTime& rhs) const noexcept;

	//! \brief inequality comparison. Equivalent to !((*this) == rhs).
	//! That is, returns true if any member(s) differ, between *this
	//! and rhs
	//!
	//! \param[in] rhs Time to compare to this
	bool operator!=(const ClockTime& rhs) const noexcept;
};


//! \class TimeStamp
//!
//! \brief A class for storing a constant time, for
//! reference (printing etc.) later
class NUCLEUS_LIB_API TimeStamp
{
	//! \brief The stored time, which this timestamp refers to
	const ClockTime m_Time;

	// delete, because TimeStamp is designed to be immutable
	TimeStamp& operator=(const TimeStamp&) = delete;
	TimeStamp& operator=(TimeStamp&&) = delete;

   public:
	//! \brief Construct TimeStamp from current time
	//!
	//! \details Constuct from windows clock time. That is, call Time
	//! to construct the current time
	TimeStamp() noexcept = default;

	//! \brief Standard (default) copy constructor
	TimeStamp(const TimeStamp& time) noexcept = default;

	//! \brief Standard (default) move constructor
	TimeStamp(TimeStamp&& time) noexcept = default;

	//! \brief get Time corresponding to this timestamp
	//!
	//! \returns const Time
	auto getTime() const noexcept -> const decltype(m_Time)&;

	//! \brief Equality comparison. Returns true iff (this->m_Time ==
	//! rhs.m_Time)
	//!
	//! \param[in] rhs TimeStamp to compare to this
	bool operator==(const TimeStamp& rhs) const noexcept;

	//! \brief Equality comparison. Equivalent to !(*this == rhs)
	//!
	//! \param[in] rhs TimeStamp to compare to this
	bool operator!=(const TimeStamp& rhs) const noexcept;
};
}  // namespace Time
}  // namespace frao

#endif