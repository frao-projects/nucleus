#ifndef FRAO_NUCLEUS_STRINGS_STRING_IMPLEMENTATION_EXPERIMENTAL
#define FRAO_NUCLEUS_STRINGS_STRING_IMPLEMENTATION_EXPERIMENTAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the experimental implementation of unicode
//! string class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <istream>	//for istreams
#include <ostream>	//for ostreams
#include <vector>	//for internal string representation
#include "../../Environment.hpp"
#include "../CodePoint.hpp"
#include "../GraphemeCluster.hpp"
#include "../Word.hpp"
#include "StringStore.hpp"

namespace frao
{
inline namespace Strings
{
namespace Experimental
{
//! \brief Experimental implementation class, for strings
//!
//! \brief check double lexical casting works, for values not
//! representable in float, because I don't think it does
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \tparam NumericalPunctuation The struct that holds the type of
//! numerical punctuation to use, when converting to and from numbers.
template<typename CharType, typename NumericalPunctuation>
class NUCLEUS_LIB_API StringIMPL
{
   public:
	//! \brief Type for referring to our own type, where appropriate
	using OurType =
		Experimental::StringIMPL<CharType, NumericalPunctuation>;

   private:
	//! \brief Stores the actual string data
	IMPL::StringStore<CharType> m_StringData;

   public:
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = CodePoint<CharType, true>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = GraphemeCluster<CharType, true>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using WordType = Word<CharType, true>;

	//! \brief create empty string
	StringIMPL();
	//! \brief create string from the specified
	//! (null-terminated) string
	//!
	//! \param[in] string pointer to const string buffer
	StringIMPL(const CharType* string);

	//! \brief Construct a StringIMPL from an ascii cstring
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	StringIMPL(const charA* string);

	//! \brief create string from a string buffer and
	//! specified size. Doesn't need to be null-terminated
	//!
	//! \param[in] string pointer to const string buffer
	//!
	//! \param[in] bytesize size of string buffer in
	//! 'string' parameter
	explicit StringIMPL(const CharType* string,
						frao::natural64 bytesize);

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_nat8 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat16 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat32 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat64 number);

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int8 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int16 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int32 number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int64 number);

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to string
	explicit StringIMPL(float number);
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to string
	explicit StringIMPL(double number);

	//! \brief concatenate two strings together. lhs will be on the
	//! front, and rhs will be the back. creates new string containing
	//! concatenated string. parameters not modified
	//!
	//! \returns newly created string containing results of the
	//! concatenation
	//!
	//! \param[in] lhs string to be the front of the new string
	//!
	//! \param[in] rhs string to be concatenated to lhs, when creating
	//! the new string
	explicit StringIMPL(const OurType& lhs, const OurType& rhs);

	//! \brief Copy construct a string, from another string of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other string, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The string to copy from
	template<typename OtherType, typename OtherNumericalPunctuation>
	explicit StringIMPL(
		const StringIMPL<OtherType, OtherNumericalPunctuation>& rhs);
	//! \brief Copy construct a string, from another string of the
	//! same underlying type as this
	//!
	//! \param[in] rhs The string to copy from
	StringIMPL(const OurType& rhs);
	//! \brief Move construct a string, from another string of the
	//! same underlying type as this
	//!
	//! \param[in] rhs The string to move from
	StringIMPL(OurType&& rhs);
	//! \brief destroy the string, and call any derived destructors
	virtual ~StringIMPL() noexcept = default;

	//! \brief Copy assign a string, from another string of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The string to copy from
	OurType& operator=(const OurType& rhs) = default;
	//! \brief Move assign a string, from another string of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The string to move from
	OurType& operator=(OurType&& rhs) = default;

	//! \brief pops (removes) the back of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the last code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_backIMPL() noexcept;
	//! \brief pops (removes) the front of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the first code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_frontIMPL() noexcept;

	//! \brief adds a codepoint to the back of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_backIMPL(const CodePointType& codepoint) noexcept;
	//! \brief adds a cluster to the back of the string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_backIMPL(const ClusterType& cluster) noexcept;
	//! \brief adds a word to the back of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_backIMPL(const WordType& word) noexcept;
	//! \brief adds a codepoint to the front of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_frontIMPL(const CodePointType& codepoint) noexcept;
	//! \brief adds a grapheme cluster to the front of the
	//! string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_frontIMPL(const ClusterType& cluster) noexcept;
	//! \brief adds a word to the front of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_frontIMPL(const WordType& word) noexcept;

	//! \brief add a code-point AT the specified index in
	//! the string, moving the EXISTING code-point at that
	//! location, and all subsequent code-points back
	//!
	//! \param[in] codepoint The code-point to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the
	//! code-point. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	void insertIMPL(const CodePointType& codepoint,
					natural64			 index) noexcept;
	//! \brief add a grapheme cluster AT the specified index
	//! in the string, moving the EXISTING grapheme cluster
	//! at that location, and all subsequent grapheme
	//! clusters back
	//!
	//! \param[in] cluster The grapheme cluster to add at
	//! the specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the grapheme
	//! cluster. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	void insertIMPL(const ClusterType& cluster,
					natural64		   index) noexcept;
	//! \brief add a word AT the specified index in the
	//! string, moving the EXISTING word at that location,
	//! and all subsequent words back
	//!
	//! \param[in] word The word to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the word. Any
	//! existing word at the location, and subsequent
	//! locations, will be moved back
	void insertIMPL(const WordType& word, natural64 index) noexcept;
	//! \brief add a string into this one AT the specified
	//! index in the string, moving the EXISTING
	//! code-point(s) at that location, and all subsequent
	//! locations, back
	//!
	//! \param[in] string The string to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the (start of
	//! the) string. Any existing code-point(s) at the
	//! location, and subsequent locations, will be moved
	//! back
	void insertIMPL(const OurType& string, natural64 index) noexcept;

	//! \brief add a string onto the back of this one
	//!
	//! \param[in] string String to add on the back of this
	//! one
	void appendIMPL(const OurType& string);

	//! \brief remove a certain number of characters,
	//! starting at the specified location
	//!
	//! \param[in] index location to start the removal of
	//! characters
	//!
	//! \param[in] length nunber of characters to remove
	void eraseIMPL(frao::natural64 index, frao::natural64 length);
	//! \brief erase entire content of string. String will
	//! be left empty
	void clearIMPL();

	//! \brief create a substring, from this string
	//!
	//! \returns A string, holding some subset of the data contained
	//! in this
	//!
	//! \param[in] startIndex The character index, in thsi string,
	//! which represents the start of the substring
	//!
	//! \param[in] length The length of the substring to be created
	OurType subStrIMPL(frao::natural64 startIndex,
					   frao::natural64 length) const;

	//! \brief size of string, in terms of grapheme clusters
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of grapheme clusters in the string
	frao::natural64 sizeIMPL() const noexcept;
	//! \brief size of string, in terms of bytes
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of bytes in the string
	frao::natural64 byteSizeIMPL() const noexcept;

	//! \brief make sure the string has sufficient memory
	//! allocated to allow future expansion to specified
	//! number of characters
	//!
	//! \param[in] characters Number of characters that string should
	//! be able to hold (ie: make sure the buffer is at
	//! least this number of characters long)
	void reserveIMPL(frao::natural64 characters);
	//! \brief gets a const pointer to the data
	//!
	//! \note null-terminated c-string returned
	//!
	//! \returns a const pointer to the data. Const in the
	//! sense of the data being immutable through the
	//! pointer. Null-terminated
	const CharType* dataIMPL() const noexcept;

	//! \brief indicates whether the string is empty
	//!
	//! \returns a boolean indicating whether the string in
	//! empty. returns true iff the string is empty
	bool isEmptyIMPL() const noexcept;

	//! \brief indexes Grapheme Clusters in this string
	//!
	//! \returns Grapheme Cluster at the specified index
	//!
	//! \param[in] index of the Grapheme Cluster that should
	//! be returned
	ClusterType getIMPL(frao::natural64 index) const noexcept;

	//! \brief find the next instance of the passed sub-string, in
	//! this string, after the indicated character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] size The size of the passed sub-string
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const CharType* string, frao::natural64 size,
		frao::natural64 startCharacter) const noexcept;
	//! \brief find the next instance of the passed sub-string, in
	//! this string, after the indicated character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the (null-terminated) sub-string to find the
	//! next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const CharType* string,
		frao::natural64 startCharacter) const noexcept;
	//! \brief find the next instance of the passed sub-string, in
	//! this string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const OurType&	string,
		frao::natural64 startCharacter) const noexcept;

	//! \brief find the next instance of the passed codepoint, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const CodePointType& codepoint,
		frao::natural64		 startCharacter) const noexcept;
	//! \brief find the next instance of the passed cluster, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const ClusterType& cluster,
		frao::natural64	   startCharacter) const noexcept;
	//! \brief find the next instance of the passed word, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const WordType& word,
		frao::natural64 startCharacter) const noexcept;

	//! \brief converts this string to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[in] number const reference to the number that
	//! should be converted
	template<typename NumberType>
	bool toNumberIMPL(NumberType& number) const noexcept;
	//! \brief Convert string to a 'case folded' version, that is
	//! suitable for comparison of semantically equal strings. ie: in
	//! english this is like converting to lowercase, so that upper
	//! and lower case letters, in the original, are identical
	//!
	//! \returns a case folded copy of this
	OurType toCaseFoldedIMPL() const noexcept;

	//! \brief Will determine the ordering of this, against a string,
	//! of any valid underlying type
	//!
	//! \tparam OtherType The type of the other string, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \details Compares the strings, with this being on the left
	//! hand side, and the other strings on the right. Will return < 0
	//! for *this < other, will return > 0 for *this > other, and will
	//! return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than, and >
	//! 0 meaning greater than
	//!
	//! \param[in] otherString The string to compare this with, on the
	//! rhs.
	template<typename OtherType, typename OtherNumPunct>
	frao::integer64 getOrderIMPL(
		const StringIMPL<OtherType, OtherNumPunct>& otherString)
		const noexcept;

	//! \brief Put the specified string into an ostream
	//!
	//! \returns The ostream, now containing the string
	//!
	//! \param[in] oStream The ostream that the string should be
	//! inserted into
	//!
	//! \param[in] string The string, that should be inserted into the
	//! oStream
	friend std::basic_ostream<CharType, std::char_traits<CharType>>&
	operator<<(std::basic_ostream<
				   CharType, std::char_traits<CharType>>& oStream,
			   const OurType&							  string);
	//! \brief extract a string, from an istream
	//!
	//! \returns The istream, after the extraction
	//!
	//! \param[in] iStream The istream that the string should be
	//! extracted from
	//!
	//! \param[out] string The string, that should be extracted from
	//! the oStream
	friend std::basic_istream<CharType, std::char_traits<CharType>>&
	operator>>(std::basic_istream<
				   CharType, std::char_traits<CharType>>& iStream,
			   OurType&									  string);
};
}  // namespace Experimental
}  // namespace Strings
}  // namespace frao

#endif