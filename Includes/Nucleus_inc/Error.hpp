#ifndef FRAO_NUCLEUS_ERROR
#define FRAO_NUCLEUS_ERROR

//! \file
//!
//! \brief Header of all defintions, functions, and classes, relating
//! to the error handling of the software
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Error/Control.hpp"
#include "Error/Stack.hpp"
#include "Error/Organisation.hpp"
#include "Error/Types.hpp"

#endif