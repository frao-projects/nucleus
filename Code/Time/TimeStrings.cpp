//! \file
//!
//! \brief implementation file for getting strings, as corresponding
//! to days, months etc.
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Time/TimeStrings.hpp"

namespace
{
//! \brief Array of charA string literals, with each containing a
//! textual representation of an integer. Positions of zero -> twenty
//! are at their respective array offsets, and thirty, fourty etc are
//! 21, 22, and so on
static constexpr const frao::charA* _numberText_array[] = {
	"zero",	"one",	 "two",	   "three",	"four",
	"five",	"six",	 "seven",	 "eight",	"nine",
	"ten",	 "eleven",  "twelve",	"thirteen", "fourteen",
	"fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
	"twenty",  "thirty",  "fourty",	"fifty",	"sixty",
	"seventy", "eighty",  "ninty"};
//! \brief Array of charA string literals, with each containing a
//! textual ordinal number. Positions of zeroth -> twentieth are at
//! their respective array offsets, and thirty, fourty etc are
//! 21, 22, and so on
static constexpr const frao::charA* _numberTextOrdinal_array[] = {
	"zeroth",	"first",		"second",	 "third",
	"fourth",	"fifth",		"sixth",	  "seventh",
	"eighth",	"nineth",		"tenth",	  "eleventh",
	"twelveth",  "thirteenth",  "fourteenth", "fifteenth",
	"sixteenth", "seventeenth", "eighteenth", "nineteenth",
	"twentieth", "thirtieth",   "fourtieth",  "fiftieth",
	"sixtieth",  "seventieth",  "eightieth",  "nintieth"};
//! \brief Array of charA string literals, with each containing a
//! numeric ordinal number. Positions of 0th -> 20th are at their
//! respective array offsets
static constexpr const frao::charA* _numberOrdinal_array[] = {
	"0th",  "1st",  "2nd",  "3rd",  "4th",  "5th",  "6th",
	"7th",  "8th",  "9th",  "10th", "11th", "12th", "13th",
	"14th", "15th", "16th", "17th", "18th", "19th"};

//! \brief Array of charA string literals, denoting the days of the
//! week. The string literal corresponding to each day of the week, is
//! located in the array according to the number of each day of the
//! week, starting with 0 for sunday
static constexpr const frao::charA* _dayOfWeek_array[] = {
	"Sunday",   "Monday", "Tuesday", "Wednesday",
	"Thursday", "Friday", "Saturday"};
//! \brief Array of charA string literals, denoting the month. The
//! string literal corresponding to each month, is located in the
//! array according to the number of each month, except december,
//! which is at 0. ie: months are at (monthNum % 12)
constexpr const frao::charA* _month_array[] = {
	"December", "January",   "February", "March",
	"April",	"May",		 "June",	 "July",
	"August",   "September", "October",  "November"};

template<frao::natural64 arrayLength>
const frao::charA* arrayCallerHelper(
	const frao::charA* const (&passedArray)[arrayLength],
	frao::natural16 number) noexcept
{
	return passedArray[number % arrayLength];
}
}  // namespace

namespace frao
{
inline namespace Time
{
namespace IMPL
{
const charA* _numbertext(natural16 number) noexcept
{
	return arrayCallerHelper(_numberText_array, number);
}
const charA* _numbertextordinal(natural16 number) noexcept
{
	return arrayCallerHelper(_numberTextOrdinal_array, number);
}
const charA* _numberordinal(natural16 number) noexcept
{
	return arrayCallerHelper(_numberOrdinal_array, number);
}
const charA* _dayofweek(natural16 day) noexcept
{
	return arrayCallerHelper(_dayOfWeek_array, day);
}
const charA* _month(natural16 month) noexcept
{
	return arrayCallerHelper(_month_array, month);
}

}  // namespace IMPL
}  // namespace Time
}  // namespace frao