param (
    # The path of the .frao folder to create the file in
    [Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
    [string]$FraoDir,
    # The scripts folder location to write to the file
    [Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
    [string]$ScriptsDir
)

Import-Module "$FraoDir/BuildFunctions.psm1"

[string]$local:OSName = Read-OSString

@{ 'scripts_path' = "$ScriptsDir" } | ConvertTo-Json |
    Out-File -FilePath "$FraoDir/${OSName}_meson_paths.json"