
# Function that will ask a user for a true/false value
function Read-Boolean {
	[OutputType([boolean])]
	param(
		[parameter(Mandatory=$true)]
		[string] $QuestionText
	)

	$local:trueAnswers = @("True", "1", "Yes")
	$local:falseAnswers = @("False", "0", "No")
	[boolean]$local:validResult | Out-Null
	$local:actualResult | Out-Null

	do {
		[string]$answer = Read-Host $QuestionText

		[boolean]$local:trueMatch = ($trueAnswers -match $answer).Length -gt 0
		[boolean]$local:falseMatch = ($falseAnswers -match $answer).Length -gt 0
		$validResult = $trueMatch -xor $falseMatch

		if($validResult) {
			$actualResult = $trueMatch
			break
		} else {
			$actualResult = $null

			if($trueMatch -and $falseMatch) {
				Write-Host 'Ambiguous answer received'
			} else {
				Write-Host 'Invalid answer received'
			}
		}
	} while($true)

	return $actualResult
}

# Function that will ask a user for a uint value
function Read-UInt16 {
	[OutputType([uint])]
	param(
		[parameter(Mandatory=$true)]
		[string] $QuestionText,
        [UInt16]$MaxValue = 65535
	)

	[UInt16]$local:result | Out-Null
    [Boolean]$local:isValid | Out-Null

	do {
		[string]$answer = Read-Host $QuestionText

        try {
            $result = [Convert]::ToUInt16($answer)

            if($result -gt $MaxValue) {
                Write-Host "Invalid Answer received: expected a value <= $MaxValue"
                $isValid = $false
            } else {
                $isValid = $true
            }
        } catch {
            Write-Host 'Invalid Answer received: not a uint16'
            $isValid = $false
        }

	} while(-not($isValid))

	return $result
}

function Read-Option {
	param(
		[string] $QuestionText,
		[scriptblock] $ValidateCommand=$null
	)

	$local:result | Out-Null
	do {
		[string] $answer = Read-Host $QuestionText

		if($ValidateCommand) {
			$result = $($ValidateCommand.Invoke($answer))
		} else {
			$result = @($true, $answer)
		}
	} while(-not($result[0]))

	$result[1]
}