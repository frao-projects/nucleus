//! \file
//!
//! \brief Implementation of user interactions with error
//! handler singleton
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Error/Control.hpp"
#include "Error/Types.hpp"

// unnamed namespace for 'local' global definitions
namespace
{
//! \todo We should allocate static memory required, and
//! then construct with placement new. (and use custom
//! deleter for unique_ptr!) This would allow us to always
//! have enough memory for the error handler (ie: never
//! throw if we run out of memory). We would then have to
//! delete and placement new on the memory again, if we
//! wanted to replace the error handler

static std::shared_ptr<frao::ErrorLog> g_ErrorLog = nullptr;
}  // namespace

namespace frao
{
inline namespace ErrorLogging
{
// global functions
const std::shared_ptr<ErrorLog> setErrorLog(
	std::shared_ptr<ErrorLog> newHandler) noexcept
{
	g_ErrorLog = newHandler;

	runtime_assert(
		g_ErrorLog,
		"ErrorLog should always be valid, at this point");

	return g_ErrorLog;
}
const std::shared_ptr<ErrorLog> getErrorLog()
{
	// if we have a ErrorLog there
	if (g_ErrorLog) return g_ErrorLog;

	// if we have nothing pointed to, create something
	return replaceErrorLog();
}
const std::shared_ptr<ErrorLog> tryGetErrorLog() noexcept
{
	return g_ErrorLog;
}
}  // namespace ErrorLogging
}  // namespace frao