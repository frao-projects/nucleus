using Module .\Versioning.psm1

param (
    [Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -PathType Leaf -Path $_})]
    [string]
    $VersionManifestPath,
    #What style of increment should be performed
    [Parameter(Mandatory=$true)]
    [ValidateSet("major","minor","release_candidate","dev_build","skip")]
    $Mode
)

[VersionSpec]$currentVersion = Get-Content -Raw -LiteralPath "$VersionManifestPath" |
                                ConvertFrom-VersionManifest

switch -Exact($Mode) {
    'major' {
        $currentVersion.Major++
        $currentVersion.Minor = 0
        $currentVersion.Patch = 0
        $currentVersion.DevBuild = 0

        $currentVersion.Status = "dv"
		break
    }
    'minor' {
        $currentVersion.Minor++
        $currentVersion.Patch = 0
        $currentVersion.DevBuild = 0

        $currentVersion.Status = "dv"
		break
    }
    'release_candidate' {
        $currentVersion.Patch++
        $currentVersion.Status = "rc"
		break
    }
    'dev_build' {
        $currentVersion.DevBuild++
        $currentVersion.Status = "dv"
		break
    }
    Default {
        Write-Warning "Unknown increment style: $Mode."
    }
    'skip' {
        Write-Host "Skipping increment"
		break
    }
}

$local:versionStr = $currentVersion.ToString()

Write-Host "new version: $versionStr"
$versionStr | Set-Content -NoNewLine -LiteralPath "$VersionManifestPath"