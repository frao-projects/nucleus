#ifndef FRAO_NUCLEUS_STRINGS_CODEPOINT_IMPLEMENTATION_EXPERIMENTAL
#define FRAO_NUCLEUS_STRINGS_CODEPOINT_IMPLEMENTATION_EXPERIMENTAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the experimental implementation of unicode
//! codepoint class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <bit>			//for std::bit_cast
#include <optional>		//for std::optional
#include <type_traits>	//for std::conditional
#include "../../Environment.hpp"
#include "../StringDefines.hpp"

namespace frao
{
inline namespace Strings
{
//! \namespace frao::Strings::Experimental
//!
//! \brief Namespace that is used for experimental (full unicode)
//! strings code
namespace Experimental
{
// clang-format off

//! \class CodePointIMPL_UTF8
//!
//! \brief Implementation class for unicode UTF8 code points
//!
//! \details
//!
//! UTF-8 CODE TABLE(S)
//!
//! UTF-8 Valid code points to UTF-8 Byte Sequence
//! ==============================================
//! | Code Point           |  Byte One   |  Byte Two   | Byte Three   | Byte Four   |
//! | -------------------: | :---------: | :---------: | :----------: | :---------: |
//! | U+00 -> U+7F         |  00 -> 7F   |     n/a     |||
//! | U+80 -> U+7FF        |  C2 -> DF   |  80 -> BF   |      n/a     ||
//! | U+800 -> U+FFF       |     E0      |  A0 -> BF   |   80 -> BF   |     n/a     |
//! | U+1000 -> U+CFFF     |  E1 -> EC   |  80 -> BF   ||     ^     |
//! | U+D000 -> U+D7FF     |     ED      |  80 -> 9F   |   80 -> BF   |     ^     |
//! | U+D800 -> U+DFFF     | Not valid: This is the set of surrogate pairs!     ||||
//! | U+E000 -> U+FFFF     |  EE -> EF   |  80 -> BF   ||     n/a     |
//! | U+10000 -> U+3FFFF   |     F0      |  90 -> BF   |   80 -> BF   ||
//! | U+40000 -> U+FFFFF   |  F1 -> F3   |  80 -> BF   |||
//! | U+100000 -> U+10FFFF |     F4      |  80 -> 8F   |  80 -> BF    ||
//! | U+110000+            | Not valid: outside the range of valid code points! ||||
//!
//!
//! UTF-8 Byte sequence to UTF-8 code-point
//! =======================================
//! | Byte One | Range                     |             Further Bytes?             |
//! |--------: | :-----------------------: | :------------------------------------: |
//! | 00 -> 7F |       U+00 -> U+7F        |           Complete Code Point          |
//! | 80 -> C1 |   Ill-formed code-point   |                  n/a                   |
//! | C2 -> DF |      U+080 -> U+7FF       |           Get next (2nd) byte          |
//! |    E0    |      U+800 -> U+FFF       |     Get next two (2nd & 3rd) bytes     |
//! | E1 -> EC |     U+1000 -> U+CFFF      |                  ^                     |
//! |    ED    |     U+D000 -> U+D7FF      |                  ^                     |
//! | EE -> EF |     U+E000 -> U+FFFF      |                  ^                     |
//! |    F0    |    U+10000 -> U+3FFFF     | Get next three (2nd & 3rd & 4th) bytes |
//! | F1 -> F3 |    U+40000 -> U+FFFFF     |                  ^                     |
//! |    F4    |   U+100000 -> U+10FFFF    |                  ^                     |
//! | F5 -> FF |   Ill-formed code-point   |                  n/a                   |
//!
//!
//! Valid 2nd Bytes
//! ===============
//! |   Byte One    | Valid Byte Two |
//! | ------------: | :------------: |
//! |      E0       |    A0 -> BF    |
//! |      ED       |    80 -> 9F    |
//! |      F0       |    90 -> BF    |
//! |      F4       |    80 -> 8F    |
//! | **Any other** |    80 -> BF    |
//!
//!
//! Further Bytes (3rd + 4th)
//! =========================
//! |  Value   |      Result      |
//! | -------: | :--------------: |
//! | 00 -> 7F |    Ill-formed    |
//! | 80 -> BF | Valid code point |
//! | C0 -> FF |    Ill-formed    |
//!
//!
//! Binary Bit distribution
//! =======================
//! |         Code Point         |  Byte One  |  Byte Two  | Byte Three |  Byte Four |
//! | :------------------------: | :--------: | :--------: | :--------: | :--------: |
//! |          0xxxxxxx          |  0xxxxxxx  |    n/a     |    n/a     |    n/a     |
//! |     00000yyy yyxxxxxx      |  110yyyyy  |  10xxxxxx  |    n/a     |    n/a     |
//! |     zzzzyyyy yyxxxxxx      |  1110zzzz  |  10yyyyyy  |  10xxxxxx  |    n/a     |
//! | 000uuuuu zzzzyyyy yyxxxxxx |  11110uuu  |  10uuzzzz  |  10yyyyyy  |  10xxxxxx  |

// clang-format on

//! \brief Get the length of a UTF8 codepoint, from the leading byte
//!
//! \note Will return 0 for a trailing byte, and -1 for an invalid
//! byte
//!
//! \tparam CharType The underlying character type. Must be 1 byte in
//! size
//!
//! \returns The length of the codepoint, of which ::byte is the first
//! byte
//!
//! \param[in] byte The leading byte, of the codepoint to examine
template<typename CharType>
integer64 getUTF8CodepointLengthIMPL(CharType byte)
{
	static_assert(sizeof(CharType) == 1);

	// get the 1 byte bit-negation of the byte
	small_nat32 negated = (~byte) & 0xFF, largestBit;

#if _MT	 // Visual studio/VS-like

	if (0 == _BitScanReverse(&largestBit, negated)) [[unlikely]]
	{
		// Then we had no set bits, in the negation. Thus, the
		// original byte was 0xFF, which is not a part of ANY
		// valid codepoint
		return -1;
	}

#else  // NOT VS-like (GCC etc)

	if (negated == 0) [[unlikely]]
	{
		// Then we had an invalid input of 0xFF, meaning two things:
		// 1) this is not part of a valid codepoint
		// 2) __builtin_clz has an undefined result
		return -1;
	}

	// No _BitScanReverse, so emulate by counting leading zeroes.
	largestBit = 31U - __builtin_clz(negated);

#endif

	// Based on where the leading set bit of the negated number is,
	// and hence where the leading 0 of the original number is, we can
	// determine if the byte starts 0, 10, 110, 1110, 11110, or
	// something invalid
	switch (largestBit)
	{
		case 7:
			// byte is of the form 0xxxxxxx (1 byte codepoint)
			return 1;
		case 6:
			// byte is of the form 10xxxxxx (trailing codepoint)
			return 0;
		case 5:
			// byte is of the form 110yyyyy (2 byte codepoint)
			return 2;
		case 4:
			// byte is of the form 1110zzzz (3 byte codepoint)
			return 3;
		case 3:
			// byte is of the form 11110uuu (4 byte codepoint)
			return 4;
		default:
			// byte is of unrecognised form
			return -1;
	}
}

//! \brief Convert a UTF8 codepoint to UTF32
//!
//! \tparam UTF8CharType The underlying UTF8 character type. Must be 1
//! byte in size
//!
//! \tparam UTF32CharType The underlying UTF32 character type. Must be
//! 4 bytes in size
//!
//! \returns The equivalent utf32 codepoint, to the input UTF8
//! codepoint
//!
//! \param[in] utf8Array The array holding the UTF8 codepoint
//!
//! \param[in] usedElemeents The number of valid elements, of the
//! ::utf8Array Array
template<typename UTF32CharType, typename UTF8CharType>
UTF32CharType convertUTF8ToUTF32IMPL(UTF8CharType (&utf8Array)[4],
									 natural64 usedElements)
{
	static_assert(sizeof(UTF8CharType) == 1);
	static_assert(sizeof(UTF32CharType) == 4);

	UTF32CharType result = 0;

	switch (usedElements)
	{
		case 4:
			result =
				std::bit_cast<UTF32CharType>(utf8Array[3] & 0x3F);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[2] & 0x3F) << 6);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[1] & 0x3F) << 12);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[0] & 0x7) << 18);
			break;
		case 3:
			result =
				std::bit_cast<UTF32CharType>(utf8Array[2] & 0x3F);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[1] & 0x3F) << 6);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[0] & 0xF) << 12);
			break;
		case 2:
			result =
				std::bit_cast<UTF32CharType>(utf8Array[1] & 0x3F);
			result |= std::bit_cast<UTF32CharType>(
				(utf8Array[0] & 0x1F) << 6);
			break;
		case 1:
			result =
				std::bit_cast<UTF32CharType>(utf8Array[0] & 0x7F);
			break;
		default:
			break;
	}

	return result;
}

//! \brief Convert a UTF32 codepoint to UTF8
//!
//! \tparam UTF8CharType The underlying UTF8 character type. Must be 1
//! byte in size
//!
//! \tparam UTF32CharType The underlying UTF32 character type. Must be
//! 4 bytes in size
//!
//! \param[out] resArray The array that will holding the resulting
//! UTF8 codepoint
//!
//! \param[out] usedElements The number of valid elements, of the
//! ::resArray Array, once it has been written to
//!
//! \param[in] input The input UTF32 codepoint, to convert to UTF8
template<typename UTF8CharType, typename UTF32CharType>
void convertUTF32ToUTF8IMPL(UTF8CharType (&resArray)[4],
							natural64&	  usedElements,
							UTF32CharType input)
{
	static_assert(sizeof(UTF8CharType) == 1);
	static_assert(sizeof(UTF32CharType) == 4);

	if (input >= 0x1'0000)
	{
		// 4 byte UTF8 codepoint
		resArray[3] =
			std::bit_cast<UTF8CharType>(0x80 + (input & 0x3F));
		resArray[2] =
			std::bit_cast<UTF8CharType>(0x80 + ((input >> 6) & 0x3F));
		resArray[1] =
			std::bit_cast<UTF8CharType>(0x80 + ((input >> 12) & 0xF));
		resArray[0] = std::bit_cast<UTF8CharType>(
			0xF0 + ((input >> 16) & 0x1F));

		usedElements = 4;
	} else if (input >= 0x800)
	{
		// 3 byte UTF8 codepoint
		resArray[3] = 0x0;
		resArray[2] =
			std::bit_cast<UTF8CharType>(0x80 + (input & 0x3F));
		resArray[1] =
			std::bit_cast<UTF8CharType>(0x80 + ((input >> 6) & 0x3F));
		resArray[0] =
			std::bit_cast<UTF8CharType>(0xE0 + ((input >> 12) & 0xF));

		usedElements = 3;
	} else if (input >= 0x80)
	{
		// 2 byte UTF8 codepoint
		resArray[3] = 0x0;
		resArray[2] = 0x0;
		resArray[1] =
			std::bit_cast<UTF8CharType>(0x80 + (input & 0x3F));
		resArray[0] =
			std::bit_cast<UTF8CharType>(0xC0 + ((input >> 6) & 0x1F));

		usedElements = 2;
	} else
	{
		// 1 byte UTF8 codepoint
		runtime_assert(input <= 0x7F);

		resArray[3] = 0x0;
		resArray[2] = 0x0;
		resArray[1] = 0x0;
		resArray[0] = std::bit_cast<UTF8CharType>(input & 0x7F);

		usedElements = 1;
	}
}

//! \brief Experimental Codepoint class for utf8 codepoints
//!
//! \tparam CharType The underlying character type. Must be 1 byte in
//! size
template<typename CharType>
class CodePointIMPL_UTF8;

// clang-format off

//! \class CodePointIMPL_UTF16
//!
//! \brief Implementation class for unicode UTF8 code points
//!
//! \details
//!
//! UTF-16 CODE TABLE(S)
//! ====================
//!
//! UTF-16 Valid code-points to UTF-16 Byte sequence
//! ================================================
//! |    UTF-32 Range     |                      Conversion?                       |
//! | :-----------------: | :----------------------------------------------------: |
//! |  U+0000 -> U+D7FF   |                         Direct                         |
//! |  U+D800 -> U+DFFF   |                  Surrogate Pair range                  |
//! |  U+E000 -> U+FFFF   |                         Direct                         |
//! | U+10000 -> U+10FFFF |               Convert to surrogate pair                |
//! |      U+110000+      | **Not valid: outside the range of valid code points!** |
//!
//!
//! Surrogate Pair Translation (Binary)
//! ===================================
//! |      Code Point Form       |              Encoding               |
//! | :------------------------: | :---------------------------------: |
//! | 000uuuuu xxxxxxxx xxxxxxxx | 110110ww wwxxxxxx 110111xx xxxxxxxx |
//!
//!	Note: wwww = uuuuu - 1
//!		This is since you must subtract 0x10000 from code-point
//!		value, to account for starting point of values that
//!		convert to surrogate pairs

// clang-format on

//! \brief Get the length of a UTF16 codepoint, from the leading
//! element
//!
//! \note Will return 0 for a trailing surrogate
//!
//! \tparam CharType The underlying character type. Must be 2 bytes in
//! size
//!
//! \returns The length of the codepoint, of which ::element is the
//! first byte
//!
//! \param[in] element The leading element, of the codepoint to
//! examine
template<typename CharType>
integer64 getUTF16CodepointLengthIMPL(CharType element)
{
	static_assert(sizeof(CharType) == 2);

	// Determine if the start of the element matches the bit pattern
	// of a leading or trailing surrogate
	if (0xDC00 == (element & 0xDC00))
	{
		// trailing surrogate bit pattern
		return 0;
	} else if (0xD800 == (element & 0xD800))
	{
		// leading surrogate bit pattern
		return 2;
	} else
	{
		// normal codepoint
		return 1;
	}
}

//! \brief Convert a UTF16 codepoint to UTF32
//!
//! \tparam UTF8CharType The underlying UTF16 character type. Must be
//! 2 bytes in size
//!
//! \tparam UTF32CharType The underlying UTF32 character type. Must be
//! 4 bytes in size
//!
//! \returns The equivalent utf32 codepoint, to the input UTF16
//! codepoint
//!
//! \param[in] utf16Array The array holding the UTF16 codepoint
//!
//! \param[in] usedElemeents The number of valid elements, of the
//! ::utf16Array Array
template<typename UTF32CharType, typename UTF16CharType>
UTF32CharType convertUTF16ToUTF32IMPL(UTF16CharType (&utf16Array)[2],
									  natural64 usedElements)
{
	static_assert(sizeof(UTF16CharType) == 2);
	static_assert(sizeof(UTF32CharType) == 4);

	UTF32CharType result = 0;

	switch (usedElements)
	{
		case 2:
			result = std::bit_cast<UTF32CharType>(
				(utf16Array[0] & 0x3FF) << 10);
			result |=
				std::bit_cast<UTF32CharType>(utf16Array[1] & 0x3FF);
			result += 0x1'0000;
			break;
		case 1:
			result = std::bit_cast<UTF32CharType>(utf16Array[0]);
			break;
		default:
			break;
	}

	return result;
}

//! \brief Convert a UTF32 codepoint to UTF16
//!
//! \tparam UTF16CharType The underlying UTF16 character type. Must be
//! 2 bytes in size
//!
//! \tparam UTF32CharType The underlying UTF32 character type. Must be
//! 4 bytes in size
//!
//! \param[out] resArray The array that will holding the resulting
//! UTF16 codepoint
//!
//! \param[out] usedElemeents The number of valid elements, of the
//! ::resArray Array, once it has been written to
//!
//! \param[in] input The input UTF32 codepoint, to convert to UTF16
template<typename UTF16CharType, typename UTF32CharType>
void convertUTF32ToUTF16IMPL(UTF16CharType (&resArray)[2],
							 natural64&	   usedElements,
							 UTF32CharType input)
{
	static_assert(sizeof(UTF16CharType) == 2);
	static_assert(sizeof(UTF32CharType) == 4);

	runtime_assert(input < 0x11'0000);

	if (input >= 0x1'0000) [[unlikely]]
	{
		// need to encode a surrogate pair
		usedElements = 2;

		resArray[0] = std::bit_cast<UTF16CharType>(
			0xD800 + (((input - 0x1'0000) >> 10) & 0x3FF));
		resArray[1] =
			std::bit_cast<UTF16CharType>(0xDC00 + (input & 0x3FF));
	} else
	{
		// can encode a normal character
		usedElements = 1;

		resArray[0] = std::bit_cast<UTF16CharType>(input & 0xFFFF);
		resArray[1] = 0x0;
	}
}

//! \brief Convert a UTF16 codepoint to UTF32
//!
//! \tparam UTF8CharType The underlying UTF16 character type. Must be
//! 2 bytes in size
//!
//! \tparam UTF8CharType The underlying UTF8 character type. Must be
//! 1 byte in size
//!
//! \param[in] utf16Array The array holding the UTF16 codepoint
//!
//! \param[in] usedUTF16Elemeents The number of valid elements, of the
//! ::utf16Array Array
//!
//! \param[out] resArray The array that will holding the resulting
//! UTF8 codepoint
//!
//! \param[out] usedUTF8Elements The number of valid elements, of the
//! ::resArray Array, once it has been written to
template<typename UTF16CharType, typename UTF8CharType>
void convertUTF16ToUTF8IMPL(UTF16CharType (&utf16Array)[2],
							natural64 usedUTF16Elements,
							UTF8CharType (&resArray)[2],
							natural64& usedUTF8Elements)
{
	static_assert(sizeof(UTF16CharType) == 2);
	static_assert(sizeof(UTF8CharType) == 1);

	if (usedUTF16Elements == 2)
	{
		// surrogate pair -> 4 byte UTF8 codepoint

		// set bits from second surrogate pair
		resArray[3] = std::bit_cast<UTF8CharType>(
			0x80 + (utf16Array[1] & 0x3F));
		resArray[2] = std::bit_cast<UTF8CharType>(
			0x80 + ((utf16Array[1] >> 6) & 0xF));

		// set unchanged bits from first surrogate pair
		resArray[2] = std::bit_cast<UTF8CharType>(
			0x80 + ((utf16Array[0] & 0x3) << 4));
		resArray[1] = std::bit_cast<UTF8CharType>(
			0x80 + ((utf16Array[0] >> 2) & 0xF));

		// get upper 4 bits from surrogate pair, and add back the
		// (equivalent of the) 0x1'000 that was removed when the
		// surrogate pair was encoded
		UTF8CharType upperPortion =
			std::bit_cast<UTF8CharType>((utf16Array[0] >> 6) & 0xF);
		upperPortion += 1;

		// finally set changed portion of first surrogate pair
		resArray[0] =
			std::bit_cast<UTF8CharType>(0xF0 + upperPortion);

		usedUTF8Elements = 4;
	} else if (utf16Array[0] >= 0x800)
	{
		// single element -> 3 byte UTF8 codepoint
		runtime_assert(usedUTF16Elements == 1);

		resArray[3] = 0x0;
		resArray[2] = std::bit_cast<UTF8CharType>(
			0x80 + (utf16Array[0] & 0x3F));
		resArray[1] = std::bit_cast<UTF8CharType>(
			0x80 + ((utf16Array[0] >> 6) & 0x3F));
		resArray[0] = std::bit_cast<UTF8CharType>(
			0xE0 + ((utf16Array[0] >> 12) & 0xF));

		usedUTF8Elements = 3;
	} else if (utf16Array[0] >= 0x80)
	{
		// single element -> 2 byte UTF8 codepoint
		runtime_assert(usedUTF16Elements == 1);

		resArray[3] = 0x0;
		resArray[2] = 0x0;
		resArray[1] = std::bit_cast<UTF8CharType>(
			0x80 + (utf16Array[0] & 0x3F));
		resArray[0] = std::bit_cast<UTF8CharType>(
			0xC0 + ((utf16Array[0] >> 6) & 0x1F));

		usedUTF8Elements = 2;
	} else
	{
		// single element -> 1 byte UTF8 codepoint
		runtime_assert(usedUTF16Elements == 1);

		resArray[3] = 0x0;
		resArray[2] = 0x0;
		resArray[1] = 0x0;
		resArray[0] =
			std::bit_cast<UTF8CharType>(utf16Array[0] & 0x7F);

		usedUTF8Elements = 1;
	}
}

//! \brief Convert a UTF8 codepoint to UTF16
//!
//! \tparam UTF16CharType The underlying UTF16 character type. Must be
//! 2 bytes in size
//!
//! \tparam UTF8CharType The underlying UTF8 character type. Must be
//! 1 byte in size
//!
//! \param[out] resArray The array that will holding the resulting
//! UTF16 codepoint
//!
//! \param[out] usedUTF16Elements The number of valid elements, of the
//! ::resArray Array, once it has been written to
//!
//! \param[in] utf8Array The array holding the UTF8 codepoint
//!
//! \param[in] usedElemeents The number of valid elements, of the
//! ::utf8Array Array
template<typename UTF16CharType, typename UTF8CharType>
void convertUTF8ToUTF16IMPL(UTF16CharType (&resArray)[2],
							natural64& usedUTF16Elements,
							UTF8CharType (&utf8Array)[4],
							natural64 usedUTF8Elements)
{
	static_assert(sizeof(UTF16CharType) == 2);
	static_assert(sizeof(UTF8CharType) == 1);

	switch (usedUTF8Elements)
	{
		case 4:	 // will have 2 element result
		{
			// set second surrogate pair, include adding prefix of
			// DC00 for indication of second surrogate
			resArray[1] =
				std::bit_cast<UTF16CharType>(utf8Array[3] & 0x3F);
			resArray[1] |= std::bit_cast<UTF16CharType>(
				(utf8Array[2] & 0xF) << 6);
			resArray[1] += 0xDC00;

			// set lower 6 bits of first surrogate pair
			resArray[0] = std::bit_cast<UTF16CharType>(
				(utf8Array[2] >> 4) & 0x3);
			resArray[0] |= std::bit_cast<UTF16CharType>(
				(utf8Array[1] & 0xF) << 2);

			// calculate upper portion of first surrogate pair,
			// representing the portions of the codepoint >= 0x1'0000
			UTF16CharType upperPortion = std::bit_cast<UTF16CharType>(
				(utf8Array[1] >> 4) & 0x3);
			upperPortion |= std::bit_cast<UTF16CharType>(
				(utf8Array[0] & 0x7) << 2);

			runtime_assert(upperPortion <= 0x10,
						   "If the upper portion is greater than 10, "
						   "then the whole codepoint is greater than "
						   "10FFFF, which is not valid");

			// subtract the equivalent of 0x1'0000, as required for
			// surrogate pairs, and insert remaining 4 bits into first
			// surrogate. Then, add D800 prefix, for indication of
			// first surrogate
			resArray[0] |= ((upperPortion - 1) << 6);
			resArray[0] += 0xD800;

			usedUTF16Elements = 2;
		}
		break;
		case 3:	 // will have 1 element result
			resArray[0] =
				std::bit_cast<UTF16CharType>(utf8Array[2] & 0x3F);
			resArray[0] |= std::bit_cast<UTF16CharType>(
				(utf8Array[1] & 0x3F) << 6);
			resArray[0] |= std::bit_cast<UTF16CharType>(
				(utf8Array[0] & 0xF) << 12);
			resArray[1] = 0x0;

			usedUTF16Elements = 1;
			break;
		case 2:	 // will have 1 element result
			resArray[0] =
				std::bit_cast<UTF16CharType>(utf8Array[1] & 0x3F);
			resArray[0] |= std::bit_cast<UTF16CharType>(
				(utf8Array[0] & 0x1F) << 6);
			resArray[1] = 0x0;

			usedUTF16Elements = 1;
			break;
		default:
			utf8Array[0] = 0x0;

			[[fallthrough]];
		case 1:	 // will have 1 element result
			resArray[0] =
				std::bit_cast<UTF16CharType>(utf8Array[0] & 0x7F);
			resArray[1] = 0x0;

			usedUTF16Elements = 1;
			break;
	}
}

//! \brief Experimental Codepoint class for utf16 codepoints
//!
//! \tparam CharType The underlying character type. Must be 2 bytes in
//! size
template<typename CharType>
class CodePointIMPL_UTF16;

// clang-format off

//! \class CodePointIMPL_UTF32
//!
//! \brief Implementation class for unicode UTF8 code points
//!
//! \details
//!
//! UTF-32 CODE
//! ===========
//!
//! UTF-32 Valid code-points to UTF-32 Byte sequence
//! ================================================
//! |    UTF-32 Range    |                         Valid?                         |
//! | :----------------: | :----------------------------------------------------: |
//! |  U+0000 -> U+D7FF  |                          Yes                           |
//! |  U+D800 -> U+DFFF  |   **Not valid: This is the set of surrogate pairs!**   |
//! | U+E000 -> U+10FFFF |                          Yes                           |
//! |     U+110000+      | **Not valid: outside the range of valid code points!** |
//!
//! \note Note: Code points in the valid range are just stored as-is!

// clang-format on

//! \brief Experimental Codepoint class for utf32 codepoints
//!
//! \tparam CharType The underlying character type. Must be 4 bytes in
//! size
template<typename CharType>
class CodePointIMPL_UTF32;


template<typename CharType>
class [[deprecated(
	"Use CodePointIMPL_UTF32 instead")]] NUCLEUS_LIB_API
	CodePointIMPL_UTF8
{
	static_assert(sizeof(CharType) == sizeof(charU8),
				  "The underlying type of a UTF8 codepoint, must be "
				  "1 byte in size!");

	//! \brief Space for any size utf8 codepoint.
	//!
	//! \note This is actually the smallest allocation we can get away
	//! with, because pointers are 8 bytes on 64-bit architectures,
	//! and unions use the memory of their largest member
	CharType m_Bytes[4];
	//! \brief how many bytes we actually use
	//!
	//! \note Cannot be 0, even if empty
	natural8 m_RealSize;

   public:
	//! \brief Construct null codepoint
	CodePointIMPL_UTF8() : m_Bytes{0x0, 0x0, 0x0, 0x0}, m_RealSize(1)
	{}
	//! \brief Construct a codepoint, from the specified string.
	//! Will get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \throws InvalidUserInput Will throw iff no valid codepoint
	//! exists in the supplied buffer
	//!
	//! \param[in] codepoint String that contains the
	//! code-point
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the
	//! last valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of
	//! the correct codepoint to read. If null, will not be
	//! written to
	CodePointIMPL_UTF8(const CharType* codepoint,
					   frao::natural64 buffersize,
					   bool getLastCodepoint, natural64* readStart)
		: CodePointIMPL_UTF8()
	{
		std::optional<natural64> codepointStart;
		natural64				 codepointLength;
		for (natural64 startIndex = 0; startIndex < buffersize;
			 ++startIndex)
		{
			natural64 offsetIndex = getLastCodepoint
										? buffersize - 1 - startIndex
										: startIndex;
			codepointLength =
				getUTF8CodepointLengthIMPL(codepoint[offsetIndex]);

			if ((codepointLength > 0)
				&& (offsetIndex + codepointLength <= buffersize))
			{
				// we know that we have a valid starting byte, and we
				// know the length of the codepoint
				codepointStart = offsetIndex;
				break;
			}
		}

		if (!codepointStart)
		{
			// we could find no valid starting codepoint
			throw invalid_input();
		}

		runtime_assert(
			(codepointLength > 0) && (codepointLength <= 4),
			"Given that we throw when we have an invalid codepoint, "
			"we should always have a valid codepointLength here");
		m_RealSize = codepointLength;

		if (readStart != nullptr)
		{
			*readStart = *codepointStart;
		}

		switch (m_RealSize)
		{
			case 4:
				m_Bytes[3] = codepoint[*codepointStart + 3];
				[[fallthrough]];
			case 3:
				m_Bytes[2] = codepoint[*codepointStart + 2];
				[[fallthrough]];
			case 2:
				m_Bytes[1] = codepoint[*codepointStart + 1];
				[[fallthrough]];
			default:
				m_Bytes[0] = codepoint[*codepointStart];
				break;
		}
	}

	//! brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePointIMPL_UTF8(charA asciiChar)
		: m_Bytes{asciiChar & 0x7F, 0x0, 0x0, 0x0}, m_RealSize(1)
	{}

	//! \brief Copy construct a utf8 codepoint, from a utf8 codepoint
	//! of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF8(
		const CodePointIMPL_UTF8<OtherType>& rhs)
		: m_Bytes{std::bit_cast<CharType>(rhs.m_Bytes[0]),
				  std::bit_cast<CharType>(rhs.m_Bytes[1]),
				  std::bit_cast<CharType>(rhs.m_Bytes[2]),
				  std::bit_cast<CharType>(rhs.m_Bytes[3])},
		  m_RealSize(rhs.m_RealSize)
	{}
	//! \brief Copy construct a utf8 codepoint, from a utf16 codepoint
	//! of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF8(
		const CodePointIMPL_UTF16<OtherType>& rhs)
		: CodePointIMPL_UTF8()
	{
		convertUTF16ToUTF8IMPL(rhs.m_Elements, rhs.m_RealSize,
							   this->m_Bytes, this->m_RealSize);
	}
	//! \brief Copy construct a utf8 codepoint, from a utf32 codepoint
	//! of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF8(
		const CodePointIMPL_UTF32<OtherType>& rhs)
		: CodePointIMPL_UTF8()
	{
		convertUTF32ToUTF8IMPL(this->m_Bytes, this->m_RealSize,
							   rhs.m_Codepoint);
	}

	//! \brief Copy construct a utf8 codepoint, from another
	//! utf8 codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL_UTF8(const CodePointIMPL_UTF8<CharType>& rhs)
		: m_Bytes{rhs.m_Bytes[0], rhs.m_Bytes[1], rhs.m_Bytes[2],
				  rhs.m_Bytes[3]},
		  m_RealSize(rhs.m_RealSize)
	{}
	//! \brief Move construct a utf8 codepoint, from another utf8
	//! codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF8(CodePointIMPL_UTF8<CharType> && rhs)
		: m_Bytes{rhs.m_Bytes[0], rhs.m_Bytes[1], rhs.m_Bytes[2],
				  rhs.m_Bytes[3]},
		  m_RealSize(rhs.m_RealSize)
	{}

	//! \brief destroy the codepoint, and call any derived destructors
	virtual ~CodePointIMPL_UTF8() = default;

	//! \brief copy assign a utf8 codepoint, from another utf8
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL_UTF8<CharType>& operator=(
		const CodePointIMPL_UTF8<CharType>& rhs)
	{
		this->m_Bytes[0] = rhs.m_Bytes[0];
		this->m_Bytes[1] = rhs.m_Bytes[1];
		this->m_Bytes[2] = rhs.m_Bytes[2];
		this->m_Bytes[3] = rhs.m_Bytes[3];

		this->m_RealSize = rhs.m_RealSize;
	}

	//! \brief Move assign a utf8 codepoint, from another utf8
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF8<CharType>& operator=(
		CodePointIMPL_UTF8<CharType>&& rhs)
	{
		this->m_Bytes[0] = rhs.m_Bytes[0];
		this->m_Bytes[1] = rhs.m_Bytes[1];
		this->m_Bytes[2] = rhs.m_Bytes[2];
		this->m_Bytes[3] = rhs.m_Bytes[3];

		this->m_RealSize = rhs.m_RealSize;
	}

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer
	frao::natural64 byteSizeIMPL() const noexcept
	{
		static_assert(sizeof(CharType) == 1);

		return m_RealSize;
	}
	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_RealSize;
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	CharType& getIMPL(frao::natural64 index) noexcept
	{
		return this->m_Bytes[index & 0x4];
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const CharType& getIMPL(frao::natural64 index) const noexcept
	{
		return this->m_Bytes[index & 0x4];
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepointIMPL() const noexcept
	{
		return convertUTF8ToUTF32IMPL<char32_t>(this->m_Bytes,
												this->m_RealSize);
	}
};

template<typename CharType>
class [[deprecated(
	"Use CodePointIMPL_UTF32 instead")]] NUCLEUS_LIB_API
	CodePointIMPL_UTF16
{
	static_assert(sizeof(CharType) == sizeof(char16_t),
				  "The underlying type of a UTF16 codepoint, must be "
				  "2 bytess in size!");

	//! \brief Space for any size utf16 codepoint.
	//!
	//! \note This is actually the smallest allocation we can get away
	//! with, because pointers are 8 bytes on 64-bit architectures,
	//! and unions use the memory of their largest member
	CharType m_Elements[2];
	//! \brief how many elements (NOT bytes, bytes/2) we actually use.
	//!
	//! \note Will almost always be 1, except for surrogate pairs
	natural8 m_RealSize;

   public:
	//! \brief Construct null codepoint
	CodePointIMPL_UTF16() : m_Elements{0x0, 0x0}, m_RealSize(1)
	{}
	//! \brief Construct a codepoint, from the specified string. Will
	//! get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \param[in] codepoint String that contains the
	//! code-point
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the last
	//! valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct codepoint to read. If null, will not be written to
	CodePointIMPL_UTF16(const CharType* codepoint,
						frao::natural64 buffersize,
						bool getLastCodepoint, natural64* readStart)
		: CodePointIMPL_UTF16()
	{
		std::optional<natural64> codepointStart;
		natural64				 codepointLength;
		for (natural64 startIndex = 0; startIndex < buffersize;
			 ++startIndex)
		{
			natural64 offsetIndex = getLastCodepoint
										? buffersize - 1 - startIndex
										: startIndex;
			codepointLength =
				getUTF16CodepointLengthIMPL(codepoint[offsetIndex]);

			if ((codepointLength > 0)
				&& (offsetIndex + codepointLength <= buffersize))
			{
				// we know that we have a valid starting byte, and
				// we know the length of the codepoint
				codepointStart = offsetIndex;
				break;
			}
		}

		if (!codepointStart)
		{
			// we could find no valid starting codepoint
			throw invalid_input();
		}

		runtime_assert(
			(codepointLength > 0) && (codepointLength <= 2),
			"Given that we throw when we have an invalid "
			"codepoint, we should always have a valid "
			"codepointLength here");
		m_RealSize = codepointLength;

		if (readStart != nullptr)
		{
			*readStart = *codepointStart;
		}

		switch (m_RealSize)
		{
			case 2:
				m_Elements[1] = codepoint[*codepointStart + 1];
				[[fallthrough]];
			default:
				m_Elements[0] = codepoint[*codepointStart];
				break;
		}
	}

	//! \brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF16
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePointIMPL_UTF16(charA asciiChar)
		: m_Elements{asciiChar & 0x7F, 0x0}, m_RealSize(1)
	{}

	//! \brief Copy construct a utf16 codepoint, from a utf8 codepoint
	//! of any valid underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF16(
		const CodePointIMPL_UTF8<OtherType>& rhs)
		: CodePointIMPL_UTF16()
	{
		convertUTF8ToUTF16IMPL(this->m_Elements, this->m_RealSize,
							   rhs.m_Bytes, rhs.m_RealSize);
	}
	//! \brief Copy construct a utf16 codepoint, from a utf16
	//! codepoint of any valid underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF16(
		const CodePointIMPL_UTF16<OtherType>& rhs)
		: m_Elements{std::bit_cast<CharType>(rhs.m_Elements[0]),
					 std::bit_cast<CharType>(rhs.m_Elements[1])},
		  m_RealSize(rhs.m_RealSize)
	{}
	//! \brief Copy construct a utf16 codepoint, from a utf32
	//! codepoint of any valid underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF16(
		const CodePointIMPL_UTF32<OtherType>& rhs)
		: CodePointIMPL_UTF16()
	{
		convertUTF32ToUTF16IMPL(this->m_Elements, this->m_RealSize,
								rhs.m_Codepoint);
	}

	//! \brief Copy construct a utf16 codepoint, from another utf16
	//! codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL_UTF16(const CodePointIMPL_UTF16<CharType>& rhs)
		: m_Elements{rhs.m_Elements[0], rhs.m_Elements[1]},
		  m_RealSize(rhs.m_RealSize)
	{}
	//! \brief Move construct a utf16 codepoint, from another utf16
	//! codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF16(CodePointIMPL_UTF16<CharType> && rhs)
		: m_Elements{rhs.m_Elements[0], rhs.m_Elements[1]},
		  m_RealSize(rhs.m_RealSize)
	{}
	//! \brief destroy the codepoint, and call any derived destructors
	virtual ~CodePointIMPL_UTF16() = default;

	//! \brief copy assign a utf16 codepoint, from another utf16
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF16<CharType>& operator=(
		const CodePointIMPL_UTF16<CharType>& rhs)
	{
		this->m_RealSize = rhs.m_RealSize;

		this->m_Elements[0] = rhs.m_Elements[0];
		this->m_Elements[1] = rhs.m_Elements[1];
	}
	//! \brief move assign a utf16 codepoint, from another utf16
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF16<CharType>& operator=(
		CodePointIMPL_UTF16<CharType>&& rhs)
	{
		this->m_RealSize = rhs.m_RealSize;

		this->m_Elements[0] = rhs.m_Elements[0];
		this->m_Elements[1] = rhs.m_Elements[1];
	}

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return sizeof(CharType) * m_RealSize;
	}
	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_RealSize;
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	CharType& getIMPL(frao::natural64 index) noexcept
	{
		return m_Elements[index & 0x1];
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const CharType& getIMPL(frao::natural64 index) const noexcept
	{
		return m_Elements[index & 0x1];
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepointIMPL() const noexcept
	{
		return convertUTF16ToUTF32IMPL<char32_t>(this->m_Elements,
												 this->m_RealSize);
	}
};


template<typename CharType>
class NUCLEUS_LIB_API CodePointIMPL_UTF32
{
	static_assert(sizeof(CharType) == sizeof(char32_t),
				  "The underlying type of a UTF32 codepoint, must be "
				  "4 bytes in size!");

	CharType m_Codepoint;

   public:
	//! \brief Construct null codepoint
	CodePointIMPL_UTF32() : m_Codepoint(0x0)
	{}
	//! \brief Construct a codepoint, from the specified string. Will
	//! get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \param[in] codepoint String that contains the
	//! code-point
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the last
	//! valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct codepoint to read. If null, will not be written to
	CodePointIMPL_UTF32(const CharType* codepoint,
						frao::natural64 buffersize,
						bool getLastCodepoint, natural64* readStart)
		: CodePointIMPL_UTF32()
	{
		natural64 offsetIndex = getLastCodepoint ? buffersize - 1 : 0;

		if (readStart != nullptr)
		{
			*readStart = offsetIndex;
		}

		m_Codepoint = codepoint[offsetIndex];
	}

	//! \brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePointIMPL_UTF32(charA asciiChar)
		: m_Codepoint(asciiChar & 0x7F)
	{}

	//! \brief Copy construct a utf32 codepoint, from a utf8 codepoint
	//! of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF32(
		const CodePointIMPL_UTF8<OtherType>& rhs)
		: m_Codepoint(convertUTF8ToUTF32IMPL<CharType>(
			rhs.m_Bytes, rhs.m_RealSize))
	{}
	//! \brief Copy construct a utf32 codepoint, from a utf16
	//! codepoint of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF32(
		const CodePointIMPL_UTF16<OtherType>& rhs)
		: m_Codepoint(convertUTF16ToUTF32IMPL<CharType>(
			rhs.m_Elements, rhs.m_RealSize))
	{}
	//! \brief Copy construct a utf32 codepoint, from a utf32
	//! codepoint of any underlying type
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL_UTF32(
		const CodePointIMPL_UTF32<OtherType>& rhs)
		: m_Codepoint(std::bit_cast<CharType>(rhs.m_Codepoint))
	{}

	//! \brief Copy construct a utf32 codepoint, from another utf32
	//! codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL_UTF32(const CodePointIMPL_UTF32<CharType>& rhs)
		: m_Codepoint(rhs.m_Codepoint)
	{}
	//! \brief Move construct a utf32 codepoint, from another utf32
	//! codepoint, of the same underlying type
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF32(CodePointIMPL_UTF32<CharType>&& rhs)
		: m_Codepoint(rhs.m_Codepoint)
	{}

	//! \brief destroy the codepoint, and call any derived destructors
	virtual ~CodePointIMPL_UTF32() = default;

	//! \brief copy assign a utf32 codepoint, from another utf32
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF32<CharType>& operator=(
		const CodePointIMPL_UTF32<CharType>& rhs)
	{
		m_Codepoint = rhs.m_Codepoint;
	}
	//! \brief Move assign a utf32 codepoint, from another utf32
	//! codepoint, of the same underlying type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL_UTF32<CharType>& operator=(
		CodePointIMPL_UTF32<CharType>&& rhs)
	{
		m_Codepoint = rhs.m_Codepoint;
	}

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return sizeof(CharType);
	}
	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 sizeIMPL() const noexcept
	{
		return 1;
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unused, exists to maintain the same interface
	//! as other IMPL types
	CharType& getIMPL(frao::natural64 index) noexcept
	{
		return m_Codepoint;
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unused, exists to maintain the same interface
	//! as other IMPL types
	const CharType& getIMPL(frao::natural64 index) const noexcept
	{
		return m_Codepoint;
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepointIMPL() const noexcept
	{
		return std::bit_cast<char32_t>(m_Codepoint);
	}
};

//! \brief Dispatches a type of character, to the type that should be
//! used to deal with that character + encoding
//!
//! \tparam CharType The type of character, for which we desire a UTF
//! encoding class
template<typename CharType>
using CodePointIMPL_T = std::conditional_t<
	sizeof(CharType) == sizeof(charU8), CodePointIMPL_UTF8<CharType>,
	std::conditional_t<sizeof(CharType) == sizeof(char16_t),
					   CodePointIMPL_UTF16<CharType>,
					   CodePointIMPL_UTF32<CharType>>>;
}  // namespace Experimental
}  // namespace Strings
}  // namespace frao

#endif