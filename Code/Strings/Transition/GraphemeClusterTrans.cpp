//! \file
//!
//! \brief Transitional implmentation of Unicode Grapheme classes
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/Transitional/GraphemeClusterIMPL_Trans.hpp"

namespace frao
{
inline namespace Strings
{
namespace Transitional
{
#if defined(NUCLEUS_HEADER_ONLY)
template class GraphemeClusterIMPL<charU8>;
template class GraphemeClusterIMPL<wchar_t>;
template class GraphemeClusterIMPL<char16_t>;
template class GraphemeClusterIMPL<char32_t>;
#endif

}  // namespace Transitional
}  // namespace Strings
}  // namespace frao
