using Module .\Generation.psm1
using Module .\Input.psm1

param(
	# The folder that holds our projects
	[Parameter(Mandatory=$true)]
	[ValidateScript({
		if(Test-Path -Path $_ -PathType Container) {
			$true
		} else {
			Write-Error "No such Project Folder!"
			exit
		}
	})]
	[string] $ProjectRoot,
	# The location of the .frao directory, of this project
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
	[string]$FraoDir
)

# Get build functions, via module, with using. Since using declarations cannot use variables,
# we have to invoke it via temporary script file, rather than at the top of the file
[string]$local:usingScriptContent = "using module '$FraoDir/BuildFunctions.psm1'"
$local:usingScript = [scriptblock]::Create($usingScriptContent)
. $usingScript

# setup the symbol map, for later
$local:knownStringMap = @{}

#Is used to pass to Read-Option
function Confirm-ProjectName {
	[OutputType([boolean], [string])]
	param(
		# The string to test whether it refers to a boolean
		[string] $answer
	)

	# Return validation and actual result
	($answer.Length -gt 0 ) -and (Test-Path -Path "$ProjectRoot\$answer" -IsValid)
	$answer
}

#will return validity in first element, and actual value in second
#Is used to pass to Read-Option
function Confirm-ProjectType {
	[OutputType([boolean], [string])]
	param(
		# The string to test whether it refers to a boolean
		[string] $answer
	)
	$local:validAnswers = @("exe", "lib")
	$local:result = $validAnswers -match $answer

	# return validity in [0], and actual result in [1]
	$result.Length -eq 1
	$result
}

# Get the name of the project we're creating, and the type of the project
$local:projectName = Read-Option "Enter a project name" -ValidateCommand ${function:Confirm-ProjectName}
$local:projectExt = Read-Option "Enter a project type (exe, lib)" -ValidateCommand ${function:Confirm-ProjectType}

$local:projectType
$local:projectPrefix
switch($projectExt) {
	"exe" { $projectType = "executable"; $projectPrefix = "";
		break}
	"lib" { $projectType = "static_library"; $projectPrefix = "lib__";
		break}
	default {throw 'Unknown project extension!'}
}

# set the specified project name and type to the symbol map, to save the user
# typing the same thing twice
$knownStringMap["#PROJECT_NAME#"] = "$projectName"
$knownStringMap["#PROJECT_NAME_CAPS#"] = "$projectName".ToUpper()
$knownStringMap["#PROJECT_FILENAME#"] = "$projectPrefix$projectName.$projectExt"
$knownStringMap["#PROJECT_RESULT_EXT#"] = "$projectExt"
$knownStringMap["#PROJECT_RESULT_TYPE#"] = "$projectType"
$knownStringMap["#PROJECT_RESULT_PREFIX#"] = "$projectPrefix"

# Get the absolute path, and then inform the user of the path we'll be writing to
$ProjectsDir = Resolve-Path $ProjectRoot
$local:copyPath = "$ProjectsDir/$projectName/Source"
Write-Host "Writing to: $copyPath"
Write-Host ''

# create the source directory
if(Test-Path -Path $copyPath -PathType Container) {
	# If the folder already exists, it needs to be empty
	$local:existingFiles = Get-ChildItem -Path $copyPath -File -Recurse

	if(($existingFiles | Measure-Object).Count -gt 0) {
		# Then we have files in the target directory. Ask the user, if they want
		# to continue anyway
		[boolean]$answer = Read-Boolean 'Files already exist in Target folder! Continue'

		if(-not($answer)) {
			Write-Host "Quitting..."
			return
		}
	}
}

Write-Host ''

# Work out where we are, and where our templates are
$local:scriptRoot = $PSScriptRoot
$local:templatesPath = "$scriptRoot/ProjectTemplates"

# Get the list of our template files
$local:fileList = Get-FileList -SourceDir $scriptRoot -NameDir "ProjectTemplates" -ToInclude *

# split the required and optional items
$local:requiredItems = @()
$local:optionalItems = @()
foreach($local:file in $fileList) {
	if($file -match "^_Options/") {
		$optionalItems += $file -replace "^_Options/", ""
	} else {
		$requiredItems += $file
	}
}

# Create the object used to store changes in paths between template and target files/directories
$local:copyFileTable = @{}
$local:copyDirTable = @{}

# Create a tree of the required items, and add them to the copy table, so they get copied
[FileTree]$local:RequiredTree = [FileTree]::new(($requiredItems -replace "\\", "/"),
									$null, $null)
$RequiredTree.Children | ForEach-Object {
	if($_.GetChildrenNames() -ne 0) {
		$copyDirTable[$_.MyName()] = $_.MyName() + '/'
	} else {
		$copyFileTable[$_.MyName()] = $_.MyName()
	}}

# For optional files, group by first path segment
# Then, per group, ask what option is desired
# copy the one selected

[FileTree]$local:OptionalTree = [FileTree]::new(($optionalItems -replace "\\", "/"),
									$null, $null)

foreach($local:selection in $OptionalTree.Children) {
	$local:selectionName = $selection.MyName()
	$local:optionNameList = $selection.GetChildrenNames()
	$local:nextKeyPrefix | Out-Null

	# We add /none, because that cannot be a valid path segment (we've already seperated
	# by /), and we need a 'none' value
	if($optionNameList.Count -eq 0) {
		# If we had no valid children, then use the name of the parent, because this is
		# a case of on/off, rather than options 1/option 2. We should also keep the key
		# prefix to just _Options, because we're choosing something directly in _Options,
		# not a sub-folder
		$optionNameList = @($selectionName, '/none')
		$nextKeyPrefix = "_Options"
	} else {
		$optionNameList += '/none'
		$nextKeyPrefix = "_Options/$selectionName"
	}

	Write-Host "Selection: $selectionName"
	if($selectionName -eq "meson.build") {
		$copyFileTable["$nextKeyPrefix/$projectType"] = $selectionName
		continue
	} elseif($selectionName -eq ".vscode") {
		if($projectExt -eq "exe") {
			# If we have an exe, we'll want to be able to launch it
			$copyFileTable["$nextKeyPrefix/launch.json"] = ".vscode/launch.json"
			continue
		}
	}

	[string]$local:optionNameString = $optionNameList -join ","
	[boolean]$local:validResult | Out-Null

	Read-Option -QuestionText "Pick an option, for '$selectionName', out of ($optionNameString)" -ValidateCommand {
		param([string]$answer)

		$local:choice = $optionNameList -match $answer

		switch($choice.Length) {
			0 { Write-Host 'Invalid answer received'; $false; $choice }
			1 { if($choice -ne '/none') {
					# Then we didn't return '/none', and have a match to an option
					# (and only one option)
					$copyFileTable["$nextKeyPrefix/$choice"] = $selectionName
				} else {
					Write-Host 'Option skipped'
				}
				$true; $choice
			}
			default { if($optionNameList -contains $choice) {
					# then we have an exact match, so use it
					$copyFileTable["$nextKeyPrefix/$choice"] = $selectionName
					$true; $choice
				} else {
					Write-Host 'Ambiguous answer received'
					$false; $choice
				}
			}
		}
	} | Out-Null
}

Write-Host ''

# make sure we have a path to write to
Confirm-Directory $copyPath

# copy all required directories to destination
foreach($local:srcFile in $copyDirTable.Keys) {
	$local:destFile = $copyDirTable[$srcFile]
	$local:destination = "$copyPath/$destFile"

	Write-Host "Copying directory ($templatesPath/$srcFile) to ($destination)"
	
	Copy-Item -Path "$templatesPath/$srcFile" -Destination "$destination" -Recurse
}
# copy all required files, and specified optional files, to destination
foreach($local:srcFile in $copyFileTable.Keys) {
	$local:destFile = $copyFileTable[$srcFile]
	$local:destination = "$copyPath/$destFile"

	Write-Host "Copying file ($templatesPath/$srcFile) to ($destination)"
	
	$local:destinationDirectory = Split-Path -Path "$destination"
	
	if(-not(Test-Path "$destinationDirectory")) {
		Write-Host "Writing dir: $destinationDirectory"
		New-Item -ItemType Directory -Force -Path "$destinationDirectory/"
	}

	if(Test-Path $destination) {
		Write-Host ''
		Write-Host "WARNING: ($destination) already exists! Replacing..."
		Write-Host ''
	} 

	Copy-Item -Path "$templatesPath/$srcFile" -Destination "$destination"
}

Write-Host ''

#iterate over all files in the copy directory, and replace any #NAME# strings
$local:files = Get-ChildItem $copyPath -File -Recurse -Force | Select-Object FullName |
	ForEach-Object {$_.FullName}
foreach($local:file in $files) {
	$local:content = Get-Content "$file"

	# Get a list of things to replace, in this file
	[regex]$local:matcherRegex = "#[A-Z0-9_]*#"
	$local:matches = $matcherRegex.Matches("$content")

	# Check whether the string is in our replacement table already
	foreach($local:match in $matches) {
		if(-not ($knownStringMap.ContainsKey("$match"))) {

			$local:symbolReplacement = Read-Host "Encountered symbol '$match'. With what should it be replaced"

			$knownStringMap["$match"] = "$symbolReplacement"
		}

		$content = $content -replace "$match", $knownStringMap["$match"]
	}

	#if a file ends in .replace, then we should remove the .replace, to get the real name
	if("$file" -match "\.replace") {
		$file | Rename-Item -NewName ($file = "$file" -replace "\.replace", "")
	}

	# If a file has a part of the path that says (literally) "^ProjName^", replace that with
	# the project name 
	if("$file" -match "#ProjName#") {
		$local:newFile = "$file" -replace "#ProjName#", "$projectName"
		$local:destinationDirectory = Split-Path -Path "$newFile"
		Write-Host "testing $destinationDirectory"
		
		if(-not(Test-Path "$destinationDirectory")) {
			New-Item -ItemType Directory -Force -Path "$destinationDirectory/"
		}
		
		$file | Move-Item -Destination "$newFile"
		$file = $newFile
	}

	Set-Content -Path $file -Value $content
}

Write-Host ''

# Inform the user of what is happening
Write-Host 'Version Information setup:'

#setup version header input file
$local:VersionTarget = "$copyPath/Includes/${projectName}_inc/Version/${projectName}Version.h.in"
$local:VersionSource = "$scriptRoot/../Includes/Nucleus_inc/Version/NucleusVersion.h.in"

$VersionSource = Resolve-Path -LiteralPath $VersionSource
Copy-Item -LiteralPath "$VersionSource" -Destination "$VersionTarget"

$local:major = Read-UInt16 -MaxValue 63 -QuestionText "Please enter major component of ${projectName} version:"
$local:minor = Read-UInt16 -MaxValue 1023 -QuestionText "Please enter minor component of ${projectName} version:"
$local:patch = Read-UInt16 -QuestionText "Please enter release patch component of ${projectName} version:"
$local:devBuild = Read-UInt16 -QuestionText "Please enter development build component of ${projectName} version:"

#Write version manifest with provided data
[string]$local:FullVersion = "$Major.$Minor.dv.$patch.$devBuild"
$local:VersionManifestTarget = "$copyPath/Code/Version/VersionManifest"
Set-Content -LiteralPath "$VersionManifestTarget" -Value "$FullVersion"
Write-Host "Complete version: $FullVersion"

Write-Host ''

# Remove empty subfolders, in case we moved any folders renaming them,
# or have any redundant folders
$copyPath | Get-ChildItem -Recurse |
	ForEach-Object { $_.FullName} |
	Sort-Object -Descending |
	Where-Object { -not(@(Get-ChildItem -Force $_)) } |
	Remove-Item

#setup git
Push-Location $copyPath
. git init --initial-branch=trunk
Pop-Location