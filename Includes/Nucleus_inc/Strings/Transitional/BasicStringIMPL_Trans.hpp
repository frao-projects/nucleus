#ifndef FRAO_NUCLEUS_STRINGS_STRING_IMPLEMENTATION_TRANSITIONAL
#define FRAO_NUCLEUS_STRINGS_STRING_IMPLEMENTATION_TRANSITIONAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the transitional implementation of unicode
//! string class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <algorithm>			   //for std::max
#include <array>				   //for std::array
#include <istream>				   //for istreams
#include <limits>				   //for numeric_limits
#include <new>					   //for std::bad_alloc
#include <ostream>				   //for ostreams
#include "../../Allocate.hpp"	   //for Allocate... and FreeSpace
#include "../../BitUtilities.hpp"  //for BitFloat
#include "../../Environment.hpp"
#include "../CodePoint.hpp"
#include "../GraphemeCluster.hpp"
#include "../Word.hpp"

namespace frao
{
inline namespace Strings
{
namespace Transitional
{
//! \brief Transitional implementation class, for strings
//!
//! \brief check double lexical casting works, for values not
//! representable in float, because I don't think it does
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \tparam NumericalPunctuation The struct that holds the type of
//! numerical punctuation to use, when converting to and from numbers.
template<typename CharType, typename NumericalPunctuation>
class NUCLEUS_LIB_API StringIMPL
{
	static_assert(std::is_integral_v<CharType>,
				  "Must use a character type!");

   public:
	//! \brief Type for referring to our own type, where appropriate
	using OurType =
		Transitional::StringIMPL<CharType, NumericalPunctuation>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = CodePoint<CharType, false>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = GraphemeCluster<CharType, false>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using WordType = Word<CharType, false>;

	//! \brief Used for unrepresentable values. For example, when a
	//! find functions fails to find
	static constexpr const natural64 npos =
		std::numeric_limits<natural64>::max();

   private:
	//! \brief The pointer to our actually stored string buffer
	//!
	//! \invariant Only null iff the capacity variable is 0
	CharType* m_StringBuffer;

	//! \brief The number of used meaningful CharType elements, in the
	//! buffer.
	//!
	//! \note Does not include null-terminator
	natural64 m_MeaningfulSize;

	//! \brief The number of CharType elements, that can be stored in
	//! the buffer
	//!
	//! \note The number of elements of CharType in the buffer. Does
	//! include space for the null-terminator
	//!
	//! \invariant Only 0 iff the buffer is null
	natural64 m_BufferSize;

	//! \brief Create a buffer, of the size requested.
	//!
	//! \note Will either succeed, or throw
	//!
	//! \note function exists entirely to make Allocation function
	//! throw
	//!
	//! \returns The vuffer, of the requested size
	//!
	//! \param[in] allocSizeReq The requested size, in elements of
	//! CharType
	//!
	//! \throws std::bad_alloc iff the allocation failed
	static CharType* allocateBuffer(natural64 allocSizeReq)
	{
		CharType* result =
			AllocateSpaceForArray<CharType>(allocSizeReq);

		if (result == nullptr)
		{
			throw std::bad_alloc();
		}

		return result;
	}

	//! \brief Copy one string into another buffer
	//!
	//! \warning Does not append a null-terminator to the writeStr.
	//! That must be done manually
	//!
	//! \param[in] readStr The input string, from which to get the
	//! string data
	//!
	//! \param[in] writeStr The string buffer, into which to write the
	//! specified string
	//!
	//! \param[in] numElemToCopy The number of elements of CharType to
	//! copy from the readStr
	static void copyString(const CharType* readStr,
						   CharType*	   writeStr,
						   natural64	   numElemToCopy)
	{
		runtime_assert(readStr != nullptr,
					   "Cannot read from a null string!");
		runtime_assert(writeStr != nullptr,
					   "Cannot write to a null string!");

		for (natural64 index = 0; index < numElemToCopy; ++index)
		{
			writeStr[index] = readStr[index];
		}
	}

	//! \brief Create a buffer, of the correct size to hold the
	//! specified input string, and fill it with the data from the
	//! input string
	//!
	//! \returns a pointer to the filled buffer
	//!
	//! \param[in] readStr The input string, from which to get the
	//! string data
	//!
	//! \param[in] readStrSize The size of the input String, in
	//! elements of the character type
	//!
	//! \param[out] resultSize The meaningfulsize of the content in
	//! the returned buffer. That is, the size of the content in the
	//! buffer, without the null-terminator
	//!
	//! \param[out] resultCapacity The total capacity of the buffer,
	//! including the space allocated for the null-terminator
	//!
	//! \throws std::bad_alloc iff the allocation failed
	static CharType* createBufferFor(const CharType* readStr,
									 natural64		 readStrSize,
									 natural64&		 resultSize,
									 natural64&		 resultCapacity)
	{
		natural64 capacity = readStrSize + 1;
		CharType* result   = allocateBuffer(capacity);

		copyString(readStr, result, readStrSize);

		result[readStrSize] = static_cast<CharType>('\0');
		resultSize			= readStrSize;
		resultCapacity		= capacity;

		return result;
	}

	//! \brief Ensure that the string buffer is at least the specified
	//! size
	//!
	//! \param[in] elementSize The number of meaningful elements, that
	//! need to be stored in the buffer
	//!
	//! \throws std::bad_alloc iff the allocation failed
	void reserveBufferFor(natural64 elementSize)
	{
		natural64 reqCapacity = elementSize + 1;

		if (reqCapacity > m_BufferSize)
		{
			// then we need to expand the buffer!
			CharType* newBuf = allocateBuffer(reqCapacity);

			if (m_MeaningfulSize != 0)
			{
				runtime_assert(m_MeaningfulSize < reqCapacity,
							   "Buffer was in invalid state!");

				copyString(m_StringBuffer, newBuf, m_MeaningfulSize);
				newBuf[m_MeaningfulSize] =
					static_cast<CharType>('\0');
			}

			FreeSpace(m_StringBuffer);
			m_StringBuffer = newBuf;
			m_BufferSize   = reqCapacity;
		}
	}
	//! \brief Will add the specified string to the end of the string
	//! buffer
	//!
	//! \param[in] inputStr The string that should be added to the
	//! string.
	//!
	//! \param[in] inputStrLen The length of the string passed, in
	//! number of elements, of the underlying character type
	//!
	//! \throws std::bad_alloc iff we attempted to allocate, and the
	//! allocation failed
	void growBufferEnd(const CharType* inputStr,
					   natural64	   inputStrLen)
	{
		if (inputStrLen == 0)
		{
			return;
		}

		// ensure we have enough room to store
		natural64 newSize = m_MeaningfulSize + inputStrLen;

		if (newSize >= m_BufferSize)
		{
			// make sure the buffer is big enough to the larger of:
			// 2*existing buffer, or the required size + null
			// terminator. We do this to ensure that we keep the
			// number allocations to a minimum. If we merely always
			// expand to fit the new data, then we'll end up
			// repeatedly allocating in slightly larger sizes (eg.
			// when calling push_back)
			reserveBufferFor(std::max(newSize + 1, 2 * m_BufferSize));
		}

		copyString(inputStr, m_StringBuffer + m_MeaningfulSize,
				   inputStrLen);
		m_StringBuffer[newSize] = static_cast<CharType>('\0');
		m_MeaningfulSize		= newSize;
	}

	//! \brief erase a section of this string
	//!
	//! \param[in] startIndex The index of the first element to erase
	//!
	//! \param[in] length The amount of elements to erase
	void eraseFromString(natural64 startIndex, natural64 length)
	{
		if ((startIndex > m_MeaningfulSize) || (length == 0))
		{
			// then we have a no-op
			return;
		}

		runtime_assert(
			m_StringBuffer != nullptr,
			"Should be impossible to have nullptr here, as the above "
			"check should have caught it, because m_MeaningfulSize "
			"should be 0 if we have a nullptr!");

		natural64 endIndex = startIndex + length;

		if (endIndex > m_MeaningfulSize)
		{
			endIndex = m_MeaningfulSize;
		}

		natural64 copyLength = m_MeaningfulSize - endIndex;
		if (copyLength != 0)
		{
			// we can just copy the data, to overwrite the old.
			copyString(m_StringBuffer + endIndex,
					   m_StringBuffer + startIndex, copyLength);
		}

		m_MeaningfulSize = startIndex + copyLength;

		// don't actually need to delete anything, just set the null
		// terminator to
		m_StringBuffer[m_MeaningfulSize] =
			static_cast<CharType>('\0');
	}

	//! \brief Get the length of a null-terminated string. That is,
	//! get the index of the null-terminator, which doubles as the
	//! meaningful size of the string
	//!
	//! \tparam OurCharType The underlying character type, of the
	//! provided string
	//!
	//! \returns The meaningful size of the string; the index of the
	//! null-terminator
	//!
	//! \param[in] string The null-terminated string to get the size
	//! of
	template<typename OurCharType>
	static natural64 getStringLength(const OurCharType* string)
	{
		runtime_assert(
			string != nullptr,
			"Cannot get the length, if there is no string!");

		natural64 stringSize = 0;
		while (string[stringSize] != static_cast<OurCharType>('\0'))
		{
			++stringSize;
		}

		return stringSize;
	}

	//! \brief create a substring, of the string provided
	//!
	//! \returns A substring of the string buffer provided
	//!
	//! \param[in] readBuffer A const pointer to the string to copy
	//! from
	//!
	//! \param[in] readBufSize The size of the provided string, to be
	//! read from
	//!
	//! \param[in] readStart The index of first position in the
	//! provides string, to copy from
	//!
	//! \param[in] readLength The length of the substring desired
	static OurType createSubString(const CharType* readBuffer,
								   natural64	   readBufSize,
								   natural64	   readStart,
								   natural64	   readLength)
	{
		runtime_assert(readBuffer != nullptr,
					   "Cannot read from a null pointer!");

		if (readStart >= readBufSize)
		{
			return OurType();
		}

		if ((readStart + readLength) > readBufSize)
		{
			// ensure we don't read over the edge of the input string
			readLength = readBufSize - readStart;
		}

		return OurType(readBuffer + readStart, readLength);
	}

   public:
	template<typename OtherType, typename OtherNumPunct>
	friend class StringIMPL;


	//! \brief create empty string
	StringIMPL()
		: m_StringBuffer(nullptr),
		  m_MeaningfulSize(0),
		  m_BufferSize(0)
	{}
	//! \brief create string from the specified
	//! (null-terminated) string
	//!
	//! \param[in] string pointer to const string buffer
	StringIMPL(const CharType* string) : StringIMPL()
	{
		if (string != nullptr)
		{
			// get the size of the string
			natural64 stringSize = getStringLength(string);

			m_StringBuffer = createBufferFor(
				string, stringSize, m_MeaningfulSize, m_BufferSize);
		}
	}

	//! \brief Construct a StringIMPL from an ascii cstring
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	StringIMPL(const charA* string) : StringIMPL()
	{
		// get the size of the string, and allocate a buffer
		m_MeaningfulSize = getStringLength(string);
		m_BufferSize	 = m_MeaningfulSize + 1;
		m_StringBuffer	 = allocateBuffer(m_BufferSize);

		for (natural64 index = 0; index < m_MeaningfulSize; ++index)
		{
			// convert to CharType, with codepoint
			CodePointType codepoint(string[index]);
			runtime_assert(codepoint.size() == 1,
						   "ASCII input should never be more than "
						   "one character per codepoint!");

			m_StringBuffer[index] = codepoint[0];
		}
		m_StringBuffer[m_MeaningfulSize] =
			static_cast<CharType>('\0');
	}

	//! \brief create string from a string buffer and
	//! specified size. Doesn't need to be null-terminated
	//!
	//! \param[in] string pointer to const string buffer
	//!
	//! \param[in] elemSize size of string buffer in
	//! 'string' parameter, in terms of number of elements of the
	//! underlying type
	explicit StringIMPL(const CharType* string,
						frao::natural64 elemSize)
		: StringIMPL()
	{
		m_StringBuffer = createBufferFor(
			string, elemSize, m_MeaningfulSize, m_BufferSize);
	}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_nat8 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat16 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat32 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to string
	explicit StringIMPL(frao::small_nat64 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int8 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int16 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int32 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to string
	explicit StringIMPL(frao::small_int64 number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to string
	explicit StringIMPL(float number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to string
	explicit StringIMPL(double number) : StringIMPL()
	{
		this->push_backIMPL(WordType(number));
	}

	//! \brief concatenate two strings together. lhs will be on the
	//! front, and rhs will be the back. creates new string containing
	//! concatenated string. parameters not modified
	//!
	//! \returns newly created string containing results of the
	//! concatenation
	//!
	//! \param[in] lhs string to be the front of the new string
	//!
	//! \param[in] rhs string to be concatenated to lhs, when creating
	//! the new string
	explicit StringIMPL(const OurType& lhs, const OurType& rhs)
		: StringIMPL()
	{
		// allocate space for the two strings, plus a null-terminator
		m_MeaningfulSize =
			lhs.m_MeaningfulSize + rhs.m_MeaningfulSize;
		m_BufferSize   = m_MeaningfulSize + 1;
		m_StringBuffer = allocateBuffer(m_BufferSize);

		copyString(lhs.m_StringBuffer, m_StringBuffer,
				   lhs.m_MeaningfulSize);
		copyString(rhs.m_StringBuffer,
				   m_StringBuffer + lhs.m_MeaningfulSize,
				   rhs.m_MeaningfulSize);
		m_StringBuffer[m_MeaningfulSize] =
			static_cast<CharType>('\0');
	}

	//! \brief Copy construct a string, from another string of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other string, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The string to copy from
	template<typename OtherType, typename OtherNumPunct>
	explicit StringIMPL(
		const StringIMPL<OtherType, OtherNumPunct>& rhs);

	//! \brief Copy construct a string, from another string of the
	//! same underlying type as this
	//!
	//! \param[in] rhs The string to copy from
	StringIMPL(const OurType& rhs) : StringIMPL()
	{
		if (rhs.m_MeaningfulSize != 0)
		{
			runtime_assert((rhs.m_BufferSize != 0)
							   && (rhs.m_StringBuffer != nullptr),
						   "Should not be able to have an empty "
						   "buffer, with non-zero meaningful size");

			m_StringBuffer = createBufferFor(
				rhs.m_StringBuffer, rhs.m_MeaningfulSize,
				m_MeaningfulSize, m_BufferSize);
		}
	}
	//! \brief Move construct a string, from another string of the
	//! same underlying type as this
	//!
	//! \param[in] rhs The string to move from
	StringIMPL(OurType&& rhs)
		: m_StringBuffer(rhs.m_StringBuffer),
		  m_MeaningfulSize(rhs.m_MeaningfulSize),
		  m_BufferSize(rhs.m_BufferSize)
	{
		rhs.m_StringBuffer	 = nullptr;
		rhs.m_MeaningfulSize = 0;
		rhs.m_BufferSize	 = 0;
	}
	//! \brief destroy the string, and call any derived destructors
	virtual ~StringIMPL() noexcept
	{
		FreeSpace(m_StringBuffer);
	}

	//! \brief Copy assign a string, from another string of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The string to copy from
	OurType& operator=(const OurType& rhs)
	{
		CharType* newBuf  = nullptr;
		natural64 newSize = 0, newCapacity = 0;

		if (rhs.m_MeaningfulSize != 0)
		{
			runtime_assert((rhs.m_BufferSize != 0)
							   && (rhs.m_StringBuffer != nullptr),
						   "Should not be able to have an empty "
						   "buffer, with non-zero meaningful size");

			newBuf = createBufferFor(rhs.m_StringBuffer,
									 rhs.m_MeaningfulSize, newSize,
									 newCapacity);
		}

		FreeSpace(m_StringBuffer);
		m_StringBuffer	 = newBuf;
		m_MeaningfulSize = newSize;
		m_BufferSize	 = newCapacity;

		return *this;
	}
	//! \brief Move assign a string, from another string of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The string to move from
	OurType& operator=(OurType&& rhs)
	{
		FreeSpace(m_StringBuffer);
		m_StringBuffer	 = rhs.m_StringBuffer;
		m_MeaningfulSize = rhs.m_MeaningfulSize;
		m_BufferSize	 = rhs.m_BufferSize;

		rhs.m_StringBuffer	 = nullptr;
		rhs.m_MeaningfulSize = 0;
		rhs.m_BufferSize	 = 0;

		return *this;
	}

	//! \brief pops (removes) the back of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the last code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_backIMPL() noexcept
	{
		if constexpr (std::is_same_v<ReturnType, WordType>)
		{
			// lastNonWhitespaceClusterPos is the last non-whitespace
			// cluster. whitespaceClusterPos is the cluster after
			// that, which holds the first whitespace after
			// lastNonWhitespaceClusterPos
			natural64 lastNonSpaceClusterPos = m_MeaningfulSize,
					  spaceClusterPos;

			// iterate backwards, so that we can find the LAST
			// non-whitespace codepoint
			for (natural64 index = 0; index < m_MeaningfulSize;
				 index			 = lastNonSpaceClusterPos)
			{
				spaceClusterPos = lastNonSpaceClusterPos;
				ClusterType nextCluster(
					m_StringBuffer, m_MeaningfulSize - index, true,
					&lastNonSpaceClusterPos);

				if ((nextCluster.size() == 0)
					|| (!nextCluster.isWhitespace()))
				{
					// then we've found the first non-whitespace
					// cluster (or we couldn't find a cluster)
					break;
				}
			}

			if (lastNonSpaceClusterPos >= m_MeaningfulSize)
			{
				// then we failed to find a word. return null word
				return WordType();
			}

			natural64 wordStart;
			WordType  result(m_StringBuffer, spaceClusterPos, true,
							 &wordStart);

			if (wordStart < m_MeaningfulSize)
			{
				natural64 wordElementLength =
					result.byteSize() / sizeof(CharType);

				if (wordElementLength)
				{
					eraseFromString(wordStart, wordElementLength);
				}
			}

			return result;
		} else
		{
			static_assert(
				std::is_same_v<
					ReturnType,
					CodePointType> || std::is_same_v<ReturnType, ClusterType>,
				"Can only pop codepoints, Grapheme Clusters, and "
				"words!");

			natural64 startLoc;

			ReturnType result(m_StringBuffer, m_MeaningfulSize, true,
							  &startLoc);

			// get the size of the popped section, in terms of
			// elements of CharType, and erase the popped section
			natural64 elementLength =
				result.byteSize() / sizeof(CharType);

			if (elementLength)
			{
				eraseFromString(startLoc, elementLength);
			}

			return result;
		}
	}

	//! \brief pops (removes) the front of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the first code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_frontIMPL() noexcept
	{
		if constexpr (std::is_same_v<ReturnType, WordType>)
		{
			// firstNonWhitespaceClusterPos is the first
			// non-whitespace cluster. clusterSize is the size of the
			// cluster
			natural64 firstNonSpaceClusterPos = m_MeaningfulSize,
					  readIndex				  = 0, clusterSize;

			for (; readIndex < m_MeaningfulSize;
				 readIndex += firstNonSpaceClusterPos + clusterSize)
			{
				ClusterType nextCluster(m_StringBuffer + readIndex,
										m_MeaningfulSize - readIndex,
										false,
										&firstNonSpaceClusterPos);

				if ((nextCluster.size() == 0)
					|| (!nextCluster.isWhitespace()))
				{
					// then we've found the first non-whitespace
					// cluster (or we couldn't find a cluster)
					break;
				}

				// this is the size, in terms of elements of CharType
				clusterSize =
					nextCluster.byteSize() / sizeof(CharType);
			}

			// because it was measured relative to the offset string
			// start
			firstNonSpaceClusterPos += readIndex;

			if (firstNonSpaceClusterPos >= m_MeaningfulSize)
			{
				// then we failed to find a word. return null word
				return WordType();
			}

			natural64 wordStart;
			WordType  result(
				 m_StringBuffer + firstNonSpaceClusterPos,
				 m_MeaningfulSize - firstNonSpaceClusterPos, false,
				 &wordStart);

			if ((wordStart + firstNonSpaceClusterPos)
				< m_MeaningfulSize)
			{
				natural64 wordElementLength =
					result.byteSize() / sizeof(CharType);

				if (wordElementLength)
				{
					eraseFromString(
						wordStart + firstNonSpaceClusterPos,
						wordElementLength);
				}
			}

			return result;
		} else
		{
			static_assert(
				std::is_same_v<
					ReturnType,
					CodePointType> || std::is_same_v<ReturnType, ClusterType>,
				"Can only pop codepoints, Grapheme Clusters, and "
				"words!");

			natural64 startLoc;

			ReturnType result(m_StringBuffer, m_MeaningfulSize, false,
							  &startLoc);

			// get the size of the popped section, in terms of
			// elements of CharType, and erase the popped section
			natural64 elementLength =
				result.byteSize() / sizeof(CharType);

			if (elementLength != 0)
			{
				eraseFromString(startLoc, elementLength);
			}

			return result;
		}
	}

	//! \brief adds a codepoint to the back of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_backIMPL(const CodePointType& codepoint) noexcept
	{
		if (codepoint.size() == 0)
		{
			return;
		}

		for (natural64 index = 0; index < codepoint.size(); ++index)
		{
			growBufferEnd(&codepoint[index], 1);
		}
	}
	//! \brief adds a cluster to the back of the string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_backIMPL(const ClusterType& cluster) noexcept
	{
		reserveBufferFor(m_MeaningfulSize
						 + (cluster.byteSize() / sizeof(CharType)));

		for (natural64 index = 0; index < cluster.size(); ++index)
		{
			push_backIMPL(cluster[index]);
		}
	}
	//! \brief adds a word to the back of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_backIMPL(const WordType& word) noexcept
	{
		reserveBufferFor(m_MeaningfulSize
						 + (word.byteSize() / sizeof(CharType)));

		for (frao::natural64 index = 0; index < word.size(); ++index)
		{
			push_backIMPL(word[index]);
		}
	}
	//! \brief adds a codepoint to the front of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_frontIMPL(const CodePointType& codepoint) noexcept
	{
		if (codepoint.size() == 0)
		{
			return;
		}

		// ensure we only have to allocate once
		OurType tempStr;
		tempStr.reserveIMPL(this->sizeIMPL() + codepoint.size());

		tempStr.push_backIMPL(codepoint);

		// then, add the existing content
		tempStr.appendIMPL(*this);
		*this = std::move(tempStr);
	}
	//! \brief adds a grapheme cluster to the front of the
	//! string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_frontIMPL(const ClusterType& cluster) noexcept
	{
		// ensure we only have to allocate once
		OurType tempStr;
		tempStr.reserveIMPL(
			this->sizeIMPL()
			+ (cluster.byteSize() / sizeof(CharType)));

		// first, add the new cluster
		tempStr.push_backIMPL(cluster);

		// then, add the existing content
		tempStr.appendIMPL(*this);
		*this = std::move(tempStr);
	}
	//! \brief adds a word to the front of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_frontIMPL(const WordType& word) noexcept
	{
		// ensure we only have to allocate once
		OurType tempStr;
		tempStr.reserveIMPL(this->sizeIMPL()
							+ (word.byteSize() / sizeof(CharType)));

		// first, add the new cluster
		tempStr.push_backIMPL(word);

		// then, add the existing content
		tempStr.appendIMPL(*this);
		*this = std::move(tempStr);
	}

	//! \brief add a code-point AT the specified index in
	//! the string, moving the EXISTING code-point at that
	//! location, and all subsequent code-points back
	//!
	//! \param[in] codepoint The code-point to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the
	//! code-point. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	//!
	//! \todo Make indexing in terms of codepoints? we're implicitly
	//! exposing the implementation, otherwise
	void insertIMPL(const CodePointType& codepoint,
					natural64			 index) noexcept
	{
		if (codepoint.size() == 0)
		{
			return;
		}

		// ensure we only have to allocate once
		OurType	  tempStr;
		natural64 codepointLength = codepoint.size();
		tempStr.reserveIMPL(sizeIMPL() + codepointLength);

		if (index > m_MeaningfulSize)
		{
			index = m_MeaningfulSize;
		}

		// copy things before the insert
		if (index != 0)
		{
			copyString(m_StringBuffer, tempStr.m_StringBuffer, index);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_StringBuffer[index] =
				static_cast<CharType>('\0');
			tempStr.m_MeaningfulSize = index;
		}

		// insert the codepoint
		tempStr.push_backIMPL(codepoint);
		runtime_assert(
			tempStr.sizeIMPL() == (index + codepointLength),
			"We added a strange amount, during push_backIMPL()!");

		// copy the rest of the string
		if (index < m_MeaningfulSize)
		{
			// then we have more to add
			natural64 remainingLength = m_MeaningfulSize - index;
			runtime_assert(
				(tempStr.m_MeaningfulSize + remainingLength)
					< tempStr.m_BufferSize,
				"Buffer overflow!");

			copyString(
				m_StringBuffer + index,
				tempStr.m_StringBuffer + tempStr.m_MeaningfulSize,
				remainingLength);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_MeaningfulSize += remainingLength;
			tempStr.m_StringBuffer[tempStr.m_MeaningfulSize] =
				static_cast<CharType>('\0');
		}

		*this = std::move(tempStr);
	}
	//! \brief add a grapheme cluster AT the specified index
	//! in the string, moving the EXISTING grapheme cluster
	//! at that location, and all subsequent grapheme
	//! clusters back
	//!
	//! \param[in] cluster The grapheme cluster to add at
	//! the specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the grapheme
	//! cluster. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	void insertIMPL(const ClusterType& cluster,
					natural64		   index) noexcept
	{
		if (cluster.size() == 0)
		{
			return;
		}

		// ensure we only have to allocate once
		OurType	  tempStr;
		natural64 clusterLength =
			(cluster.byteSize() / sizeof(CharType));
		tempStr.reserveIMPL(sizeIMPL() + clusterLength);

		if (index > m_MeaningfulSize)
		{
			index = m_MeaningfulSize;
		}

		// copy things before the insert
		if (index != 0)
		{
			copyString(m_StringBuffer, tempStr.m_StringBuffer, index);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_StringBuffer[index] =
				static_cast<CharType>('\0');
			tempStr.m_MeaningfulSize = index;
		}

		// insert the cluster
		tempStr.push_backIMPL(cluster);
		runtime_assert(
			tempStr.sizeIMPL() == (index + clusterLength),
			"We added a strange amount, during push_backIMPL()!");

		// copy the rest of the string
		if (index < m_MeaningfulSize)
		{
			// then we have more to add
			natural64 remainingLength = m_MeaningfulSize - index;
			runtime_assert(
				(tempStr.m_MeaningfulSize + remainingLength)
					< tempStr.m_BufferSize,
				"Buffer overflow!");

			copyString(
				m_StringBuffer + index,
				tempStr.m_StringBuffer + tempStr.m_MeaningfulSize,
				remainingLength);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_MeaningfulSize += remainingLength;
			tempStr.m_StringBuffer[tempStr.m_MeaningfulSize] =
				static_cast<CharType>('\0');
		}

		*this = std::move(tempStr);
	}
	//! \brief add a word AT the specified index in the
	//! string, moving the EXISTING word at that location,
	//! and all subsequent words back
	//!
	//! \param[in] word The word to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the word. Any
	//! existing word at the location, and subsequent
	//! locations, will be moved back
	void insertIMPL(const WordType& word, natural64 index) noexcept
	{
		if (word.size() == 0)
		{
			return;
		}

		// ensure we only have to allocate once
		OurType	  tempStr;
		natural64 wordLength = (word.byteSize() / sizeof(CharType));
		tempStr.reserveIMPL(sizeIMPL() + wordLength);

		if (index > m_MeaningfulSize)
		{
			index = m_MeaningfulSize;
		}

		// copy things before the insert
		if (index != 0)
		{
			copyString(m_StringBuffer, tempStr.m_StringBuffer, index);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_StringBuffer[index] =
				static_cast<CharType>('\0');
			tempStr.m_MeaningfulSize = index;
		}

		// insert the word
		tempStr.push_backIMPL(word);
		runtime_assert(
			tempStr.sizeIMPL() == (index + wordLength),
			"We added a strange amount, during push_backIMPL()!");

		// copy the rest of the string
		if (index < m_MeaningfulSize)
		{
			// then we have more to add
			natural64 remainingLength = m_MeaningfulSize - index;
			runtime_assert(
				(tempStr.m_MeaningfulSize + remainingLength)
					< tempStr.m_BufferSize,
				"Buffer overflow!");

			copyString(
				m_StringBuffer + index,
				tempStr.m_StringBuffer + tempStr.m_MeaningfulSize,
				remainingLength);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_MeaningfulSize += remainingLength;
			tempStr.m_StringBuffer[tempStr.m_MeaningfulSize] =
				static_cast<CharType>('\0');
		}

		*this = std::move(tempStr);
	}
	//! \brief add a string into this one AT the specified
	//! index in the string, moving the EXISTING
	//! code-point(s) at that location, and all subsequent
	//! locations, back
	//!
	//! \param[in] string The string to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the (start of
	//! the) string. Any existing code-point(s) at the
	//! location, and subsequent locations, will be moved
	//! back
	void insertIMPL(const OurType& string, natural64 index) noexcept
	{
		if (string.sizeIMPL() == 0)
		{
			return;
		}

		// ensure we only have to allocate once
		OurType	  tempStr;
		natural64 rhsStringLength = string.sizeIMPL();
		tempStr.reserveIMPL(sizeIMPL() + rhsStringLength);

		if (index != 0)
		{
			copyString(m_StringBuffer, tempStr.m_StringBuffer, index);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_StringBuffer[index] =
				static_cast<CharType>('\0');
			tempStr.m_MeaningfulSize = index;
		}

		tempStr.appendIMPL(string);

		if (index < m_MeaningfulSize)
		{
			// then we have more to add
			natural64 remainingLength = m_MeaningfulSize - index;
			runtime_assert(
				(tempStr.m_MeaningfulSize + remainingLength)
					< tempStr.m_BufferSize,
				"Buffer overflow!");

			copyString(
				m_StringBuffer + index,
				tempStr.m_StringBuffer + tempStr.m_MeaningfulSize,
				remainingLength);

			// make sure that tempStr is in a valid state, before we
			// call one of its functions
			tempStr.m_MeaningfulSize += remainingLength;
			tempStr.m_StringBuffer[tempStr.m_MeaningfulSize] =
				static_cast<CharType>('\0');
		}

		*this = std::move(tempStr);
	}

	//! \brief add a string onto the back of this one
	//!
	//! \param[in] string String to add on the back of this
	//! one
	void appendIMPL(const OurType& string)
	{
		growBufferEnd(string.m_StringBuffer, string.m_MeaningfulSize);
	}

	//! \brief remove a certain number of characters,
	//! starting at the specified location
	//!
	//! \param[in] index location to start the removal of
	//! characters
	//!
	//! \param[in] length nunber of characters to remove
	void eraseIMPL(frao::natural64 index, frao::natural64 length)
	{
		if (index + length > sizeIMPL()) length = npos;

		eraseFromString(index, length);
	}
	//! \brief erase entire content of string. String will
	//! be left empty
	void clearIMPL()
	{
		m_MeaningfulSize = 0;

		if (m_StringBuffer != nullptr)
		{
			m_StringBuffer[0] = static_cast<CharType>('\0');
			m_MeaningfulSize  = 0;
		}
	}

	//! \brief create a substring, from this string
	//!
	//! \returns A string, holding some subset of the data contained
	//! in this
	//!
	//! \param[in] startIndex The character index, in thsi string,
	//! which represents the start of the substring
	//!
	//! \param[in] length The length of the substring to be created
	OurType subStrIMPL(frao::natural64 startIndex,
					   frao::natural64 length) const
	{
		// make sure that if we'd go off the end, that we just ask for
		// everything else
		if (startIndex + length > m_MeaningfulSize) length = npos;

		// do the actual sub-string-ing, and create a StringIMPL with
		// it
		return createSubString(m_StringBuffer, m_MeaningfulSize,
							   startIndex, length);
	}

	//! \brief size of string, in terms of grapheme clusters
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of grapheme clusters in the string
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_MeaningfulSize;
	}
	//! \brief size of string, in terms of bytes
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of bytes in the string
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return m_MeaningfulSize * sizeof(CharType);
	}

	//! \brief make sure the string has sufficient memory
	//! allocated to allow future expansion to specified
	//! number of characters
	//!
	//! \param[in] characters Number of characters that string should
	//! be able to hold (ie: make sure the buffer is at
	//! least this number of characters long)
	void reserveIMPL(frao::natural64 characters)
	{
		reserveBufferFor(characters);
	}
	//! \brief gets a const pointer to the data
	//!
	//! \note null-terminated c-string returned
	//!
	//! \returns a const pointer to the data. Const in the
	//! sense of the data being immutable through the
	//! pointer. Null-terminated
	const CharType* dataIMPL() const noexcept
	{
		return m_StringBuffer;
	}

	//! \brief indicates whether the string is empty
	//!
	//! \returns a boolean indicating whether the string in
	//! empty. returns true iff the string is empty
	bool isEmptyIMPL() const noexcept
	{
		return m_MeaningfulSize == 0;
	}

	//! \brief indexes Grapheme Clusters in this string
	//!
	//! \returns Grapheme Cluster at the specified index
	//!
	//! \param[in] index of the Grapheme Cluster that should
	//! be returned
	ClusterType getIMPL(frao::natural64 index) const noexcept
	{
		runtime_assert(index < m_MeaningfulSize);

		// get the next cluster, after that index, technically. (this
		// is a downside of us doing character-level indexing)
		return ClusterType(m_StringBuffer + index,
						   m_MeaningfulSize - index);
	}

	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts. If there is
	//! no next instance,
	//!
	//! \param[in] string the sub-string to find the next
	//! instance of
	//!
	//! \param[in] strSize The size of the passed sub-string
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const CharType* string, frao::natural64 strSize,
		frao::natural64 startCharacter) const noexcept
	{
		runtime_assert(string != nullptr);

		// check strings for equality
		for (frao::natural64 resultindex = startCharacter;
			 resultindex < m_MeaningfulSize; ++resultindex)
		{
			frao::natural64 searchIndex = 0;
			while (m_StringBuffer[resultindex + searchIndex]
				   == string[searchIndex])
			{
				if ((searchIndex + 1) < strSize)
				{
					++searchIndex;
				} else
				{
					return resultindex;
				}
			}
		}

		return npos;
	}
	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts
	//!
	//! \param[in] string the (null-terminated) sub-string
	//! to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const CharType* string,
		frao::natural64 startCharacter) const noexcept
	{
		runtime_assert(string != nullptr);

		// find the size of the passed string
		frao::natural64 argumentStringSize = 0;
		while (+(string[argumentStringSize]) != 0)
		{
			++argumentStringSize;
		}

		return findNextIMPL(string, argumentStringSize,
							startCharacter);
	}
	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next
	//! instance of
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const OurType&	string,
		frao::natural64 startCharacter) const noexcept
	{
		runtime_assert(string.sizeIMPL() != 0);

		return findNextIMPL(string.m_StringBuffer,
							string.m_MeaningfulSize, startCharacter);
	}
	//! \brief find the next instance of the passed codepoint, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const CodePointType& codepoint,
		frao::natural64		 startCharacter) const noexcept
	{
		OurType tempStr;
		tempStr.push_backIMPL(codepoint);

		return this->findNextIMPL(tempStr, startCharacter);
	}
	//! \brief find the next instance of the passed cluster, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const ClusterType& cluster,
		frao::natural64	   startCharacter) const noexcept
	{
		OurType tempStr;
		tempStr.push_backIMPL(cluster);

		return this->findNextIMPL(tempStr, startCharacter);
	}
	//! \brief find the next instance of the passed word, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNextIMPL(
		const WordType& word,
		frao::natural64 startCharacter) const noexcept
	{
		OurType tempStr;
		tempStr.push_backIMPL(word);

		return this->findNextIMPL(tempStr, startCharacter);
	}

	//! \brief converts this string to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[out] number reference to where the result should be
	//! held
	template<typename NumberType>
	bool toNumberIMPL(NumberType& number) const noexcept
	{
		WordType word(m_StringBuffer, m_MeaningfulSize);

		return word.toNumber(number);
	}
	//! \brief Convert string to a 'case folded' version, that is
	//! suitable for comparison of semantically equal strings. ie: in
	//! english this is like converting to lowercase, so that upper
	//! and lower case letters, in the original, are identical
	//!
	//! \returns a case folded copy of this
	OurType toCaseFoldedIMPL() const noexcept
	{
		// copy this
		OurType result = *this;

		// find capitals, and make lowercase
		for (frao::natural64 index = 0; index < m_MeaningfulSize;
			 ++index)
		{
			CharType character = m_StringBuffer[index];
			if ((+character >= 0x61) && (+character <= 0x7A))
				character -= 32;

			result.m_StringBuffer[index] = character;
		}

		return result;
	}

	//! \brief Will determine the ordering of this, against a string,
	//! of any valid underlying type
	//!
	//! \tparam OtherType The type of the other string, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \details Compares the strings, with this being on the left
	//! hand side, and the other strings on the right. Will return < 0
	//! for *this < other, will return > 0 for *this > other, and will
	//! return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than, and >
	//! 0 meaning greater than
	//!
	//! \param[in] otherString The string to compare this with, on the
	//! rhs.
	template<typename OtherType, typename OtherNumPunct>
	frao::integer64 getOrderIMPL(
		const StringIMPL<OtherType, OtherNumPunct>& otherString)
		const noexcept
	{
		if (m_StringBuffer == otherString.m_StringBuffer)
		{
			return 0;
		} else
		{
			// check relative sizes of strings
			frao::integer64 diff =
				static_cast<frao::integer64>(m_MeaningfulSize)
				- static_cast<frao::integer64>(
					otherString.m_MeaningfulSize);
			frao::natural64 maxcharacter =
				(diff < 0) ? m_MeaningfulSize
						   : otherString.m_MeaningfulSize;

			for (frao::natural64 charindex = 0;
				 charindex < maxcharacter; ++charindex)
			{
				// we can just cast everything to a larger type.
				// Specifically, a 64-bit int, which can hold any of
				// the otehr possible values, and then just take the
				// difference, to find the order
				integer64 thisChar = static_cast<integer64>(
							  m_StringBuffer[charindex]),
						  otherChar = static_cast<integer64>(
							  otherString.m_StringBuffer[charindex]),
						  charDiff = thisChar - otherChar;

				if (charDiff != 0)
				{
					return charDiff;
				}
			}

			// since all chars are equal for compariable
			// portion, it follows that order is just the size order
			return diff;
		}
	}

	//! \brief Put the specified string into an ostream
	//!
	//! \returns The ostream, now containing the string
	//!
	//! \param[in] oStream The ostream that the string should be
	//! inserted into
	//!
	//! \param[in] string The string, that should be inserted into the
	//! oStream
	friend std::basic_ostream<CharType, std::char_traits<CharType>>&
	operator<<(std::basic_ostream<
				   CharType, std::char_traits<CharType>>& oStream,
			   const OurType&							  string)
	{
		return (oStream << string.m_StringBuffer);
	}
	//! \brief extract a string, from an istream
	//!
	//! \returns The istream, after the extraction
	//!
	//! \param[in] iStream The istream that the string should be
	//! extracted from
	//!
	//! \param[out] string The string, that should be extracted from
	//! the oStream
	friend std::basic_istream<CharType, std::char_traits<CharType>>&
	operator>>(std::basic_istream<
				   CharType, std::char_traits<CharType>>& iStream,
			   OurType&									  string)
	{
		constexpr static const frao::natural64 count = 80;
		static_assert(count > 0, "Cannot have a buffer size of 0!");

		CharType buffer[count];
		auto streamDelim = NumericalPunctuation::getStreamDelimiter();

		bool continueLoop = true;

		do
		{
			iStream.getline(&buffer[0], count, streamDelim);
			string.appendIMPL(OurType(&buffer[0]));

			switch (iStream.rdstate())
			{
				case std::ios_base::failbit:
					iStream.clear();  // clear flags
					continue;

				case std::ios_base::goodbit:
					[[fallthrough]];
				case std::ios_base::badbit:
					[[fallthrough]];
				case std::ios_base::eofbit:

					continueLoop = false;
					break;

				default:
					runtime_assert(
						false);	 // should not be called, ever
					continueLoop = false;
			}
		} while (continueLoop);

		return iStream;
	}
};	// namespace Transitional

template<typename CharType, typename NumericalPunctuation>
template<typename OtherType, typename OtherNumPunct>
StringIMPL<CharType, NumericalPunctuation>::StringIMPL(
	const StringIMPL<OtherType, OtherNumPunct>& rhs)
	: StringIMPL()
{
	// this is an approximation, for needed size of the string. Is
	// probably wrong, unless we're actually dealing with ascii
	reserveBufferFor(rhs.m_MeaningfulSize);

	for (natural64 index = 0; index < rhs.sizeIMPL();)
	{
		natural64					codepointStart;
		CodePoint<OtherType, false> nextCodepoint(
			rhs.dataIMPL() + index, rhs.sizeIMPL() - index, false,
			&codepointStart);

		CodePointType converted(nextCodepoint);

		for (natural64 cpindex = 0; cpindex < converted.size();
			 ++cpindex)
		{
			growBufferEnd(&converted[cpindex], 1);
		}
		natural64 increment = codepointStart + nextCodepoint.size();

		if (increment == 0)
		{
			increment = 1;
		}

		index += increment;
	}
}
}  // namespace Transitional
}  // namespace Strings
}  // namespace frao

#endif