//! \file
//!
//! \brief Transitional implmentation of Unicode Codepoint classes
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1

#include <cuchar>	 //for conversion to/from char16_t and char32_t
#include <cwchar>	 //for conversions to/from wide chars
#include <limits>	 //for std::numeric_limits
#include <locale>	 //for locales, and checking for whitespace etc
#include <optional>	 //for storing the locale, before it is constructed
#include "Strings/StringDefines.hpp"  //for checking what type of wchar we have
#include "Strings/Transitional/CodePointIMPL_Trans.hpp"

#if defined(NUCLEUS_HEADER_ONLY)
template class frao::Strings::Transitional::CodePointIMPL<
	frao::charU8>;
template class frao::Strings::Transitional::CodePointIMPL<wchar_t>;
template class frao::Strings::Transitional::CodePointIMPL<char16_t>;
template class frao::Strings::Transitional::CodePointIMPL<char32_t>;
#endif

namespace frao
{
inline namespace Strings
{
namespace Transitional::IMPL
{
//! \brief The locale we use when checking a character is whitespace,
//! newline etc
static std::optional<std::locale> g_OurLocale;

bool setOurLocale(const charA* localeName) noexcept
{
	try
	{
		g_OurLocale = std::locale(localeName);
		runtime_assert(g_OurLocale.has_value(),
					   "Locale was not successfully constructed!");

		std::locale::global(*g_OurLocale);

		return true;
	} catch (const std::runtime_error&)
	{
		return false;
	}
}
std::locale& getOurLocale() noexcept
{
	if (!g_OurLocale.has_value())
	{
		g_OurLocale = std::locale();
		runtime_assert(g_OurLocale.has_value(),
					   "Locale was not successfully constructed!");
	}

	return *g_OurLocale;
}
const std::string getOurLocaleName() noexcept
{
	return getOurLocale().name();
}

bool convToWCHAR(const charU8* utf8Buffer, natural64 bufferLength,
				 wchar_t& output) noexcept
{
	std::mbstate_t state = std::mbstate_t{};
	wchar_t		   wideCharacter;

	size_t returnCode = std::mbrtowc(
		&wideCharacter, reinterpret_cast<const char*>(utf8Buffer),
		bufferLength, &state);

	switch (returnCode)
	{
		//-2 and -1, converted to size_t, are the failure cases of
		// mbrtowc(). They denote and incomplete or invalid input,
		// respectively
		case static_cast<size_t>(-2):
			[[fallthrough]];
		case static_cast<size_t>(-1):
			return false;
		case 0:
			wideCharacter = L'\0';
			[[fallthrough]];
		default:
			output = wideCharacter;
			return true;
	}
}
bool convToWCHAR(char16_t character, wchar_t& output) noexcept
{
	if constexpr (g_WChar_Large)
	{
		// then wchar is 4 bytes, so we need to convert char16 => char
		// => wchar
		charU8 charBuffer[MB_LEN_MAX];

		std::mbstate_t state = std::mbstate_t{};

		size_t returnCode = std::c16rtomb(
			reinterpret_cast<char*>(charBuffer), character, &state);

		switch (returnCode)
		{
			//-1, converted to size_t, is the failure cases of
			// c16rtomb()
			case static_cast<size_t>(-1):
				return false;
			case 0:
				output = L'\0';
				return true;
			default:
				return convToWCHAR(charBuffer, returnCode, output);
		}
	} else
	{
		// then wchar is 2 bytes, the same as char16_t. We can
		// therefore use the same bit pattern
		output = *reinterpret_cast<wchar_t*>(&character);
		return true;
	}
}
bool convToWCHAR(char32_t character, wchar_t& output) noexcept
{
	if constexpr (g_WChar_Large)
	{
		// then wchar is 4 bytes, the same as char32_t. We can
		// therefore use the same bit pattern
		output = *reinterpret_cast<wchar_t*>(&character);
		return true;
	} else
	{
		// then wchar is 2 bytes, so we need to convert char32 => char
		// => wchar
		charU8 charBuffer[MB_LEN_MAX];

		std::mbstate_t state = std::mbstate_t{};

		size_t returnCode = std::c32rtomb(
			reinterpret_cast<char*>(charBuffer), character, &state);

		switch (returnCode)
		{
			//-1, converted to size_t, is the failure cases of
			// c16rtomb()
			case static_cast<size_t>(-1):
				return false;
			case 0:
				output = L'\0';
				return true;
			default:
				return convToWCHAR(charBuffer, returnCode, output);
		}
	}
}

bool convToUTF(const charU8* utf8Buffer, natural64 bufferLength,
			   char16_t& output) noexcept
{
	std::mbstate_t state = std::mbstate_t{};
	char16_t	   c16char;

	size_t returnCode = std::mbrtoc16(
		&c16char, reinterpret_cast<const char*>(utf8Buffer),
		bufferLength, &state);

	switch (returnCode)
	{
		// then we have a surrogate pair. Our transitional
		// implementation doesn't handle surrogate pairs, so return
		// false
		case static_cast<size_t>(-3):
			[[fallthrough]];
		//-2 and -1, converted to size_t, are the failure cases of
		// mbrtowc(). They denote and incomplete or invalid input,
		// respectively
		case static_cast<size_t>(-2):
			[[fallthrough]];
		case static_cast<size_t>(-1):
			return false;
		case 0:
			c16char = u'\0';
			[[fallthrough]];
		default:
			output = c16char;
			return true;
	}
}
bool convToUTF(wchar_t character, char16_t& output) noexcept
{
	if constexpr (!g_WChar_Large)
	{
		output = static_cast<char16_t>(character);
		return true;
	} else
	{
		return convToUTF(static_cast<char32_t>(character), output);
	}
}
bool convToUTF(char32_t character, char16_t& output) noexcept
{
	charU8	  utf8Buffer[MB_LEN_MAX];
	natural64 bufferLength;

	if (!convToUTF8(character, utf8Buffer, bufferLength))
	{
		return false;
	}

	return convToUTF(utf8Buffer, bufferLength, output);
}
bool convToUTF(const charU8* utf8Buffer, natural64 bufferLength,
			   char32_t& output) noexcept
{
	std::mbstate_t state = std::mbstate_t{};
	char32_t	   c32char;

	size_t returnCode = std::mbrtoc32(
		&c32char, reinterpret_cast<const char*>(utf8Buffer),
		bufferLength, &state);

	switch (returnCode)
	{
		// then we have a multi-character utf32 sequence. This isn't
		// possible, but the mbrtoc32 documentation indicates
		// returning -3 is what would happen in such a case, so we
		// have it here for completeness.
		case static_cast<size_t>(-3):
			[[fallthrough]];
		//-2 and -1, converted to size_t, are the failure cases of
		// mbrtowc(). They denote and incomplete or invalid input,
		// respectively
		case static_cast<size_t>(-2):
			[[fallthrough]];
		case static_cast<size_t>(-1):
			return false;
		case 0:
			c32char = U'\0';
			[[fallthrough]];
		default:
			output = c32char;
			return true;
	}
}
bool convToUTF(wchar_t character, char32_t& output) noexcept
{
	if constexpr (g_WChar_Large)
	{
		output = static_cast<char32_t>(character);
		return true;
	} else
	{
		return convToUTF(static_cast<char16_t>(character), output);
	}
}
bool convToUTF(char16_t character, char32_t& output) noexcept
{
	charU8	  utf8Buffer[MB_LEN_MAX];
	natural64 bufferLength;

	if (!convToUTF8(character, utf8Buffer, bufferLength))
	{
		return false;
	}

	return convToUTF(utf8Buffer, bufferLength, output);
}


bool lengthNextUTF8(const charU8* inputData, natural64 inputSize,
					natural64& calculatedSize) noexcept
{
	std::mbstate_t state = std::mbstate_t{};

	size_t returnCode = std::mbrlen(
		reinterpret_cast<const char*>(inputData), inputSize, &state);

	switch (returnCode)
	{
		//-2 and -1, converted to size_t, are the failure cases of
		// mbrlen(). They denote and incomplete or invalid input,
		// respectively
		case static_cast<size_t>(-2):
			[[fallthrough]];
		case static_cast<size_t>(-1):
			calculatedSize = 0;
			return false;
		// although returned 0 is in some ways a special case, it's
		// actually perfectly well served by the below default case
		default:
			runtime_assert(returnCode <= MB_LEN_MAX);
			calculatedSize = returnCode;
			return true;
	}
}
bool acquireNextUTF8(const charU8* inputData, natural64 inputSize,
					 charU8* outputBuffer, natural64& startPosition,
					 natural64& writtenBytes) noexcept
{
	natural64 index = 0, runningSize = 0;

	// loop, until we find a valid codepoint, or end of string
	for (; index < inputSize; ++index)
	{
		if (lengthNextUTF8(inputData + index, inputSize - index,
						   runningSize))
		{
			break;
		}
	}

	if ((index >= inputSize) || (runningSize == 0))
	{
		// then we failed to find a valid codepoint. Either because we
		// reach end of buffer, or because we reached a null codepoint
		return false;
	}

	// iterate over the found codepoint, and write to where required
	for (natural64 copyIndex = 0; copyIndex < runningSize;
		 ++copyIndex)
	{
		outputBuffer[copyIndex] = inputData[index + copyIndex];
	}

	writtenBytes  = runningSize;
	startPosition = index;
	return true;
}
bool acquireLastUTF8(const charU8* inputData, natural64 inputSize,
					 charU8* outputBuffer, natural64& startPosition,
					 natural64& writtenBytes) noexcept
{
	// get the easliest possible position, of the start of the final
	// codepoint
	const natural64 readStart =
		(inputSize > MB_LEN_MAX) ? (inputSize - MB_LEN_MAX) - 1 : 0;
	natural64 latestStart  = static_cast<natural64>(-1),
			  latestSize   = static_cast<natural64>(-1),
			  runningStart = readStart, runningSize;

	// then walk the codepoints, until we reach the last valid one
	while (runningStart < inputSize)
	{
		if (lengthNextUTF8(inputData + runningStart,
						   inputSize - runningStart, runningSize))
		{
			if (runningSize == 0)
			{
				// then we reached the null character. This must be
				// the end of the string, so stop looping
				break;
			}

			// we have found a valid codepoint, so note its location
			// and size, and move on past it
			latestStart = runningStart;
			latestSize	= runningSize;
			runningStart += runningSize;
		} else
		{
			// then we failed to get the length of the next codepoint.
			// So it's probably invalid. check further, and see if we
			// can find another
			++runningStart;
		}
	}

	if ((latestStart & latestSize) == static_cast<natural64>(-1))
	{
		// Because we cannot have both max size and max start
		// position, regardless of length of string, this is only
		// possible if we've failed to find a valid codepoint.
		return false;
	}

	// record what we found
	for (natural64 index = 0; index < latestSize; ++index)
	{
		outputBuffer[index] = inputData[latestStart + index];
	}
	writtenBytes  = latestSize;
	startPosition = latestStart;

	return true;
}
integer64 orderCodepointsUTF8Only(const charU8* lhsBuffer,
								  natural64		lhsLength,
								  const charU8* rhsBuffer,
								  natural64		rhsLength) noexcept
{
	constexpr const natural64 maxInt =
		std::numeric_limits<integer64>::max();

	runtime_assert((lhsLength <= maxInt) && (rhsLength <= maxInt),
				   "We cannot have a length longer than what is "
				   "representable in a 64-bit integer");
	runtime_assert((lhsBuffer != nullptr) && (rhsBuffer != nullptr),
				   "");

	integer64 relative = static_cast<integer64>(lhsLength)
						 - static_cast<integer64>(rhsLength);

	if (relative != 0)
	{
		return relative;
	}

	for (natural64 index = 0; index < lhsLength; ++index)
	{
		integer64 diff = static_cast<integer64>(lhsBuffer[index])
						 - static_cast<integer64>(rhsBuffer[index]);

		if (diff != 0)
		{
			return diff;
		}
	}

	// we can't find a difference, so return equality
	return 0;
}

bool isWhitespace_WCHAR(wchar_t character) noexcept
{
	return std::isspace(character, getOurLocale());
}
bool isWhitespace_UTF8(const charU8* utf8Buffer,
					   natural64	 bufferLength) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(utf8Buffer, bufferLength, wideCharacter))
	{
		return isWhitespace_WCHAR(wideCharacter);
	}
	return false;
}
bool isWhitespace_UTF16(char16_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isWhitespace_WCHAR(wideCharacter);
	}
	return false;
}
bool isWhitespace_UTF32(char32_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isWhitespace_WCHAR(wideCharacter);
	}
	return false;
}

bool isBlank_WCHAR(wchar_t character) noexcept
{
	return std::isblank(character, getOurLocale());
}
bool isBlank_UTF8(const charU8* utf8Buffer,
				  natural64		bufferLength) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(utf8Buffer, bufferLength, wideCharacter))
	{
		return isBlank_WCHAR(wideCharacter);
	}
	return false;
}
bool isBlank_UTF16(char16_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isBlank_WCHAR(wideCharacter);
	}
	return false;
}
bool isBlank_UTF32(char32_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isBlank_WCHAR(wideCharacter);
	}
	return false;
}

bool isNewline_WCHAR(wchar_t character) noexcept
{
	return isWhitespace_WCHAR(character)
		   && (!isBlank_WCHAR(character));
}
bool isNewline_UTF8(const charU8* utf8Buffer,
					natural64	  bufferLength) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(utf8Buffer, bufferLength, wideCharacter))
	{
		return isNewline_WCHAR(wideCharacter);
	}
	return false;
}
bool isNewline_UTF16(char16_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isNewline_WCHAR(wideCharacter);
	}
	return false;
}
bool isNewline_UTF32(char32_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isNewline_WCHAR(wideCharacter);
	}
	return false;
}

bool isNumeric_WCHAR(wchar_t character) noexcept
{
	return std::isdigit(character, getOurLocale());
}
bool isNumeric_UTF8(const charU8* utf8Buffer,
					natural64	  bufferLength) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(utf8Buffer, bufferLength, wideCharacter))
	{
		return isNumeric_WCHAR(wideCharacter);
	}
	return false;
}
bool isNumeric_UTF16(char16_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isNumeric_WCHAR(wideCharacter);
	}
	return false;
}
bool isNumeric_UTF32(char32_t character) noexcept
{
	wchar_t wideCharacter;

	if (convToWCHAR(character, wideCharacter))
	{
		return isNumeric_WCHAR(wideCharacter);
	}
	return false;
}
}  // namespace Transitional::IMPL
}  // namespace Strings
}  // namespace frao