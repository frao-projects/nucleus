#ifndef FRAO_BASIC_API_STRINGS_BASIC
#define FRAO_BASIC_API_STRINGS_BASIC

//! \file
//!
//! \brief Header of the Interface of unicode string
//! class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <type_traits>	//for std::conditional
#include "Experimental/BasicStringIMPL_Expr.hpp"
#include "StringDefines.hpp"  //for default string type variable
#include "Transitional/BasicStringIMPL_Trans.hpp"

namespace frao
{
inline namespace Strings
{
// tell the compiler that we will define these elsewhere
NUCLEUS_HEADER_EXTERN template class Transitional::StringIMPL<
	charU8, AsciiNumericalPunctuation<charU8, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::StringIMPL<
	wchar_t, AsciiNumericalPunctuation<wchar_t, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::StringIMPL<
	char16_t, AsciiNumericalPunctuation<char16_t, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::StringIMPL<
	char32_t, AsciiNumericalPunctuation<char32_t, false>>;

#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
NUCLEUS_HEADER_EXTERN template class Experimental::StringIMPL<
	charU8, AsciiNumericalPunctuation<charU8, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::StringIMPL<
	wchar_t, AsciiNumericalPunctuation<wchar_t, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::StringIMPL<
	char16_t, AsciiNumericalPunctuation<char16_t, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::StringIMPL<
	char32_t, AsciiNumericalPunctuation<char32_t, true>>;
#endif

// forward declarations
template<typename CharType, bool UseExperimental,
		 template<typename, bool> class NumericalPunctuation>
class Basic_String;

//! \brief frao(Basic)API class to hold unicode
//! (UTF-8/16/32) strings, in their entirety. Operates in
//! terms of a string buffer, but also the smaller unicode
//! string classes (code-point, grapheme cluster, word)
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \tparam UseExperimental Should be experimental version of the
//! unicode classes be used? True if we should. Will only have an
//! effect if g_Experimental_Strings_Allowed is true, which it will be
//! if FRAO_EXPERIMENTAL_STRINGS_ALLOWED was defined for this project
//!
//! \tparam NumericalPunctuation The struct that holds the type of
//! numerical punctuation to use, when converting to and from numbers.
//! By default, set to AsciiNumericalPunctuation
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default,
		 template<typename, bool> class NumericalPunctuation =
			 AsciiNumericalPunctuation>
class NUCLEUS_LIB_API Basic_String final
	: private std::conditional_t<
		  UseExperimental && g_Experimental_Strings_Allowed,
		  Experimental::StringIMPL<
			  CharType, NumericalPunctuation<CharType, true>>,
		  Transitional::StringIMPL<
			  CharType, NumericalPunctuation<CharType, false>>>
{
	//! \brief template for possible base classes of this class.
	//! Dispatches to experimental or transtional base class as
	//! appropriate. Used so that we don't have to whole conditional
	//! statement every time we need to refer to another possible base
	//! class
	template<typename CharType_templ>
	using BaseClass_template = std::conditional_t<
		UseExperimental && g_Experimental_Strings_Allowed,
		Experimental::StringIMPL<
			CharType_templ,
			NumericalPunctuation<CharType_templ, true>>,
		Transitional::StringIMPL<
			CharType_templ,
			NumericalPunctuation<CharType_templ, false>>>;

	//! \brief using declaration for the base class of this one. Used
	//! for simplicity of referring to it.
	using BaseClass = BaseClass_template<CharType>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ThisType =
		Basic_String<CharType, UseExperimental, NumericalPunctuation>;

	//! \brief alias template, so that it is clear(er) that
	//! transitional and experimental implementations are mutually
	//! incompatible
	//!
	//! \tparam OtherType The character type of the similiar type
	template<typename OtherType>
	using SimilarType = Basic_String<OtherType, UseExperimental,
									 NumericalPunctuation>;

   public:
	// all instantiations are friends! :D
	template<typename OtherType, bool OtherUseExperimental,
			 template<typename, bool> class OtherNumPunc>
	friend class Basic_String;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = CodePoint<CharType, UseExperimental>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = GraphemeCluster<CharType, UseExperimental>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using WordType = Word<CharType, UseExperimental>;

	//! \brief Used for unrepresentable values. For example, when a
	//! find functions fails to find
	using BaseClass::npos;

	//! \brief create empty string
	Basic_String() : BaseClass()
	{}
	//! \brief create string from the specified
	//! (null-terminated) string
	//!
	//! \param[in] string pointer to const string buffer
	Basic_String(const CharType* string) : BaseClass(string)
	{}

	//! \brief Construct a Basic_String from an ascii cstring
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	Basic_String(const charA* string) : BaseClass(string)
	{}

	//! \brief create string from a string buffer and
	//! specified size. Doesn't need to be null-terminated
	//!
	//! \param[in] string pointer to const string buffer
	//!
	//! \param[in] bytesize size of string buffer in
	//! 'string' parameter
	explicit Basic_String(const CharType* string,
						  frao::natural64 bytesize)
		: BaseClass(string, bytesize)
	{}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to string
	explicit Basic_String(frao::small_nat8 number) : BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to string
	explicit Basic_String(frao::small_nat16 number)
		: BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to string
	explicit Basic_String(frao::small_nat32 number)
		: BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to string
	explicit Basic_String(frao::small_nat64 number)
		: BaseClass(number)
	{}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to string
	explicit Basic_String(frao::small_int8 number) : BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to string
	explicit Basic_String(frao::small_int16 number)
		: BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to string
	explicit Basic_String(frao::small_int32 number)
		: BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to string
	explicit Basic_String(frao::small_int64 number)
		: BaseClass(number)
	{}

	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to string
	explicit Basic_String(float number) : BaseClass(number)
	{}
	//! \brief create string that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to string
	explicit Basic_String(double number) : BaseClass(number)
	{}

	//! \brief concatenate two strings together. lhs will be on the
	//! front, and rhs will be the back. creates new string containing
	//! concatenated string. parameters not modified
	//!
	//! \returns newly created string containing results of the
	//! concatenation
	//!
	//! \param[in] lhs string to be the front of the new string
	//!
	//! \param[in] rhs string to be concatenated to lhs, when creating
	//! the new string
	explicit Basic_String(const ThisType& lhs, const ThisType& rhs)
		: BaseClass(lhs, rhs)
	{}


	//! \brief Copy construct a string, from another string of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other string, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The string to copy from
	template<typename OtherType>
	Basic_String(const SimilarType<OtherType>& rhs)
		: BaseClass(
			static_cast<const BaseClass_template<OtherType>&>(rhs))
	{}

	//! \brief standard copy constructor
	//!
	//! \param[in] rhs string to copy from
	Basic_String(const ThisType& rhs) : BaseClass(rhs)
	{}

	//! \brief standard move constructor
	//!
	//! \param[in] rhs string to move from. Left in a
	//! potentially invalid state
	Basic_String(ThisType&& rhs) : BaseClass(std::move(rhs))
	{}
	~Basic_String() noexcept override = default;

	//! \brief standard copy assignment operator
	//!
	//! \param[in] rhs string to copy from
	ThisType& operator=(const ThisType& rhs)
	{
		static_cast<BaseClass&>(*this) = rhs;

		return *this;
	}
	//! \brief standard move assignment operator
	//!
	//! \param[in] rhs string to move from. Left in a
	//! potentially invalid state
	ThisType& operator=(ThisType&& rhs)
	{
		static_cast<BaseClass&>(*this) = std::move(rhs);

		return *this;
	}

	//! \brief pops (removes) the back of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the last code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_back() noexcept
	{
		return this->template pop_backIMPL<ReturnType>();
	}
	//! \brief pops (removes) the front of the string, and
	//! returns a copy of the removed code-point / grapheme
	//! cluster / word (dependent on template parameter)
	//!
	//! \tparam ReturnType Should be one of
	//! CodePoint<CharType>, GraphemeCluster<CharType>,
	//! Word<CharType>, where CharType is the underlying
	//! character type of this string. Will be the type of
	//! the return value
	//!
	//! \returns ReturnType Will return the first code-point
	//! / grapheme cluster / word in the string, dependent
	//! on what was specified in the template parameter
	template<typename ReturnType>
	ReturnType pop_front() noexcept
	{
		return this->template pop_frontIMPL<ReturnType>();
	}

	//! \brief adds a codepoint to the back of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_back(const CodePointType& codepoint) noexcept
	{
		return this->push_backIMPL(codepoint);
	}
	//! \brief adds a cluster to the back of the string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_back(const ClusterType& cluster) noexcept
	{
		return this->push_backIMPL(cluster);
	}
	//! \brief adds a word to the back of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_back(const WordType& word) noexcept
	{
		return this->push_backIMPL(word);
	}

	//! \brief adds a codepoint to the front of the string
	//!
	//! \param[in] codepoint code-point to add to the back
	//! of the string
	void push_front(const CodePointType& codepoint) noexcept
	{
		return this->push_frontIMPL(codepoint);
	}
	//! \brief adds a grapheme cluster to the front of the
	//! string
	//!
	//! \param[in] cluster grapheme cluster to add to the
	//! back of the string
	void push_front(const ClusterType& cluster) noexcept
	{
		return this->push_frontIMPL(cluster);
	}
	//! \brief adds a word to the front of the string
	//!
	//! \param[in] word word to add to the back of the
	//! string
	void push_front(const WordType& word) noexcept
	{
		return this->push_frontIMPL(word);
	}

	//! \brief add a code-point AT the specified index in
	//! the string, moving the EXISTING code-point at that
	//! location, and all subsequent code-points back
	//!
	//! \param[in] codepoint The code-point to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the
	//! code-point. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	//!
	//! \todo Make indexing in terms of codepoints? we're implicitly
	//! exposing the implementation, otherwise
	void insert(const CodePointType& codepoint,
				natural64			 index) noexcept
	{
		return this->insertIMPL(codepoint, index);
	}
	//! \brief add a grapheme cluster AT the specified index
	//! in the string, moving the EXISTING grapheme cluster
	//! at that location, and all subsequent grapheme
	//! clusters back
	//!
	//! \param[in] cluster The grapheme cluster to add at
	//! the specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indicates the point at which to insert the grapheme
	//! cluster. Any existing code-point at the location,
	//! and subsequent locations, will be moved back
	void insert(const ClusterType& cluster, natural64 index) noexcept
	{
		return this->insertIMPL(cluster, index);
	}
	//! \brief add a word AT the specified index in the
	//! string, moving the EXISTING word at that location,
	//! and all subsequent words back
	//!
	//! \param[in] word The word to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the word. Any
	//! existing word at the location, and subsequent
	//! locations, will be moved back
	void insert(const WordType& word, natural64 index) noexcept
	{
		return this->insertIMPL(word, index);
	}
	//! \brief add a string into this one AT the specified
	//! index in the string, moving the EXISTING
	//! code-point(s) at that location, and all subsequent
	//! locations, back
	//!
	//! \param[in] string The string to add at the
	//! specified location
	//!
	//! \param[in] index unsigned 64-bit integer that
	//! indates the point at which to insert the (start of
	//! the) string. Any existing code-point(s) at the
	//! location, and subsequent locations, will be moved
	//! back
	void insert(const ThisType& string, natural64 index) noexcept
	{
		return this->insertIMPL(string, index);
	}

	//! \brief add a string onto the back of this one
	//!
	//! \param[in] string String to add on the back of this
	//! one
	void append(const ThisType& string)
	{
		this->appendIMPL(string);
	}

	//! \brief Add a unicode string (of any underlying type)
	//! onto the back of this one
	//!
	//! \param[in] string null-terminated string, of any
	//! underlying type, too add to the end of this string
	template<typename OtherType>
	void append(const OtherType* string)
	{
		// make c_str into Basic_String object, then convert object to
		// our encoding, and append it
		if constexpr (std::is_same_v<OtherType, charA>)
		{
			this->appendIMPL(ThisType(string));
		} else
		{
			this->appendIMPL(static_cast<ThisType>(
				SimilarType<OtherType>(string)));
		}
	}

	//! \brief remove a certain number of characters,
	//! starting at the specified location
	//!
	//! \param[in] index location to start the removal of
	//! characters
	//!
	//! \param[in] length nunber of characters to remove
	void erase(frao::natural64 index, frao::natural64 length)
	{
		return this->eraseIMPL(index, length);
	}

	//! \brief erase entire content of string. String will
	//! be left empty
	void clear()
	{
		this->clearIMPL();
	}

	//! \brief get (a copy of) a section of this string,
	//! starting at the specified location, and copying
	//! specifed number of grapheme clusters. If the
	//! (starting point of the copy + length of copy) > size
	//! of string, then will return copy of remainder of
	//! string, starting at specifed start index
	//!
	//! \returns new string of same underlying type as this,
	//! containing the substring that is the result of this
	//! operation
	//!
	//! \param[in] startIndex index of the first grapheme
	//! cluster that should be included in the sub-string
	//!
	//! \param[in] length number of grapheme clusters to
	//! copy into sub-string
	ThisType subStr(frao::natural64 startIndex,
					frao::natural64 length) const
	{
		ThisType result;
		static_cast<BaseClass&>(result) =
			this->subStrIMPL(startIndex, length);

		return result;
	}

	//! \brief size of string, in terms of grapheme clusters
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of grapheme clusters in the string
	frao::natural64 size() const noexcept
	{
		return this->sizeIMPL();
	}
	//! \brief size of string, in terms of bytes
	//!
	//! \returns unsigned 64-bit integer containing the
	//! number of bytes in the string
	frao::natural64 byteSize() const noexcept
	{
		return this->byteSizeIMPL();
	}

	//! \brief make sure the string has sufficient memory
	//! allocated to allow future expansion to specified
	//! number of characters
	//!
	//! \param[in] characters Number of characters that string should
	//! be able to hold (ie: make sure the buffer is at
	//! least this number of characters long)
	void reserve(frao::natural64 characters)
	{
		this->reserveIMPL(characters);
	}
	//! \brief gets a const pointer to the data
	//!
	//! \note null-terminated c-string returned
	//!
	//! \returns a const pointer to the data. Const in the
	//! sense of the data being immutable through the
	//! pointer. Null-terminated
	const CharType* data() const noexcept
	{
		return this->dataIMPL();
	}

	//! \brief indicates whether the string is empty
	//!
	//! \returns a boolean indicating whether the string in
	//! empty. returns true iff the string is empty
	bool isEmpty() const noexcept
	{
		return this->isEmptyIMPL();
	}

	//! \brief indexes Grapheme Clusters in this string
	//!
	//! \returns Grapheme Cluster at the specified index
	//!
	//! \param[in] index of the Grapheme Cluster that should
	//! be returned
	ClusterType get(frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief indexes Grapheme Clusters in this string
	//!
	//! \returns Grapheme Cluster at the specified index
	//!
	//! \param[in] index of the Grapheme Cluster that should
	//! be returned
	ClusterType operator[](frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next
	//! instance of
	//!
	//! \param[in] strSize The size of the passed sub-string
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNext(
		const CharType* string, frao::natural64 strSize,
		frao::natural64 startCharacter) const noexcept
	{
		return this->findNextIMPL(string, strSize, startCharacter);
	}
	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \pre passed sub-string must not be null
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts
	//!
	//! \param[in] string the (null-terminated) sub-string
	//! to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNext(
		const CharType* string,
		frao::natural64 startCharacter) const noexcept
	{
		return this->findNextIMPL(string, startCharacter);
	}
	//! \brief find the next instance of the passed
	//! sub-string, in this string, after the indicated
	//! character
	//!
	//! \returns index of the character of this string, at
	//! which, the next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next
	//! instance of
	//!
	//! \param[in] startCharacter first character to search, in
	//! order to find the indicated sub-string
	frao::natural64 findNext(
		const ThisType& string,
		frao::natural64 startCharacter) const noexcept
	{
		return this->findNextIMPL(string, startCharacter);
	}
	//! \brief find the next instance of the passed codepoint, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const CodePointType& codepoint,
		frao::natural64		 startCharacter) const noexcept
	{
		return this->findNextIMPL(codepoint, startCharacter);
	}
	//! \brief find the next instance of the passed cluster, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const ClusterType& cluster,
		frao::natural64	   startCharacter) const noexcept
	{
		return this->findNextIMPL(cluster, startCharacter);
	}
	//! \brief find the next instance of the passed word, in this
	//! string, after the indicated character
	//!
	//! \returns index of the character of this string, at which, the
	//! next instance of the sub-string starts
	//!
	//! \param[in] string the sub-string to find the next instance of
	//!
	//! \param[in] startCharacter first character to search, in order
	//! to find the indicated sub-string
	frao::natural64 findNext(
		const WordType& word,
		frao::natural64 startCharacter) const noexcept
	{
		return this->findNextIMPL(word, startCharacter);
	}


	//! \brief converts this string to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[out] number reference to where the result should be
	//! held
	template<typename NumberType>
	bool toNumber(NumberType& number) const noexcept
	{
		return this->toNumberIMPL(number);
	}

	//! \brief perform unicode case folding. This is an
	//! operation whereby strings are converted to a
	//! 'caseless' form for proper case-agnostic comparison.
	//! For english speakers this means comparing upper and
	//! lower case forms of the same letter to be
	//! equivalent, for example
	//!
	//! \returns Case-folded copy of this string
	ThisType toCaseFolded() const noexcept
	{
		ThisType result;
		static_cast<BaseClass&>(result) = this->toCaseFoldedIMPL();

		return result;
	}

	// Append operators

	//! \brief concatenate two strings together. *this will
	//! be on the front, and 'otherString' will be the back.
	//! returns new string containing concatenated string.
	//! parameters not modified
	//!
	//! \returns newly created string containing results of
	//! the concatenation
	//!
	//! \param[in] otherString string to be concatenated to
	//! the back of *this.
	//!
	//! \note neither explicit 'otherString' nor implicit
	//! 'this' parameter will be modified during this
	//! operation
	template<typename OtherType>
	ThisType operator+(
		const SimilarType<OtherType>& otherString) const
	{
		return ThisType(*this, static_cast<ThisType>(otherString));
	}

	//! \brief concatenate two strings together. *this will
	//! be on the front, and 'otherString' will be the back.
	//! returns new string containing concatenated string.
	//! parameters not modified
	//!
	//! \returns newly created string containing results of
	//! the concatenation
	//!
	//! \param[in] otherString string (null-terminated; of
	//! any underlying type) to be concatenated to the back
	//! of *this.
	//!
	//! \note neither explicit 'otherString' nor implicit
	//! 'this' parameter will be modified during this
	//! operation
	template<typename OtherType>
	ThisType operator+(const OtherType* otherString) const
	{
		ThisType result(*this);

		result.append(otherString);

		return result;
	}

	//! \brief concatenate another string onto this. *this
	//! will be on the front, and 'otherString' will be the
	//! back. returns reference for chaining
	//!
	//! \returns reference to this string, which will have
	//! the added string at the back
	//!
	//! \param[in] otherString string to be concatenated to
	//! the back of *this.
	template<typename OtherType>
	ThisType& operator+=(const SimilarType<OtherType>& otherString)
	{
		this->appendIMPL(otherString.data());
		return *this;
	}

	//! \brief concatenate another string onto this. *this
	//! will be on the front, and 'otherString' will be the
	//! back. returns reference for chaining
	//!
	//! \returns reference to this string, which will have
	//! the added string at the back
	//!
	//! \param[in] otherString string to be concatenated to
	//! the back of *this.
	template<typename OtherType>
	ThisType& operator+=(const OtherType* otherString)
	{
		this->append(otherString);

		return *this;
	}

	//! \brief get the ordering of two words. Stand-in for
	//! the upcoming C++20 (?) <=> spaceship operator
	//!
	//! \returns <0, 0, >0 for less-than, equal-to and
	//! greater-than, respectively
	//!
	//! \param[in] otherString the value to compare on the rhs of
	//! the order. ie: (*this <=> rhs). string passed to rhs
	//! may have any underlying type with the same implementation
	//! strategy (exp or trans)
	template<typename OtherType>
	frao::integer64 getOrder(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString);
	}

	// Strict Comparisons

	//! \brief uses getOrder() function to dermine if *this
	//! is before otherString
	//!
	//! \returns bool which has value true iff *this is
	//! strictly before otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator<(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) < 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after otherString
	//!
	//! \returns bool which has value true iff *this is
	//! strictly after otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator>(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) > 0;
	}

	// Non-Strict Comparisons

	//! \brief uses getOrder() function to dermine if *this
	//! is before or equal to otherString
	//!
	//! \returns bool which has value true iff *this is
	//! before or equal to otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator<=(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) <= 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after or equal to otherString
	//!
	//! \returns bool which has value true iff *this is
	//! after or equal to otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator>=(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) >= 0;
	}

	// Equality Comparisons

	//! \brief uses getOrder() function to dermine if *this
	//! and otherString are equal
	//!
	//! \returns bool which has value true iff *this is
	//! equal to otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator==(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) == 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! and otherString are unequal
	//!
	//! \returns bool which has value true iff *this is
	//! not equal to otherString
	//!
	//! \param[in] otherString const reference to another string, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator!=(
		const SimilarType<OtherType>& otherString) const noexcept
	{
		return this->getOrderIMPL(otherString) != 0;
	}

	//! \brief append a code-point to the end of this string
	//!
	//! \returns reference to this string, for chaining
	//!
	//! \param[in] codepoint Code-point to add to the end of
	//! the string
	ThisType& operator<<(const CodePointType& codepoint) noexcept
	{
		this->push_backIMPL(codepoint);

		return *this;
	}

	//! \brief append a grapheme cluster to the end of this
	//! string
	//!
	//! \returns reference to this string, for chaining
	//!
	//! \param[in] cluster Grapheme Cluster to add to the
	//! end of the string
	ThisType& operator<<(const ClusterType& cluster) noexcept
	{
		this->push_backIMPL(cluster);

		return *this;
	}

	//! \brief append a word to the end of this string
	//!
	//! \returns reference to this string, for chaining
	//!
	//! \param[in] word Word to add to the end of the string
	ThisType& operator<<(const WordType& word) noexcept
	{
		this->push_backIMPL(word);

		return *this;
	}

	//! \brief append a string to the end of this string
	//!
	//! \returns reference to this string, for chaining
	//!
	//! \param[in] string String to append to the end of
	//! this string
	ThisType& operator<<(const ThisType& string) noexcept
	{
		this->appendIMPL(string);

		return *this;
	}

	//! \brief append a string to the end of this string
	//!
	//! \returns reference to this string, for chaining
	//!
	//! \param[in] string (null-terminated) string to append
	//! to the back of this one
	template<typename OtherType>
	ThisType& operator<<(const OtherType* string) noexcept
	{
		this->append(string);

		return *this;
	}

	//! \brief Get the front code-point out of this string
	//!
	//! \returns reference to this string, for chaining.
	//! Will have removed the code-point that previously was
	//! at the front
	//!
	//! \param[out] codepoint code-point object that will
	//! now hold the code-point, that was previously at the
	//! front of this string
	ThisType& operator>>(CodePointType& codepoint) noexcept
	{
		codepoint = this->template pop_frontIMPL<CodePointType>();

		return *this;
	}

	//! \brief Get the front grapheme cluster out of this
	//! string
	//!
	//! \returns reference to this string, for chaining.
	//! Will have removed the grapheme cluster that
	//! previously was at the front
	//!
	//! \param[out] cluster grapheme cluster object that
	//! will now hold the grapheme cluster, that was
	//! previously at the front of this string
	ThisType& operator>>(ClusterType& cluster) noexcept
	{
		cluster = this->template pop_frontIMPL<ClusterType>();

		return *this;
	}

	//! \brief Get the front word out of this string
	//!
	//! \returns reference to this string, for chaining.
	//! Will have removed the word that previously was at
	//! the front
	//!
	//! \param[out] word word object that will now hold
	//! the word, that was previously at the front of this
	//! string
	ThisType& operator>>(WordType& word) noexcept
	{
		word = this->template pop_frontIMPL<WordType>();

		return *this;
	}

	//! \brief convert a copy of this string to
	//! **different** underlying type
	//!
	//! \tparam OtherType underlying type that new string
	//! shpuld have. **Must** be different to existing type
	//!
	//! \returns String of different underlying type,
	//! containing the same conceptual string, in a
	//! different UTF form (8/16/32)
	template<typename OtherType>
	explicit operator SimilarType<OtherType>() const
	{
		// should call constructor for StringIMPL
		return SimilarType<OtherType>(*this);
	}

	//! \brief Stream from a string (our Basic_String) to a
	//! std::ostream. ie: read one of our strings into an ostream
	//!
	//! \returns the passed ostream parameter, with the added string.
	//! This is returned for chaining purposes, ie: ostream << u8"This
	//! is a "_ustring << u8"String!"_ustring
	//!
	//! \param[in, out] oStream A std::basic_ostream that will take
	//! the string, and add it to the end
	//!
	//! \param[in] string A string to add to the ostream
	friend std::basic_ostream<CharType, std::char_traits<CharType>>&
	operator<<(std::basic_ostream<
				   CharType, std::char_traits<CharType>>& oStream,
			   const Basic_String<CharType, UseExperimental,
								  NumericalPunctuation>&  string)
	{
		return oStream << static_cast<const BaseClass&>(string);
	}
	//! \brief Stream from a std::istream to a string (our
	//! Basic_String). ie: read some input from an istream into a
	//! string
	//!
	//! \returns the passes istream parameter, after the operation to
	//! read from it. This is returned for chaining purposes, ie:
	//! stringInput2 << (stringInput1 << istream)
	//!
	//! \param[in, out] iStream A std::basic_istream that will have
	//! the data to be read
	//!
	//! \param[in] string String taht will hold the read data
	friend std::basic_istream<CharType, std::char_traits<CharType>>&
	operator>>(
		std::basic_istream<CharType, std::char_traits<CharType>>&
			iStream,
		Basic_String<CharType, UseExperimental, NumericalPunctuation>&
			string)
	{
		return iStream >> static_cast<BaseClass&>(string);
		;
	}
};

inline namespace Operators
{
//! \brief Construct a utf8 string, from a user-defined string literal
//!
//! \note function is actually called with u8"STRING"_ustring, where
//! STRING is the word to construct the object from
//!
//! \returns a constructed utf8 string object
//!
//! \param[in] str Pointer to the string, that we should create the
//! string from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Basic_String<charU8> operator"" _ustring(const charU8* str,
												size_t		  size)
{
	return Basic_String<charU8>{str, size};
}
//! \brief Construct a wide string, from a user-defined string literal
//!
//! \note function is actually called with L"STRING"_ustring, where
//! STRING is the word to construct the object from
//!
//! \returns a constructed wide string object
//!
//! \param[in] str Pointer to the string, that we should create the
//! string from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Basic_String<wchar_t> operator"" _ustring(const wchar_t* str,
												 size_t			size)
{
	return Basic_String<wchar_t>{str, size};
}
//! \brief Construct a utf16 string, from a user-defined string
//! literal
//!
//! \note function is actually called with u"STRING"_ustring, where
//! STRING is the word to construct the object from
//!
//! \returns a constructed utf16 string object
//!
//! \param[in] str Pointer to the string, that we should create the
//! string from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Basic_String<char16_t> operator"" _ustring(const char16_t* str,
												  size_t size)
{
	return Basic_String<char16_t>{str, size};
}
//! \brief Construct a utf32 string, from a user-defined string
//! literal
//!
//! \note function is actually called with U"STRING"_ustring, where
//! STRING is the word to construct the object from
//!
//! \returns a constructed utf32 string object
//!
//! \param[in] str Pointer to the string, that we should create the
//! string from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Basic_String<char32_t> operator"" _ustring(const char32_t* str,
												  size_t size)
{
	return Basic_String<char32_t>{str, size};
}
}  // namespace Operators

// instantiate normal Basic_String versions, for API boundary
NUCLEUS_HEADER_EXTERN template class frao::Strings::Basic_String<
	frao::charU8>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Basic_String<
	char16_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Basic_String<
	wchar_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Basic_String<
	char32_t>;

//! \brief typedef from Basic_String, to the more sensibly named
//! UnicodeString
template<typename CharType>
using UnicodeString = Basic_String<CharType>;

//! \brief Type used to represent UTF8-encoded strings
typedef UnicodeString<charU8> utf8string;

//! \brief Type used to represent UTF16-encoded strings
typedef UnicodeString<char16_t> utf16string;

//! \brief Type used to represent UTF32-encoded strings
typedef UnicodeString<char32_t> utf32string;

//! \brief Type used to represent windows-encoded strings
//! (windows basically uses UTF16. This class is
//! distinguished from the UTF16 version because it uses
//! wchar rather than char16. nb: wchar is platform
//! dependant!). In future this class may be used for
//! platform-native wchar type (on any platform?)
typedef UnicodeString<wchar_t> widestring;
}  // namespace Strings
}  // namespace frao

#endif	// !FRAO_BASIC_API_STRINGS_BASIC
