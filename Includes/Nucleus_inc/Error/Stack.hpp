#ifndef FRAO_BASIC_API_ERROR_STACK
#define FRAO_BASIC_API_ERROR_STACK

//! \file
//!
//! \brief Header of frao Error Stack structures
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <atomic>		 //for std::atomic
#include <functional>	//for std::function
#include <mutex>		 //for std::unique_lock()
#include <shared_mutex>  //for std::shared_mutex, std::shared_lock()
#include "../Allocate.hpp"
#include "../Environment.hpp"
#include "Types.hpp"

namespace frao
{
inline namespace ErrorLogging
{
namespace IMPL
{
//! \brief object that actually holds ErrorResolver data
struct ResolveKernel final
{
	//! \brief A functor used to determine whether the specified error
	//! is one that we can try to resolve
	std::function<ErrorAcceptance(BaseError&)> m_Filter;
	//! \brief A functor used to actually perform the attempt at a
	//! resolution, of the specified error
	std::function<ErrorResolution(BaseError&)> m_Handler;

   public:
	//! \brief Construct a ResolveKernel, containing data to deal with
	//! errors
	//!
	//! \param[in] filter The functor that shall be used to determine
	//! whether an error can be accepted for attempted resolution.
	//!
	//! \note This function may be called much more frequently than
	//! the handler function, as it may be called for every error, if
	//! a weak reference is held by the errorlog
	//!
	//! \param[in] handler The functor that shall be called, to
	//! attempt to resolve an error.
	ResolveKernel(std::function<ErrorAcceptance(BaseError&)> filter,
				  std::function<ErrorResolution(BaseError&)> handler);
	//! \brief Deleted copy constructor, since we should never have
	//! ResolveKernel copies
	ResolveKernel(const ResolveKernel&) = delete;
	//! \brief Deleted move constructor, since we should never need to
	//! move ResolveKernel
	ResolveKernel(ResolveKernel&&) = delete;
	//! \brief defaulted destructor
	~ResolveKernel() noexcept = default;

	//! \brief Deleted copy assignment operator, since we should never
	//! have ResolveKernel copies
	ResolveKernel& operator=(const ResolveKernel&) = delete;
	//! \brief Deleted move assignment operator, since we should never
	//! need to move ResolveKernel
	ResolveKernel& operator=(ResolveKernel&&) = delete;

	//! \brief Determine whether we accept the provided error, for
	//! attempts of resolution
	//!
	//! \returns A status code that indicates whether, for the error
	//! supplied, we can attempt to resolve the error via this
	//! object's Handler function
	//!
	//! \param[in] error The error that we wish to determine our
	//! acceptance of
	ErrorAcceptance errorAccepted(BaseError& error);

	//! \brief Try and resolve an error
	//!
	//! \returns A status code that indicates the success, or lack
	//! thereof, of the error resolution
	//!
	//! \param[in] error The error that we should attempt to resolve
	ErrorResolution tryResolveError(BaseError& error);
};
//! \brief object that holds owner/weak ref count to the ResolveKernel
//! (/ErrorResolver)
struct ResolveCount final
{
	//! \brief Mutex for Stack Kernel object.
	//!
	//! \details used to ensure that the ResolveKernel object, that
	//! this count exists to ensure safe access to, is only deleted
	//! when it isn't  being accessed
	std::shared_mutex m_KernelMutex;
	//!\ brief A count of the weak references, only
	//!
	//! \details An atomic is sufficient here, since only one object
	//! (and thread) shall modify m_Owner, and it follows that we only
	//! need to ensure that modifying the number of references is
	//! atomic, and that loads are not reordered before stores.
	std::atomic<natural64> m_WeakRefs;
	//! \brief A pointer to the current owner. Will be equal to
	//! nullptr iff we have no owner
	std::atomic<const void*> m_Owner;

   public:
	//! \pre ownerLoc must not be nullptr
	ResolveCount(void* ownerLoc) noexcept;
	//! \brief Deleted copy constructor, since we should never have
	//! ResolveCount copies
	ResolveCount(const ResolveCount&) = delete;
	//! \brief Deleted move constructor, since we should never need to
	//! move ResolveCount
	ResolveCount(ResolveCount&&) = delete;
	//! \brief defaulted destructor
	~ResolveCount() noexcept = default;

	//! \brief Deleted copy assignment operator, since we should never
	//! have ResolveCount copies
	ResolveCount& operator=(const ResolveCount&) = delete;
	//! \brief Deleted move assignment operator, since we should never
	//! need to move ResolveCount
	ResolveCount& operator=(ResolveCount&&) = delete;

	//! \brief acquire a pointer to count, incrementing weak refs
	//! count
	//!
	//! \returns a pointer to this, after having incremented the weak
	//! reference count
	ResolveCount* acquireWeakRef() noexcept;

	//! \brief Acquire a unique lock for the ResolveKernel object
	//!
	//! \note Intended to control construction and destruction of the
	//! ResolveKernel object
	//!
	//! \returns A unique lock for unique access to the kernel object
	std::unique_lock<std::shared_mutex> getOwnerLock();
	//! \brief Acquire a shared lock for the ResolveKernel object
	//!
	//! \note Intended to control normal access to the ResolveKernel
	//! object
	//!
	//! \returns A unique lock for shared access to the kernel object
	std::shared_lock<std::shared_mutex> getAccessLock();

	//! \brief Transfers the ownership of the kernel, if the provided
	//! pointer matches that of the owner
	//!
	//! \param[in] priorOwner A pointer to the previous owner, for
	//! verification
	//!
	//! \param[in] newOwner The pointer to be set as the new owner.
	//! Can be nullptr, to simply remove ownership of the Kernel
	void transferOwnership(const void* const priorOwner,
						   const void* const newOwner) noexcept;

	//! \brief decrement the current weak reference count, and return
	//! the subsequent real (not weak) reference count
	//!
	//! \returns Real count after decrement, which takes into account
	//! existence of owner
	natural64 decrementCount() noexcept;

	//! \brief Determines whether we currently have any owner
	//!
	//! \returns true iff we have an owner
	bool hasOwner() const noexcept;
	//! \brief Returns the real ref count
	//!
	//! \details The real ref count is equal to the simple ref count,
	//! iff we don't have an owner. If we do, then the real ref count
	//! will be one higher than the simple ref count
	//!
	//! \returns The real reference count, which includes the owner,
	//! if one exists
	natural64 refCount() const noexcept;
};

}  // namespace IMPL

// forward declare, so ErrorResolver can be friends with
// ResolveReferrer
class NUCLEUS_LIB_API ResolveReferrer;

//! \brief class created to filter and resolve errors. Intended to be
//! created on the stack
//!
//! \pre ErrorResolver must be directly accessible from one thread
//! only. For access from other threads, use a weak reference
//! (ResolveReferrer)
class NUCLEUS_LIB_API ErrorResolver final
{
	static_assert(alignof(IMPL::ResolveCount) > 0);
	static_assert(alignof(IMPL::ResolveCount)
				  & (alignof(IMPL::ResolveCount)));

	//! \brief A pointer to the Kernel object.
	//!
	//! \invariant never null, unless we have an invalid object (after
	//! moving)
	IMPL::ResolveKernel* m_Kernel;
	//! \brief A pointer to the count object, that controls the Kernel
	//! object
	//!
	//! \invariant null iff m_Kernel is
	IMPL::ResolveCount* m_Count;

	//! \brief Used to control the (potential) destruction of a
	//! ResolveCount
	//!
	//! \param[in] owner The owner of the ResolveKernel object. That
	//! is, the this pointer of the object that calls destroy()
	//!
	//! \param[in,out] kernel A reference to the ResolveKernel
	//! pointer, the object at the end of which, shall be destroyed
	//!
	//! \param[in, out] count A reference to the ResolveCount pointer,
	//! which shall have its weak ref count decremented, and which may
	//! be destroyed, if the count becomes 0
	static void destroy(void* const			  owner,
						IMPL::ResolveKernel*& kernel,
						IMPL::ResolveCount*&  count) noexcept;

   public:
	//! \brief Our weak reference class needs access to
	//! ErrorResolver's internals, to create itself
	friend class ResolveReferrer;

	// TODO: proper error handling, if an allocation fails (try catch
	// around )

	//! \brief Create a new ErrorResolver object
	//!
	//! \param[in] filter The functor that shall be used to determine
	//! whether an error can be accepted for attempted resolution.
	//!
	//! \note This function may be called much more frequently than
	//! the handler function, as it may be called for every error, if
	//! a weak reference is held by the errorlog
	//!
	//! \param[in] handler The functor that shall be called, to
	//! attempt to resolve an error.
	//!
	//! \exception MemAlloc Will be thrown iff we cannot allocate the
	//! internal data or count objects
	ErrorResolver(std::function<ErrorAcceptance(BaseError&)> filter,
				  std::function<ErrorResolution(BaseError&)> handler);
	//! \brief A deleted move constructor. Must be deleted, to ensure
	//! that there is only ever one owner of m_Kernel
	ErrorResolver(const ErrorResolver&) = delete;
	//! \brief Move contructor, which shall invalidate the rhs object
	//!
	//! \param[in] rhs Object to move from
	//!
	//! \pre rhs must no have been from before
	ErrorResolver(ErrorResolver&& rhs) noexcept;

	//! \brief A deleted move assignment operator. Must be deleted, to
	//! ensure that there is only ever one owner of m_Kernel
	ErrorResolver& operator=(const ErrorResolver&) = delete;
	//! \brief Move assignment operator, which shall invalidate the
	//! rhs object
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs Object to move from
	//!
	//! \pre rhs must no have been from before
	ErrorResolver& operator=(ErrorResolver&& rhs) noexcept;

	//! \brief destructor for ResolveReferrer. Will safely delete
	//! internal kernel object, as well as counter, if we were the
	//! last owner
	~ErrorResolver() noexcept;

	//! \brief Determine whether we accept the provided error, for
	//! attempts of resolution
	//!
	//! \returns A status code that indicates whether, for the error
	//! supplied, we can attempt to resolve the error via this
	//! object's Handler function
	//!
	//! \param[in] error The error that we wish to determine our
	//! acceptance of
	ErrorAcceptance errorAccepted(BaseError& error);

	//! \brief Try and resolve an error
	//!
	//! \returns A status code that indicates the success, or lack
	//! thereof, of the error resolution
	//!
	//! \param[in] error The error that we should attempt to resolve
	ErrorResolution tryResolveError(BaseError& error);
};
//! \brief Weak refence class, to ErrorResolver
class NUCLEUS_LIB_API ResolveReferrer final
{
	//! \brief A pointer to the Kernel object.
	//!
	//! \note can be null, if main owner is destroyed
	IMPL::ResolveKernel* m_Stack;
	//! \brief A pointer to the count object, that controls the Kernel
	//! object
	//!
	//! \invariant never null
	IMPL::ResolveCount* m_Count;

	//! \brief Used to control the (potential) destruction of a
	//! ResolveCount
	//!
	//! \param[in, out] count A reference to the ResolveCount pointer,
	//! which shall have it's weak ref count decremented, and which
	//! may be destroyed, if the count becomes 0
	static void destroy(IMPL::ResolveCount*& count) noexcept;

   public:
	//! \brief Create a weak reference object, to an ErrorResolver
	//!
	//! \param[in] errStack The object, to which a weak reference
	//! should be created \pre errStack must exist for the duration of
	//! the creation of this object
	ResolveReferrer(const ErrorResolver& errStack) noexcept;
	//! \brief Copy a weak reference object, that points to an
	//! ErrorResolver
	//!
	//! \param[in] ResolveReferrer The weak reference object, from
	//! which a weak reference should be copied \pre ResolveReferrer
	//! must exist for the duration of the creation of this object
	ResolveReferrer(const ResolveReferrer& ResolveReferrer) noexcept;
	//! \brief A deleted move constructor. Must be deleted, to ensure
	//! that a weak ref always has a count
	ResolveReferrer(ResolveReferrer&&) = delete;

	//! \brief Copy assign a weak reference object, that points to an
	//! ErrorResolver
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The weak reference object, from which a weak
	//! reference should be copied \pre rhs must exist for the
	//! duration of the creation of this object
	ResolveReferrer& operator=(const ResolveReferrer& rhs) noexcept;

	//! \brief A deleted move assignment operator. Must be deleted, to
	//! ensure that a weak ref always has a count
	ResolveReferrer& operator=(ResolveReferrer&&) = delete;

	//! \brief destructor for ResolveReferrer. Will safely delete
	//! internal counter, if we were the last owner
	~ResolveReferrer() noexcept;

	//! \brief Determine whether we accept the provided error, for
	//! attempts of resolution
	//!
	//! \returns A status code that indicates whether, for the error
	//! supplied, we can attempt to resolve the error via this
	//! object's Handler function
	//!
	//! \param[in] error The error that we wish to determine our
	//! acceptance of
	//!
	//! \exception NotFound iff
	ErrorAcceptance errorAccepted(BaseError& error);

	//! \brief Try and resolve an error
	//!
	//! \returns A status code that indicates the success, or lack
	//! thereof, of the error resolution
	//!
	//! \param[in] error The error that we should attempt to resolve
	ErrorResolution tryResolveError(BaseError& error);
};
}  // namespace ErrorLogging
}  // namespace frao

#endif