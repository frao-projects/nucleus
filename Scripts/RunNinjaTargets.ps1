using Module .\NinjaFunctions.psm1

param(
	[string[]] $Targets,
	[Parameter(Mandatory = $true)]
	[string] $Compiler,
	# The directory that holds the builds
	[Parameter(Mandatory = $true)]
	[ValidateScript({ Test-Path -Path $_ -PathType Container })]
	[string] $BuildDir,
	# The location of the .frao directory, of this project
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
	[string]$FraoDir,
	[string] $BuildConfig,
	[switch] $Rebuild)

# Get build functions, via module, with using. Since using declarations cannot use variables,
# we have to invoke it via temporary script file, rather than at the top of the file
[string]$local:usingScriptContent = "using module '$FraoDir/BuildFunctions.psm1'"
$local:usingScript = [scriptblock]::Create($usingScriptContent)
. $usingScript

$local:usedOS = Read-OSString
$local:CompilerData = Get-Content -Raw -LiteralPath "$FraoDir/${usedOS}_compiler_data.json"

#Get compilers
[Boolean]$AllCompilers = ($Compiler -eq "ALL")
[string[]]$local:CompilerNames = $CompilerData | Get-CompilerNamesList | ForEach-Object {
	if($AllCompilers -or ($_ -eq $Compiler)) {
		return $_
	}
}

if(-not($CompilerNames)) {
	throw "No configured compiler could be found matching name '$Compiler'"
}

#Get config
[Boolean]$AllConfigs = ($BuildConfig -eq "ALL")
[string[]]$local:Configs = $CompilerData | Get-ConfigList | ForEach-Object {
	if($AllConfigs -or ($_ -eq $BuildConfig)) {
		return $_
	}
}

if(-not($Configs)) {
	throw "No configuration could be found matching name '$BuildConfig'"
}

#Make sure vs environment (in particular) is setup, if necessary
$CompilerData | Confirm-CompilerEnvironment

foreach ($local:compiler in $CompilerNames) {
	foreach ($local:config in $Configs) {
	$local:params = @{
			'BuildDir'        = $BuildDir;
			'Targets'         = $Targets;
			'Compiler'        = "$compiler";
			'BuildConfig'     = "$config";
			'CleanBuild'      = [Bool]$Rebuild;
			'OS'			  = $usedOS
		}

		Invoke-NinjaCycle @params
	}
}
