
// This must be in one source file per exe. It defines a main for the tests.
// if it's the same file as tests, then those tests will compile slower
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
