#ifndef FRAO_NUCLEUS_ENVIRONMENT
#define FRAO_NUCLEUS_ENVIRONMENT

//! \file
//!
//! \brief Header of all defintions, functions, and classes, relating
//! to the environment of the software (OS env, Compiler env, API env,
//! etc)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Environment/Static.hpp"
#include "Environment/Runtime.hpp"

#endif