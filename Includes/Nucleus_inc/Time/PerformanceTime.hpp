#ifndef FRAO_NUCLEUS_TIME_PERFORMANCE_TIME
#define FRAO_NUCLEUS_TIME_PERFORMANCE_TIME


//! \file
//!
//! \brief Header of performance time structures, used for recording
//! short-scale times elapsed, between discrete points
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cmath>   //for std::isfinite
#include <limits>  //for std::numeric_limits
#include <vector>  // for std::vector
#include "../Environment.hpp"

namespace frao
{
inline namespace Time
{
//! \brief Stats class, that calculates the mean/variance etc of a
//! running series of individual values, without storing any of the
//! entered value
//!
//! \note uses Welford's online algorithm
class NUCLEUS_LIB_API RunningStats final
{
	//! \brief The running sample mean, of the provided samples
	double m_SampleMean;
	//! \brief The running M2 (intermediate value, used to calculate
	//! variance), of the provided samples
	double m_M2;
	//! \brief The running count of samples provided
	natural64 m_Count;

   public:
	//\brief Construct an empty RunningStats object, with no data
	RunningStats() noexcept;

	//! \brief Update the stats, with a new sample
	//!
	//! \param[in] value The new value, to be incorporated into the
	//! running stats
	void update(double value) noexcept;

	//! \brief Get the number of samples provided, so far, to the
	//! class
	//!
	//! \returns The number of provided samples
	natural64 sampleCount() const noexcept;

	//! \brief Gets the sample mean, of the provided samples
	//!
	//! \note Results not meaningful until at least one value has been
	//! passed.
	//!
	//! \returns The calculated sample mean. Will be a valid number
	//! (not NAN), if at least one value has been passed. May be
	//! anything, if not
	double mean() const noexcept;
	//! \brief Gets the standard error of the mean, of the provided
	//! samples
	//!
	//! \note Results not meaningful until at least two values have
	//! been passed
	//!
	//! \returns The calculated sample standard deviation. Will be a
	//! valid number (not NAN), if at least two values has been
	//! passed. May be anything, if not
	double standardError() const noexcept;

	//! \brief Gets the sample variance, of the provided samples
	//!
	//! \note Results not meaningful until at least two values have
	//! been passed
	//!
	//! \returns The calculated sample variance. Will be a valid
	//! number (not NAN), if at least two values has been passed. May
	//! be anything, if not
	double sampleVariance() const noexcept;
	//! \brief Gets the population variance
	//!
	//! \note Results not meaningful until at least one value has been
	//! passed
	//!
	//! \returns The calculated population variance. Will be a valid
	//! number (not NAN), if at least one value has been passed. May
	//! be anything, if not
	double populationVariance() const noexcept;
	//! \brief Gets the sample standard deviation, of the provided
	//! samples
	//!
	//! \note Results not meaningful until at least two values have
	//! been passed
	//!
	//! \returns The calculated sample standard deviation. Will be a
	//! valid number (not NAN), if at least two values has been
	//! passed. May be anything, if not
	double sampleStandardDeviation() const noexcept;
	//! \brief Gets the population standard deviation
	//!
	//! \note Results not meaningful until at least one value has been
	//! passed
	//!
	//! \returns The calculated population standard deviation. Will be
	//! a valid number (not NAN), if at least one value has been
	//! passed. May be anything, if not
	double populationStandardDeviation() const noexcept;
};

//! \brief A basic timer class, that simply records a start time, and
//! then reports time elapsed since then
class NUCLEUS_LIB_API BasicTimer final
{
	//! \brief The point at which timing started
	integer64 m_StartTime;

   public:
	//! \brief Create a basic timer, and start timing
	BasicTimer() noexcept;

	//! \brief reset the timer, to start counting from this point
	//!
	//! \returns The amount of time elapsed, immediately before the
	//! reset
	double resetTimer() noexcept;

	//! \brief Get the time that has elapsed, since the timer started
	//! counting
	//!
	//! \returns A time, in ms, corresponding to the amount of time
	//! elapsed since the timer was started
	double getElapsedTime() const noexcept;
};

//! \brief Timer that has an on and off state, and which records the
//! total time it has spent in the ON position
class NUCLEUS_LIB_API SwitchTimer final
{
	//! \brief Used to hold the current timer, and determine the time
	//! in the current peroid
	BasicTimer m_RunningTime;
	//! \brief Used to hold the elapsed time of the 'on' state, and
	//! also whether the 'on' state is active
	//!
	//! \details The absolute value of this, is the time elapsed, in
	//! total, during all previous 'on' states. The sign bit is used
	//! for whether the timer is in the 'on' or 'off' state. We use
	//! the sign bit, since the elapsed time can never be negative. A
	//! negative value, with the sign bit set, means ON
	double m_UsedTime;

   public:
	//! \brief resets the timer to 0, as if it had just been created
	//!
	//! \param[in] startOn Should the timer be started in the on
	//! state, or the off state? True for on, false for off.
	SwitchTimer(bool startOn = true) noexcept;

	//! \brief resets the timer to 0, as if it had just been created
	//!
	//! \returns The amount of time elapsed, immediately before the
	//! reset
	//!
	//! \param[in] startOn Should the timer be start in the on state,
	//! or the off state? True for on, false for off.
	double resetTimer(bool startOn = true) noexcept;

	//! \brief get the total amount of time that the timer has been in
	//! the on state.
	double getElapsedOnTime() const noexcept;

	//! \brief Set the state of the timer, to on or off
	//!
	//! \param[in] on Should the timer be in the on state? If true,
	//! timer will be set to on. If false, off
	void switchState(bool on) noexcept;

	//! \brief determines whether the timer is in the on state
	//!
	//! \returns true iff the timer is on
	bool isOn() const noexcept;
};

// So the compiler won't complain about using STL containers in the interface. Obviously
// doing this can cause problems, esp. if we use a different compiler / flags for compilation
// of dll / consumer of dll, but it should mostly work, if we can keep those straight. Which
// our build system should anyway
DISABLE_VC_WARNING(4251)

//! \brief Timer that records times of a series of laps. That is,
//! intervals which immediately follow on, from the previous interval
class NUCLEUS_LIB_API LapTimer
{
	//! \brief Stores the times at which a lap finished. So lap 0
	//! finishing time will be recorded at [0]. End of 1 at [1], etc
	std::vector<double> m_Laps;
	//! \brief Records the total time, since the start of recording.
	BasicTimer m_TotalTime;

   public:
	//! \brief construct a timer, and start timing
	LapTimer();

	//! \brief reset the timer, to start counting from this point
	//!
	//! \returns The amount of time elapsed, immediately before the
	//! reset
	double resetTimer() noexcept;

	//! \brief End teh current lap, and start a new one
	//!
	//! \returns The final time, of the ended lap
	double startNextLap();

	//! \brief Get the current number of laps
	//!
	//! \returns The number of laps, up to the currently ongoing lap.
	//! eg. if the currently ongoing lap is lap 4, this will return 4
	natural64 numberLaps() const noexcept;

	//! \brief Retreives the total time, since the timer was last
	//! reset
	//!
	//! \returns The total time taken, since the start of the timing
	//! period
	double totalElapsedTime() const noexcept;
	//! \brief Retreives the total time, between when the timer was
	//! last reset, and the start of the specified lap
	//!
	//! \returns The time between the start of the timer, and the
	//! start of the specified lap
	//!
	//! \param[in] lapIndex The lap, whose start point should be
	//! compared to the last timer reset. Must not be greater than the
	//! number of the current lap
	double timeUntilLap(natural64 lapIndex) const noexcept;
	//! \brief Retreives he time taken so far this lap
	//!
	//! \returns The time taken, for the currently ongoing lap, so far
	double thisLapTime() const noexcept;
	//! \brief Calculates the time taken for the lap in question.
	//!
	//! \returns The timer taken between the start and end of the
	//! specified lap
	//!
	//! \param[in] lapIndex The lap whose time should be retreived.
	//! Must not be greater than the number of the current lap
	double elapsedLapTime(natural64 lapIndex) const noexcept;
};

//retore disabled c4251 warning, for using non-exporting class in interface
RESTORE_VC_WARNING(4251)

//! \brief A timer which records the total times that the timer has
//! been in one of a series of mutually exclusive states
//!
//! \tparam NumberStates The number of states, for which we desire
//! times
template<natural64 NumberStates>
class NUCLEUS_LIB_API StateTimer
{
	//! \brief Used to store total time spent in each state. One
	//! element for each state
	double m_StateTimes[NumberStates];
	//! \brief Used to store the time since the last time the state
	//! was changed
	BasicTimer m_ThisStateStart;
	//! \brief Used to store the current state
	natural64 m_CurrentState;

   public:
	//! \brief Create a state timer, starting in the given state
	//!
	//! \param[in] startState The state in which to start the timer
	StateTimer(natural64 startState) noexcept
		: m_StateTimes{},
		  m_ThisStateStart(),
		  m_CurrentState(startState % NumberStates)
	{
		runtime_assert(startState < NumberStates,
					   "Specified state index must be less than the "
					   "number of states!");
	}

	//! \brief Change the current state of the timer
	//!
	//! \returns The time spent in the previous state, this time.
	//! \note Return value is not the total time, but rather, the time
	//! spent between going into and out of, the state
	//!
	//! \param[in] newState The state to which we should switch. Must
	//! be less than NumberStates
	double switchState(natural64 newState) noexcept
	{
		runtime_assert(newState < NumberStates,
					   "Specified state index must be less than the "
					   "number of states!");

		double prevTime = m_ThisStateStart.resetTimer();
		newState %= NumberStates;

		m_CurrentState = newState;
		m_StateTimes[newState] += prevTime;

		return prevTime;
	}

	//! \brief Reset the timer, and all states. Will remain in the
	//! current state
	void resetTimer() noexcept
	{
		for (natural64 stateIndex = 0; stateIndex < NumberStates;
			 ++stateIndex)
		{
			m_StateTimes[stateIndex] = 0.0;
		}

		m_ThisStateStart.resetTimer();
	}
	//! \brief Reset the timer, and all states. Will start timer in
	//! the specified state
	//!
	//! \param[in] newState The state into which we should start the
	//! new timer. Must be less than NumberStates
	void resetTimer(natural64 newState) noexcept
	{
		runtime_assert(newState < NumberStates,
					   "Specified state index must be less than the "
					   "number of states!");

		resetTimer();
		m_CurrentState = newState % NumberStates;
	}
	//! \brief Reset the given state's timer. All others will be
	//! unaffected
	//!
	//! \returns The previous time of the state that has been reset.
	//! That is, the time before being reset.
	//!
	//! \param[in] stateIndex The state that should be reset. Must be
	//! less than NumberStates
	double resetState(natural64 stateIndex) noexcept
	{
		runtime_assert(stateIndex < NumberStates,
					   "Specified state index must be less than the "
					   "number of states!");

		stateIndex %= NumberStates;
		double stateTime = m_StateTimes[stateIndex];

		if (m_CurrentState == stateIndex)
		{
			stateTime += m_ThisStateStart.resetTimer();
		}

		m_StateTimes[stateIndex] = 0.0;
		return stateTime;
	}

	//! \brief Retreives the currently active state
	//!
	//! \returns The currently active state
	natural64 currentState() const noexcept
	{
		return m_CurrentState;
	}

	//! \brief The total time that the timer has spent in the given
	//! state
	//!
	//! \returns The time that the timer has spent in the given state
	//!
	//! \param[in] stateIndex The state that should be retreived. Must
	//! be less than NumberStates
	double stateTime(natural64 stateIndex) const noexcept
	{
		runtime_assert(stateIndex < NumberStates,
					   "Specified state index must be less than the "
					   "number of states!");

		return m_StateTimes[stateIndex % NumberStates];
	}
	//! \brief The total time that the timer has spent in all states,
	//! added together
	//!
	//! \note Not fast, with a large number of states, as all states
	//! must be iterated over
	//!
	//! \returns The cumulative time of all states.
	double totalElapsedTime() const noexcept
	{
		double totalTime = 0.0;

		for (natural64 stateIndex = 0; stateIndex < NumberStates;
			 ++stateIndex)
		{
			totalTime += m_StateTimes[stateIndex];
		}

		return totalTime;
	}
};


//! \brief Timer used to record how timings are distributed, for a
//! series of laps, that immediately start after the previous ended
//!
//! \tparam NumBuckets How buckets should we have, between the maximum
//! and minimum values
template<natural64 NumBuckets>
class NUCLEUS_LIB_API LapDistributionTimer
{
	//! \brief array that holds how many timings have been in each
	//! bucket. Will always be at least 2 buckets, for < min, and >
	//! max. The reminaing buckets will be equally spaced between max
	//! and min
	natural64 m_Buckets[NumBuckets + 2];
	//! \brief Records the time for the whole running period
	BasicTimer m_RunningTimer;
	//! \brief Records the time for the current lap start
	double m_LapStart;
	//! \brief The value below which a timing is out of range, of the
	//! distribution. Values below this will be put into the (< min)
	//! underflow bucket
	double m_MinValue;
	//! \brief The value above which a timing is out of range, of the
	//! distribution. Values above this will be put into the (>= max)
	//! overflow bucket
	double m_MaxValue;

   public:
	//! \brief Setup a lap disctribution, based on the minimum and
	//! maximum values specified. Buckets will be evenly distributed,
	//! betweeen the two
	//!
	//! \param[in] minimumValue The minimum value, of the range that
	//! we are interested in.
	//!
	//! \param[in] maximumValue The maximum value, of the range that
	//! we are interested in.
	LapDistributionTimer(double minimumValue,
						 double maximumValue) noexcept
		: m_Buckets{},
		  m_RunningTimer(),
		  m_LapStart(),
		  m_MinValue(minimumValue),
		  m_MaxValue(maximumValue)
	{
		runtime_assert(std::isfinite(m_MinValue),
					   "Minimum value must be finite!");
		runtime_assert(std::isfinite(m_MaxValue),
					   "Maximum value must be finite!");
		runtime_assert(m_MinValue <= m_MaxValue,
					   "Values must be ordered");
	}

	//! \brief Calculates the width of a single bucket, for a normal
	//! bucket.
	//!
	//! \note under- and over-flow buckets are infinitely wide
	//!
	//! \returns The width of any standard bucket
	double bucketWidth() const noexcept
	{
		return (m_MaxValue - m_MinValue) / NumBuckets;
	}

	//! \brief Reset the timer to zero, restoring all counts to 0
	//!
	//! \returns The total time that the timer had been running
	double resetTimer() noexcept
	{
		return m_RunningTimer.resetTimer();
	}

	//! \brief Start a new lap, putting the time of the last lap into
	//! the correct bucket
	//!
	//! \returns The time elapsed, during the previous lap
	double startNextLap() noexcept
	{
		// get time of new lap, and record last lap time
		double newLapStart = m_RunningTimer.getElapsedTime(),
			   lastLap	   = newLapStart - m_LapStart;
		m_LapStart		   = newLapStart;

		// get the distance from m_MinValue, in terms of number of
		// buckets. Then, get the real index, with values below
		// m_MinValue being transformed to 0, and values above
		// m_MaxValue being transformed to NumBuckets + 1
		double rawBucketIndex =
				   std::floor((lastLap - m_MinValue) / bucketWidth()),
			   realBucketIndex =
				   std::min(std::max(rawBucketIndex + 1.0, 0.0),
							static_cast<double>(NumBuckets + 1));

		// increase the bucket count by one
		++(m_Buckets[static_cast<integer64>(realBucketIndex)]);

		return lastLap;
	}

	//! \brief Get the minimum value, for this distribution. This is
	//! the lowest value, that is in the main range. That is, the
	//! lowest value that will NOT be included in the underflow bucket
	//!
	//! \returns The minimum value. This is the lowest value, to not
	//! be included in the underflow bucket
	double minValue() const noexcept
	{
		return m_MinValue;
	}
	//! \brief Get the maximum value, for this distribution. This is
	//! the lowest value, that is NOT in the main range. That is, the
	//! lowest value that will be included in the overflow bucket
	//!
	//! \returns The maximum value. This is the lowest value to be
	//! included in the overflow bucket
	double maxValue() const noexcept
	{
		return m_MaxValue;
	}
	//! \brief Get the minimum value, for the given bucket. This is
	//! the lowest value, that is in the bucket
	//!
	//! \returns The minimum value. This is the lowest value to be
	//! included in the bucket in question
	//!
	//! \param[in] bucketIndex The bucketIndex, of the bucket in
	//! question. Normal buckets have range [1, NumBuckets]. The
	//! underflow (time < min) bucket is always at 0, and the overflow
	//! (time >= max) is always at NumBuckets + 1
	double minBucket(natural64 bucketIndex) const noexcept
	{
		runtime_assert(bucketIndex < (NumBuckets + 2),
					   "The bucketIndex must not be greater than the "
					   "size of the bucket array");

		if (bucketIndex == 0)
		{
			// The underflow bucket has no minimum, so return -Inf
			return -std::numeric_limits<double>::infinity();
		} else if (bucketIndex > NumBuckets)
		{
			// Just to make sure that bucket indices out of range,
			// return the max value
			return m_MaxValue;
		}

		// transform the bucketIndex, to one that records the offset
		// from min. Then the result is the width of a single bucket,
		// compared to how many buckets from min, we are away
		return (bucketIndex - 1) * bucketWidth();
	}
	//! \brief Get the maximum value, for the given bucket. This is
	//! the lowest value, that is NOT in the bucket
	//!
	//! \returns The maximum value. This is the lowest value, to not
	//! be included in the bucket in question
	//!
	//! \param[in] bucketIndex The bucketIndex, of the bucket in
	//! question. Normal buckets have range [1, NumBuckets]. The
	//! underflow (time < min) bucket is always at 0, and the overflow
	//! (time >= max) is always at NumBuckets + 1
	double maxBucket(natural64 bucketIndex) const noexcept
	{
		runtime_assert(bucketIndex < (NumBuckets + 2),
					   "The bucketIndex must not be greater than the "
					   "size of the bucket array");

		// the maximum of one bucket, is the minimum of the next. This
		// is true for all buckets except the overflow bucket
		return (bucketIndex > NumBuckets)
				   ? std::numeric_limits<double>::infinity()
				   : minBucket(bucketIndex + 1);
	}

	//! \brief Reports the number of buckets, for this distribution.
	//! Will include the overflow buckets in the count, of requested
	//!
	//! \returns The numnber of buckets, which may include the
	//! overflow buckets, of which there are always 2
	//!
	//! \param[in] includeOverflow Should the overflow buckets be
	//! included in the count. There are always two overflow buckets
	natural64 numberBuckets(
		bool includeOverflow = false) const noexcept
	{
		return includeOverflow ? (NumBuckets + 2) : NumBuckets;
	}

	//! \brief Calculates the total number of laps that have been
	//! recorded
	//!
	//! \note Calculation requires iteration over all buckets, and is
	//! therefore not necessarily quick
	//!
	//! \returns The total number of laps, in the distribution
	//!
	//! \param[in] includeOverflow Should the overflow buckets be
	//! included in the count. There are always two overflow buckets
	natural64 numberLaps(bool includeOverflow = false) const noexcept
	{
		natural64 minIndex, maxIndex;

		if (includeOverflow)
		{
			minIndex = 0;
			maxIndex = NumBuckets + 1;
		} else
		{
			minIndex = 1;
			maxIndex = NumBuckets;
		}

		natural64 lapCount = 0;

		for (natural64 index = minIndex; index <= maxIndex; ++index)
		{
			lapCount += m_Buckets[index];
		}

		return lapCount;
	}

	//! \brief Get the count, of the number of times a lap time has
	//! fallen within the range of a given bucket
	//!
	//! \returns The number of laps that have fallen within the range
	//! of the specified bucket
	//!
	//! \param[in] bucketIndex The bucketIndex, of the bucket in
	//! question. Normal buckets have range [1, NumBuckets]. The
	//! underflow (time < min) bucket is always at 0, and the overflow
	//! (time >= max) is always at NumBuckets + 1
	natural64 countForBucket(natural64 bucketIndex) const noexcept
	{
		runtime_assert(bucketIndex < (NumBuckets + 2),
					   "The bucketIndex must not be greater than the "
					   "size of the bucket array");

		return m_Buckets[bucketIndex % (NumBuckets + 2)];
	}
};
}  // namespace Time
}  // namespace frao

#endif