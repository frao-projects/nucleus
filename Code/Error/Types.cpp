//! \file
//!
//! \brief Implementation of frao Error Types
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Error/Types.hpp"

namespace frao
{
inline namespace ErrorLogging
{
// BaseError functions
BaseError::BaseError(utf8string location, utf8string message)
	: m_ThrowLocation(std::move(location)),
	  m_Message(std::move(message)),
	  m_TimeStamp()
{}
//
utf8string BaseError::getWhere() const
{
	return m_ThrowLocation;
}
utf8string BaseError::getMessage() const
{
	return m_Message;
}
const ClockTime& BaseError::getTime() const
{
	return m_TimeStamp;
}
}  // namespace ErrorLogging
}  // namespace frao