//! \file
//!
//! \brief Implementation of time and timing functions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

//! \cond DOXYGEN_IGNORE

// To disable annoying macros that come with windows.h
// so that we don't get collisions on the name max()
#ifndef NOMINMAX
#define NOMINMAX
#endif

//! \endcond

#include "Time/PerformanceTime.hpp"
#include <cmath>  //for std::signbit

#if defined(_WIN32)	 // MSVC stdlib / Windows only

#include <Windows.h>  //for QueryPerformanceXXX functions

#elif defined(__linux__)

#include <ctime>  //for clock_gettime etc

#endif

#if defined(__linux)

namespace
{
//! \brief Private global which encodes the number nanoseconds per
//! second
constexpr static const frao::integer64 g_NsPerS = 1'000'000'000;
}  // namespace

#endif

namespace frao
{
inline namespace Time
{
namespace
{
//! \brief Get the high-performance, relative count. Returned count is
//! only meaningful in comparison. Logic is OS-dependent
//!
//! \warning If the function fails, it will return -1. On windows,
//! this wont ever happen. On linux it merely shouldn't
//!
//! \returns relative count. Absolute value is meaningless, and
//! returned values should be compared, for relative counts
integer64 getPerformanceCount_IMPL() noexcept
{
#if defined(_WIN32)

	integer64 counts;

	// function returns non-zero, if it succeeded
	BOOL success = QueryPerformanceCounter((LARGE_INTEGER*) &counts);

	// function should always succeed, on win XP or later
	runtime_assert(success == TRUE);

	return counts;

#elif defined(__linux__)

	timespec times;
	int		 res = clock_gettime(CLOCK_MONOTONIC_RAW, &times);

	integer64 result = -1;

	if (res != -1)
	{
		// the first time we call this function, get the base time
		static const time_t s_FirstTime = times.tv_sec;

		// This is the number of seconds we can safely store, given a
		// billion nanoseconds in a second, and the maximum positive
		// value we can store in our return type
		constexpr static const frao::integer64 maxSecs =
			0x7FFF'FFFF'FFFF'FFFF / g_NsPerS;

		time_t relativeSecs = times.tv_sec - s_FirstTime;

		// since we store the first time we call this function, we can
		// guarantee that the values will be meaningful for
		// (2^64)/(10^9) seconds. This is several hundred years
		result = times.tv_nsec % g_NsPerS;
		result += (relativeSecs % maxSecs) * g_NsPerS;
	}

	return result;

#else  // Not windows or linux, Calling function in wrong OS

	static_assert(false,
				  "Attempted to compile getPerformanceCount_IMPL() "
				  "function in unrecognised OS");

#endif
}

//! \brief turn a relative count into a time. Logic is OS-dependent
//!
//! \returns A time, in ms. Represents the time elapsed, during the
//! relativeCount
//!
//! \param[in] relativeCount The result of subtracting two counts,
//! which will be converted to a time
double translateRelativeCountToTime_IMPL(
	integer64 relativeCount) noexcept
{
#if defined(_WIN32)

	// on startup, we need to get the meaning of each count
	static auto getMSPerCount = []() -> double {
		integer64 countsPerSec;

		BOOL success =
			QueryPerformanceFrequency((LARGE_INTEGER*) &countsPerSec);
		runtime_assert(success
					   == TRUE);  // ON Win XP and later, function
								  // should ALWAYS succeed

		// it makes no sense to return 0, since that would mean that
		// the cpu isn't running?
		runtime_assert(countsPerSec != 0);

		return 1000.0 / static_cast<double>(countsPerSec);
	};

	static const double msPerCount = getMSPerCount();

	return static_cast<double>(relativeCount) * msPerCount;

#elif defined(__linux__)

	// count is already in nanoseconds, so just convert to ms
	return static_cast<double>(relativeCount) / 1'000'000.0;

#else  // Not windows or linux, Calling function in wrong OS

	static_assert(false,
				  "Attempted to compile DebugPauseExecution() "
				  "function in unrecognised OS");

#endif
}
}  // namespace

// RunningStats functions
RunningStats::RunningStats() noexcept
	: m_SampleMean(0.0), m_M2(0.0), m_Count(0)
{}
//
void RunningStats::update(double value) noexcept
{
	++m_Count;
	double preDelta = value - m_SampleMean;

	m_SampleMean += preDelta / static_cast<double>(m_Count);
	double postDelta = value - m_SampleMean;

	m_M2 += preDelta * postDelta;
}
//
natural64 RunningStats::sampleCount() const noexcept
{
	return m_Count;
}
//
double RunningStats::mean() const noexcept
{
	if (m_Count == 0)
	{
		// we don't actually have to return this - could return the
		// internal 0.0 value. But since we have no samples, we
		// consider the mean to have no value
		return std::numeric_limits<double>::quiet_NaN();
	}

	return m_SampleMean;
}
double RunningStats::standardError() const noexcept
{
	if (m_Count < 2)
	{
		return std::numeric_limits<double>::quiet_NaN();
	}

	return sampleStandardDeviation()
		   / std::sqrt(static_cast<double>(m_Count));
}
//
double RunningStats::sampleVariance() const noexcept
{
	if (m_Count < 2)
	{
		return std::numeric_limits<double>::quiet_NaN();
	}

	return m_M2 / static_cast<double>(m_Count - 1);
}
double RunningStats::populationVariance() const noexcept
{
	if (m_Count == 0)
	{
		return std::numeric_limits<double>::quiet_NaN();
	}

	return m_M2 / static_cast<double>(m_Count);
}
double RunningStats::sampleStandardDeviation() const noexcept
{
	return std::sqrt(sampleVariance());
}
double RunningStats::populationStandardDeviation() const noexcept
{
	return std::sqrt(populationVariance());
}

// BasicTimer functions
BasicTimer::BasicTimer() noexcept
	: m_StartTime(getPerformanceCount_IMPL())
{}
//
double BasicTimer::resetTimer() noexcept
{
	double result = getElapsedTime();

	m_StartTime = getPerformanceCount_IMPL();

	return result;
}
//
double BasicTimer::getElapsedTime() const noexcept
{
	integer64 countDiff = getPerformanceCount_IMPL() - m_StartTime;
	runtime_assert(countDiff >= 0,
				   "Time must move forwards! Timer used should be"
				   "monotonically increasing, but this assumption "
				   "appears to have been violated.");

	return translateRelativeCountToTime_IMPL(countDiff);
}


// SwitchTimer functions
SwitchTimer::SwitchTimer(bool startOn) noexcept
	: m_RunningTime(), m_UsedTime(startOn ? -0.0 : 0.0)
{}
//
double SwitchTimer::resetTimer(bool startOn) noexcept
{
	double result = getElapsedOnTime();

	m_UsedTime = startOn ? -0.0 : 0.0;
	m_RunningTime.resetTimer();

	return result;
}
//
double SwitchTimer::getElapsedOnTime() const noexcept
{
	double storedTime = m_UsedTime;

	if (isOn())
	{
		storedTime += m_RunningTime.getElapsedTime();
	}

	return storedTime;
}
//
void SwitchTimer::switchState(bool on) noexcept
{
	if (isOn() != on)
	{
		// then we have a change of state, to be executed
		m_UsedTime *= -1.0;

		if (on)
		{
			// we were in the OFF state ( <0 ), and have (just) moved
			// to the ON state. So we need to reset the timer, to
			// count this ON state
			m_RunningTime.resetTimer();
		} else
		{
			// we were in the ON state ( <0 ), and have (just) moved
			// to the off state. So we add the elapsed time, to the
			// recorded ON time
			m_UsedTime += m_RunningTime.getElapsedTime();
		}
	}
}
//
bool SwitchTimer::isOn() const noexcept
{
	// return ON, if the sign bit IS SET. eg: if the value is
	// negative, it is on
	return std::signbit(m_UsedTime);
}


// LapTimer functions
LapTimer::LapTimer() : m_Laps(), m_TotalTime()
{}
//
double LapTimer::resetTimer() noexcept
{
	m_Laps.clear();
	return m_TotalTime.resetTimer();
}
//
double LapTimer::startNextLap()
{
	double elapsedTime = m_TotalTime.getElapsedTime();
	m_Laps.emplace_back(elapsedTime);

	return elapsedTime;
}
//
natural64 LapTimer::numberLaps() const noexcept
{
	// sicne we record a lap at the end of a lap, the number of laps
	// will be the size of the lap record + 1
	return m_Laps.size() + 1;
}
//
double LapTimer::totalElapsedTime() const noexcept
{
	return m_TotalTime.getElapsedTime();
}
double LapTimer::timeUntilLap(natural64 lapIndex) const noexcept
{
	if ((lapIndex == 0) || (m_Laps.size() == 0))
	{
		// then we want the start of the forst lap. Which is always
		// 0.0
		return 0.0;
	} else if (lapIndex < numberLaps())
	{
		// normal case, of a lap that isn't the first, which has
		// actually started
		return m_Laps[lapIndex - 1];
	} else
	{
		runtime_assert(
			lapIndex == numberLaps(),
			"The specified lap must not be greater than the "
			"number of laps!");
		// we include this case, although it pertains to a lap that
		// hasn't started, so as to to prevent buffer overreads
		return m_TotalTime.getElapsedTime();
	}
}
double LapTimer::thisLapTime() const noexcept
{
	return m_TotalTime.getElapsedTime()
		   - ((m_Laps.size() != 0) ? m_Laps.back() : 0.0);
}
double LapTimer::elapsedLapTime(natural64 lapIndex) const noexcept
{
	// the elapsed time on a lap will be the difference between the
	// star of it's lap, and the next lap
	return timeUntilLap(lapIndex + 1) - timeUntilLap(lapIndex);
}

}  // namespace Time
}  // namespace frao
