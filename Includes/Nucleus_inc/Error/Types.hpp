#ifndef FRAO_BASIC_API_ERROR_TYPES
#define FRAO_BASIC_API_ERROR_TYPES

//! \file
//!
//! \brief Header of frao Error Types
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <vector>  //for std::vector
#include "../Strings.hpp"
#include "../Time.hpp"

namespace frao
{
//! \brief Inline namespace, used for Error types (for throwing), and
//! logging of those errors. Successful actions, or minor errors
//! (warnings) can also be logged
inline namespace ErrorLogging
{
//! \brief When determining whether an error is accepted for attempted
//! resolution, this enumeration is used. The enumeration signals
//! whether an error has been accepted, or if the functor to determine
//! such, has ceased to exist
enum class ErrorAcceptance
{
	//! \brief Used for when the associated resolution functor cannot
	//! be accessed anymore.
	NoFunctor,
	//! \brief Used for when an error such as the one specified,
	//! cannot be attempted to be resolved, using the associated
	//! resolution functor
	NotAccepted,
	//! \brief Used for when an error such as the one specified, can
	//! be attempted to be resolved, using the associated resolution
	//! functor
	Accepted
};
//! \brief When determining whether an error has been resolved, this
//! enumeration is used. It can also signal when the functor to
//! resolve the error, has ceased to exist
enum class ErrorResolution
{
	//! \brief Used for when the associated resolution functor cannot
	//! be accessed anymore.
	NoFunctor,
	//! \brief Used fror when the error has failed to be resolved
	Unresolved,
	//! \brief Used for when the error has been resolved
	Resolved
};

//! \brief Helper class, used to store and retreive a parameter pack
//! of parent types
//!
//! \tparam Parents A parameter pack of every parent type
template<typename... Parents>
struct NUCLEUS_LIB_API ParentHelper
{
	//! \brief using declaration, which is used to apply child type,
	//! parents, and additional supplied types, to a template
	//!
	//! \tparam Templ The template class, which takes a child type,
	//! and parameter pack of parents, onto which child type, parents,
	//! and ExtraParams should be applied
	//!
	//! \tparam ChildName The name of the type that is a child of all
	//! the parent types. Will be applied first, to the supplied
	//! template
	//!
	//! \tparam ExtraParams Any extra parameters that should be
	//! applied to the end of stored parameter pack, when passing
	//! types to the supplied template
	template<template<typename, typename...> class Templ,
			 class ChildName, typename... ExtraParams>
	using Apply = Templ<ChildName, Parents..., ExtraParams...>;
};

//! \brief Basic Error type, that can be used to catch all frao
//! errors, and which defines the error interface
class NUCLEUS_LIB_API BaseError
{
	//! \brief The location in the code, of the thrown error
	//! (function, etc.)
	utf8string m_ThrowLocation;
	//! \brief The message, attached to the thrown error
	utf8string m_Message;
	//! \brief The time, at which the error was thrown
	ClockTime m_TimeStamp;

   public:
	//! \brief Construct a BaseError, from a code location, and a
	//! message
	//!
	//! \param[in] location The location in the code, that emitted the
	//! error. Should be function name, and LOC, at least
	//!
	//! \param[in] message message A brief-ish message about what
	//! actually happened to cause things to go wrong. Or failing
	//! that, what actually broke, and how it did so.
	BaseError(utf8string location, utf8string message);
	//! \brief Copy construct a BaseError
	//!
	//! \param[in] rhs The object from which to create this BaseError
	BaseError(const BaseError& rhs) = default;
	//! \brief Move construct a BaseError
	//!
	//! \param[in] rhs The object from which to create this BaseError
	BaseError(BaseError&& rhs) noexcept = default;
	//! \brief Virtual destructor, so that derived objects correctly
	//! delete
	virtual ~BaseError() noexcept = default;

	//! \brief Copy assign a BaseError
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this BaseError
	BaseError& operator=(const BaseError& rhs) = default;
	//! \brief Move assign a BaseError
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this BaseError
	BaseError& operator=(BaseError&& rhs) = default;

	//! \brief get the location in the code, that the error happened
	//!
	//! \returns string with content that indicates the location of
	//! the error, in the code
	utf8string getWhere() const;

	//! \brief get the details of the error, wrt how it occurred /
	//! what broke etc.
	//!
	//! \returns string with content that was given to this object on
	//! creation, about the nature of the error
	utf8string getMessage() const;

	//! \brief get the Time that the original thrower object
	//! was created (ie: a 'Error occured at this time'
	//! record)
	//!
	//! \returns const reference to the stored frao::ClockTime
	//! object
	const ClockTime& getTime() const;

	//! \brief Get the name of the created error only (as opposed to
	//! error and all parents)
	//!
	//! \returns A string with the main error type name
	virtual utf8string mainName() const noexcept = 0;
	//! \brief Get the name of the created error, and all parents (and
	//! parents of parents, etc)
	//!
	//! \returns An array of the error name, and all parent error
	//! names
	virtual std::vector<utf8string> errorNames() const noexcept = 0;
};

//! \brief Catchable class, that can be used to catch any error type,
//! and all children error types
//!
//! \details This class will catch Errors of type ErrorType, as well
//! as any error types that list ErrorType as a parent
//!
//! \tparam ErrorType The actual error type
template<typename ErrorType>
class CatchableError : public virtual BaseError
{
   protected:
	//! \brief Exists solely to be called from Error, and hence does
	//! not call the base class constructor, since Error MUST
	CatchableError()
	{}

   public:
	//! \brief Copy construct a CatchableError
	//!
	//! \param[in] rhs The object from which to create this
	//! CatchableError
	CatchableError(const CatchableError& rhs) = default;
	//! \brief Move construct a CatchableError
	//!
	//! \param[in] rhs The object from which to create this
	//! CatchableError
	CatchableError(CatchableError&& rhs) noexcept = default;
	//! \brief Virtual destructor, so that derived objects correctly
	//! delete
	virtual ~CatchableError() noexcept override = default;

	//! \brief Copy assign a CatchableError
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this
	//! CatchableError
	CatchableError& operator=(const CatchableError& rhs) = default;
	//! \brief Move assign a CatchableError
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this
	//! CatchableError
	CatchableError& operator=(CatchableError&& rhs) = default;

	//! \brief Get an ascii character literal of the Error name
	//!
	//! \returns An ascii character literal, that contains the error's
	//! name
	static constexpr const charA* ownName() noexcept
	{
		return ErrorType::m_Name;
	}
};

//! \brief Declaration of Error generator type, which takes error
//! data, and the error data of its parents, and creates a real error
//! type.
//!
//! \tparam MainErrorData The struct that holds data for the actual
//! error type
//!
//! \tparam ParentData A parameter pack of structs that hold data for
//! 'MainErrorData's parent types
template<typename MainErrorData, typename... ParentData>
class Error;

//! \brief Alias template that is used to get the catchable type, from
//! a error data struct
//!
//! \tparam ErrorData The error data struct, whose catchable type we
//! require
template<typename ErrorData>
using catch_type = CatchableError<ErrorData>;

//! \brief Alias template that is used to get the createable type (for
//! throwing etc), from a error data struct
//!
//! \tparam ErrorData The error data struct, whose createable type we
//! require
template<typename ErrorData>
using create_type =
	typename ErrorData::parents::template Apply<Error, ErrorData>;

//! \brief Alias template that is used to get the correct inherited
//! type, from a error data struct
//!
//! \tparam ErrorData The error data struct, whose inherited type we
//! require
//!
//! \tparam ExtraParents Any extra parents that should also be
//! inherited from. Used to ensure that all 1st and 2nd order parents
//! (parents / grandparents) are correctly inherited from, without
//! 2nd order + parents having to be manually declared by the child
//! type
template<typename ErrorData, typename... ExtraParents>
using inherit_types =
	typename ErrorData::parents::template Apply<Error, ErrorData,
												ExtraParents...>;

//! \brief Specialisation of a Error generator type, for no parents
//!
//! \tparam MainErrorData The struct that holds data for the actual
//! error type
template<typename MainErrorData>
class Error<MainErrorData> : public virtual catch_type<MainErrorData>
{
   protected:
	//! \brief Exists solely to be called from Error, and hence does
	//! not call the base class / catch class constructors, since
	//! Error MUST
	Error()
		: BaseError(u8"", u8"")
	{}

   public:
	//! \brief The type that should be used to catch this error
	//! (remember to add the ref, to catch by ref)
	using catch_type = frao::catch_type<MainErrorData>;


	//! \brief Construct a CatchableError, from a code location, and a
	//! message
	//!
	//! \param[in] location The location in the code, that emitted the
	//! error. Should be function name, and LOC, at least
	//!
	//! \param[in] message message A brief-ish message about what
	//! actually happened to cause things to go wrong. Or failing
	//! that, what actually broke, and how it did so.
	Error(utf8string location, utf8string message)
		: BaseError(std::move(location), std::move(message)),
		  catch_type()
	{}
	//! \brief Copy construct an Error
	//!
	//! \param[in] rhs The object from which to create this Error
	Error(const Error& rhs) = default;
	//! \brief Move construct an Error
	//!
	//! \param[in] rhs The object from which to create this Error
	Error(Error&& rhs) noexcept = default;
	//! \brief Virtual destructor, so that derived objects correctly
	//! delete
	virtual ~Error() noexcept override = default;

	//! \brief Copy assign a Error
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this Error
	Error& operator=(const Error& rhs) = default;
	//! \brief Move assign a Error
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this Error
	Error& operator=(Error&& rhs) = default;

	//! \brief Get the name of the primary error
	//!
	//! \returns The name of the primary (top most / orginally
	//! created) error
	virtual utf8string mainName() const noexcept override
	{
		return utf8string(catch_type::ownName());
	}
	//! \brief Get an array of the primary error, and all parents,
	//! name.
	//!
	//! \note May contain duplicate error names
	//!
	//! \returns An array, that contains the name of the primary
	//! error, and all parents
	virtual std::vector<utf8string> errorNames() const
		noexcept override
	{
		return std::vector<utf8string>{catch_type::ownName()};
	}
};

//! \brief Specialisation of a Error generator type, for 1+ parents
//!
//! \note The type is designed to ensure that it inherits from the
//! catchable types of every parent (and grandparent etc) error, in
//! addition to that of this error
//!
//! \tparam MainErrorData The struct that holds data for the actual
//! error type
//!
//! \tparam ParentData The struct that holds data for the first parent
//!
//! \tparam OtherParents The parameter pack struct that holds data for
//! any subsequent parents
template<typename MainErrorData, typename ParentData,
		 typename... OtherParents>
class Error<MainErrorData, ParentData, OtherParents...>
	: public virtual inherit_types<ParentData, OtherParents...>,
	  public virtual catch_type<MainErrorData>
{
   public:
	//! \brief The type that should be used to catch this error
	//! (remember to add the ref, to catch by ref)
	using catch_type = frao::catch_type<MainErrorData>;
	//! \brief The type that this error type will inherit from
	using inherit_types =
		frao::inherit_types<ParentData, OtherParents...>;

   protected:
	//! \brief Exists solely to be called from Error, and hence does
	//! not call the base class / catch class constructors, since
	//! Error MUST
	Error() : inherit_types()
	{}

   public:
	//! \brief Construct a CatchableError, from a code location, and a
	//! message
	//!
	//! \param[in] location The location in the code, that emitted the
	//! error. Should be function name, and LOC, at least
	//!
	//! \param[in] message message A brief-ish message about what
	//! actually happened to cause things to go wrong. Or failing
	//! that, what actually broke, and how it did so.
	Error(utf8string location, utf8string message)
		: BaseError(std::move(location), std::move(message)),
		  inherit_types(),
		  catch_type()
	{}
	//! \brief Copy construct an Error
	//!
	//! \param[in] rhs The object from which to create this Error
	Error(const Error& rhs) = default;
	//! \brief Move construct an Error
	//!
	//! \param[in] rhs The object from which to create this Error
	Error(Error&& rhs) noexcept = default;
	//! \brief Virtual destructor, so that derived objects correctly
	//! delete
	virtual ~Error() noexcept override = default;

	//! \brief Copy assign a Error
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this Error
	Error& operator=(const Error& rhs) = default;
	//! \brief Move assign a Error
	//!
	//! \returns A reference to this, for chaining
	//!
	//! \param[in] rhs The object from which to assign this Error
	Error& operator=(Error&& rhs) = default;

	//! \brief Get the name of the primary error
	//!
	//! \returns The name of the primary (top most / orginally
	//! created) error
	virtual utf8string mainName() const noexcept override
	{
		return utf8string(catch_type::ownName());
	}
	//! \brief Get an array of the primary error, and all parents,
	//! name.
	//!
	//! \note May contain duplicate error names
	//!
	//! \returns An array, that contains the name of the primary
	//! error, and all parents
	virtual std::vector<utf8string> errorNames() const
		noexcept override
	{
		auto names = inherit_types::errorNames();
		names.emplace_back(catch_type::ownName());
		return names;
	}
};

//====================
// Generic Error Types
//====================
// Error types that have no parents. These are basic classes of
// errors, that happen in basic code everywhere.
//====================

//! \brief 'Error' type that is used for when things successfully
//! execute. We need this so that the log can store important
//! successful actions as well as failures
struct NUCLEUS_LIB_API Success
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Success";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error type that is used when the process cannot, for
//! whatever reason, allocate more memory. This is usually an
//! extremely serious error, and is normally fatal to the program
struct NUCLEUS_LIB_API MemAlloc
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Memory Allocation";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error that is used when something strange has gone wrong.
//! That is, cases where it is not entirely clear what the error is,
//! just that there is one
struct NUCLEUS_LIB_API Unknown
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Unknown";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for failures of conversion, between different types
//! of objects / data
struct NUCLEUS_LIB_API ConversionFailure
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Conversion Failure";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for when a required action of the program / API is
//! simply not possible, because of dificiencies in the hardware or
//! the program environment. That is, something sufficiently
//! inalterable, that it cannot ever be conceivably resolved in
//! program
struct NUCLEUS_LIB_API SpecNotSupported
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Spec Not Supported";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for when a failure is experienced during the process
//! of destroying something. When it just refuses to die. For example,
//! a failure in deleting a file directory might raise this error.
struct NUCLEUS_LIB_API DestructionFailure
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Destruction Failure";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for when something happens that prevents correct text
//! formatting / rendering. When data cannot be read, that details
//! glyph placement, for example
struct NUCLEUS_LIB_API TextFormatting
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Text Formatting";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//========================
// Acquisition Error Types
//========================
// Error types for when a function tries to find or get a piece of
// data or a resource, and fails
//========================

//! \brief Error for when something cannot be found or obtained. Also
//! a base error for more specific cases
struct NUCLEUS_LIB_API AcquisitionFailure
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Acquisition Failure";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for when a resource or some data is expected, but
//! simply cannot be located in the expected location
struct NUCLEUS_LIB_API NotFound
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Not Found";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<AcquisitionFailure>;
};

//! \brief Error for when there is a failure in that actual obtaining
//! of a resource. Say, a file, for example
struct NUCLEUS_LIB_API ResourceAcquisition
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Resource Acquisition";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<AcquisitionFailure>;
};

//! \brief Error for when some data cannot be obtained from somewhere.
//! For example, it might be thrown when a user passes a null buffer
//! to a function expecting that the buffer will never be empty
struct NUCLEUS_LIB_API DataAcquisition
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Data Acquisition";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<AcquisitionFailure>;
};

//==========================
// Invalid usage Error Types
//==========================
// Error types for when a function is asked to do something that is
// inappropriate for that function (or just in general). ie: if a user
// has entered some input in a way that is incomprehensible or just
// plain wrong
//==========================

//! \brief Error for when a user calls a function at a time, or in a
//! fashion that doing so is simply not valid, or when a user supplies
//! a function with invalid parameters. Also a base type for more
//! specific cases
struct NUCLEUS_LIB_API InvalidFunctionCall
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Invalid Function Call";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for circumstances in which the input to a function
//! (or anything else), is simply invalid or incorrect the given
//! circumstances. For example, passing a nullptr to a function that
//! expects it will never receive one
struct NUCLEUS_LIB_API InvalidUserInput
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Invalid User Input";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InvalidFunctionCall>;
};

//! \brief Error for when, during the process of attempting to read a
//! file, some data in the file is invalid for the given location in
//! the file. For example, if the next thing in the file is expected
//! to be a number between 1 and 10, and the content at that location
//! is 15, this error may be rasied
struct NUCLEUS_LIB_API InvalidFileFormat
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Invalid File Format";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InvalidFunctionCall>;
};

//! \brief Error for when many solicitations are made of a given
//! system, such that said system is unable to service all of them.
//! For example, asking for too much async File IO at once may raise
//! this error
struct NUCLEUS_LIB_API TooManyRequests
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Too Many Requests";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InvalidFunctionCall>;
};

//============================
// AccessViolation Error Types
//============================
// Error types for when a function attempts to access something in a
// location that that function does not / should not have access to
//============================

//! \brief Error for when something tries to access something that
//! they aren't allowed to have / touch / look at. I'd gve an example,
//! but everyone knows what it looks like when someone / something
//! tries to access something that they haven't been given permission
//! to access. May be a base class for more specialised error types
struct NUCLEUS_LIB_API AccessViolation
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Access Violation";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for trying to access more things than you are allowed
//! to access. For example, attempting to access, via an array,
//! something outside of that array's bounds
struct NUCLEUS_LIB_API OutsideBounds
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Outside Bounds";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<AccessViolation>;
};


//===========================
// Initialisation Error Types
//===========================
// Error types for when something initialise as expected / at all
// often used for windows subsystems ie: direct3d, direct2d not
// initialising correctly
//===========================

//! \brief Error for when there is an error during initialisation,
//! that prevents something from being correctly initialised /
//! created. May be a base class for more specific types of error
struct NUCLEUS_LIB_API InitialisationFailure
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "InitialisationFailure";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<>;
};

//! \brief Error for when something cannot be created, that was
//! expected to be
struct NUCLEUS_LIB_API CreationFailure
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Creation Failure";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InitialisationFailure>;
};

//! \brief Error failures of windows startup code. For example,
//! failures of window cretion
//!
//! \todo This class is quite domain specific, for an error
//! type. We should consider changing this to indicate what
//! *happened* rather than indicating what domain the error
//! occurred in
struct NUCLEUS_LIB_API WinStartUp
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Windows Start Up";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InitialisationFailure>;
};

//! \brief Error for failures when setting up basic Direct3D
//! structures / processes. For example, a failure to create a render
//! target might raise this error
//!
//! \todo This class is quite domain specific, for an error
//! type. We should consider changing this to indicate what
//! *happened* rather than indicating what domain the error
//! occurred in
struct NUCLEUS_LIB_API Direct3DStart
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Direct3D Start";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InitialisationFailure>;
};

//! \brief Error for failures when setting basic Direct2D structures /
//! processes
//!
//! \todo This class is quite domain specific, for an error
//! type. We should consider changing this to indicate what
//! *happened* rather than indicating what domain the error
//! occurred in
struct NUCLEUS_LIB_API Direct2DStart
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Direct2D Start";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InitialisationFailure>;
};

//! \brief Error for failures when setting up the ribbon UI of a
//! windows application
//!
//! \todo This class is quite domain specific, for an error
//! type. We should consider changing this to indicate what
//! *happened* rather than indicating what domain the error
//! occurred in
struct NUCLEUS_LIB_API RibbonStart
{
	//! \brief A string literal, containing the name of the error
	static constexpr const charA* m_Name = "Ribbon Start";
	//! \brief A helper type, used to store the list of parent error
	//! types
	using parents = ParentHelper<InitialisationFailure>;
};
}  // namespace ErrorLogging
}  // namespace frao


#endif  //! FRAO_BASIC_API_ERROR_TYPES