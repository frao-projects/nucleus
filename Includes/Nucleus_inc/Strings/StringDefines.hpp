#ifndef FRAO_NUCLEUS_STRINGS_IMPL_DEFINES
#define FRAO_NUCLEUS_STRINGS_IMPL_DEFINES

//! \file
//!
//! \brief Header of basic definitions, required for the string API
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <cstdint>	  //for WCHAR_MAX macro
#include <exception>  //for std::exception
#include "../Environment.hpp"//for dll export api

// experimental / transitional flag
namespace frao
{
inline namespace Strings
{
//! \brief constant compile-time variable, which will be used to
//! determine whether the Experimental can be used at all
constexpr const bool g_Experimental_Strings_Allowed
#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
	= true;
#else
	= false;
#endif
//! \brief constant compile-time variable, which will be used to
//! determine whether the Experimental or Transitional string API
//! should be used by default
constexpr const bool g_Experimental_Strings_By_Default
#ifdef FRAO_EXPERIMENTAL_STRINGS_DEFAULT
	= true && g_Experimental_Strings_Allowed;
#else
	= false;
#endif

//! \brief We need to determine the size of a wchar, so that we can
//! select types appropriately
constexpr const bool g_WChar_Large
#if WCHAR_MAX >= 0x10000
#define FRAO_WCHAR_LARGE
	= true;
#else
#define FRAO_WCHAR_SMALL
	= false;
#endif

// Disable c4275 for inheriting from non-exporting classes, since the class in question is a
// standard library class
DISABLE_VC_WARNING(4275)

//! \struct conversion_failure
//!
//! \brief For strings classes (BasicAPI/Strings) we don't
//! yet have access to our exception system, but we need
//! exceptions nonetheless (for failures in conversion). So,
//! this struct is for when we have those failures in
//! conversion that need to throw
struct NUCLEUS_LIB_API conversion_failure : public std::exception
{
	explicit conversion_failure()
		: std::exception()
	{}

	const char* what() const noexcept override
	{
		return "Conversion Error!";
	}
};

//! \struct invalid_input
//!
//! \brief For strings classes (BasicAPI/Strings) we don't
//! yet have access to our exception system, but we need
//! exceptions nonetheless (for invalidly specified strings). So,
//! this struct is for when we have those invalidly specified strings
struct NUCLEUS_LIB_API invalid_input : public std::exception
{
	explicit invalid_input() : std::exception()
	{}

	const char* what() const noexcept override
	{
		return "Invalid input Error!";
	}
};

//We no longer need to suppress the inheriting from non-exported function warning
RESTORE_VC_WARNING(4275)

}  // namespace Strings
}  // namespace frao
#endif