#ifndef FRAO_BASIC_API_ERROR_ORGANISATION
#define FRAO_BASIC_API_ERROR_ORGANISATION

//! \file
//!
//! \brief Header of frao Error Organisational structures
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <list>			 //for std::list
#include <memory>		 //for std::shared_ptr
#include <mutex>		 //for std::unique_lock
#include <new>			 //for std::bad_alloc
#include <shared_mutex>  //for std::shared_lock
#include <tuple>		 //for std::tuple
#include "Stack.hpp"
#include "Types.hpp"
#include "../Strings.hpp"

namespace frao
{
inline namespace ErrorLogging
{
//! \brief Is used to specify whether to repeated errors of
//! the same type should be amalgamed into one log entry,
//! with a count of how many times that error occurred
enum class ErrorMatchingMode
{
	//! \brief Do not amalgamate errors of the same type
	Disabled,
	//! \brief Amalgamate errors that have the same type, regardless
	//! of message, ErrorStatus and call location
	Coarse,
	//! \brief Amalgamate errors that have the same type and
	//! ErrorStatus, regardless of message and call location
	Rough,
	//! \brief Amalgamate errors that have the same type / error
	//! status / call location, regardless of message
	//!
	//! \note Since call location uniquely identifies a particular
	//! error, this mode can be useful if the error message does not
	//! carry information, that differs from call to call
	CallLocation,
	//! \brief Amalgamate errors only if message, status, and type
	//! match
	Strict
};

//! \brief Is used to indicate the status of an error, and whether it
//! has been resolved
enum class ErrorStatus
{
	//! \brief Used to describe something which, although described by
	//! an Error object, is not actually something which has gone
	//! wrong. eg. Success Error type, and any similar
	NotAnError,
	//! \brief Used to describe minor errors, which may not end up
	//! being a real issue
	Warning,
	//! \brief Used to describe ongoing and serious errors
	Active,
	//! \brief Used to describe errors which have been dealt with
	Resolved
};

// So the compiler won't complain about using STL containers in the interface. Obviously
// doing this can cause problems, esp. if we use a different compiler / flags for compilation
// of dll / consumer of dll, but it should mostly work, if we can keep those straight. Which
// our build system should anyway
DISABLE_VC_WARNING(4251)

//! \brief Class for receiving and logging errors
class NUCLEUS_LIB_API ErrorLog
{
	//! \brief List of error pairs. first member added error, second
	//! member how many times that error has been added, third member
	//! the status of the error
	//!
	//! \note this MUST be a list, so that iterators are not
	//! invalidated, unless what they point to is removed
	//!
	//! \invariant Will never be empty
	std::list<std::tuple<std::shared_ptr<BaseError>, natural64,
						 ErrorStatus>>
		m_ErrorList;
	//! \brief Deque of ResolveReferrer weak reference objects, which
	//! functions as a stack of error resolvers
	//!
	//! \note this MUST be a list, so that iterators are not
	//! invalidated, unless what they point to is removed
	std::list<ResolveReferrer> m_ResolveStack;

	//! \brief Name of the file that shall receive the log
	utf8string m_LogFileName;

	//! \brief Used to ensure only one thread can write at a time, and
	//! writes and reads don't take place simultaneously. Multiple
	//! reads is obviously fine
	//!
	//! \note mutable because const functions obviously have to
	//! acquire the mutex!
	mutable std::shared_mutex m_WriteMutex;

	//! \brief error list iterator to one past the last logged error,
	//! that has been output to the log file
	//!
	//! \note will point to end(), if all current errors have been
	//! output
	decltype(m_ErrorList)::iterator m_OnePastLastLoggedError;

	//! \brief iterator that points to the error that is considered
	//! the most recent active error
	//!
	//! \note Because we use a list to hold errors, this iterator is
	//! always valid, as long as the error still exists
	decltype(m_ErrorList)::iterator m_ActiveError;

	//! \brief distance of error match check
	//!
	//! \details When matching (amalgamating) errors of the same type,
	//! how many far through the log should we check? Denotes the
	//! number of errors to check (from the most recent error in the
	//! log)
	natural64 m_CheckBack;

	//! \brief Do we already have a file, and we need to append to it,
	//! instead of overwriting?
	bool m_AppendToFile;

	//! \brief Get a string of the text to write, to the log
	//!
	//! \returns The string, which is to be written to the log file
	utf8string getLogToWrite() const;

	//! \brief Add an error, to the list, automatically matching prior
	//! errors and setting the active error.
	//!
	//! \param[in] error The error to add to the log
	//!
	//! \param[in] mode The mode of the error to add to the log
	//!
	//! \param[in] status The status of the error to add to the log
	void addErrorIMPL(std::shared_ptr<BaseError> error,
					  ErrorMatchingMode mode, ErrorStatus status);

   public:
	//! \brief Helper typedef, for the errorlist iterator type
	using iterator = decltype(m_ErrorList)::iterator;
	//! \brief Helper typedef, for the errorlist const_iterator type
	using const_iterator = decltype(m_ErrorList)::const_iterator;
	//! \brief Helper typedef, for the errorlist reverse_iterator type
	using reverse_iterator = decltype(m_ErrorList)::reverse_iterator;

	//! \brief Create an empty (except for 'log created' message) log
	//!
	//! \param[in] logPath The path of the log file, to write errors
	//! to
	//!
	//! \param[in] checkBack How far back the error-matching check
	//! should go
	ErrorLog(utf8string logPath = "log.txt", natural64 checkBack = 5);
	//! \brief standard defaulted copy constructor
	//!
	//! \param[in] rhs The ErrorLog to copy from
	ErrorLog(const ErrorLog& rhs);
	//! \brief standard defaulted move constructor
	//!
	//! \param[in] rhs The ErrorLog to move from
	ErrorLog(ErrorLog&& rhs);

	//! \brief **Copy assignment operator has been deleted, and is
	//! unusable**
	ErrorLog& operator=(const ErrorLog&) = delete;
	//! \brief **Move assignment operator has been deleted, and is
	//! unusable**
	ErrorLog& operator=(ErrorLog&&) = delete;

	//! \brief Attempt to resolve the given error, using the resolve
	//! stack.
	//!
	//! \note This function does not touch the log at all. Use
	//! addError or similar for a function that logs as well
	//!
	//! \returns The status of the error resolution attempt. ie:
	//! whether it succeeded
	//!
	//! \param[in] error The error to attempt to resolve
	ErrorResolution justResolve(BaseError& error);

	//! \brief Creates an ErrorResolver object, from the functors that
	//! shall make it up
	//!
	//! \returns An ErrorResolver object, to be placed in the scope
	//! that we wish to have its error resolution function
	//!
	//! \param[in] filter The functor that shall determine if a given
	//! functor can be attempted to be resolved, using the resolution
	//! functor
	//!
	//! \param[in] handler The functor that shall attempt to resolve
	//! the passed errors
	ErrorResolver addErrorResolver(
		std::function<ErrorAcceptance(BaseError&)> filter,
		std::function<ErrorResolution(BaseError&)> handler);

	//! \brief adds an error to the log, of Type ErrType
	//!
	//! \tparam ErrType The type of the error that should be
	//! created
	//!
	//! \param[in] location String that denotes the location in the
	//! code, of the error. Passed to the constructor of the specified
	//! ErrorType, for the 'location' parameter
	//!
	//! \param[in] message String that holds the error message,
	//! describing the circumstances of the error. Passed to the
	//! constructor of the specified ErrorType, for the 'message'
	//! parameter
	//!
	//! \param[in] status What is the status of the error? Is it a
	//! warning? Should the error be considered active?
	//!
	//! \param[in] mode Whether to amalgamate similar errors
	//!
	//! \exception MemAlloc throws if we cannot allocate
	//! memory for the error, for any reason
	template<typename ErrType>
	void addError(utf8string		location,
				  utf8string		message			 = utf8string(),
				  bool				attemptToResolve = false,
				  ErrorStatus		status = ErrorStatus::Warning,
				  ErrorMatchingMode mode   = ErrorMatchingMode::Rough)
	{
		try
		{
			auto error = std::make_shared<create_type<ErrType>>(
				std::move(location), std::move(message));

			// if we're trying to resolve the error, before adding it,
			// do so
			if (attemptToResolve
				&& (justResolve(*error) == ErrorResolution::Resolved))
			{
				status = ErrorStatus::Resolved;
			}

			addErrorIMPL(std::move(error), mode, status);
		} catch (std::bad_alloc&)
		{
			// we cannot add, so just throw the new error
			throw create_type<MemAlloc>(FILELOC_UTF8, u8""_ustring);
		}
	}

	//! \brief create an error, add it to the error handler (as
	//! the active error), and then return it (to be thrown)
	//!
	//! \tparam ErrType The type of the error that should be
	//! created
	//!
	//! \returns An Error<ExceptionType> object, created using the
	//! supplied information, and added to the ErrorLog
	//!
	//! \param[in] location String that denotes the location in the
	//! code, of the error. Passed to the constructor of the specified
	//! ErrorType, for the 'location' parameter
	//!
	//! \param[in] message String that holds the error message,
	//! describing the circumstances of the error. Passed to the
	//! constructor of the specified ErrorType, for the 'message'
	//! parameter
	//!
	//! \param[in] mode Whether to amalgamate similar errors
	//!
	//! \exception MemAlloc throws if we cannot allocate
	//! memory for the error, for any reason
	template<typename ExceptionType>
	create_type<ExceptionType> createException(
		utf8string location, utf8string message,
		ErrorMatchingMode mode = ErrorMatchingMode::Rough)
	{
		addError<ExceptionType>(location, message, false, ErrorStatus::Active, mode);

		return create_type<ExceptionType>(std::move(location),
										  std::move(message));
	}

	//! \brief Tries to resolve the given error, and if it manages to
	//! resolve the error, amend the status of the error in the log,
	//! to resolved
	//!
	//! \returns A boolean indicating whether the error could be
	//! resolved
	//!
	//! \param[in] error reference to the error that should be
	//! attempted to be resolved
	bool resolveError(BaseError& error);

	//! \brief finds the specified error in the log, if it
	//! exists. If the error is found, it will decrement
	//! that error's count, and once that error has a count
	//! of 0, it will be removed, as if it had never
	//! existed. (Error count only applies if amalgamating
	//! errors. If errors are not being amalgamated, then
	//! this function will remove the error, if found)
	//!
	//! \param[in] error const reference to the error that
	//! should be replaced
	void removeError(const BaseError& error);

	//! \details will (attempt to) log all errors to the log
	//! file (on disk), that have not already been written, using std
	//! IO (for simplicity, and because the frao Error system is a
	//! dependency of the frao File system, the frao file system is
	//! not used to log errors)
	void logErrors();

	//! \brief will clear the stored log. **will not clear
	//! log on disk!**
	void clearLog();

	//! \brief gets last error in the log!
	//!
	//! \returns iterator to last error in the log
	iterator getLastError();

	//! \brief gets last error in the log!
	//!
	//! \returns const_iterator to last error in the log
	const_iterator getLastError() const;

	//! \brief gets active error in the log!
	//!
	//! \returns iterator to active error in the log
	iterator getActiveError();

	//! \brief gets active error in the log!
	//!
	//! \returns const_iterator to active error in the log
	const_iterator getActiveError() const;

	//! \brief Gets the earliest error, that has not already been
	//! written to the log file. Will equal end, if all errors ahve
	//! been written to the log file
	//!
	//! \returns An iterator to the next element, that should be
	//! written to the log file
	iterator getNextErrorToLog() const;

	//! \brief Get an iterator to the first stored error, in the list
	//!
	//! \returns An iterator of the first stored error, in the list
	const_iterator begin() const;
	//! \brief Get an iterator to the end of the list. That is, once
	//! past the last element in it
	//!
	//! \returns An iterator to one past the last error in the list
	const_iterator end() const;

	//! \brief set which error should be considered the
	//! 'active' error
	//!
	//! \param[in] iter an iterator (wrt this log) that
	//! points to the error that should be considered active
	void setActiveError(iterator iter);
};

//retore disabled c4251 warning, for using non-exporting class in interface
RESTORE_VC_WARNING(4251)
}  // namespace ErrorLogging
}  // namespace frao

#endif  //! FRAO_BASIC_API_ERROR_ORGANISATION