﻿#ifndef FRAO_BASIC_API_STRINGS_GRAPHEMECLUSTER
#define FRAO_BASIC_API_STRINGS_GRAPHEMECLUSTER

//! \file
//!
//! \brief Header of the Interface of unicode
//! grapheme cluster class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <type_traits>	//for std::conditional
#include "Experimental/GraphemeClusterIMPL_Expr.hpp"
#include "StringDefines.hpp"  //for default string type variable
#include "Transitional/GraphemeClusterIMPL_Trans.hpp"

namespace frao
{
inline namespace Strings
{
// tell the compiler that we will define these elsewhere
NUCLEUS_HEADER_EXTERN template class Transitional::
	GraphemeClusterIMPL<charU8>;
NUCLEUS_HEADER_EXTERN template class Transitional::
	GraphemeClusterIMPL<wchar_t>;
NUCLEUS_HEADER_EXTERN template class Transitional::
	GraphemeClusterIMPL<char16_t>;
NUCLEUS_HEADER_EXTERN template class Transitional::
	GraphemeClusterIMPL<char32_t>;
#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
NUCLEUS_HEADER_EXTERN template class Experimental::
	GraphemeClusterIMPL<charU8>;
NUCLEUS_HEADER_EXTERN template class Experimental::
	GraphemeClusterIMPL<wchar_t>;
NUCLEUS_HEADER_EXTERN template class Experimental::
	GraphemeClusterIMPL<char16_t>;
NUCLEUS_HEADER_EXTERN template class Experimental::
	GraphemeClusterIMPL<char32_t>;
#endif

//! \brief Given a null-terminated string, determine it's length,
//! in number of characters
//!
//! \returns A number euqal to the number of characters checked,
//! before the null terminator was found
//!
//! \param[in] string The null-terminated string to scan for the
//! null-terminator
template<typename OurCharType>
static natural64 getBufferLength(const OurCharType* string) noexcept
{
	frao::natural64 stringEnd = 0;
	while (string[stringEnd] != static_cast<OurCharType>('\0'))
	{
		++stringEnd;
	}

	return stringEnd;
}

//! \brief determine if the next grapheme cluster is whitespace or not
//!
//! \tparam CharType The character type, of the grapheme cluster for
//! which we wish to establish whitespeace-ness
//!
//! \tparam UseExperimental If true, will use the experimental routine
//!
//! \returns bool, with value true iff the next grapheme cluster is
//! whitespace
//!
//! \param[in] character string containing at least one grapheme
//! cluster
//!
//! \param[in] buffersize unsigned 64-bit integer that indicates the
//! size of the buffer referred to in 'character'
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
bool isWhitespace(const CharType* character,
				  frao::natural64 buffersize) noexcept
{
	if constexpr (UseExperimental && g_Experimental_Strings_Allowed)
	{
		return Experimental::isWhitespaceIMPL_E(character,
												buffersize);
	} else
	{
		return Transitional::isWhitespaceIMPL(character, buffersize);
	}
}

//! \brief determine if the next grapheme cluster in a string is blank
//! or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined as blank
//!
//! \param[in] string A pointer to a string, of specified size, that
//! contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
bool isBlank(const CharType* string,
			 frao::natural64 bufferSize) noexcept
{
	if constexpr (UseExperimental && g_Experimental_Strings_Allowed)
	{
		return Experimental::isBlankIMPL_E(string, bufferSize);
	} else
	{
		return Transitional::isBlankIMPL(string, bufferSize);
	}
}

//! \brief determine if the next grapheme cluster is a newline or not
//!
//! \tparam CharType The character type, of the grapheme cluster for
//! which we wish to establish newline-ness
//!
//! \tparam UseExperimental If true, will use the experimental routine
//!
//! \returns bool, with value true iff the next grapheme cluster is a
//! newline
//!
//! \param[in] string string containing at least one grapheme
//! cluster
//!
//! \param[in] buffersize unsigned 64-bit integer that indicates the
//! size of the buffer referred to in 'character'
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
bool isNewline(const CharType* string,
			   frao::natural64 buffersize) noexcept
{
	if constexpr (UseExperimental && g_Experimental_Strings_Allowed)
	{
		return Experimental::isNewlineIMPL_E(string, buffersize);
	} else
	{
		return Transitional::isNewlineIMPL(string, buffersize);
	}
}

//! \brief determine if the next grapheme cluster is a numeric or not
//!
//! \tparam CharType The character type, of the grapheme cluster for
//! which we wish to establish numeric-ness
//!
//! \tparam UseExperimental If true, will use the experimental routine
//!
//! \returns bool, with value true iff the next grapheme cluster is
//! numeric
//!
//! \param[in] character string containing at least one grapheme
//! cluster
//!
//! \param[in] buffersize unsigned 64-bit integer that indicates the
//! size of the buffer referred to in 'character'
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
bool isNumeric(const CharType* character,
			   frao::natural64 buffersize) noexcept
{
	if constexpr (UseExperimental && g_Experimental_Strings_Allowed)
	{
		return Experimental::isNumericIMPL_E(character, buffersize);
	} else
	{
		return Transitional::isNumericIMPL(character, buffersize);
	}
}

namespace IMPL
{
//! \brief template for possible base classes of GraphemeCluster.
//! Dispatches to experimental or transtional base class as
//! appropriate. Used so that we don't have to whole conditional
//! statement every time we need to refer to another possible base
//! class
//!
//! \note Has to be outside of GraphemeCluster, so that the class can
//! use it to specify its own base class
template<typename CharType_templ, bool UseExperimental>
using ClusterBaseClass_template = std::conditional_t<
	UseExperimental && g_Experimental_Strings_Allowed,
	Experimental::GraphemeClusterIMPL<CharType_templ>,
	Transitional::GraphemeClusterIMPL<CharType_templ>>;

// Hack to get the base class
// unimplemented to make sure it's only used
// in unevaluated contexts (sizeof, decltype, alignof)
template<class T, class U>
T base_of(U T::*);
}  // namespace IMPL

//! \class GraphemeCluster
//!
//! \brief Class to encapsulate the Unicode Grapheme Cluster
//! concept. A grapheme cluster is, distinct from a
//! character in the programming sense, what a user would
//! perceieve as one character. This may include diacritic
//! marks, for example 'Å' could be written 'Å' or 'A' and then ' ̊ '
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
class NUCLEUS_LIB_API GraphemeCluster final
	: public IMPL::ClusterBaseClass_template<CharType,
											 UseExperimental>
{
	//! \brief using declaration for the base class of this one. Used
	//! for simplicity of referring to it.
	using BaseClass =
		IMPL::ClusterBaseClass_template<CharType, UseExperimental>;
	// using BaseClass =
	// decltype(IMPL::base_of(&GraphemeCluster::sizeIMPL));

   public:
	template<typename OtherType, bool OtherUseExperimental>
	friend class GraphemeCluster;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ThisType = GraphemeCluster<CharType, UseExperimental>;

	//! \brief alias template, so that it is clear(er) that
	//! transitional and experimental implementations are mutually
	//! incompatible
	//!
	//! \tparam OtherType The character type of the similiar type
	template<typename OtherType>
	using SimilarType = GraphemeCluster<OtherType, UseExperimental>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = CodePoint<CharType, UseExperimental>;

	//! \brief construct empty grapheme cluster
	GraphemeCluster() : BaseClass()
	{}
	//! \brief Construct a grapheme cluster, from the specified
	//! string. Will get either the first or last grapheme cluster in
	//! the string, depending on the value of getLastCluster
	//!
	//! \param[in] cluster string buffer that conatains at
	//! least one cluster. Must not be null
	//!
	//! \param[in] buffersize size of the buffer used for
	//! the string in 'cluster'. Must not be 0
	//!
	//! \param[in] getLastCluster iff true, will scan for the last
	//! valid grapheme cluster in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct grapheme cluster to read. If null, will not be written
	//! to
	GraphemeCluster(const CharType* cluster,
					frao::natural64 buffersize,
					bool			getLastCluster = false,
					natural64*		readStart	   = nullptr)
		: BaseClass(cluster, buffersize, getLastCluster, readStart)
	{}
	//! brief Construct a grapheme cluster, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8/16/32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	GraphemeCluster(charA asciiChar) : BaseClass(asciiChar)
	{}

	//! \brief Copy construct a GraphemeCluster, from another
	//! GraphemeCluster of any underlying type that differs from that
	//! of this
	//!
	//! \tparam OtherType The type of the other GraphemeCluster, that
	//! we are copying from. One of [charU8, char16_t, wchar_t,
	//! char32_t]
	//!
	//! \param[in] rhs The GraphemeCluster to copy from
	template<typename OtherType>
	GraphemeCluster(const SimilarType<OtherType>& rhs)
		: BaseClass(static_cast<const IMPL::ClusterBaseClass_template<
						OtherType, UseExperimental>&>(rhs))
	{}

	//! \brief standard copy constructor
	//!
	//! \param[in] rhs grapheme cluster to construct from
	GraphemeCluster(const ThisType& rhs) : BaseClass(rhs)
	{}

	//! \brief standard move constructor
	//!
	//! \param[in, out] rhs grapheme cluster to construct
	//! from. Left in a potentially invalid state
	GraphemeCluster(ThisType&& rhs) : BaseClass(std::move(rhs))
	{}
	~GraphemeCluster() override = default;

	//! \brief standard copy assignment operator
	//!
	//! \param[in] rhs grapheme cluster to construct from
	ThisType& operator=(const ThisType& rhs)
	{
		static_cast<BaseClass&>(*this) = rhs;

		return *this;
	}

	//! \brief standard move assignment operator
	//!
	//! \param[in, out] rhs grapheme cluster to construct from. Left
	//! in a potentially invalid state
	ThisType& operator=(ThisType&& rhs)
	{
		static_cast<BaseClass&>(*this) = std::move(rhs);

		return *this;
	}

	//! \brief determines whether this grapheme cluster is whitespace
	//!
	//! \returns bool of value true iff this grapheme cluster is
	//! whitespace
	bool isWhitespace() const noexcept
	{
		return this->isWhitespaceIMPL_m();
	}
	//! \brief determines whether this grapheme cluster is considered
	//! blank
	//!
	//! \returns bool of value true iff this grapheme cluster is blank
	bool isBlank() const noexcept
	{
		return this->isBlankIMPL_m();
	}
	//! \brief determines whether this grapheme cluster is a newline
	//!
	//! \returns bool of value true iff this grapheme cluster is a
	//! newline
	bool isNewline() const noexcept
	{
		return this->isNewlineIMPL_m();
	}
	//! \brief determines whether this grapheme cluster is numeric
	//!
	//! \returns bool of value true iff this grapheme cluster is
	//! numeric
	bool isNumeric() const noexcept
	{
		return this->isNumericIMPL_m();
	}

	//! \brief returns memory size of grapheme cluster, in
	//! bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSize() const noexcept
	{
		return this->byteSizeIMPL();
	}
	//! \brief returns logical size of grapheme cluster, in
	//! code-points
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of code-points
	frao::natural64 size() const noexcept
	{
		return this->sizeIMPL();
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	CodePointType& get(frao::natural64 index) noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A const code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	const CodePointType& get(frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	CodePointType& operator[](frao::natural64 index) noexcept
	{
		return static_cast<IMPL::ClusterBaseClass_template<
			CharType, UseExperimental>*>(this)
			->getIMPL(index);
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A const code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	const CodePointType& operator[](
		frao::natural64 index) const noexcept
	{
		// return static_cast<const IMPL::ClusterBaseClass_template<
		//	CharType, UseExperimental>*>(this)->getIMPL(index);
		// return static_cast<const IMPL::ClusterBaseClass_template<
		//	CharType, UseExperimental>*>(this)->getIMPL(index);
		return static_cast<const BaseClass*>(this)->getIMPL(index);
	}

	//! \brief get the ordering of two grapheme clusters.
	//! Stand-in for the upcoming C++20 (?) <=> spaceship
	//! operator
	//!
	//! \returns <0, 0, >0 for less-than, equal-to and
	//! greater-than, respectively
	//!
	//! \param[in] rhs the value to compare on the rhs of
	//! the order. ie: (*this <=> rhs). grapheme cluster
	//! passed to rhs may have any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	frao::integer64 getOrder(
		const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs);
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is before rhs
	//!
	//! \returns bool which has value true iff *this is
	//! strictly before rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator<(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) < 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after rhs
	//!
	//! \returns bool which has value true iff *this is
	//! strictly after rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator>(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) > 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is before or equal to rhs
	//!
	//! \returns bool which has value true iff *this is
	//! before or equal to rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator<=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) <= 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after or equal to rhs
	//!
	//! \returns bool which has value true iff *this is
	//! after or equal to rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator>=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) >= 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is equal to rhs
	//!
	//! \returns bool which has value true iff *this is
	//! equal to rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator==(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) == 0;
	}
	//! \brief uses getOrder() function to dermine if *this
	//! and rhs are unequal
	//!
	//! \returns bool which has value true iff *this is
	//! not equal to rhs
	//!
	//! \param[in] rhs const reference to another
	//! grapheme cluster, of any underlying type with the same
	//! implementation strategy (exp or trans)
	template<typename OtherType>
	bool operator!=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) != 0;
	}

	//! \brief convert grapheme cluster to different
	//! underlying representation. ie: convert UTF-16 to
	//! UTF-8 (say)
	template<typename OtherType>
	explicit operator SimilarType<OtherType>() const noexcept
	{
		// should call constructor for GraphemeClusterIMPL
		return GraphemeCluster<OtherType>(*this);
	}
};

inline namespace Operators
{
//! \brief Construct a utf8 grapheme cluster, from a user-defined
//! string literal
//!
//! \note function is actually called with u8"CLUSTER"_ucluster, where
//! CLUSTER is the grapheme cluster to construct the object from
//!
//! \note If more than one grapheme cluster is supplied, the first
//! will be taken
//!
//! \returns a constructed utf8 grapheme cluster object
//!
//! \param[in] str Pointer to the string, that we should create the
//! grapheme cluster from. Not explicitly passed by the user, as this
//! is a user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline GraphemeCluster<charU8> operator"" _ucluster(const charU8* str,
													size_t size)
{
	return GraphemeCluster<charU8>{str, size};
}
//! \brief Construct a wide grapheme cluster, from a user-defined
//! string literal
//!
//! \note function is actually called with L"CLUSTER"_ucluster, where
//! CLUSTER is the grapheme cluster to construct the object from
//!
//! \note If more than one grapheme cluster is supplied, the first
//! will be taken
//!
//! \returns a constructed wide grapheme cluster object
//!
//! \param[in] str Pointer to the string, that we should create the
//! grapheme cluster from. Not explicitly passed by the user, as this
//! is a user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline GraphemeCluster<wchar_t> operator"" _ucluster(
	const wchar_t* str, size_t size)
{
	return GraphemeCluster<wchar_t>{str, size};
}
//! \brief Construct a utf16 grapheme cluster, from a user-defined
//! string literal
//!
//! \note function is actually called with u"CLUSTER"_ucluster, where
//! CLUSTER is the grapheme cluster to construct the object from
//!
//! \note If more than one grapheme cluster is supplied, the first
//! will be taken
//!
//! \returns a constructed utf16 grapheme cluster object
//!
//! \param[in] str Pointer to the string, that we should create the
//! grapheme cluster from. Not explicitly passed by the user, as this
//! is a user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline GraphemeCluster<char16_t> operator"" _ucluster(
	const char16_t* str, size_t size)
{
	return GraphemeCluster<char16_t>{str, size};
}
//! \brief Construct a utf32 grapheme cluster, from a user-defined
//! string literal
//!
//! \note function is actually called with U"CLUSTER"_ucluster, where
//! CLUSTER is the grapheme cluster to construct the object from
//!
//! \note If more than one grapheme cluster is supplied, the first
//! will be taken
//!
//! \returns a constructed utf32 grapheme cluster object
//!
//! \param[in] str Pointer to the string, that we should create the
//! grapheme cluster from. Not explicitly passed by the user, as this
//! is a user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline GraphemeCluster<char32_t> operator"" _ucluster(
	const char32_t* str, size_t size)
{
	return GraphemeCluster<char32_t>{str, size};
}
}  // namespace Operators

// instantiate normal GraphemeCluster versions, for API boundary
NUCLEUS_HEADER_EXTERN template class frao::Strings::GraphemeCluster<
	frao::charU8>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::GraphemeCluster<char16_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::GraphemeCluster<wchar_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::GraphemeCluster<char32_t>;


//! \brief Type used to represent UTF8-encoded grapheme
//! clusters (grapheme clusters are a unicode concept)
typedef GraphemeCluster<charU8> utf8cluster;

//! \brief Type used to represent UTF16-encoded grapheme
//! clusters (grapheme clusters are a unicode concept)
typedef GraphemeCluster<char16_t> utf16cluster;

//! \brief Type used to represent UTF32-encoded grapheme
//! clusters (grapheme clusters are a unicode concept)
typedef GraphemeCluster<char32_t> utf32cluster;

//! \brief Type used to represent windows-encoded grapheme
//! clusters (windows basically uses UTF16. This class is
//! distinguished from the UTF16 version because it uses
//! wchar rather than char16. nb: wchar is platform
//! dependant!). In future this class may be used for
//! platform-native wchar type (on any platform?)
typedef GraphemeCluster<wchar_t> widecluster;
}  // namespace Strings

}  // namespace frao

#endif	// !FRAO_BASIC_API_STRINGS_GRAPHEMECLUSTER
