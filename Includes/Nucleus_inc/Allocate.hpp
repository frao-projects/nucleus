#ifndef FRAO_NUCLEUS_ALLOCATE
#define FRAO_NUCLEUS_ALLOCATE

#include <numeric>  // for std::lcm
#include <cstdlib>// for free
#include "Environment.hpp"

//! \file
//!
//! \brief Header of Functions for allocation and initialisation of
//! memory regions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

namespace frao
{
//! \brief Allocates memory of the specified size, and then **does
//! not** initialise or construct anything in that memory! *Objects
//! must be freed with FreeSpace()*
//!
//! \returns untyped ptr to the beginning of the newly allocated
//! memory section. Will return nullptr, if allocation failed
//!
//! \param[in] bytes Number of the bytes of memory that should be
//! allocated. Must be a multiple of alignment. Must be non-zero.
//!
//! \param[in] alignment The alignment of the memory we wish to
//! return. Alignment used must be a power of two, and must be
//! non-zero
NUCLEUS_LIB_API void* AllocateSpaceOnly(natural64 bytes,
						natural64 alignment) noexcept;

//! \brief Frees memory allocated by one of the frao::Allocate...
//! functions
//!
//! \tparam Type The type of the pointer that is to be deallocated
//!
//! \param[in,out] memory Reference to pointer that should be
//! deallocated. Must be a pointer that was created by one of the
//! frao::Allocate... functions. Will be set to nullptr
//!
//! \todo Add a destruction and deallocation function. And maybe an
//! array destruction and deallocation function
template<typename Type>
void FreeSpace(Type*& memory) noexcept
{
#if defined(FRAO_LIBVER_MICROSOFT)
	_aligned_free(static_cast<void*>(memory));
#else
	free(static_cast<void*>(memory));
#endif

	memory = nullptr;
}

//! \brief Destroy an object created by Allocate or AllocateArray.
//!
//! \tparam Type The object of the object to destroy. Must be the same
//! as the type that the object was allocated with
//!
//! \param[in,out] memory Reference to pointer that should have it's
//! destructor called, and be deallocated. Must be a pointer that was
//! created by one of the frao::Allocate... functions. Will be set to
//! nullptr
template<typename Type>
void DestroyObject(Type*& object) noexcept(noexcept(object->~Type()))
{
	object->~Type();
	FreeSpace(object);
}

//! \brief Allocates space required for a given Type, and returns a
//! pointer to a memory location suitable for this type. **Will not**
//! construct object of type 'Type'! *Objects must be freed with
//! FreeSpace()*
//!
//! \tparam Type The type of the object, the size of which will be the
//! size of the returned memory, and he type of the return value
//!
//! \returns pointer of type 'Type', that will point to
//! *unconstructed* memory the size of 'Type'. May return nullptr,
//! when failing
template<typename Type>
Type* AllocateSpaceFor() noexcept
{
	constexpr const natural64 TypeSize  = sizeof(Type),
							  Alignment = alignof(Type);

	static_assert((Alignment & (Alignment - 1)) == 0,
				  "Alignment must always be a power of two! Anything "
				  "else is ill-formed");

	// size has to be a multiple of the alignment, so enforce that
	natural64 RealSize = std::lcm(TypeSize, Alignment);

	return static_cast<Type*>(AllocateSpaceOnly(RealSize, Alignment));
}

//! \brief Allocate an array of 'Type' elements. Number of elements
//! equal to 'numArrayElements'. **Will not** construct objects in the
//! array. *array must be freed with FreeSpace()*
//!
//! \tparam Type The type of elements that the array will have. Will
//! be used for size of array, in memory, as well as type of returned
//! pointer
//!
//! \returns a pointer to an array of memory that can store
//! 'numArrayElements' 'Type' elements contiguously. May return
//! nullptr, when failing
//!
//! \param[in] numArrayElemnents the number of elements (of type
//! 'Type') that the memory region should be big enough to hold
//! contiguously
template<typename Type>
Type* AllocateSpaceForArray(natural64 numArrayElements) noexcept
{
	constexpr const natural64 TypeSize  = sizeof(Type),
							  Alignment = alignof(Type);

	static_assert((Alignment & (Alignment - 1)) == 0,
				  "Alignment must always be a power of two! Anything "
				  "else is ill-formed");

	// size has to be a multiple of the alignment, so enforce that
	natural64 RealSize = std::lcm(TypeSize, Alignment);

	return static_cast<Type*>(
		AllocateSpaceOnly(RealSize * numArrayElements, Alignment));
}

//! \brief allocates and constructs a 'Type' object. **Object must be
//! destructed manually!** (ie: with objPtr->~Type()), and then
//! *Object must be freed with FreeSpace()*
//!
//! \tparam Type The type of the element to be constructed
//!
//! \tparam Args Parameter pack of the arguments to call the
//! constructor of 'Type' with. To be inferred by the passed
//! arguments, not manually specifed
//!
//! \returns Ptr to the created object. May return nullptr, if
//! allocation fails
//!
//! \param[in] args parameter pack of arguments to be perfectly
//! forwarded to the constructor of 'Type'
//!
//! \exception Any if the constructor throws for any reason
template<typename Type, typename... Args>
Type* Allocate(Args&&... args)
{
	Type* result = AllocateSpaceFor<Type>();

	// traditionally, placement new will have it's own nullptr check,
	// but my understanding is that this is no longer (as of c++17)
	// required, and hence this check is required
	if (result != nullptr)
	{
		new (result) Type(std::forward<Args>(args)...);
	}

	return result;
}

//! \brief Allocate space for an array, and then construct all members
//! of this array with parameter pack supplied. **array elements must
//! be destructed manually!** (ie: with objPtr->~Type()), and then
//! *array must be freed with FreeSpace()*
//!
//! \tparam Type Type of element, of the array
//!
//! \tparam Args Parameter pack of Types that the
//!
//! \returns ptr to start of array of 'Type' objects, array has
//! 'numArrayElements' number of elements (and is contiguous). May
//! alternatively return nullptr, if allocation fails
//!
//! \param[in] numArrayElements number of elements, of type 'Type'
//! that the array should hold
//!
//! \param[in] args parameter pack of arguments to be **copied** to
//! the constructor of 'Type'
//!
//! \exception Any if any constructor throws for any reason
template<typename Type, typename... Args>
Type* AllocateArray(natural64 numArrayElements, Args... args)
{
	Type* result = AllocateSpaceForArray<Type>(numArrayElements);

	// traditionally, placement new will have it's own nullptr check,
	// but my understanding is that this is no longer (as of c++17)
	// required, and hence this check is required
	if (result != nullptr)
	{
		for (natural64 elementIndex = 0;
			 elementIndex < numArrayElements; ++elementIndex)
		{
			// construct from supplied arguments - arguments are
			// deliberately copied! This is because we can't move from
			// the same parameter multiple times!
			new (result + elementIndex) Type(args...);
		}
	}

	return result;
}

}  // namespace frao

#endif  // !FRAO_NUCLEUS_ALLOCATE
