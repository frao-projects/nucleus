#ifndef FRAO_BASIC_API_ERROR_CONTROL
#define FRAO_BASIC_API_ERROR_CONTROL

//! \file
//!
//! \brief Header for user interactions with error handler
//! singleton
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <memory>  //for std::shared_ptr
#include "Organisation.hpp"
#include "../Strings.hpp"

namespace frao
{
inline namespace ErrorLogging
{
//! \brief Replace the stored global ErrorLog, with the provided
//! handler
//!
//! \returns const shared_ptr to the new global ErrorLog
//!
//! \param[in] newHandler An ErrorLog, which will replace the
//! global error handler. Left in an invalid state, after being moved
//! from
const std::shared_ptr<ErrorLog> NUCLEUS_LIB_API setErrorLog(
	std::shared_ptr<ErrorLog> newHandler) noexcept;

//! \brief Will get the currently stored global ErrorLog. If there
//! is no currently created error handler, will create one first, and
//! then return it
//!
//! \returns const shared_ptr to the current global ErrorLog.
//! Guaranteed to be non-empty
//!
//! \exception MemAlloc throws if we need to create an ErrorLog,
//! but there is no memory left for one
const std::shared_ptr<ErrorLog> NUCLEUS_LIB_API getErrorLog();
//! \brief Will get the currently stored global ErrorLog. If there
//! is no currently created error handler, will return nullptr
//!
//! \returns const shared_ptr to ErrorLog. Will be empty, if we
//! have no current ErrorLog
const std::shared_ptr<ErrorLog> NUCLEUS_LIB_API tryGetErrorLog() noexcept;

//! \brief create a new error handler, in the global ErrorLog slot
//!
//! \tparam Args Parameter pack of argument types, with which to
//! create a new ErrorLog
//!
//! \returns const shared_ptr to the new global ErrorLog
//!
//! \param[in] args A parameter pack of arguments, which will be
//! forwarded to the ErrorLog constructor, for that object's
//! creation
//!
//! \exception MemAlloc throws if there is no memory left,
//! for a new memory handler
template<typename... Args>
const std::shared_ptr<ErrorLog> replaceErrorLog(
	Args&&... args)
{
	try
	{
		return setErrorLog(std::make_shared<ErrorLog>(
			std::forward<Args>(args)...));
	} catch (const std::bad_alloc&)
	{
		throw create_type<MemAlloc>(FILELOC_UTF8, utf8string());
	}
}

}  // namespace ErrorLogging
}  // namespace frao

#endif  //! FRAO_BASIC_API_ERROR_CONTROL