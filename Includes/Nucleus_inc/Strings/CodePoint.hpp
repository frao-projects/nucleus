#ifndef FRAO_NUCLEUS_STRINGS_CODEPOINT
#define FRAO_NUCLEUS_STRINGS_CODEPOINT

//! \file
//!
//! \brief Header of the Interface of unicode codepoint
//! class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <compare>		//for std::strong_ordering
#include <type_traits>	//for std::conditional
#include "Experimental/CodePointIMPL_Expr.hpp"
#include "StringDefines.hpp"  //for default string type variable
#include "Transitional/CodePointIMPL_Trans.hpp"

namespace frao
{
//! \namespace frao::Strings
//!
//! \brief namespace that contains code for frao string
//! classes, and any unicode code
inline namespace Strings
{
// tell the compiler that we will define these elsewhere
NUCLEUS_HEADER_EXTERN template class Transitional::CodePointIMPL<
	wchar_t>;
NUCLEUS_HEADER_EXTERN template class Transitional::CodePointIMPL<
	char16_t>;
NUCLEUS_HEADER_EXTERN template class Transitional::CodePointIMPL<
	char32_t>;
#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
NUCLEUS_HEADER_EXTERN template class Experimental::CodePointIMPL_UTF8<
	charU8>;
#ifdef FRAO_WCHAR_SMALL
NUCLEUS_HEADER_EXTERN template class Experimental::
	CodePointIMPL_UTF16<wchar_t>;
#else
NUCLEUS_HEADER_EXTERN template class Experimental::
	CodePointIMPL_UTF32<wchar_t>;
#endif
NUCLEUS_HEADER_EXTERN template class Experimental::
	CodePointIMPL_UTF16<char16_t>;
NUCLEUS_HEADER_EXTERN template class Experimental::
	CodePointIMPL_UTF32<char32_t>;
#endif

//! \class CodePoint
//!
//! \brief A class to represent the smallest logical unit in
//! unicode: the CodePoint
//!
//! \note There are a series of UTF-8 / UTF-16 / UTF-32
//! Codepoint tables in the Implementation header. These are
//! not in the generated documentation because of
//! limitations with generating tables with doxygen /
//! breathe / sphinx
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default>
class NUCLEUS_LIB_API CodePoint final
	: private std::conditional_t<
		  UseExperimental && g_Experimental_Strings_Allowed,
		  Experimental::CodePointIMPL_T<CharType>,
		  Transitional::CodePointIMPL<CharType>>
{
	//! \brief template for possible base classes of this class.
	//! Dispatches to experimental or transtional base class as
	//! appropriate. Used so that we don't have to whole conditional
	//! statement every time we need to refer to another possible base
	//! class
	template<typename CharType_templ>
	using BaseClass_template = std::conditional_t<
		UseExperimental && g_Experimental_Strings_Allowed,
		Experimental::CodePointIMPL_T<CharType_templ>,
		Transitional::CodePointIMPL<CharType_templ>>;

	//! \brief using declaration for the base class of this one. Used
	//! for simplicity of referring to it.
	using BaseClass = BaseClass_template<CharType>;

   public:
	template<typename Othertype, bool OtherUseExperimental>
	friend class CodePoint;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ThisType = CodePoint<CharType, UseExperimental>;

	//! \brief alias template, so that it is clear(er) that
	//! transitional and experimental implementations are mutually
	//! incompatible
	//!
	//! \tparam OtherType The character type of the similiar type
	template<typename OtherType>
	using SimilarType = CodePoint<OtherType, UseExperimental>;

	//! \brief Construct null codepoint
	CodePoint() : BaseClass()
	{}
	//! \brief Construct a codepoint, from the specified string. Will
	//! get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \param[in] codepoint String that contains the
	//! code-point. Must not be null
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string. Must not be zero
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the last
	//! valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct codepoint to read. If null, will not be written to
	CodePoint(const CharType* codepoint, frao::natural64 buffersize,
			  bool		 getLastCodepoint = false,
			  natural64* readStart		  = nullptr)
		: BaseClass(codepoint, buffersize, getLastCodepoint,
					readStart)
	{}
	//! brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8/16/32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePoint(charA asciiChar) : BaseClass(asciiChar)
	{}

	//! \brief Copy construct a codepoint, from another codepoint of
	//! any underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	CodePoint(const SimilarType<OtherType>& rhs)
		: BaseClass(
			static_cast<const BaseClass_template<OtherType>&>(rhs))
	{}

	//! \brief Copy construct a codepoint, from another codepoint of
	//! any underlying type
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePoint(const ThisType& rhs) : BaseClass(rhs)
	{}

	//! \brief standard move construct
	//!
	//! \param[in, out] rhs Code-point that will be moved to
	//! this object. Left in a potentially invalid state
	CodePoint(ThisType&& rhs) : BaseClass(std::move(rhs))
	{}

	//! \brief destroy the codepoint, and call any derived destructors
	~CodePoint() override = default;

	//! \brief standard copy assignment operator
	//!
	//! \param[in] rhs Code-point, of any underlying type, that will
	//! be copied to this object. Will be left unaltered
	ThisType& operator=(const ThisType& rhs)
	{
		static_cast<BaseClass&>(*this) = rhs;

		return *this;
	}

	//! \brief standard move construct
	//!
	//! \param[in, out] rhs Code-point that will be moved to
	//! this object. Left in a potentially invalid state
	ThisType& operator=(ThisType&& rhs)
	{
		static_cast<BaseClass&>(*this) = std::move(rhs);

		return *this;
	}

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer, with size in terms of bytes
	frao::natural64 byteSize() const noexcept
	{
		return this->byteSizeIMPL();
	}

	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 size() const noexcept
	{
		return this->sizeIMPL();
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	CharType& get(frao::natural64 index) noexcept
	{
		return this->getIMPL(index);
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const CharType& get(frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	CharType& operator[](frao::natural64 index) noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying const character reference to
	//! that section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const CharType& operator[](frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepoint() const noexcept
	{
		return this->getFullCodepointIMPL();
	}

#if defined(__cpp_impl_three_way_comparison) \
	&& defined(__cpp_lib_three_way_comparison)
	//! \brief Get the ordering of two code-points.
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! comparing with
	//!
	//! \returns strong_ordering (less, equal, greater) of this
	//! codepoint wrt the rhs codepoint
	//!
	//! \param[in] rhs the value to compare on the rhs of
	//! the order. ie: (*this <=> rhs). Code-point passed to
	//! rhs may have any underlying type with the same implementation
	//! strategy (exp or trans)
	template<typename OtherType>
	std::strong_ordering operator<=>(
		const SimilarType<OtherType>& rhs) const noexcept
	{
		char32_t lhsCodepoint = this->getFullCodepointIMPL(),
				 rhsCodepoint = rhs.getFullCodepointIMPL();

		return lhsCodepoint <=> rhsCodepoint;
	}
#else

	//! \brief get the ordering of two code-points. Stand-in
	//! for the upcoming C++20 (?) <=> spaceship operator
	//!
	//! \returns <0, 0, >0 for less-than, equal-to and
	//! greater-than, respectively
	//!
	//! \param[in] rhs the value to compare on the rhs of
	//! the order. ie: (*this <=> rhs). Code-point passed to
	//! rhs may have any underlying type with the same implementation
	//! strategy (exp or trans)
	template<typename OtherType>
	frao::integer64 getOrder(
		const SimilarType<OtherType>& rhs) const noexcept
	{
		char32_t lhsCodepoint = this->getFullCodepointIMPL(),
				 rhsCodepoint = rhs.getFullCodepointIMPL();

		return static_cast<frao::integer64>(lhsCodepoint)
			   - static_cast<frao::integer64>(rhsCodepoint);
	}

#endif

	//! \brief convert code-point to different underlying
	//! representation. ie: convert UTF-16 to UTF-8 (say)
	template<typename OtherType>
	operator SimilarType<OtherType>() const noexcept
	{
		// should call constructor for CodePointIMPL
		return SimilarType<OtherType>(*this);
	}
};

//! \namespace frao::Strings::Operators
//!
//! \brief namespace dedicated to literals that create
//! frao strings and other classes (ie: codepoint,
//! grapheme-cluster etc.) Have their own namespace so that
//! they can be pulled into the global namespace, when
//! required
inline namespace Operators
{
//! \brief Construct a utf8 codepoint, from a user-defined string
//! literal
//!
//! \note function is actually called with u8"CODEPOINT"_upoint, where
//! CODEPOINT is the codepoint to construct the object from
//!
//! \note If more than one codepoint is supplied, the first will be
//! taken
//!
//! \returns a constructed utf8 codepoint object
//!
//! \param[in] str Pointer to the string, that we should create the
//! codepoint from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline CodePoint<charU8> operator"" _upoint(const charU8* str,
											size_t		  size)
{
	return CodePoint<charU8>(str, size);
}
//! \brief Construct a wide codepoint, from a user-defined string
//! literal
//!
//! \note function is actually called with L"CODEPOINT"_upoint, where
//! CODEPOINT is the codepoint to construct the object from
//!
//! \note If more than one codepoint is supplied, the first will be
//! taken
//!
//! \returns a constructed wide codepoint object
//!
//! \param[in] str Pointer to the string, that we should create the
//! codepoint from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline CodePoint<wchar_t> operator"" _upoint(const wchar_t* str,
											 size_t			size)
{
	return CodePoint<wchar_t>(str, size);
}
//! \brief Construct a utf16 codepoint, from a user-defined string
//! literal
//!
//! \note function is actually called with u"CODEPOINT"_upoint, where
//! CODEPOINT is the codepoint to construct the object from
//!
//! \note If more than one codepoint is supplied, the first will be
//! taken
//!
//! \returns a constructed utf16 codepoint object
//!
//! \param[in] str Pointer to the string, that we should create the
//! codepoint from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline CodePoint<char16_t> operator"" _upoint(const char16_t* str,
											  size_t		  size)
{
	return CodePoint<char16_t>(str, size);
}
//! \brief Construct a utf32 codepoint, from a user-defined string
//! literal
//!
//! \note function is actually called with U"CODEPOINT"_upoint, where
//! CODEPOINT is the codepoint to construct the object from
//!
//! \note If more than one codepoint is supplied, the first will be
//! taken
//!
//! \returns a constructed utf32 codepoint object
//!
//! \param[in] str Pointer to the string, that we should create the
//! codepoint from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline CodePoint<char32_t> operator"" _upoint(const char32_t* str,
											  size_t		  size)
{
	return CodePoint<char32_t>(str, size);
}
}  // namespace Operators

// instantiate normal CodePoint versions, for API boundary
NUCLEUS_HEADER_EXTERN template class frao::Strings::CodePoint<
	frao::charU8>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::CodePoint<
	char16_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::CodePoint<
	wchar_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::CodePoint<
	char32_t>;


//! \brief Type used to represent UTF8-encoded codepoints
//! (codepoints are a unicode concept)
typedef CodePoint<charU8> utf8codepoint;

//! \brief Type used to represent UTF16-encoded codepoints
//! (codepoints are a unicode concept)
typedef CodePoint<char16_t> utf16codepoint;

//! \brief Type used to represent UTF32-encoded codepoints
//! (codepoints are a unicode concept)
typedef CodePoint<char32_t> utf32codepoint;

//! \brief Type used to represent windows-encoded codepoints
//! (windows basically uses UTF16. This class is
//! distinguished from the UTF16 version because it uses
//! wchar rather than char16. nb: wchar is platform
//! dependant!). In future this class may be used for
//! platform-native wchar type (on any platform?)
typedef CodePoint<wchar_t> widecodepoint;
}  // namespace Strings
}  // namespace frao

#endif	// !FRAO_BASIC_API_STRINGS_CODEPOINT
