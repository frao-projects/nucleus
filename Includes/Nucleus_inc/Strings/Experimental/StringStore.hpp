#ifndef FRAO_NUCLEUS_STRINGS_STRINGSTORE
#define FRAO_NUCLEUS_STRINGS_STRINGSTORE

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the string storage struct, for contiguous string
//! memory management
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "../../Environment.hpp"
#include "../../Allocate.hpp"

namespace frao
{
inline namespace Strings
{
namespace Experimental
{
namespace IMPL
{
//! \brief A storage class for representing arbitrary-length strings,
//! of one of the character types [::charU8, ::char16_t, ::char32_t,
//! ::wchar_t]
//!
//! \tparam CharType The underlying type of the string [::charU8,
//! ::char16_t, ::char32_t, ::wchar_t].
template<typename CharType>
class StringStore final
{
	//! \brief The size of a single element of the underlying
	//! codepoint storage type
	static constexpr const natural64 ms_ElementSize =
		sizeof(CharType);
	//! \brief The smallest size, in elements of ::CharType,
	//! that can will be stored in the dynamic storage
	//!
	//! \note This size is not necessarily the number of
	//! codepoints/clusters, since UTF8 and UTF16 codepoints can span
	//! multiple of their underlying storage type
	static constexpr const natural64 ms_DynamicTransitionSize =
		(sizeof(std::uintptr_t) + sizeof(natural64)) / ms_ElementSize;

	static_assert(ms_DynamicTransitionSize > 1,
				  "We need that the short data is valid for at least "
				  "single element strings");

	union IMPL_StoreUnion
	{
		//! \brief Array for holding data of short strings, <
		//! #ms_DynamicTransitionSize
		//!
		//! \note Last element should always be null, so cannot store
		//! strings of the same size of the array
		CharType m_ShortData[ms_DynamicTransitionSize];
		//! \brief struct for holding long words >=
		//! #ms_DynamicTransitionSize
		struct
		{
			//! \brief Ptr to array for holding data of long strings
			//! >= #ms_DynamicTransitionSize
			//!
			//! \note Last element should always be null
			CharType* m_StringPtr;
			//! \brief The allocated size of the array to which
			//! #m_StringPtr refers
			//!
			//! \note Should be one larger than the actual useful size
			//! of the string, since the allocated string should
			//! always be null-terminated
			natural64 m_Capacity;
		} m_LongData;
	} m_Union;

	//! \brief The size of the string, in elements of the underlying
	//! storage type (::CharType)
	//!
	//! \note This size may be compared to #ms_DynamicTransitionSize,
	//! to determine which of the union members is active
	//! (#m_ShortData or #m_LongData). If greater than or equal to
	//! #ms_DynamicTransitionSize, #m_LongData will be active. If not,
	//! #m_ShortData will be
	//!
	//! \warning This is NOT the length in terms of number of
	//! codepoints/clusters
	natural64 m_UsedSize;

	//! \brief Indicates whether the string is using the long storage
	//!
	//! \returns True iff the long storage is active
	bool isLongData() const noexcept
	{
		return m_UsedSize >= ms_DynamicTransitionSize;
	}
	//! \brief Indicates whether the string is using the short storage
	//!
	//! \returns True iff the short storage is active
	bool isShortData() const noexcept
	{
		return !isLongData();
	}

	//! \brief Will free any allocated memory
	void destroyData() noexcept
	{
		if (isLongData())
		{
			FreeSpace(m_Union.m_LongData.m_StringPtr);
		}
	}

   public:
	//! \brief create empty string
	StringStore() : m_Union{{}}, m_UsedSize(0)
	{}
	//! \brief create string from the specified string
	//!
	//! \param[in] string pointer to const string buffer
	//!
	//! \param[in] elementSize length of the #string pointer, in
	//! elements of ::CharType
	StringStore(const CharType* string, natural64 elementSize)
		: m_Union{}, m_UsedSize(elementSize)
	{
		if (isLongData())
		{
			m_Union.m_LongData.m_Capacity = m_UsedSize + 1;
			m_Union.m_LongData.m_StringPtr =
				AllocateSpaceForArray(m_Union.m_LongData.m_Capacity);
		}

		// Yes, this is safe. See data() function
		CharType* writeTo = reinterpret_cast<CharType*>(&m_Union);

		for (natural64 index = 0; index < m_UsedSize; ++index)
		{
			writeTo[index] = string[index];
		}

		// add null-terminator
		writeTo[m_UsedSize] = 0x0;
	}

	//! \brief Construct a StringStore from an ascii cstring
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	//!
	//! \param[in] elementSize length of the ::string pointer, in
	//! elements of ::charA
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	StringStore(const charA* string, natural64 elementSize)
		: m_Union{}, m_UsedSize(elementSize)
	{
		if (isLongData())
		{
			m_Union.m_LongData.m_Capacity = m_UsedSize + 1;
			m_Union.m_LongData.m_StringPtr =
				AllocateSpaceForArray(m_Union.m_LongData.m_Capacity);
		}

		// Yes, this is safe. See data() function
		CharType* writeTo = reinterpret_cast<CharType*>(&m_Union);

		for (natural64 index = 0; index < m_UsedSize; ++index)
		{
			writeTo[index] = 0x7F & string[index];
		}

		// add null-terminator
		writeTo[m_UsedSize] = 0x0;
	}

	//! \brief Standard copy constructor.
	//!
	//! \param[in] rhs The store to copy from
	StringStore(const StringStore& rhs) : m_Union{},
		m_UsedSize(rhs.size())
	{
		if (isLongData())
		{
			m_Union.m_LongData.m_Capacity = m_UsedSize + 1;
			m_Union.m_LongData.m_StringPtr =
				AllocateSpaceForArray<CharType>(m_Union.m_LongData.m_Capacity);
		}

		// Yes, this is safe. See data() function
		CharType* writeTo = reinterpret_cast<CharType*>(&m_Union);
		const CharType* readFrom = rhs.data();

		for (natural64 index = 0; index < m_UsedSize; ++index)
		{
			writeTo[index] = readFrom[index];
		}

		// add null-terminator
		writeTo[m_UsedSize] = 0x0;
	}
	//! \brief Standard move constructor. Empties rhs object
	//!
	//! \note This constructor relies on the #m_Union variable having
	//! a trivial move constructor
	//!
	//! \param[in] rhs The store to move from
	StringStore(StringStore&& rhs) : m_Union(std::move(rhs.m_Union)),
		m_UsedSize(rhs.size())
	{
		rhs.m_Union.m_ShortData[0] = rhs.m_Union.m_ShortData[1]
			= rhs.m_Union.m_ShortData[2] = rhs.m_Union.m_ShortData[3] = 0x0;
		rhs.m_UsedSize			= 0;
	}
	//! \brief Standard destructor, which deallocates internal array
	//! if necessary
	~StringStore() noexcept
	{
		destroyData();
	}

	//! \brief Standard copy constructor, which deallocs + allocs this
	//! if necessary
	//!
	//! \returns A reference to this
	//!
	//! \param[in] rhs The store to copy from
	StringStore& operator=(const StringStore& rhs)
	{
		natural64 rhsSize = rhs.size();

		if (isLongData())
		{
			if (rhs.isLongData()
				&& (m_Union.m_LongData.m_Capacity <= rhsSize))
			{
				// both are long, and rhs size is greater than can be
				// held in lhs
				destroyData();

				m_Union.m_LongData.m_Capacity = rhsSize + 1;
				m_Union.m_LongData.m_StringPtr =
					AllocateSpaceForArray<CharType>(
						m_Union.m_LongData.m_Capacity);
			} else if (rhs.isShortData())
			{
				// lhs is long & rhs is short
				destroyData();
			}
		} else if (rhs.isLongData())
		{
			// lhs is short & rhs is long
			m_Union.m_LongData.m_Capacity = rhsSize + 1;
			m_Union.m_LongData.m_StringPtr =
				AllocateSpaceForArray<CharType>(m_Union.m_LongData.m_Capacity);
		}

		m_UsedSize = rhsSize;

		// Yes, this is safe. See data() function
		CharType* writeTo = reinterpret_cast<CharType*>(&m_Union);
		const CharType* readFrom = rhs.data();

		for (natural64 index = 0; index < m_UsedSize; ++index)
		{
			writeTo[index] = readFrom[index];
		}

		// add null-terminator
		writeTo[m_UsedSize] = 0x0;

		return *this;
	}
	//! \brief Standard move constructor, which empties input
	//!
	//! \returns A reference to this
	//!
	//! \param[in] rhs The store to move from
	StringStore& operator=(StringStore&& rhs)
	{
		// simple trivial copy of union data
		m_Union	   = rhs.m_Union;
		m_UsedSize = rhs.m_UsedSize;

		// invalidate/empty rhs
		rhs.m_Union.m_ShortData[0] = rhs.m_Union.m_ShortData[1]
			= rhs.m_Union.m_ShortData[2] = rhs.m_Union.m_ShortData[3] = 0x0;
		rhs.m_UsedSize			= 0;

		return *this;
	}

	//! \brief Get a pointer to the null-terminated string stored
	//!
	//! \returns A null-terminated pointer
	const CharType* data() const noexcept
	{
		// this is fine, since a union has the same address as does
		// any of its non-static members. So for short data, this is
		// just casting the array start to a ptr. For long data, the
		// first element of the struct - the long data ptr -  is at
		// the same address as is the struct. Thus, we can safely
		// obtain either type of data by casting the union to
		// CharType*
		return reinterpret_cast<const CharType*>(&m_Union);
	}

	//! \brief returns logical size of string, in elements of
	//! ::CharType
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of ::CharType
	natural64 size() const noexcept
	{
		return m_UsedSize;
	}
	//! \brief returns byte size of string
	//!
	//! \returns unsigned 64-bit integer indicating size in bytes
	natural64 byteSize() const noexcept
	{
		return sizeof(CharType) * size();
	}
};
}  // namespace IMPL
}  // namespace Experimental
}  // namespace Strings
}  // namespace frao

#endif