﻿#ifndef FRAO_NUCLEUS_ENVIRONMENT_STATIC_API_DEFINES
#define FRAO_NUCLEUS_ENVIRONMENT_STATIC_API_DEFINES

//! \file
//!
//! \brief File of core defines, that should be included in all frao
//! project files (implicitly or explicitly)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>  //for integer types

//! \namespace frao
//!
//! \brief Basic namespace for all frao-project names
namespace frao
{
inline namespace Environment
{
//! \defgroup IntTypes Integer Types
//!
//! \brief platform independent definitions of integer variables, of
//! various sizes

//! \defgroup IntFastTypes Fastest Integer Types
//!
//! \ingroup IntTypes
//!
//! \brief platform independent, fastest variables of at least the
//! specified size
//!
//! \details These are platform independent, and guarantee a size of
//! at least the amount of bits specified. They may be bigger (e.g. 16
//! bit int will often be actually a 32 bit int), depending on the
//! speed of the underlying types on the architecture, but they will
//! be of at least the size specified.
//!
//! @{

typedef uint_fast8_t
	natural8;  //!< \brief fastest unsigned integer on this platform
			   //!< that is at least 8 bits
typedef uint_fast16_t
	natural16;  //!< \brief fastest unsigned integer on this platform
				//!< that is at least 16 bits
typedef uint_fast32_t
	natural32;  //!< \brief fastest unsigned integer on this platform
				//!< that is at least 32 bits
typedef uint_fast64_t
	natural64;  //!< \brief fastest unsigned integer on this platform
				//!< that is at least 64 bits
typedef int_fast8_t
	integer8;  //!< \brief fastest signed integer on this platform
			   //!< that is at least 8 bits
typedef int_fast16_t
	integer16;  //!< \brief fastest signed integer on this platform
				//!< that is at least 16 bits
typedef int_fast32_t
	integer32;  //!< \brief fastest signed integer on this platform
				//!< that is at least 32 bits
typedef int_fast64_t
	integer64;  //!< \brief fastest signed integer on this platform
				//!< that is at least 64 bits

//! @}

//! \defgroup IntSmallTypes Smallest Integer Types
//!
//! \ingroup IntTypes
//!
//! \brief platform independent, smallest variables of at least the
//! specified size
//!
//! \details These are platform independent, and guarantee a size of
//! at least the amount of bits specified. They will be of the
//! smallest type that has at least the number of bits specified.
//! Consequently, they may be bigger on unusual archictures, but they
//! will never be smaller (and will almost always be of the exact size
//! specified). i.e if the architecture doesn't offer a 16 bit int,
//! then the 16 bit int type will be filled by the smallest type that
//! is of at least that size. If said architecture had a 17 bit int,
//! then that would be offered instead, regardless of speed
//!
//! @{
typedef uint_least8_t
	small_nat8;  //!< \brief smallest unsigned integer on this
				 //!< platform that is at least 8 bits
typedef uint_least16_t
	small_nat16;  //!< \brief smallest unsigned integer on this
				  //!< platform that is at least 8 bits
typedef uint_least32_t
	small_nat32;  //!< \brief smallest unsigned integer on this
				  //!< platform that is at least 8 bits
typedef uint_least64_t
	small_nat64;  //!< \brief smallest unsigned integer on this
				  //!< platform that is at least 8 bits
typedef int_least8_t
	small_int8;  //!< \brief smallest signed integer on this platform
				 //!< that is at least 8 bits
typedef int_least16_t
	small_int16;  //!< \brief smallest signed integer on this platform
				  //!< that is at least 8 bits
typedef int_least32_t
	small_int32;  //!< \brief smallest signed integer on this platform
				  //!< that is at least 8 bits
typedef int_least64_t
	small_int64;  //!< \brief smallest signed integer on this platform
				  //!< that is at least 8 bits

//! @}

//! \defgroup IntStandard Standard Integer Types
//!
//! \ingroup IntTypes
//!
//! \brief Standard ints, used for standard purposes. For
//! when size is more or less irrelevant, or hasn't been
//! overly considered
//!
//! @{
typedef natural32 natural;  //!< \brief basic unsigned integer, for
							//!< use when you don't care about size
typedef integer32 integer;  //!< \brief basic signed integer, for use
							//!< when you don't care about size
[[deprecated("Use ptr_nat, instead")]] typedef uintptr_t
	ptr_size;  //!< \brief unsigned integer type capable of
			   //!< holding a pointer, on this platform
			   //!< \note Use ptr_nat instead

typedef std::uintptr_t
	ptr_nat;  //!< \brief unsigned type capable of
			  //!< holding a pointer, on this platform
typedef std::intptr_t
	ptr_int;  //!< \brief unsigned type capable of
			  //!< holding a pointer, on this platform
//! @}

//! \brief ASCII character type, on this platform
using charA = char;
//! \brief UTF8 character type, on this platform
using charU8 =
#if defined(__cpp_char8_t)
	char8_t;
#else
	char;
#endif

constexpr const float PI_F =
	3.1415927f;  //!< \brief The constant pi, to float accuracy
constexpr const double PI_D =
	3.141592653589793;  //!< \brief The constant pi, to double accuracy
}  // namespace Environment
}  // namespace frao

//! \def NUCLEUS_LIB_API
//!
//! \brief Will contain the necessary dll export or import declspec


#if defined _WIN32//Windows defines

//! \brief Designates that something is part of the library's API, and
//! should be exported
#if defined(NUCLEUS_EXPORT)
#define NUCLEUS_LIB_API __declspec(dllexport)
#define NUCLEUS_HEADER_EXTERN
#elif defined(NUCLEUS_IMPORT) //end NUCLEUS_EXPORT section
#define NUCLEUS_LIB_API __declspec(dllimport)
#define NUCLEUS_HEADER_EXTERN
#else //end NUCLEUS_IMPORT section
#define NUCLEUS_LIB_API
#define NUCLEUS_HEADER_EXTERN extern
#define NUCLEUS_HEADER_ONLY
#endif //end NUCLEUS_LIB_API on windows section

#else//end windows

//On non-windows platforms, we don't have dlls, so define as nothing
#define NUCLEUS_LIB_API

#endif//end os specific defs

//! \brief stringify helper 1
#define STRINGISE1(N) #N
//! \brief stringify helper 2
#define STRINGISE2(N) STRINGISE1(N)

//! \brief  macro to stringify line number
#define LINESTR STRINGISE2(__LINE__)
//! \brief macro to get string of file and line number
#define FILELOCATION __FILE__ "(" LINESTR ")"
//! \brief macro to get utf8 string of file and line number
#define FILELOC_UTF8 (frao::charU8*) (FILELOCATION)

#if defined(__clang__)  // clang flavours
//! \brief on clang, this meaningless, because it's for suppressing
//! cl.exe warnings
#define SUPPRESS_VC_WARNING(WARNNUM)
//! \brief on clang, this meaningless, because it's for disabling
//! cl.exe warnings
#define DISABLE_VC_WARNING(WARNNUM)
//! \brief on clang, this meaningless, because it's for restoring
//! cl.exe warnings
#define RESTORE_VC_WARNING(WARNNUM)

// NOTE: the __clang__ macro will detect for clang, so that's very
// straightforward. But checking for _MT is a hack, designed to
// distinguish between clang and clang-cl. It works because _MT is
// defined if you're using the VS run-time library, which means
// clang-cl is going to define it, but not clang (probably)
#if !defined(_MT)  // regular clang (GCC-like) section

//! \brief Proxy function, which just makes the contents a string
#define STRINGISE(x) #x

//! \brief Proxy function, for constructing warning input to _Pragma, since clang expects this to be a string.
#define MAKE_WARNING_TEXT(x) STRINGISE(message x)

//! \brief Cause the specified message to be emitted as a warning
//!
//! \note This will emit several notes, in addition to the warnings,
//! which we should work out how to suppress
#define EMIT_WARNING(STR) _Pragma(MAKE_WARNING_TEXT(STR))

#else  // clang-cl (VS-like) section

//! \brief Cause the specified message to be emitted as a warning
//!
//! \details Clang will automatically emit file/line information, for
//! pragma message, so no need to add that information on top, like
//! the VS version
#define EMIT_WARNING(STR) __pragma(message(#STR))

#endif  // end clang vs clang-cl sections
#elif defined(__GNUC__) || defined(__GNUG__) //GCC
//! \brief on gcc, this meaningless, because it's for suppressing
//! cl.exe warnings
#define SUPPRESS_VC_WARNING(WARNNUM)
//! \brief on gcc, this meaningless, because it's for disabling
//! cl.exe warnings
#define DISABLE_VC_WARNING(WARNNUM)
//! \brief on gcc, this meaningless, because it's for restoring
//! cl.exe warnings
#define RESTORE_VC_WARNING(WARNNUM)

//! \brief proxy function, to perform the actual pragma (ie: message
//! or whatever) 'x'
#define DO_PRAGMA(x) _Pragma(#x)
//! \brief Cause the specified message to be emitted as a warning
#define EMIT_WARNING(STR) DO_PRAGMA(GCC warning #STR)

#else   // visual studio 'cl.exe'
//! \brief Cause the specified message to be emitted as a warning
//!
//! \details Uses warning c4199, since this is a warning that has no
//! description itself, and is used as a proxy for external (to vc++
//! compiler) warnings
#define EMIT_WARNING(STR)                      \
	__pragma(message(FILELOCATION ": warning " \
								  "c4199: " #STR))
//! \brief Cause a specified Visual c++ compiler warning to be
//! suppressed, on the next line
#define SUPPRESS_VC_WARNING(WARNNUM) \
	__pragma(warning(suppress : WARNNUM))
//! \brief Cause a specified Visual c++ compiler warning to be
//! disabled,until further notice
#define DISABLE_VC_WARNING(WARNNUM) \
	__pragma(warning(disable : WARNNUM))
//! \brief Cause a specified Visual c++ compiler warning to be
//! restored to normal status
#define RESTORE_VC_WARNING(WARNNUM) \
	__pragma(warning(default : WARNNUM))

#endif  // end compiler-specific sections

#endif  // FRAO_BASIC_API_CORE_DEFINES