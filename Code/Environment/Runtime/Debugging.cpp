//! \file
//!
//! \brief Implementation of basic debugging functions, used across
//! either this project, or across several others
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

//! \cond DOXYGEN_IGNORE

// To disable annoying macros that come with windows.h
// so that we don't get collisions on the name max()

#ifndef NOMINMAX
#define NOMINMAX
#endif

//! \endcond

#include "Environment/Runtime/Debugging.hpp"
#include "Environment/Static.hpp"

#if defined(_WIN32)	 // MSVC stdlib / Windows only

#include <Windows.h>  //for messagebox, and debug break
#include <string>	  //for std::wstring

#else  // Not MSVC stdlib / Windows

#include <sys/ptrace.h>	 //for ptrace
#include <unistd.h>		 //for execve
#include <csignal>		 //for raise
#include <string>		 //for std::string

#endif

namespace frao
{
inline namespace Environment
{
namespace IMPL
{
bool _runtime_assert_no_msg(bool condition) noexcept
{
	if constexpr (assertsEnabled())
	{
		if (!condition)
		{
			DebugPauseExecution();
		}
	}

	return condition;
}
}  // namespace IMPL


namespace
{
//! \brief Will create a generic messagebox, in the OS-appropriate way
//!
//! \note Text will may be converted to another UTF encoding, on
//! certain OS'
//!
//! \param[in] message The UTF8 message to display in the
//! messagebox
//!
//! \param[in] caption The UTF8 caption to display, on the
//! messagebox
//!
//! \param[in] errorBox Should we create an error box, or a normal
//! message box? True for error box
void MessageBox_IMPL(const charU8* message, const charU8* caption,
					 bool errorBox) noexcept
{
#if defined(_WIN32)

	UINT style = MB_OK;

	size_t msgLen = MultiByteToWideChar(
			   CP_UTF8, 0, reinterpret_cast<const char*>(message), -1,
			   nullptr, 0),
		   capLen = MultiByteToWideChar(
			   CP_UTF8, 0, reinterpret_cast<const char*>(caption), -1,
			   nullptr, 0);

	std::wstring msgStr(msgLen, L'\0'), capStr(capLen, L'\0');

	// since we have to pass as smaller sizes, make sure they fit
	runtime_assert(msgStr.size() <= std::numeric_limits<int>::max());
	runtime_assert(capStr.size() <= std::numeric_limits<int>::max());

	int msgRes = MultiByteToWideChar(
			CP_UTF8, 0, reinterpret_cast<const char*>(message), -1,
			msgStr.data(), static_cast<int>(msgStr.size())),
		capRes = MultiByteToWideChar(
			CP_UTF8, 0, reinterpret_cast<const char*>(caption), -1,
			capStr.data(), static_cast<int>(capStr.size()));

	if (errorBox)
	{
		style |= MB_ICONERROR | MB_TASKMODAL | MB_SETFOREGROUND;
	}

	MessageBoxW(
		nullptr,
		(msgRes > 0) ? msgStr.c_str() : L"Error converting message",
		(capRes > 0) ? capStr.c_str() : L"Error converting caption",
		style);


#elif defined(__linux__)

	char infoCmd[] = "--info", errorCmd[] = "--error",
		 exe[] = "zenity";

	// don't think we need to add quotes around the data, since we're
	// passing as a single variable, not via bash or anything
	std::string fullMessage("--text="), fullCaption("--title=");

	// even if we're using not using ASCII, but are using UTF8 this
	// should be fine, since UTF8 doesn't contain null bytes, except
	// for null itself
	fullMessage.append(reinterpret_cast<const char*>(message));
	fullCaption.append(reinterpret_cast<const char*>(caption));

	const char*		   pathname = "/bin/zenity";
	char *const		   args[]	= {exe, errorBox ? errorCmd : infoCmd,
						   fullMessage.data(), fullCaption.data(),
						   nullptr},
				*const env[]	= {nullptr};

	execve(pathname, args, env);

#else  // Not windows or linux, Calling function in wrong OS

	static_assert(false,
				  "Attempted to compile MessageBox_IMPL() function "
				  "in unrecognised OS");

#endif
}
}  // namespace

bool DebuggerPresent() noexcept
{
#if defined(_WIN32)

	// compare against FALSE macro, not TRUE, because windows
	// documentation says that a debugger being present result in a
	// 'nonzero' result. It does not say that such a nonzero value
	// need necessarily be TRUE (although this is probably what is
	// meant, it's better to be safe here, and take them at their
	// word)
	return IsDebuggerPresent() != FALSE;

#elif defined(__linux__)

	if (ptrace(PTRACE_TRACEME, 0, nullptr, nullptr) < 0)
	{
		// couldn't complete, because we have a debugger attached
		// already
		return true;
	}

	return false;

#else  // Not windows or linux, Calling function in wrong OS

	static_assert(false,
				  "Attempted to compile DebuggerPresent() function "
				  "in unrecognised OS");

#endif
}

void DebugPauseExecution() noexcept
{
	if (DebuggerPresent())
	{
#if defined(_WIN32)

		// DebugBreak() shouldn't be called unless we do have a
		// debugger present, since calling it in another circumstance
		// may cause termination
		DebugBreak();

#elif defined(__linux__)

		// The Linux version, with linux logic to raise a signal trap
		raise(SIGTRAP);

#else  // Not windows or linux, Calling function in wrong OS

		static_assert(false,
					  "Attempted to compile DebugPauseExecution() "
					  "function in unrecognised OS");

#endif
	}
}
void CreateRuntimeErrorBox(const charU8* message,
						   const charU8* caption) noexcept
{
	MessageBox_IMPL(message, caption, true);
}

}  // namespace Environment
}  // namespace frao
