#ifndef FRAO_NUCLEUS_STRINGS_GRAPHEMECLUSTER_IMPLEMENTATION_EXPERIMENTAL
#define FRAO_NUCLEUS_STRINGS_GRAPHEMECLUSTER_IMPLEMENTATION_EXPERIMENTAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the experimental implementation of unicode
//! grapheme cluster class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <initializer_list>	 //for initializer_list constructor
#include <vector>			 //for internal m_DataArray implementation
#include "../../Environment.hpp"
#include "../CodePoint.hpp"
#include "StringStore.hpp"

namespace frao
{
inline namespace Strings
{
namespace Experimental
{
//! \brief Experimental function to determine whether the first
//! cluster in a string is whitespace or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined as
//! whitespace
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isWhitespaceIMPL_E(const CharType* string,
						frao::natural64 bufferSize) noexcept;

//! \brief Experimental function to determine whether the first
//! cluster in a string is blank or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined
//! as blank
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isBlankIMPL_E(const CharType* string,
				   frao::natural64 bufferSize) noexcept;

//! \brief Experimental function to determine whether the first
//! cluster in a string is a newline or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined as a
//! newline
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isNewlineIMPL_E(const CharType* string,
					 frao::natural64 bufferSize) noexcept;
//! \brief Experimental function to determine whether the first
//! cluster in a string is numeric or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined
//! as numeric
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isNumericIMPL_E(const CharType* string,
					 frao::natural64 bufferSize) noexcept;

//! \brief Experimental implementation class, for grapheme clusters
//!
//! \tparam CharType The underlying type of the grapheme cluster
//! [charU8, char16_t, char32_t, wchar_t].
template<typename CharType>
class NUCLEUS_LIB_API GraphemeClusterIMPL
{
	//! \brief Stores the actual cluster data
	IMPL::StringStore<CharType> m_ClusterData;

   public:
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = CodePoint<CharType, true>;

	//! \brief construct empty grapheme cluster, containing nothing
	GraphemeClusterIMPL() : m_ClusterData()
	{}
	//! \brief Construct a grapheme cluster, from the specified
	//! string. Will get either the first or last grapheme cluster in
	//! the string, depending on the value of getLastCluster
	//!
	//! \param[in] cluster string buffer that conatains at
	//! least one cluster
	//!
	//! \param[in] buffersize size of the buffer used for
	//! the string in 'cluster'
	//!
	//! \param[in] getLastCluster iff true, will scan for the last
	//! valid grapheme cluster in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct grapheme cluster to read. If null, will not be written
	//! to
	GraphemeClusterIMPL(const CharType* cluster,
						frao::natural64 buffersize,
						bool getLastCluster, natural64* readStart);

	//! \brief Construct a grapheme cluster, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8/16/32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	GraphemeClusterIMPL(charA asciiChar)
		: m_ClusterData(&asciiChar, 1)
	{}

	//! \brief construct a grapheme cluster from a list of codepoints
	//!
	//! \param[in] codepoints An initialiser list, containing the
	//! desired codepoints
	GraphemeClusterIMPL(
		std::initializer_list<CodePointType> codepoints);

	//! \brief Copy construct a grapheme cluster, from another cluster
	//! of any underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other grapheme cluster, that
	//! we are copying from. One of [charU8, char16_t, wchar_t,
	//! char32_t]
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	template<typename OtherType>
	explicit GraphemeClusterIMPL(
		const GraphemeClusterIMPL<OtherType>& rhs);
	//! \brief Copy construct a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL(const GraphemeClusterIMPL<CharType>& rhs) =
		default;
	//! \brief Move construct a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL(GraphemeClusterIMPL<CharType>&& rhs) =
		default;
	//! \brief destroy the grapheme cluster, and call any derived
	//! destructors
	virtual ~GraphemeClusterIMPL() = default;

	//! \brief Copy assign a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL<CharType>& operator		  =(
		   const GraphemeClusterIMPL<CharType>& rhs) = default;
	//! \brief Move assign a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL<CharType>& operator	 =(
		  GraphemeClusterIMPL<CharType>&& rhs) = default;

	//! \brief determines whether this grapheme cluster is
	//! whitespace
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is whitespace
	bool isWhitespaceIMPL_m() const noexcept;

	//! \brief determines whether this grapheme cluster is considered
	//! blank
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is blank
	bool isBlankIMPL_m() const noexcept;

	//! \brief determines whether this grapheme cluster is a
	//! newline
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is a newline
	bool isNewlineIMPL_m() const noexcept;

	//! \brief determines whether this grapheme cluster is numeric
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is numeric
	bool isNumericIMPL_m() const noexcept;

	//! \brief returns memory size of grapheme cluster, in
	//! bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return m_ClusterData.byteSize();
	}
	//! \brief returns logical size of grapheme cluster, in
	//! code-points
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of code-points
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_ClusterData.size();
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	CodePointType& getIMPL(frao::natural64 index) noexcept;
	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A const code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	const CodePointType& getIMPL(
		frao::natural64 index) const noexcept;

	//! \brief Will determine the ordering of this, against a
	//! grapheme cluster, of any valid underlying type
	//!
	//! \tparam OtherType The type of the other cluster, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \details Compares the grapheme clusters, with this being on
	//! the left hand side, and the other grapheme cluster on the
	//! right. Will return < 0 for *this < other, will return > 0 for
	//! *this > other, and will return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than, and >
	//! 0 meaning greater than
	//!
	//! \param[in] otherCluster The grapheme cluster to compare this
	//! with, on the rhs.
	template<typename OtherType>
	frao::integer64 getOrderIMPL(const GraphemeClusterIMPL<OtherType>&
									 otherCluster) const noexcept;
};
}  // namespace Experimental
}  // namespace Strings
}  // namespace frao
#endif