//! \file
//!
//! \brief Implementation of frao Error Organisational
//! structures
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Error/Organisation.hpp"
#include <filesystem>  //for platform-agnostic paths
#include <fstream>
#include "Environment.hpp"

namespace frao
{
inline namespace ErrorLogging
{
//! \brief convenience typedef, for output file stream
using OFStreamType =
	std::basic_ofstream<charU8, std::char_traits<charU8>>;

//! \class ErrorLog
//!
//! \todo We need to add make the ErrorLog allocate some
//! (small) amount of memory up front (on creation), so that
//! if there is some fatal error that stops us from
//! allocating new memory, that we can still log what went
//! wrong

// ErrorLog functions
// private
utf8string ErrorLog::getLogToWrite() const
{
	// lock for reading, since we only use this function to collate
	// the file string
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);
	utf8string							toWrite;

	for (auto iter = m_OnePastLastLoggedError;
		 iter != m_ErrorList.end(); ++iter)
	{
		utf8string errorName = std::get<0>(*iter)->mainName();
		toWrite += errorName;

		switch (std::get<2>(*iter))
		{
			case ErrorStatus::NotAnError:
				break;
			case ErrorStatus::Resolved:
				toWrite += u8" Resolved Error";
				break;
			case ErrorStatus::Warning:
				toWrite += u8" Warning";
				break;
			default:
				toWrite += u8" Error";
				break;
		}

		toWrite += u8"\t";

		// If the error is in the log, it must have happened once. So
		// stating the number of times is redundant, unless it is at
		// least 2
		if (std::get<1>(*iter) > 1)
		{
			toWrite += u8"Occurred ";
			toWrite += utf8string(std::get<1>(*iter));
			toWrite += u8" times\t";
		}

		if constexpr (Environment::checkCompileMode()
					  == Environment::CompileMode::Debug)
		{
			toWrite += u8"In ";
			toWrite += std::get<0>(*iter)->getWhere();
			toWrite += u8"\t";
		}

		toWrite += std::get<0>(*iter)->getMessage();
		toWrite += u8"\t";

		if (std::get<1>(*iter) != 0)
		{
			toWrite += u8"starting ";
		}

		toWrite += u8"at ";
		toWrite +=
			std::get<0>(*iter)->getTime().time24hr<charU8, true>();
		toWrite += u8" On ";
		toWrite +=
			std::get<0>(*iter)->getTime().dateShort<charU8, true>();
		toWrite += u8"\r\n";
	}


	return toWrite;
}
//
void ErrorLog::addErrorIMPL(std::shared_ptr<BaseError> error,
							ErrorMatchingMode		   mode,
							ErrorStatus				   status)
{
	runtime_assert(error, "We must have a valid error!");

	if (error->mainName() == utf8string(Success::m_Name))
	{
		status = ErrorStatus::NotAnError;
	}

	// acquire a write-lock, and setup variables for checkback
	std::shared_lock<std::shared_mutex> readlock(m_WriteMutex);
	natural64							checkback =
		  (mode != ErrorMatchingMode::Disabled) ? m_CheckBack : 0;
	iterator		 loc	 = m_ErrorList.end();
	reverse_iterator checkIt = m_ErrorList.rbegin();

	if (checkback > m_ErrorList.size())
	{
		// because we can't check messages that don't
		// exist...
		checkback = m_ErrorList.size();
	}

	for (; checkback != 0; --checkback, ++checkIt)
	{
		// cache error, so that we don't have to call std::get<0>...
		// all the time, which isn't the most readable thing anyway.
		const auto& loopError = std::get<0>(*checkIt);

		// Go to the correct level of checks, that need to pass for
		// amalgamation
		switch (mode)
		{
			case ErrorMatchingMode::Strict:
				if (loopError->getMessage() != error->getMessage())
				{
					// then messages don't match - no amalgamation
					continue;
				}

				[[fallthrough]];
			case ErrorMatchingMode::CallLocation:
				if (loopError->getWhere() != error->getWhere())
				{
					// then error locations don't match, and the
					// functions were created from different places -
					// no amalgamation
					continue;
				}

				[[fallthrough]];
			case ErrorMatchingMode::Rough:
				if (std::get<2>(*checkIt) != status)
				{
					// then error status' don't match - no
					// amalgamation
					continue;
				}

				[[fallthrough]];
			case ErrorMatchingMode::Coarse:
				if (loopError->mainName() != error->mainName())
				{
					// then error types don't match - no amalgamation
					continue;
				}

				// then all required checks passed
				break;
			default:
				runtime_assert(
					false,
					"default case should not be possible; All modes "
					"are accounted for, and Disabled mode not "
					"possible in this loop");
				break;
		}

		// this will get the associated forward iterator
		loc = (++checkIt).base();

		// get rid of read lock, and have write lock, instead
		readlock.unlock();
		std::unique_lock<std::shared_mutex> templock(m_WriteMutex);

		// do increment
		++(std::get<1>(*loc));

		// get rid of write lock, and re-lock read lock
		templock.unlock();
		readlock.lock();

		break;
	}

	// get rid of read lock, and do write lock
	readlock.unlock();
	std::unique_lock<std::shared_mutex> templock(m_WriteMutex);

	if (checkback == 0)
	{
		// then no repeats - add error


		m_ErrorList.emplace(m_ErrorList.end(), error, 1ULL, status);
		templock.unlock();

		loc = getLastError();

		templock.lock();

		// we need to check if we have a marker for the next log entry
		// to write. If we have no such marker, we need to add the
		// element just added as it
		if (m_OnePastLastLoggedError == m_ErrorList.end())
		{
			m_OnePastLastLoggedError = loc;
		}
	}

	// sets error to be the active error, if wanted
	if (status == ErrorStatus::Active) m_ActiveError = loc;
}
ErrorResolution ErrorLog::justResolve(BaseError& error)
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);
	ErrorResolution result = ErrorResolution::Unresolved;

	for (auto rIter = m_ResolveStack.rbegin();
		 (result != ErrorResolution::Resolved)
		 && (rIter != m_ResolveStack.rend());)
	{
		switch (rIter->errorAccepted(error))
		{
			case ErrorAcceptance::NoFunctor:
			{				
				// get rid of read lock, and do write lock
				readLock.unlock();
				std::unique_lock<std::shared_mutex> tempLock(m_WriteMutex);

				// The resolver pointed to, no longer exists. Erase
				// the element, and set the iterator to the next
				// element
				rIter = decltype(rIter)(
					m_ResolveStack.erase(std::next(rIter).base()));

				// get rid of write lock, and re-lock read lock
				tempLock.unlock();
				readLock.lock();
			}
			break;
			case ErrorAcceptance::Accepted:
				result = rIter->tryResolveError(error);
				++rIter;  // is case we didn't resolve it
				break;
			default:
				++rIter;
				break;
		}
	}

	return result;
}
// public
ErrorLog::ErrorLog(utf8string logPath, natural64 checkBack)
	: m_ErrorList(),
	  m_ResolveStack(),
	  m_LogFileName(std::move(logPath)),
	  m_WriteMutex(),
	  m_OnePastLastLoggedError(m_ErrorList.end()),
	  m_ActiveError(m_ErrorList.end()),
	  m_CheckBack(checkBack),
	  m_AppendToFile(false)
{
	// make sure that the errorlist is not empty
	m_ErrorList.emplace_back(
		std::make_shared<create_type<Success>>(
			FILELOC_UTF8, u8"Log startup complete"_ustring),
		1ULL, ErrorStatus::NotAnError);
	m_OnePastLastLoggedError = m_ErrorList.begin();
	m_ActiveError			 = m_ErrorList.begin();
}
// we use a function try-block, to ensure that locks are always
// released
ErrorLog::ErrorLog(const ErrorLog& rhs)
try : m_ErrorList(
	(rhs.m_WriteMutex.lock_shared(),
	 rhs.m_ErrorList)),	 // NOTE: this is slightly unusual, but
						 // will call lock_shared(), and then
						 // assign

	m_ResolveStack(rhs.m_ResolveStack),
	m_LogFileName(rhs.m_LogFileName), m_WriteMutex(),
	m_OnePastLastLoggedError(rhs.m_OnePastLastLoggedError),
	m_ActiveError(rhs.m_ActiveError), m_CheckBack(rhs.m_CheckBack),
	m_AppendToFile(rhs.m_AppendToFile)
{
	rhs.m_WriteMutex.unlock_shared();
} catch (...)
{
	// we must unlock the rhs handler, even if we throw
	rhs.m_WriteMutex.unlock_shared();
}
ErrorLog::ErrorLog(ErrorLog&& rhs)
try : m_ErrorList(
	(rhs.m_WriteMutex.lock(),
	 std::move(rhs.m_ErrorList))),	// NOTE: this is slightly unusual,
									// but will call lock(), and then
									// move assign

	m_ResolveStack(std::move(rhs.m_ResolveStack)),
	m_LogFileName(std::move(rhs.m_LogFileName)), m_WriteMutex(),
	m_OnePastLastLoggedError(rhs.m_OnePastLastLoggedError),
	m_ActiveError(rhs.m_ActiveError), m_CheckBack(rhs.m_CheckBack),
	m_AppendToFile(rhs.m_AppendToFile)
{
	rhs.m_WriteMutex.unlock();
} catch (...)
{
	// we must unlock the rhs handler, even if we throw
	rhs.m_WriteMutex.unlock();
}
//! \todo Find a way to deal with duplicate errors
bool ErrorLog::resolveError(BaseError& error)
{
	bool resolved = (justResolve(error) == ErrorResolution::Resolved);

	if (resolved)
	{
		// then we should mark it as resolved

		// setup read lock, because we might only need to read
		std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

		// get error that has been resolved
		for (auto iter = m_ErrorList.begin();
			 iter != m_ErrorList.end(); ++iter)
		{
			if ((std::get<0>(*iter)->mainName() == error.mainName())
				&& (std::get<0>(*iter)->getTime() == error.getTime()))
			{
				// get rid of read lock, and acquire a write lock
				readLock.unlock();
				std::unique_lock<std::shared_mutex> writeLock(
					m_WriteMutex);

				// set the error to resolved
				std::get<2>(*iter) = ErrorStatus::Resolved;
				return resolved;
			}
		}

		// unlock, ready for adding the resolved error
		readLock.unlock();

		// then we had no error to resolve!
		utf8string message =
			u8"Could not find logged error that matched resolved error. Resolved error type: "_ustring
			+ error.mainName()
			+ u8"\tResolved error message: "_ustring
			+ error.getMessage()
			+ u8"\tResolved error location: "_ustring
			+ error.getWhere();
		addError<NotFound>(FILELOC_UTF8, message, false,
						   ErrorStatus::Resolved);
	}

	return resolved;
}
//
ErrorResolver ErrorLog::addErrorResolver(
	std::function<ErrorAcceptance(BaseError&)> filter,
	std::function<ErrorResolution(BaseError&)> handler)
{
	ErrorResolver resolver(std::move(filter), std::move(handler));

	std::unique_lock<std::shared_mutex> writeLock(m_WriteMutex);
	ResolveReferrer						weakPtr(resolver);
	m_ResolveStack.push_back(weakPtr);

	return resolver;
}
//
void ErrorLog::removeError(const BaseError& error)
{
	// setup read lock, because we might only need to read
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	// find error that will be removed
	auto founditer = m_ErrorList.end();
	for (auto iter = m_ErrorList.begin(); iter != m_ErrorList.end();
		 ++iter)
	{
		if ((std::get<0>(*iter)->mainName() == error.mainName())
			&& (std::get<0>(*iter)->getTime() == error.getTime()))
			founditer = iter;
	}

	if (founditer != m_ErrorList.end())
	{
		// get rid of read lock, and acquire a write lock
		readLock.unlock();
		std::unique_lock<std::shared_mutex> writeLock(m_WriteMutex);

		if (m_OnePastLastLoggedError == founditer)
			++m_OnePastLastLoggedError;

		if (m_ActiveError == founditer)
		{
			m_ActiveError = m_ErrorList.end();

			if (m_ErrorList.size()) --m_ActiveError;
		}

		--(std::get<1>(*founditer));

		if (std::get<1>(*founditer) == 0)
		{
			m_ErrorList.erase(founditer);
		}
	}
}
//
void ErrorLog::logErrors()
{
	// we use standard files here, for two reasons:
	// 1. Portability + cross-platform robustness, since
	// logErrors() is not particularly a performance concern
	// - not in the 		write of the file, anyway
	// (performance is bounded by log collation time mostly)
	// 2. We don't have our FileIO here, since it relies on
	// this
	utf8string buffer(getLogToWrite());

	if (buffer.size() == 0)
	{
		// then we've already written everything possible
		return;
	}

	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);
	OFStreamType						file;
	std::ios_base::openmode				mode = std::ios_base::out;

	// should we append to the file, as we've written to this file
	// before, or should we
	if (m_AppendToFile)
	{
		mode |= std::ios_base::app;
	} else
	{
		mode |= std::ios_base::trunc;
	}

	// Open file, with filesystem worrying about OS considerations
	file.open(std::filesystem::path(
				  m_LogFileName.data(),
				  m_LogFileName.data() + m_LogFileName.byteSize()),
			  mode);

	// get rid of the read lock, because we're done reading, and test
	// the file object (which should always have succeeded)
	readLock.unlock();
	runtime_assert(static_cast<bool>(file),
				   "Failed to write to log!");

	file << buffer;

	// close the file, and open this for writing
	// TODO: switch to indexes, instead of iterators, and we can
	// switch m_App... and m_OneP... to atomic types. Which means we
	// could get rid of this write lock
	file.close();
	std::unique_lock<std::shared_mutex> writeLock(m_WriteMutex);

	m_AppendToFile			 = true;
	m_OnePastLastLoggedError = m_ErrorList.end();
}
void ErrorLog::clearLog()
{
	// we obviously need a write lock, to clear the log
	std::unique_lock<std::shared_mutex> writeLock(m_WriteMutex);

	m_ErrorList.clear();
	m_AppendToFile = false;

	// make sure invariants fulfilled
	m_ErrorList.emplace_back(
		std::make_shared<create_type<Success>>(
			FILELOC_UTF8, u8"Successfully cleared log!"_ustring),
		1ULL, ErrorStatus::NotAnError);
	m_OnePastLastLoggedError = m_ErrorList.begin();
	m_ActiveError			 = m_ErrorList.begin();
}
//
ErrorLog::iterator ErrorLog::getLastError()
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return --(m_ErrorList.end());
}
ErrorLog::const_iterator ErrorLog::getLastError() const
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return --(m_ErrorList.cend());
}
ErrorLog::iterator ErrorLog::getActiveError()
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return m_ActiveError;
}
ErrorLog::const_iterator ErrorLog::getActiveError() const
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return m_ActiveError;
}
//
ErrorLog::iterator ErrorLog::getNextErrorToLog() const
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return m_OnePastLastLoggedError;
}
//
ErrorLog::const_iterator ErrorLog::begin() const
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return m_ErrorList.begin();
}
ErrorLog::const_iterator ErrorLog::end() const
{
	std::shared_lock<std::shared_mutex> readLock(m_WriteMutex);

	return m_ErrorList.end();
}
//
void ErrorLog::setActiveError(iterator iter)
{
	std::unique_lock<std::shared_mutex> writeLock(m_WriteMutex);

	if (iter != m_ErrorList.end()) m_ActiveError = iter;
}
}  // namespace ErrorLogging
}  // namespace frao