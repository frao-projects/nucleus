#ifndef FRAO_NUCLEUS_STRINGS_GRAPHEMECLUSTER_IMPLEMENTATION_TRANSITIONAL
#define FRAO_NUCLEUS_STRINGS_GRAPHEMECLUSTER_IMPLEMENTATION_TRANSITIONAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the transitional implementation of unicode
//! grapheme cluster class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "../../Environment.hpp"
#include "../CodePoint.hpp"

namespace frao
{
inline namespace Strings
{
namespace Transitional
{
//! \brief Transitional function to determine whether the first
//! cluster in a string is whitespace or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined as
//! whitespace
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isWhitespaceIMPL(const CharType* string,
					  frao::natural64 bufferSize) noexcept
{
	runtime_assert(string != nullptr);
	runtime_assert(bufferSize != 0);

	if constexpr (sizeof(CharType) == sizeof(charU8))
	{
		// then we assume we have a utf8 string
		return IMPL::isWhitespace_UTF8(string, bufferSize);
	} else if constexpr (std::is_same_v<CharType, wchar_t>)
	{
		// then we have a wide string
		return IMPL::isWhitespace_WCHAR(*string);
	} else if constexpr (std::is_same_v<CharType, char16_t>)
	{
		// then we have a u16 string
		return IMPL::isWhitespace_UTF16(*string);
	} else
	{
		// then we have a u32 string
		static_assert(std::is_same_v<CharType, char32_t>,
					  "We must have a string type!");

		return IMPL::isWhitespace_UTF32(*string);
	}
}
//! \brief Transitional function to determine whether the first
//! cluster in a string is blank or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined
//! as blank
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isBlankIMPL(const CharType* string,
				 frao::natural64 bufferSize) noexcept
{
	runtime_assert(string != nullptr);
	runtime_assert(bufferSize != 0);

	if constexpr (sizeof(CharType) == sizeof(charU8))
	{
		// then we assume we have a utf8 string
		return IMPL::isBlank_UTF8(string, bufferSize);
	} else if constexpr (std::is_same_v<CharType, wchar_t>)
	{
		// then we have a wide string
		return IMPL::isBlank_WCHAR(*string);
	} else if constexpr (std::is_same_v<CharType, char16_t>)
	{
		// then we have a u16 string
		return IMPL::isBlank_UTF16(*string);
	} else
	{
		// then we have a u32 string
		static_assert(std::is_same_v<CharType, char32_t>,
					  "We must have a string type!");

		return IMPL::isBlank_UTF32(*string);
	}
}
//! \brief Transitional function to determine whether the first
//! cluster in a string is a newline or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined as a
//! newline
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isNewlineIMPL(const CharType* string,
				   frao::natural64 bufferSize) noexcept
{
	runtime_assert(string != nullptr);
	runtime_assert(bufferSize != 0);

	if constexpr (sizeof(CharType) == sizeof(charU8))
	{
		// then we assume we have a utf8 string
		return IMPL::isNewline_UTF8(string, bufferSize);
	} else if constexpr (std::is_same_v<CharType, wchar_t>)
	{
		// then we have a wide string
		return IMPL::isNewline_WCHAR(*string);
	} else if constexpr (std::is_same_v<CharType, char16_t>)
	{
		// then we have a u16 string
		return IMPL::isNewline_UTF16(*string);
	} else
	{
		// then we have a u32 string
		static_assert(std::is_same_v<CharType, char32_t>,
					  "We must have a string type!");

		return IMPL::isNewline_UTF32(*string);
	}
}
//! \brief Experimental function to determine whether the first
//! cluster in a string is numeric or not
//!
//! \tparam OtherType The type of the other string. One of [charU8,
//! char16_t, wchar_t, char32_t]
//!
//! \returns true if the grapheme cluster could be determined
//! as numeric
//!
//! \param[in] string A pointer to a string, of specified size,
//! that contains the cluster to be examined
//!
//! \param[in] bufferSize The size of the buffer that contains the
//! string, in elements
template<typename CharType>
bool isNumericIMPL(const CharType* string,
				   frao::natural64 bufferSize) noexcept
{
	runtime_assert(string != nullptr);
	runtime_assert(bufferSize != 0);

	if constexpr (sizeof(CharType) == sizeof(charU8))
	{
		// then we assume we have a utf8 string
		return IMPL::isNumeric_UTF8(string, bufferSize);
	} else if constexpr (std::is_same_v<CharType, wchar_t>)
	{
		// then we have a wide string
		return IMPL::isNumeric_WCHAR(*string);
	} else if constexpr (std::is_same_v<CharType, char16_t>)
	{
		// then we have a u16 string
		return IMPL::isNumeric_UTF16(*string);
	} else
	{
		// then we have a u32 string
		static_assert(std::is_same_v<CharType, char32_t>,
					  "We must have a string type!");

		return IMPL::isNumeric_UTF32(*string);
	}
}


//! \brief Transitional implementation class, for grapheme clusters
//!
//! \tparam CharType The underlying type of the grapheme cluster
//! [charU8, char16_t, char32_t, wchar_t].
template<typename CharType>
class NUCLEUS_LIB_API GraphemeClusterIMPL
{
   protected:
	//! \brief The stored grapheme cluster. Our transitional
	//! implementation assumes that grapheme clusters will only have
	//! one codepoint
	CodePoint<CharType, false> m_Data;

   public:
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using CodePointType = decltype(m_Data);

	//! \brief construct empty grapheme cluster
	GraphemeClusterIMPL() : m_Data()
	{}
	//! \brief Construct a grapheme cluster, from the specified
	//! string. Will get either the first or last grapheme cluster in
	//! the string, depending on the value of getLastCluster
	//!
	//! \param[in] cluster string buffer that conatains at
	//! least one cluster. Must not be null
	//!
	//! \param[in] buffersize size of the buffer used for
	//! the string in 'cluster'. Must not be 0
	//!
	//! \param[in] getLastCluster iff true, will scan for the last
	//! valid grapheme cluster in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct grapheme cluster to read. If null, will not be written
	//! to
	GraphemeClusterIMPL(const CharType* cluster,
						frao::natural64 buffersize,
						bool getLastCluster, natural64* readStart)
		: m_Data(cluster, buffersize, getLastCluster, readStart)
	{}

	//! brief Construct a grapheme cluster, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8/16/32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	GraphemeClusterIMPL(charA asciiChar) : m_Data(asciiChar)
	{}

	//! \brief Copy construct a grapheme cluster, from another cluster
	//! of any underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other grapheme cluster, that
	//! we are copying from. One of [charU8, char16_t, wchar_t,
	//! char32_t]
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	template<typename OtherType>
	explicit GraphemeClusterIMPL(
		const GraphemeClusterIMPL<OtherType>& rhs)
		: m_Data(static_cast<decltype(m_Data)>(rhs.getIMPL(0)))
	{
		runtime_assert(rhs.sizeIMPL() != 0,
					   "We assume that grapheme clusters are always "
					   "of size 1. Class must be re-written, without "
					   "that assumption, if this fires");
	}
	//! \brief Copy construct a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL(const GraphemeClusterIMPL<CharType>& rhs)
		: m_Data(rhs.getIMPL(0))
	{
		runtime_assert(rhs.sizeIMPL() != 0,
					   "We assume that grapheme clusters are always "
					   "of size 1. Class must be re-written, without "
					   "that assumption, if this fires");
	}
	//! \brief Move construct a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL(GraphemeClusterIMPL<CharType>&& rhs) =
		default;
	//! \brief destroy the grapheme cluster, and call any derived
	//! destructors
	virtual ~GraphemeClusterIMPL() = default;

	//! \brief Copy assign a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL<CharType>& operator		  =(
		   const GraphemeClusterIMPL<CharType>& rhs) = default;
	//! \brief Move assign a grapheme cluster, from another cluster
	//! of the same underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The grapheme cluster to copy from
	GraphemeClusterIMPL<CharType>& operator	 =(
		  GraphemeClusterIMPL<CharType>&& rhs) = default;

	//! \brief determines whether this grapheme cluster is
	//! whitespace
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is whitespace
	bool isWhitespaceIMPL_m() const noexcept
	{
		return isWhitespaceIMPL(&(m_Data[0]), m_Data.size());
	}
	//! \brief determines whether this grapheme cluster is considered
	//! blank
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is blank
	bool isBlankIMPL_m() const noexcept
	{
		return isBlankIMPL(&(m_Data[0]), m_Data.size());
	}
	//! \brief determines whether this grapheme cluster is a
	//! newline
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is a newline
	bool isNewlineIMPL_m() const noexcept
	{
		return isNewlineIMPL(&(m_Data[0]), m_Data.size());
	}
	//! \brief determines whether this grapheme cluster is numeric
	//!
	//! \returns bool of value true iff this grapheme
	//! cluster is numeric
	bool isNumericIMPL_m() const noexcept
	{
		return isNumericIMPL(&(m_Data[0]), m_Data.size());
	}

	//! \brief returns memory size of grapheme cluster, in
	//! bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return m_Data.byteSize();
	}
	//! \brief returns logical size of grapheme cluster, in
	//! code-points
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of code-points
	frao::natural64 sizeIMPL() const noexcept
	{
		return 1;
	}

	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	CodePointType& getIMPL(frao::natural64 index) noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Data;
	}
	//! \brief get a section of this grapheme cluster.
	//! Grapheme clusters may be composed of multiple
	//! code-points, and this funciton will return the
	//! code-point at the index in question
	//!
	//! \returns A const code-point reference of the same
	//! underlying type as this grapheme cluster
	//!
	//! \param[in] index indicating the section of the
	//! grapheme cluster to reference
	const CodePointType& getIMPL(frao::natural64 index) const noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Data;
	}

	//! \brief Will determine the ordering of this, against a
	//! grapheme cluster, of any valid underlying type
	//!
	//! \tparam OtherType The type of the other cluster, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \details Compares the grapheme clusters, with this being on
	//! the left hand side, and the other grapheme cluster on the
	//! right. Will return < 0 for *this < other, will return > 0 for
	//! *this > other, and will return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than, and >
	//! 0 meaning greater than
	//!
	//! \param[in] otherCluster The grapheme cluster to compare this
	//! with, on the rhs.
	template<typename OtherType>
	frao::integer64 getOrderIMPL(const GraphemeClusterIMPL<OtherType>&
									 otherCluster) const noexcept
	{
		// check relative sizes of clusters
		frao::integer64 diff =
			static_cast<frao::integer64>(this->sizeIMPL())
			- static_cast<frao::integer64>(otherCluster.sizeIMPL());
		frao::natural64 maxcluster =
			(diff < 0) ? this->sizeIMPL() : otherCluster.sizeIMPL();

		for (frao::natural64 index = 0; index < maxcluster; ++index)
		{
#if defined(__cpp_impl_three_way_comparison) && defined(__cpp_lib_three_way_comparison)

			std::strong_ordering rawOrder =
				this->m_Data <=> otherCluster.getIMPL(index);
			frao::integer64 order;

			// stupid (and ideally temporary) hack bc strong_ordering
			// isn't convertible to int
			if (rawOrder < 0)
			{
				order = -1;
			} else if (rawOrder > 0)
			{
				order = 1;
			} else
			{
				// must be equal
				order = 0;
			}

#else

			frao::integer64 order =
				m_Data.getOrder(otherCluster.getIMPL(index));
#endif

			if (order != 0)
			{
				return order;
			}
		}

		// since all codepoints are equal for compariable
		// portion, it follows that order is just the size order
		return diff;
	}
};
}  // namespace Transitional
}  // namespace Strings
}  // namespace frao
#endif