#ifndef FRAO_NUCLEUS_ENVIRONMENT_RUNTIME_DEBUGGING
#define FRAO_NUCLEUS_ENVIRONMENT_RUNTIME_DEBUGGING

//! \file
//!
//! \brief Header of basic debugging functions (asserts etc.) that are
//! necessary for basic functioning of multiple frao projects
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "../Static.hpp"
#include <exception> //for std::exception

namespace frao
{
inline namespace Environment
{
namespace IMPL
{
//! \brief A debug mode function, that will cause a pause of execution
//! with no message
//!
//! \returns Whatever was passed in condition, so that this can be
//! used in an expression
//!
//! \param[in] condition If this is true (and Debug is the compile
//! mode), execution will be paused
NUCLEUS_LIB_API bool _runtime_assert_no_msg(bool condition) noexcept;
}  // namespace IMPL

//! \defgroup DebugFunctions Debug Functions
//!
//! \brief functions for use in debugging scenarios, and probably not
//! much else
//!
//! \note At least one function in this group won't compile outside of
//! a debug build
//!
//! {

//! \brief Will determine whether a debugger is currently running
//!
//! \returns A value of true iff a debugger has been detected as
//! running, and false otherwise
NUCLEUS_LIB_API bool DebuggerPresent() noexcept;

//! \brief A **DEBUG ONLY** function, that will pause execution
//!
//! \note Will break the build, if used outside of a debug build. For
//! this reason, there is a compile time check as to whether this is a
//! debug build. If the check determines that this is not a debug
//! build
NUCLEUS_LIB_API void DebugPauseExecution() noexcept;

//! \brief Creates an error box with the specified message
//!
//! \param[in] message *UTF8* message to display on the error box. Null-terminated
//!
//! \param[in] caption The caption to display, at the top of the error
//! box. Null-terminated
NUCLEUS_LIB_API void CreateRuntimeErrorBox(
	const charU8* message,
	const charU8* caption = u8"Runtime Error!") noexcept;

//! \brief an assert for *debug* builds, that will pause execution and
//! create an error box if the condition isn't met
//!
//! \returns bool that denotes whether the condition evaluated to true
//!
//! \param[in] condition const reference to a parameter that can be
//! casted to bool
template<typename Type>
bool runtime_assert(const Type& condition) noexcept
{
	return IMPL::_runtime_assert_no_msg(static_cast<bool>(condition));
}

//! \brief an assert for *debug* builds, that will pause execution and
//! create an error box, with the message provided, if the condition
//! isn't met
//!
//! \returns bool that denotes whether the condition evaluated to true
//!
//! \param[in] condition const reference to a parameter that can be
//! casted to bool
//!
//! \param[in] message c-array of characters, that holds a string (ie:
//! a strings literal)
template<typename Type, natural64 Size>
bool runtime_assert(
	const Type& condition,
	[[maybe_unused]] const charA (&message)[Size]) noexcept
{
	bool result = static_cast<bool>(condition);

	if constexpr (Environment::assertsEnabled())
	{
		if (!result)
		{
			CreateRuntimeErrorBox(reinterpret_cast<const charU8*>(message));
			DebugPauseExecution();
		}
	}

	return result;
}

//! @}


//! \brief Safely deletes 'new'ed memory, and sets *obj to null
//!
//! \param[in, out] obj pointer to the pointer that was passed to new

template<class T>
inline void SafeDelete(T** obj) noexcept
{
	// deleting a null ptr has no harmful side effects
	// anyway, so no need for a test.
	delete *obj;
	*obj = nullptr;
}

//! \brief Safely Deletes [array] 'new'ed memory, and sets *obj to
//! null
//!
//! \param[in, out] obj pointer to the pointer that was passed to
//! new[]
template<class T>
inline void SafeArrayDelete(T** obj) noexcept
{
	// deleting a null ptr has no harmful side effects
	// anyway, so no need for a test.
	delete[] * obj;
	*obj = nullptr;
}

}  // namespace Environment
}  // namespace frao

#endif	// FRAO_NUCLEUS_CORE_FUNCTIONS
