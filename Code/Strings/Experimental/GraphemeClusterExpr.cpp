//! \file
//!
//! \brief Experimental implmentation of Unicode Grapheme classes
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/Experimental/GraphemeClusterIMPL_Expr.hpp"

#if defined(FRAO_EXPERIMENTAL_STRINGS_ALLOWED) \
	&& defined(NUCLEUS_HEADER_ONLY)
// frao::Strings::Experimental::GraphemeClusterIMPL instantiations
template class frao::Strings::Experimental::GraphemeClusterIMPL<
	charU8>;
template class frao::Strings::Experimental::GraphemeClusterIMPL<
	wchar_t>;
template class frao::Strings::Experimental::GraphemeClusterIMPL<
	char16_t>;
template class frao::Strings::Experimental::GraphemeClusterIMPL<
	char32_t>;
#endif