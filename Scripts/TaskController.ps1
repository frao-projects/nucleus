param(
	# The type of task to perform
	[Parameter(Mandatory=$true)]
	[string]$CommandName,
	# The location of the .frao directory, of this project
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
	[string]$FraoDir,
	#Extra inputs for some commands
    [parameter(ValueFromRemainingArguments = $true)]
	[string[]]$ExtraInputs
)


#Outputs a hashtable containing the calculated arguments
function ConvertTo-Hashtable {
	param (
		# The list of parameters / arguments to convert to hash table. NOTE: switch
		# parameters should be followed by a 'true'/'false' boolean string, as appropriate
		[Parameter(Mandatory=$true)]
		[string[]]
		$ArgsList
	)

	[hashtable]$result = @{}
	$lastCommand = $null
	[bool]$IsSingleString = $False

	for ($i = 0; $i -lt $ArgsList.Count; $i++) {
		$local:ThisElement = $ArgsList[$i]

		if((-not [string]::IsNullOrEmpty($ThisElement)) -and ($ThisElement.Substring(0, 1) -eq "-")) {
			$lastCommand = $ThisElement.Substring(1)

			#All elements can be arrays, unless otherwise specified, later
			$IsSingleString = $False
						
			if($ThisElement.Contains('@')) {
				$elements = $ThisElement -split "@"
								
				if($elements[1] -eq "string") {
					$lastCommand = $elements[0].Substring(1)
					$IsSingleString = $True
				}
			}

			$result += @{ "$lastCommand" = $null}
		} elseif(-not [string]::IsNullOrEmpty($lastCommand)) {
			if($result.ContainsKey($lastCommand)) {
				$local:outBool = $null

				if([bool]::TryParse($ThisElement, [ref]$outBool)) {
					# Then we have a boolean argument - assume we want to pass a single
					# boolean value, rather than an array of strings
					$result.$lastCommand = $outBool
	
					# So we skip any subsequent arguments
					$lastCommand = $null
					continue
				} elseif($null -eq $result.$lastCommand) {
					$result.$lastCommand = $ThisElement
				} elseif($IsSingleString) {
					$result.$lastCommand += " "
					$result.$lastCommand += $ThisElement
				} elseif(-not ($result.$lastCommand -is [array])) {
					#Then we don't have an array, but we need another argument - so make one
					$local:lastArg = $result["$lastCommand"]
					
					$result.$lastCommand = @("$lastArg", "$ThisElement")
				} else {
					# Add another argument to the array
					$result.$lastCommand += "$ThisElement"
				}
			}
		}
	}

	return $result
}

# Note: exact string comparison is case-insensitive
switch -Exact ($CommandName) {
	'Increment' {
		Write-Host "Increment command demanded."

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/IncrementVersion.ps1" @realTable
		break
	}
	'Generate_Project' { 
		Write-Host "Generate Project command demanded. Command to be tested:"

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/SetupNewProject.ps1" -FraoDir "$FraoDir" @realTable
		break
	}
	'Regenerate_meson.build_source_list' {
		Write-Host "Regenerate meson.build source list command demanded."

		if($ExtraInputs) {
			$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
			. "$PSScriptRoot/GenerateMeson.ps1" -SourceDir '.' -NameDir 'Code' @realTable
		} else {
			. "$PSScriptRoot/GenerateMeson.ps1" -SourceDir '.' -NameDir 'Code'
		}

		break
	}
	'Build' {
		Write-Host "Build command demanded."

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/RunNinjaTargets.ps1" -FraoDir "$FraoDir" @realTable
		break
	}
	'Documentation' {
		Write-Host "Documentation Build command demanded."

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/RunNinjaTargets.ps1" -FraoDir "$FraoDir" @realTable
		break
	}
	'Test' {
		Write-Host "Test command demanded."

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/RunNinjaTargets.ps1" -FraoDir "$FraoDir" @realTable
		break
	}
	'Install' {
		Write-Host "Install command demanded."
		
		Write-Host ("Extra args:" + ($ExtraInputs -join ", "))

		$realTable = ConvertTo-Hashtable -ArgsList $ExtraInputs
		. "$PSScriptRoot/RunNinjaTargets.ps1" -FraoDir "$FraoDir" -Targets install @realTable
		break
	}
	Default {
		throw "Command '$CommandName' not known! Aborting."
	}
}