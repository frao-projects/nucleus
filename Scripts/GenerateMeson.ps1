using Module .\Generation.psm1

param(
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
	[string]$SourceDir,
	[string]$NameDir,
	[string]$WriteFile="meson.build",
	[string]$FileVariableName="source_files",
	[string[]]$NonTestableFiles=@("wWinMain.c*", "main.c*"),
	[string[]]$Extensions=@("*.cpp", "*.hpp")
)

#get the lists of files
$local:TestableList = Get-FileList -SourceDir $SourceDir -NameDir $NameDir -ToInclude $Extensions -ToExclude $NonTestableFiles
$local:NonTestableList = @()

if($NonTestableFiles.Length -gt 0)
{
	$local:NonTestableList = Get-FileList -SourceDir $SourceDir -NameDir $NameDir -ToInclude $NonTestableFiles
}

if(($TestableList.Length -eq 0 ) -and ($NonTestableList.Length -eq 0))
{
	throw 'No File could be determined!'
}

#open the testable file list, write file section, and then close it
[string]$local:mesonFileContent="testable_$FileVariableName = files("
$mesonFileContent += , @($TestableList) | New-FileList
$mesonFileContent += "`r`n`t)"

#open the nontestable file list, write file section, and then close it
$mesonFileContent +="`r`n`r`nuntestable_$FileVariableName = files("
$mesonFileContent += , @($NonTestableList) | New-FileList
$mesonFileContent += "`r`n`t)"

#add a testable/untestable file collation, as well
$mesonFileContent += "`r`n`r`n$FileVariableName = [testable_$FileVariableName, untestable_$FileVariableName]"

[string]$local:buildDir = "$SourceDir"
if($NameDir.Length -gt 0)
{
	$buildDir += "\$NameDir"
}

#write the text to a new file
Write-NewFile -existingDir $buildDir -filePath $WriteFile -fileData $mesonFileContent
Write-Host "Meson generated"