param(
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Leaf})]
	[string]$TargetFile,
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Container})]
	[string]$ShortcutLocation,
	[Parameter(Mandatory=$true)]
	[string]$ShortcutName
)

$FullLocation = (Get-Item -Path $ShortcutLocation).FullName
$ShortcutFullPath = Join-Path -Path $FullLocation -ChildPath $ShortcutName

if(-not(Test-Path -Path $ShortcutFullPath -IsValid))
{
	throw 'Specified Shortcut full path was invalid!'
}

if($IsWindows)
{
	#On windows we create a .lnk file
	$LNK = New-Object -ComObject WScript.Shell
	$Shortcut = $LNK.CreateShortcut("$ShortcutFullPath")
	$Shortcut.TargetPath = $TargetFile
	$Shortcut.Save()
} else {
	#On non-windows, we create a symbolic link
	New-Item -Path "$ShortcutFullPath" -ItemType SymbolicLink -Value "$TargetFile"
}