#ifndef FRAO_NUCLEUS_STRINGS_WORD_IMPLEMENTATION_TRANSITIONAL
#define FRAO_NUCLEUS_STRINGS_WORD_IMPLEMENTATION_TRANSITIONAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the transitional implementation of unicode
//! word class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <algorithm>		 //for std::find/ std::find_if
#include <array>			 //for std::array
#include <cmath>			 //for floor / pow etc
#include <initializer_list>	 //for initializer_list constructor
#include <vector>			 //for internal m_DataArray implementation
#include "../../BitUtilities.hpp"  //for BitFloat
#include "../../Environment.hpp"
#include "../GraphemeCluster.hpp"

namespace frao
{
inline namespace Strings
{
namespace Transitional
{
//! \brief Transitional implementation class, for words
//!
//! \tparam CharType The underlying type of the word [charU8,
//! char16_t, char32_t, wchar_t].
template<typename CharType, typename NumericalPunctuation>
class NUCLEUS_LIB_API WordIMPL
{
   protected:
	//! \brief The stored grapheme clusters. We obviously cannot
	//! assume that words have any particular maximum length, so we
	//! need a dynamic array
	std::vector<GraphemeCluster<CharType, false>> m_DataArray;

   public:
	//! \brief Type for referring to our own type, where appropriate
	using OurType =
		Transitional::WordIMPL<CharType, NumericalPunctuation>;
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = typename decltype(m_DataArray)::value_type;

   protected:
	//! \brief Convert a string to its constituent digits
	//!
	//! \returns A boolean indicating whether the operation was
	//! successful. The operation will fail, if the string contains
	//! anything other than digits
	//!
	//! \param[in] string The string to convert to digits. Must not
	//! contain anything but digits, of the form specified in
	//! digitClusters
	//!
	//! \param[in] digitClusters a const reference to an array that
	//! holds each digit's representation
	//!
	//! \param[out] digits A reference to an array, that will hold the
	//! retreived digits
	static bool stringToDigits(
		OurType							   word,
		const std::array<ClusterType, 10>& digitClusters,
		std::vector<natural8>&			   digits)
	{
		// compare a section of our string, to see if it contains the
		// specified cluster
		auto stringHas = [&word](ClusterType cluster,
								 natural64&	 stringIndex) -> bool {
			natural64 clusterElements =
				cluster.byteSize() / sizeof(CharType);
			if ((stringIndex + clusterElements) > word.sizeIMPL())
			{
				return false;
			}

			natural64 codepointIndex = 0;

			for (; codepointIndex < cluster.size(); ++codepointIndex)
			{
				if (word.m_DataArray[stringIndex + codepointIndex]
					!= cluster)
				{
					return false;
				}
			}

			// if we got a match, return the character after the match
			stringIndex += codepointIndex;
			return true;
		};

		auto getDigit = [&digitClusters, &stringHas](
							natural64& stringIndex,
							natural8&  digitNum) -> bool {
			for (natural8 digitIndex = 0; digitIndex < 10;
				 ++digitIndex)
			{
				if (stringHas(digitClusters[digitIndex], stringIndex))
				{
					digitNum = digitIndex;
					return true;
				}
			}

			return false;
		};

		digits.clear();

		for (natural64 charIndex = 0; charIndex < word.sizeIMPL();)
		{
			natural8 nextDigit;

			if (getDigit(charIndex, nextDigit))
			{
				digits.push_back(nextDigit);
			} else
			{
				// this isn't a digit!
				return false;
			}
		}

		return true;
	}

	//! \brief create a substring, of the string provided
	//!
	//! \pre We require that readStart comes before, or is equal to,
	//! readEnd, and in the same container. As a consequence,
	//! readStart must also point to a valid element
	//!
	//! \returns A subword of the range provided
	//!
	//! \param[in] readStart The iterator of the first position to
	//! include in the sub-word, from the array of grapheme clusters
	//! to copy from
	//!
	//! \param[in] readEnd The iterator of one past the final position
	//! to include in the sub-word
	static OurType createSubWord(
		typename decltype(m_DataArray)::const_iterator readStart,
		typename decltype(m_DataArray)::const_iterator readEnd)
	{
		OurType result;

		if ((readStart == readEnd) || ((readEnd - readStart) < 0))
		{
			return result;
		}

		for (auto iter = readStart; iter != readEnd; ++iter)
		{
			result.appendIMPL(*iter);
		}

		return result;
	}
	//! \brief Convert a series of digits, to the value (ignoring
	//! sign) they are meant to represent
	//!
	//! \tparam IntegerType The type of the integer, to be created
	//! from the digits
	//!
	//! \returns A boolean, indictating whether the conversion was
	//! successful
	//!
	//! \param[in, out] number A reference to the location at which to
	//! store the created integer, if successful. Upon entry to the
	//! function, is read to see if it's value lies above or below 0.
	//! If below, a negative number will be created
	//!
	//! \param[in] digits The digits, from which to create the
	//! integer. Highest-order digit at the front of the array
	template<typename IntegerType>
	static bool getIntegerValueFromDigits(
		IntegerType& number, std::vector<natural8> digits)
	{
		static_assert(std::numeric_limits<IntegerType>::is_integer,
					  "Type must be an integer!");

		// if the number is meant to be negative, we want a negative
		// magnitude
		integer64 magnitude = (number < 0) ? -1 : 1,
				  lastMag = magnitude, nextnumber = 0;
		number = static_cast<IntegerType>(nextnumber);

		for (auto rIter = digits.rbegin(); rIter != digits.rend();
			 ++rIter, magnitude *= 10)
		{
			if (std::abs(magnitude) < std::abs(lastMag))
			{
				// magnitude overflowed
				return false;
			}

			lastMag = magnitude;
			nextnumber += (*rIter) * magnitude;

			if (std::abs(nextnumber)
				< std::abs(static_cast<integer64>(number)))
			{
				// then the number overflowed
				return false;
			}

			number = static_cast<IntegerType>(nextnumber);
		}

		return true;
	}

	//! \brief Get a signed integer, from the stored string
	//!
	//! \tparam SignedType The type, of the signed integer
	//!
	//! \returns true, if the operation completed successfully
	//!
	//! \param[out] number The signed integer, that is the
	//! result of converting this string to signed int
	//!
	//! \param[in] startLoc iterator of m_DataArray, to the start of
	//! the position in the string, to start looking for a signed
	//! integer
	template<typename SignedType>
	bool getSignedFromString(
		SignedType&									   number,
		typename decltype(m_DataArray)::const_iterator startLoc)
		const noexcept
	{
		static_assert(
			std::numeric_limits<SignedType>::is_integer
				&& std::numeric_limits<SignedType>::is_signed,
			"Type must be a signed integer!");

		if (startLoc == m_DataArray.end())
		{
			// then we failed the sanity check of "Does this operation
			// make sense"
			return false;
		}

		auto numberStr = NumericalPunctuation::getNumberString();
		ClusterType negationChar =
			NumericalPunctuation::getNegationCharacter();

		// check for negative symbol, at start of string
		if ((*startLoc) == negationChar)
		{
			number = -1;
			++startLoc;

			// make sure it's still long enough
			if (startLoc == m_DataArray.end())
			{
				number = 0;
				return false;
			}
		} else
		{
			number = 1;
		}

		std::vector<natural8> digits;
		if (!stringToDigits(
				createSubWord(startLoc, m_DataArray.end()), numberStr,
				digits))
		{
			// then we had no actual digits
			return false;
		}

		return getIntegerValueFromDigits(number, digits);
	}
	//! \brief Get an unsigned integer, from the stored string
	//!
	//! \tparam UnsignedType The type, of the unsigned integer
	//!
	//! \returns true, if the operation completed successfully
	//!
	//! \param[out] number The unsigned integer, that is the
	//! result of converting this string to unsigned int
	template<typename UnsignedType>
	bool getUnsignedFromString(UnsignedType& number) const noexcept
	{
		static_assert(
			std::numeric_limits<UnsignedType>::is_integer
				&& !std::numeric_limits<UnsignedType>::is_signed,
			"Type must be an unsigned integer!");

		if (sizeIMPL() == 0)
		{
			// then we failed the sanity check of "Does this operation
			// make sense"
			return false;
		}

		auto numberStr = NumericalPunctuation::getNumberString();

		std::vector<natural8> digits;
		if (!stringToDigits(*this, numberStr, digits))
		{
			// then we had no actual digits
			return false;
		}

		// number is of an unsigned type, and hence has a positive
		// value, entering the function. So the digits will be read
		// correctly
		return getIntegerValueFromDigits(number, digits);
	}


	//! \brief Get a floating point number, from the stored string
	//!
	//! \tparam FloatType The type, of the floating point number
	//!
	//! \returns true, if the operation completed successfully
	//!
	//! \param[out] number The floating point number, that is the
	//! result of converting this string to float
	template<typename FloatType>
	bool getFloatFromString(FloatType& number) const noexcept
	{
		auto numberStr = NumericalPunctuation::getNumberString();
		ClusterType negationChar =
						NumericalPunctuation::getNegationCharacter(),
					dotChar = NumericalPunctuation::getDotCharacter(),
					eChar	= NumericalPunctuation::getECharacter();

		bool positive = true, hasDot = false, hasExponent = false;

		auto numStartLoc = m_DataArray.begin();

		if (numStartLoc == m_DataArray.end())
		{
			number = std::numeric_limits<FloatType>::quiet_NaN();
			return false;
		}

		// check for negative symbol, at start of string
		if ((*numStartLoc) == negationChar)
		{
			positive = false;
			++numStartLoc;

			// make sure it's still long enough
			if (numStartLoc == m_DataArray.end())
			{
				number = std::numeric_limits<FloatType>::quiet_NaN();
				return false;
			}
		}

		// functor to find the specified word, in the given range of
		// grapheme clusters
		using itType = typename decltype(m_DataArray)::const_iterator;
		auto findWord = [](const OurType& word, itType begin,
						   itType end) -> itType {
			runtime_assert(begin <= end);

			if (word.sizeIMPL() == 0)
			{
				return end;
			}

			for (auto iter = begin; iter != end; ++iter)
			{
				natural64 remaining = end - iter;

				// iterate over word, until we find a non-match, or
				// the end
				for (natural64 index = 0;; ++index)
				{
					if (index >= word.sizeIMPL())
					{
						// we reached the end of the word, so we have
						// a match
						return iter;
					} else if (index >= remaining)
					{
						// we have reached the end of the string, with
						// no match
						return end;
					} else if ((*(iter + index))
							   != word.getIMPL(index))
					{
						// the search word doesn't match this part of
						// the searchee word
						break;
					}
				}
			}

			return end;
		};

		// check for +/- Inf
		if (findWord(NumericalPunctuation::getInfString(),
					 numStartLoc, m_DataArray.end())
			!= m_DataArray.end())
		{
			// we have +/- Inf
			if (positive)
			{
				// we have a positive infinity
				number = std::numeric_limits<FloatType>::infinity();
				return true;
			} else
			{
				// we have a negative infinity
				number = -std::numeric_limits<FloatType>::infinity();
				return true;
			}
		}

		// check for NaN
		if (findWord(NumericalPunctuation::getNaNString(),
					 numStartLoc, m_DataArray.end())
			!= m_DataArray.end())
		{
			// we have a nan
			number = std::numeric_limits<FloatType>::quiet_NaN();
			return true;
		}

		// get the location of the E
		auto eLoc = std::find(numStartLoc, m_DataArray.end(), eChar);
		if (eLoc != m_DataArray.end())
		{
			// make sure that the e wasn't the last thing in the
			// string. If it was, set hasExponent to false
			hasExponent = (eLoc + 1) != m_DataArray.end();
		}

		// find the dot
		auto dotLoc = std::find(numStartLoc, eLoc, dotChar);
		if (dotLoc != m_DataArray.end())
		{
			// make sure that the dot wasn't the last thing in the
			// string. If it was, set hasDot to false
			hasDot = (dotLoc + 1) != eLoc;
		}

		std::vector<natural8> mainDigits, fractionalDigits;

		if ((dotLoc != numStartLoc)
			&& !stringToDigits(createSubWord(numStartLoc, dotLoc),
							   numberStr, mainDigits))
		{
			// then we had characters before the dot, and they weren't
			// digits
			number = std::numeric_limits<FloatType>::quiet_NaN();
			return false;
		}

		if (hasDot
			&& !stringToDigits(createSubWord(dotLoc + 1, eLoc),
							   numberStr, fractionalDigits))
		{
			// then the characters after the dot weren't digits
			number = std::numeric_limits<FloatType>::quiet_NaN();
			return false;
		}

		// ensure we have somewhere to store the exponent
		integer16 exponent = 0;
		if (hasExponent)
		{
			// then we have digits after the exponent
			if (!getSignedFromString(exponent, eLoc + 1))
			{
				// if there's nothing after the exponent, act as if we
				// have no exponent
				// hasExponent = false;

				// Then we had something after the exponent, but we
				// failed to determine what it was (possibly because
				// it wasn't a number)
				number = std::numeric_limits<FloatType>::quiet_NaN();
				return false;
			}
		}

		number = 0.0;

		// make sure that we don't have too many fractional digits, to
		// represent in the target type
		FloatType magnitude;
		do
		{
			magnitude = static_cast<FloatType>(
				std::pow(0.1, fractionalDigits.size()));

			if (magnitude >= std::numeric_limits<FloatType>::min())
			{
				break;
			}

			fractionalDigits.pop_back();
		} while (fractionalDigits.size() != 0);

		// we start from the smallest part of the fraction, in order
		// to minimise rounding errors, while adding up
		for (auto rIter = fractionalDigits.rbegin();
			 rIter != fractionalDigits.rend();
			 ++rIter, magnitude *= 10.0f)
		{
			number += (*rIter) * magnitude;
		}

		// we should approximately have 1.0
		runtime_assert(
			(magnitude < static_cast<FloatType>(1.05f))
				&& (magnitude >= static_cast<FloatType>(0.95f)),
			"Regardless of how many fractional digits we "
			"have, we should have ~1.0 by now");
		magnitude = 1.0f;  // set to exactly 1, because we're probably
						   // slightly off

		// now add the main part of the number, again from the
		// smallest part of it
		for (auto rIter = mainDigits.rbegin();
			 rIter != mainDigits.rend(); ++rIter, magnitude *= 10.0f)
		{
			number += (*rIter) * magnitude;
		}

		if (hasExponent)
		{
			// if we have an exponent, apply it
			number *= static_cast<FloatType>(
				std::pow(static_cast<FloatType>(10.0), exponent));
		}
		if (!positive)
		{
			// negate the number
			number = -number;
		}

		return true;
	}

	//! \brief Get a word of the specified integer value. That
	//! is, lexically cast an integer value to string
	//!
	//! \tparam InputType The type of integer, that shall be
	//! converted to string
	//!
	//! \returns A word, which corresponds to the word
	//! representation of that number. eg: a value of 91, will
	//! return
	//! {'9', '1'}
	//!
	//! \param[in] number The integer value to comvert to string
	//!
	//! \param[in] units The highest power of ten that can be held
	//! in the integer value
	template<typename InputType>
	static OurType getWordOf(InputType number,
							 InputType units) noexcept
	{
		static_assert(std::is_integral<InputType>::value,
					  "InputType must be an integral type!");
		runtime_assert(units
					   != 0);  // because dividing by zero is bad!

		OurType resultStr;
		auto	castStr = NumericalPunctuation::getNumberString();

		// remove leading zeros!
		while ((units != 0) && ((number / units) == 0))
		{
			units /= 10;
		}

		// because we can't divide by zero, below
		if (units == 0) units = 1;

		// store number as unsigned type,
		using UnsignedType = std::make_unsigned_t<InputType>;
		UnsignedType realNumber;
		if (number < 0)
		{
			// for a negative number, make positive, and store the
			// negation char
			resultStr.appendIMPL(
				NumericalPunctuation::getNegationCharacter());

			// this correctly converts/negates negative signed numbers
			// to unsigned, regardless of representation. This is
			// because the signed -> unsigned conversion results in a
			// value such that the result is congruent to the original
			// value modulo 2^n. Then, the subtraction on an unsigned
			// int does 2^n - value. eg: for -128 in a int8, this will
			// first be converted to 127. This will then be subtracted
			// from 0, (rather than negated, in order to supress
			// warnings), by calculating 2^n-127 = 255-127 = 128.
			// Thus, we end up with the correct value
			realNumber = 0 - static_cast<UnsignedType>(number);
		} else
		{
			realNumber = static_cast<UnsignedType>(number);
		}

		do
		{
			InputType usenum = realNumber / units;

			runtime_assert(usenum < 10);
			resultStr.appendIMPL(castStr[usenum]);

			realNumber %= units;

			if (units > 1)
			{
				units /= 10;
			} else
			{
				break;
			}
		} while (true);

		return resultStr;
	}

	//! \brief convert parsed data, of a fractional (floating point)
	//! number, to a string representing that fraction (float)
	//!
	//! \tparam ExponentStart The first power of ten that should be
	//! written using scientific notation "1.234E3", rather than
	//! normal "1234"
	//!
	//! \returns A string, of the specified float/fraction
	//!
	//! \param[in] digits The digits of the number, in order from
	//! highest value to lowest. eg: for a value 12.03, the digits
	//! would be {1, 2, 0, 3}
	//!
	//! \param[in] powerTen The power of ten, of the fraction. ie: the
	//! order of magnitude of the greatest digit
	//!
	//! \param[in] signBit should be set to true, if the number is
	//! negative
	template<frao::natural32 ExponentStart>
	static OurType getFractionWordOf(
		const std::vector<frao::natural32>& digits,
		frao::integer32 powerTen, bool signBit) noexcept
	{
		OurType resultStr;
		auto	numberStr = NumericalPunctuation::getNumberString();

		if (signBit)
		{
			resultStr.appendIMPL(
				NumericalPunctuation::getNegationCharacter());
		}

		bool skipExponent =
			(powerTen < static_cast<frao::integer32>(ExponentStart))
			&& (powerTen
				> -static_cast<frao::integer32>(ExponentStart));
		frao::natural32 digitStart =
							(skipExponent && (powerTen < 0))
								? static_cast<frao::natural32>(
									1 - powerTen)
								: 0,
						decimalPointAt =
							(skipExponent && (powerTen > 0))
								? static_cast<frao::natural32>(
									1 + powerTen)
								: 1,
						higherbound = digitStart < decimalPointAt
										  ? decimalPointAt
										  : digitStart,
						digitIndex = 0;

		// iterate over ambiguous protion, where the next
		// character could be 0, a digit, or a dot
		for (frao::natural32 realIndex = 0; realIndex <= higherbound;
			 ++realIndex)
		{
			if (realIndex == decimalPointAt)
			{
				resultStr.appendIMPL(
					NumericalPunctuation::getDotCharacter());
			} else if ((digitIndex < digits.size())
					   && (realIndex >= digitStart))
			{
				runtime_assert(digits[digitIndex] < 10);

				resultStr.appendIMPL(numberStr[digits[digitIndex]]);
				++digitIndex;
			} else
			{
				resultStr.appendIMPL(numberStr[0]);
			}
		}

		// now that we've done the ambiguous portion, we can
		// just add digits, without the checks for dot or
		// leading zeros
		while (digitIndex < digits.size())
		{
			runtime_assert(digits[digitIndex] < 10);

			resultStr.appendIMPL(numberStr[digits[digitIndex]]);

			++digitIndex;
		}

		if (!skipExponent)
		{
			resultStr.appendIMPL(
				NumericalPunctuation::getECharacter());

			resultStr.appendIMPL(getWordOf<frao::integer32>(powerTen, 10));
		}

		return resultStr;
	}

	//! \brief Get the base ten digits of the specified number
	//!
	//! \tparam FloatType The type of the floating point, from which
	//! we are retreiving this information
	//!
	//! \tparam Natural Type The type of unsigned integer, of equal
	//! size (in bytes) to the float, which we are using to analyse
	//! the float
	//!
	//! \returns An array, containing the calculated digits
	//!
	//! \param[in] number The number which we wish to find the digits
	//! of
	//!
	//! \param[out] digit The number of digits written
	//!
	//! \param[out] powerTen The calculated power of ten, of the
	//! number
	template<typename FloatType, typename NaturalType>
	static std::vector<frao::natural32> getDigitsFromFloat(
		BitFloat<FloatType, NaturalType> number,
		frao::integer32&				 powerTen) noexcept
	{
		static_assert(std::numeric_limits<FloatType>::is_iec559,
					  "floating point must be IEEE754!");

		std::vector<frao::natural32> resultDigits;

		if (number.num_n == 0)
		{
			// ie (biasedExponent == 0) && (prelimMantissa == 0)
			// we have +/- 0
			resultDigits.push_back(0);

			powerTen = 0;
		} else if (number.num_n
				   == std::numeric_limits<FloatType>::max())
		{
			// we have max number possible in this format!
			if constexpr (std::is_same_v<FloatType, float>)
			{
				resultDigits.push_back(3);
				resultDigits.push_back(4);
				resultDigits.push_back(0);
				resultDigits.push_back(2);
				resultDigits.push_back(8);
				resultDigits.push_back(2);
				resultDigits.push_back(3);
				resultDigits.push_back(5);

				powerTen = 38;
			} else
			{
				resultDigits.push_back(1);
				resultDigits.push_back(7);
				resultDigits.push_back(9);
				resultDigits.push_back(7);
				resultDigits.push_back(6);
				resultDigits.push_back(9);
				resultDigits.push_back(3);
				resultDigits.push_back(1);
				resultDigits.push_back(3);
				resultDigits.push_back(4);
				resultDigits.push_back(8);
				resultDigits.push_back(6);
				resultDigits.push_back(2);
				resultDigits.push_back(3);
				resultDigits.push_back(1);
				resultDigits.push_back(5);
				resultDigits.push_back(8);

				powerTen = 308;
			}

		} else
		{
			// we get the subsequent and antecedent values!
			// we know this is safe because we aren't a special
			// value, or one of the adjacent to special values

			BitFloat<FloatType, NaturalType> running(0.0f),
				sub(number.num_n + 1), ante(number.num_n - 1);

			constexpr FloatType two = static_cast<FloatType>(2.0);

			// find the "bucket" that rounds to this number. have to
			// divide before addition, ensure we don't overflow for
			// large numbers
			FloatType low = (ante.num_f / two) + (number.num_f / two),
					  high = (sub.num_f / two) + (number.num_f / two);

			// we get the next power of ten after high
			// (inclusive of high)
			FloatType integerpower = std::floor(std::log10(high)),
					  magnitude	   = std::pow(10.0f, integerpower),
					  remaining	   = number.num_f / magnitude, next;

			// minus 1 because in the string we Don't want a
			// leading zero!
			powerTen = static_cast<frao::integer32>(integerpower);

			bool higherThanLow, noDifferenceInIncrement;

			do
			{
				runtime_assert(remaining < 10.0f);

				// cast will truncate
				resultDigits.push_back(
					static_cast<frao::natural32>(remaining));
				remaining -=
					static_cast<FloatType>(resultDigits.back());
				remaining *= static_cast<FloatType>(10.0);

				// add calculated digit to a running total of
				// the number we have so far
				running.num_f +=
					static_cast<FloatType>(resultDigits.back())
					* magnitude;
				next =
					running.num_f + magnitude;	// to get next number!
				magnitude /= 10;

				// see if number so far fulfills exit conditions
				// of being large enough to be in the bucket, or
				// that incrementing number will result in a
				// number also in the bucket!
				higherThanLow			= running.num_f >= low;
				noDifferenceInIncrement = next <= high;
			} while ((!higherThanLow) && (!noDifferenceInIncrement));

			// check if we need to correct the last digit in
			// digits
			if (higherThanLow && noDifferenceInIncrement)
			{
				// break tie!
				BitFloat<FloatType, NaturalType> nextdiff(
					next - number.num_f),
					prevdiff(number.num_f - running.num_f);

				// this will make both prevdiff and nextdiff
				// positive! (for easier comparison). It does this by
				// ensuring the signed bit is 0
				nextdiff.num_n &= std::numeric_limits<
					std::make_signed_t<NaturalType>>::max();
				prevdiff.num_n &= std::numeric_limits<
					std::make_signed_t<NaturalType>>::max();

				if (prevdiff == nextdiff)
				{
					// then number is between the two, and is
					// equidistant. Break tie with bankers round
					if ((number.num_n & 0x1) != 0)
					{
						// then last digit is odd! add one to
						// make even, since both versions are
						// equidistant to real value
						resultDigits.back() += 1;
					}
				} else if (nextdiff < prevdiff)
				{
					// nextdiff is closer, so we need to correct
					// last digit
					resultDigits.back() += 1;
				}
			} else if (!higherThanLow)
			{
				// then we know that higherThanLow == false &&
				// noDifferenceInIncrement == true return next
				// number, ie: digits with incremented last
				// digit!

				// to get the next number, find the lowest value digit
				// we can increment, without having to carry the 1
				for (auto rIter = resultDigits.rbegin();
					 rIter != resultDigits.rend();
					 rIter = resultDigits.rbegin())
				{
					if ((*rIter) < 9)
					{
						*rIter += 1;
						break;
					} else
					{
						resultDigits.pop_back();
					}
				}

				if (resultDigits.size() == 0)
				{
					// then we had to carry out of the whole number -
					// increase the power of ten, and set 1
					powerTen += 1;
					resultDigits = {1};
				}
			}
		}

		runtime_assert(
			resultDigits.back() < 10,
			"It should be impossible to overflow, since the loop "
			"would have resolved the previous iteration, if an "
			"increment would cause us to have to carry the one up "
			"the magnitude scale");

		return resultDigits;
	}

	//! \brief Create a word that has value of the number specified
	//! (ie: convert binary representation of number to human-readable
	//! represenatation)
	//!
	//! \tparam FloatType The type of the floating point, from which
	//! we are retreiving this information
	//!
	//! \tparam Natural Type The type of unsigned integer, of equal
	//! size (in bytes) to the float, which we are using to analyse
	//! the float
	//!
	//! \returns A word, of the specified float
	//!
	//! \param[in] number floating point that should be converted to
	//! string
	template<typename FloatType, typename NaturalType>
	static OurType getWordOfFloat(FloatType number)
	{
		static_assert(std::numeric_limits<FloatType>::is_iec559,
					  "float->Basic_String conversion requires "
					  "IEEE 754 floating points!");

		using UnionType = BitFloat<FloatType, NaturalType>;

		OurType result;

		// intialise first member in union
		UnionType num(number);
		const frao::natural64
			prelimMantissa = num.num_n & UnionType::maxMantissa(),
			biasedExponent = (num.num_n >> UnionType::exponentShift())
							 & UnionType::maxBiasedExponent(),
			positiveMask =
				prelimMantissa
				| (biasedExponent << UnionType::exponentShift());
		bool signBit = (num.num_n & (~positiveMask)) != 0;

		// we get rid of the sign bit: we no longer need it!
		// (doing so will simplify some stuff)
		num.num_n &= positiveMask;

		if (biasedExponent == UnionType::maxBiasedExponent())
		{
			// then we have a 'special' number
			if (prelimMantissa == 0)
			{
				// we have +/- Infinity
				if (signBit)
				{
					result.appendIMPL(
						NumericalPunctuation::getNegationCharacter());
				}

				result.appendIMPL(static_cast<const OurType&>(
					NumericalPunctuation::getInfString()));
			} else
			{
				// then have a NaN (Not sure about quiet or
				// signalling, but we don't really care)
				result.appendIMPL(static_cast<const OurType&>(
					NumericalPunctuation::getNaNString()));
			}
		} else
		{
			frao::integer32 powerTen;

			auto digits = getDigitsFromFloat(num, powerTen);
			result = getFractionWordOf<3>(digits, powerTen, signBit);
		}

		return result;
	}

   public:
	//! \brief construct empty word
	WordIMPL() : m_DataArray()
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_nat8 number)
		: WordIMPL(getWordOf<frao::small_nat8>(number, 100))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat16 number)
		: WordIMPL(getWordOf<frao::small_nat16>(number, 10000))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat32 number)
		: WordIMPL(getWordOf<frao::small_nat32>(number, 1000000000U))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to a word
	explicit WordIMPL(frao::small_nat64 number)
		: WordIMPL(getWordOf<frao::small_nat64>(number, 10000000000000000000ULL))
	{}

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int8 number)
		: WordIMPL(getWordOf<frao::small_int8>(number, 100))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int16 number)
		: WordIMPL(getWordOf<frao::small_int16>(number, 10000))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int32 number)
		: WordIMPL(getWordOf<frao::small_int32>(number, 1000000000))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to a word
	explicit WordIMPL(frao::small_int64 number)
		: WordIMPL(getWordOf<frao::small_int64>(number, 1000000000000000000LL))
	{}

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to a word
	explicit WordIMPL(float number)
		: WordIMPL(getWordOfFloat<float, small_nat32>(number))
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to a word
	explicit WordIMPL(double number)
		: WordIMPL(getWordOfFloat<double, small_nat64>(number))
	{}

	//! \brief Construct a word, from the specified string. Will get
	//! either the first or last word in the string, depending on the
	//! value of getLastWord
	//!
	//! \param[in] string pointer to a buffer containing a string.
	//! This string has at least one word. Must not be null
	//!
	//! \param[in] buffersize unsigned 64-bit integer than contains
	//! the size of the buffer pointed to in 'string'. Must not be 0
	//!
	//! \param[in] getLastWord iff true, will scan for the last valid
	//! word in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct word to read. If null, will not be written to
	WordIMPL(const CharType* string, frao::natural64 buffersize,
			 bool getLastWord, natural64* readStart)
		: m_DataArray()
	{
		runtime_assert(string != nullptr);
		runtime_assert(buffersize != 0);

		decltype(m_DataArray) proxyArray;
		natural64			  tempReadStart,
			earliestRead = static_cast<natural64>(-1);

		for (natural64 index = 0; index < buffersize;)
		{
			ClusterType cluster(
				(getLastWord ? string : string + index),
				buffersize - index, getLastWord, &tempReadStart);

			if (earliestRead > tempReadStart)
			{
				earliestRead = tempReadStart;
			}

			if (cluster.isWhitespace())
			{
				break;
			} else
			{
				// add cluster, as it wasn't whitespace.
				proxyArray.emplace_back(std::move(cluster));

				// Work out next offset index. forwards and backwards
				// obviously increment differently
				if (getLastWord)
				{
					if (tempReadStart == 0)
					{
						// then we got all the way to the beginning of
						// the string, without finding whitespace -
						// break, because we can't check any further,
						// and should construct out of the whole
						// string
						break;
					}
					runtime_assert(buffersize >= tempReadStart);

					// make sure the next read is limited to the
					// clusters before the one we just read
					index = buffersize - tempReadStart;
				} else
				{
					// increment by the number of elements of CharType
					// that cluster has, added to where we started
					natural64 increment =
						tempReadStart
						+ (cluster.byteSize() / sizeof(CharType));

					index += increment;
					runtime_assert(increment > 0,
								   "Faulty logic means loop will "
								   "never terminate!");
				}
			}
		}

		if (getLastWord)
		{
			// then we need to reverse the proxyArray, into
			// m_DataArray
			m_DataArray.reserve(proxyArray.size());

			for (auto rIter = proxyArray.rbegin();
				 rIter != proxyArray.rend(); ++rIter)
			{
				m_DataArray.emplace_back(*rIter);
			}
		} else
		{
			// then proxyArray holds the data we need, so just use
			// that
			m_DataArray = std::move(proxyArray);
		}

		if (readStart != nullptr)
		{
			*readStart = earliestRead;
		}
	}

	//! \brief Construct a word from an ascii string
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	WordIMPL(const charA* string) : m_DataArray()
	{
		natural64 length = getBufferLength(string);
		m_DataArray.reserve(length);

		for (natural64 index = 0; index < length; ++index)
		{
			ClusterType cluster(string[index]);

			if (cluster.isWhitespace())
			{
				break;
			} else
			{
				m_DataArray.emplace_back(std::move(cluster));
			}
		}
	}
	//! \brief construct word from the first word in the specified
	//! string
	//!
	//! \param[in] clusters A list of grapheme clusters, to make into
	//! a word (note: they must actually consitute a [single] word, as
	//! far as unicode algorithms are concerned)
	WordIMPL(std::initializer_list<ClusterType> clusters)
		: m_DataArray(std::move(clusters))
	{}

	//! \brief Copy construct a word, from another word of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other word, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The word to copy from
	template<typename OtherType, typename OtherNumericalPunctuation>
	explicit WordIMPL(
		const WordIMPL<OtherType, OtherNumericalPunctuation>& rhs)
		: m_DataArray()
	{
		for (auto& cluster : rhs.m_DataArray)
		{
			m_DataArray.append(static_cast<ClusterType>(cluster));
		}
	}

	//! \brief Copy construct a word, from another word of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The word to copy from
	WordIMPL(const OurType& rhs) = default;
	//! \brief Move construct a word, from another word of the same
	//! underlying type as this
	//!
	//! \param[in] rhs The word to move from
	WordIMPL(OurType&& rhs) = default;
	//! \brief destroy the word, and call any derived destructors
	virtual ~WordIMPL() = default;

	//! \brief Copy assign a word, from another word of the same
	//! underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The word to copy from
	OurType& operator=(const OurType& rhs) = default;
	//! \brief Move assign a word, from another word of the same
	//! underlying type as this
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The word to move from
	OurType& operator=(OurType&& rhs) = default;

	//! \brief add a grapheme cluster to the end of this
	//! word
	//!
	//! \param[in] cluster to append to word. Should be a
	//! cluster that makes sense to append to this word.
	//! (ie: don't try and append a space to 'dog', for
	//! example)
	void appendIMPL(const ClusterType& cluster)
	{
		m_DataArray.push_back(cluster);
	}
	//! \brief add a grapheme cluster to the end of this
	//! word
	//!
	//! \param[in] cluster to append to word. Should be a
	//! cluster that makes sense to append to this word.
	//! (ie: don't try and append a space to 'dog', for
	//! example)
	void appendIMPL(const OurType& word)
	{
		for (natural64 clusterIndex = 0;
			 clusterIndex < word.sizeIMPL(); ++clusterIndex)
		{
			appendIMPL(word.getIMPL(clusterIndex));
		}
	}
	//! \brief remove all grapheme clusters from this word,
	//! rendering it empty
	void clearIMPL()
	{
		m_DataArray.clear();
	}

	//! \brief returns memory size of word, in bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSizeIMPL() const noexcept
	{
		frao::natural64 result = 0;

		for (const auto& cluster : m_DataArray)
		{
			result += cluster.byteSize();
		}

		return result;
	}
	//! \brief returns logical size of word, in grapheme
	//! clusters
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of grapheme clusters
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_DataArray.size();
	}

	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A Grapheme cluster reference of the same
	//! underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	ClusterType& getIMPL(frao::natural64 index) noexcept
	{
		runtime_assert(index < m_DataArray.size());

		return m_DataArray[index];
	}
	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A const Grapheme cluster reference of the
	//! same underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	const ClusterType& getIMPL(frao::natural64 index) const noexcept
	{
		runtime_assert(index < m_DataArray.size());

		return m_DataArray[index];
	}

	//! \brief converts this word to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[out] number reference to where the result should be
	//! held
	template<typename NumberType>
	bool toNumberIMPL(NumberType& number) const noexcept
	{
		static_assert(std::is_arithmetic<NumberType>::value,
					  "Can only convert to an arithmetic type!");

		if constexpr (std::is_floating_point<NumberType>::value)
		{
			// we need to convert to a float!
			return getFloatFromString(number);
		} else
		{
			// we need to convert to an integral type

			if constexpr (std::is_signed<NumberType>::value)
			{
				// we have a signed integer
				return getSignedFromString(number,
										   m_DataArray.begin());
			} else
			{
				// we have an unsigned integer
				static_assert(std::is_unsigned<NumberType>::value,
							  "Something went very wrong, because "
							  "all arithmetic types should be either "
							  "signed, unsigned or floating point");

				return getUnsignedFromString(number);
			}
		}
	}

	//! \brief Will determine the ordering of this, against a word, of
	//! any valid underlying type
	//!
	//! \tparam OtherType The type of the other word, that we are
	//! comparing with. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \details Compares the words, with this being on the left hand
	//! side, and the other words on the right. Will return < 0 for
	//! *this < other, will return > 0 for *this > other, and will
	//! return  0 for *this == other
	//!
	//! \returns An integer, which will hold the result of the
	//! comparison, with 0 meaning equal, < 0 meaning less than, and >
	//! 0 meaning greater than
	//!
	//! \param[in] otherWord The word to compare this with, on the
	//! rhs.
	template<typename OtherType, typename OtherNumericalPunctuation>
	frao::integer64 getOrderIMPL(
		const WordIMPL<OtherType, OtherNumericalPunctuation>&
			otherWord) const noexcept
	{
		// check relative sizes of words
		frao::integer64 diff =
			static_cast<frao::integer64>(this->sizeIMPL())
			- static_cast<frao::integer64>(otherWord.sizeIMPL());
		frao::natural64 maxcluster =
			(diff < 0) ? this->sizeIMPL() : otherWord.sizeIMPL();

		for (frao::natural64 index = 0; index < maxcluster; ++index)
		{
			frao::integer64 order =
				m_DataArray[index].getOrder(otherWord.getIMPL(index));

			if (order != 0)
			{
				return order;
			}
		}

		// since all clusters are equal for compariable portion, it
		// follows that order is just the size order
		return diff;
	}
};
}  // namespace Transitional
}  // namespace Strings
}  // namespace frao

#endif