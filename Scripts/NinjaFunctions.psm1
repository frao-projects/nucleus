#Create arrays of valid compilers/ build configs
set-variable -name ValidCompilers -value ([string[]]@("VS", "Clang_Cl", "Clang", "GCC")) -option Constant
set-variable -name ValidBuildConfigs -value ([string[]]@("debug", "release")) -option Constant

#ensures that scripts stop, when we encounter non-terminating errors
$ErrorActionPreference = 'Stop'

function Push-BuildSubDirectory {
	param (
		#The name of the build configuration (debug, release, etc), for this build directory
		[Parameter(Mandatory=$true)]
		[ValidateScript({$ValidBuildConfigs.contains($_)})]
		[string]$BuildConfig,
		#The name of the (main, c++) compiler, for this build directory
		[Parameter(Mandatory=$true)]
		[ValidateScript({$ValidCompilers.contains($_)})]
		[string]$Compiler,
		#The base level build directory, that holds the compiler/buildconfig sub-directories
		[Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container})]
		[string]$BuildDir,
		#The name of the OS
		[Parameter(Mandatory=$true)]
		[string]$OS
	)

	$local:subDir = ((Join-Path -Path $BuildDir -ChildPath $OS) |
						Join-Path -ChildPath $Compiler) |
						Join-Path -ChildPath $BuildConfig

	if(Test-Path -Path $subDir -PathType Container)
	{
		Push-Location $subDir
	}
	else
	{
		#failure case - could not find path
		throw "Could not find the determined build sub-directory:	$subDir"
	}
}

function Pop-PreviousDirectory {
	Pop-Location
}

function Invoke-Ninja {
	param (
		#What ninja target should be invoked
		[string] $Target,
		#should the target be cleaned, before building?
		[switch] $CleanBuild
	)

	#try to clean, if specified
	if($CleanBuild)
	{
		ninja -t clean $Target

		if($LastExitCode -ne 0)
		{
			throw "Ninja -t clean $Target failed!"
		}
	}

	#try to build
	ninja $Target

	if($LastExitCode -ne 0)
	{
		throw "Ninja $Target failed!"
	}
}

function Invoke-NinjaCycle {
	param(
		#The names of the targets to invoke
		[string[]] $Targets,
		# The directory that holds the builds
		[Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container})]
		[string] $BuildDir,
		#The name of the OS
		[Parameter(Mandatory=$true)]
		[string]$OS,
		#The name of the (main, c++) compiler, for this build directory
		[string] $Compiler,
		#The name of the build configuration (debug, release, etc), for this build directory
		[string] $BuildConfig,
		#Should the target be cleaned, before calling it?
		[switch] $CleanBuild
	)

	#navigate to build directory
	$dirParams = @{
		'BuildConfig' = "$BuildConfig";
		'Compiler' = "$Compiler";
		'BuildDir' = "$BuildDir";
		'OS' = "$OS";
	}
	Push-BuildSubDirectory @dirParams

	#If we have no target, assume we're going for a no-target invocation.
	#If we are, then we need to make sure that ninja gets invoked, so add an empty target
	if($null -eq $Targets) {
		$Targets = @("")
	} elseif ($Targets.Size -eq 0) {
		$Targets += ""
	}

	try {
		#perform targets, from the build directory
		foreach($local:target in $Targets) {
			Invoke-Ninja -CleanBuild:$CleanBuild -Target $target
		}

	}
	finally {
		#restore starting directory
		Pop-PreviousDirectory
	}
}
