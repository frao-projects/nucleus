class VersionSpec {
    [UInt16]$Major
    [Uint16]$Minor
    [string]$Status
    [UInt16]$Patch
    [Uint16]$DevBuild

    VersionSpec(
        [Uint16]$major,
        [Uint16]$minor,
        [string]$status,
        [Uint16]$patch,
        [Uint16]$dev_build
    ) {
        $this.Major = $major
        $this.Minor = $minor
        $this.Status = $status
        $this.Patch = $patch
        $this.DevBuild = $dev_build
    }

    [string] ToString() {
        return "$($this.Major).$($this.Minor).$($this.Status).$($this.Patch).$($this.DevBuild)"
    }
}

function ConvertFrom-VersionManifest {
    param (
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [String]
        $FileData
    )

    # '\.' is a regex for literal .
    [string[]]$local:Sections = $FileData -split '\.'

    if($Sections.Count -lt 5) {
        throw "Invalid version file: could not be parsed"
    }

    [UInt16]$local:MajorVersion = [UInt16]$Sections[0]
    [UInt16]$local:MinorVersion = [UInt16]$Sections[1]
    [string]$local:StatusVersion = $Sections[2]
    [UInt16]$local:ReleasePatchVersion = [UInt16]$Sections[3]
    [UInt16]$local:DevBuildVersion = [UInt16]$Sections[4]

    return [VersionSpec]::new($MajorVersion, $MinorVersion, $StatusVersion, $ReleasePatchVersion, $DevBuildVersion)
}
