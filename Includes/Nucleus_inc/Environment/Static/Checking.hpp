#ifndef FRAO_NUCLEUS_ENVIRONMENT_STATIC_CHECKING
#define FRAO_NUCLEUS_ENVIRONMENT_STATIC_CHECKING

//! \file
//!
//! \brief Header of checking functions, for programming environment,
//! and especially compile-time environment
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <ciso646>	//used for detecting standard library
#include <cstddef>
#if __has_include(<version>)
#include <version>
#endif
#include "APIDefines.hpp"
#include "NucleusVersion.h"

namespace frao
{
inline namespace Environment
{
//! \brief enum used to describe c++ standards. has c++03 through
//! c++20
enum class CPPStandard : small_nat16
{
	CPP03 = 2003,
	CPP11 = 2011,
	CPP14 = 2014,
	CPP17 = 2017,
	CPP20 = 2020,
	CPPLatest = 65535
};

//! \brief Detect the current version of c++ used.
//!
//! \note Depends on the __cplusplus macro having the correct value,
//! which on visual studio compilers requires the /Zc:__cplusplus
//! option
NUCLEUS_LIB_API constexpr CPPStandard checkCPPStandard() noexcept
{
#if __cplusplus > 202002L
	return CPPStandard::CPPLatest;
#elif __cplusplus > 201703L
	return CPPStandard::CPP20;
#elif __cplusplus > 201402L
	return CPPStandard::CPP17;
#elif __cplusplus > 201103L
	return CPPStandard::CPP14;
#elif __cplusplus > 199711L
	return CPPStandard::CPP11;
#else
	return CPPStandard::CPP03;
#endif
}

//! \todo work out if c++14 is the correct minimum standard. It might
//! be c++17, or even c++20, since we use c++latest
static_assert(static_cast<small_nat16>(checkCPPStandard()) >= 2014,
			  "Api requires a c++ standard of at least c++14");

//! \brief enum used to describe possible compilers. For now has only
//! the major 3.
enum class CompilerType
{
	UNKNOWN,  //!< \brief Any compiler that we haven't added explicit
			  //!< checking for yet
	GCC,	   //!< \brief GNU C++ Compiler
	CLANG,	   //!< \brief Clang Compiler
	CLANG_CL,  //!< \brief Clang-cl Compiler
	MSC		   //!< \brief Microsoft c++ Compiler
};

//! \brief Detect the compiler used. Originally intended to
//! distinguish available intrinsics and similar
//!
//! \note The existence of a compiler in this check should NOT be
//! taken to imply that this project has been tested, or even
//! compiles, with that compiler.
NUCLEUS_LIB_API constexpr CompilerType checkCompiler() noexcept
{
	// order here is important. Clang, I believe, defines multiple of
	// these
#if defined(__clang__)
#if defined(_MT)
	return CompilerType::CLANG_CL;
#else
	return CompilerType::CLANG;
#endif
#elif defined(__GNUG__)
	return CompilerType::GCC;
#elif defined(_MSC_VER)
	return CompilerType::MSC;
#else
	return CompilerType::UNKNOWN;
#endif
}

//! \brief enum used to describe possible standard libraries. For now
//! has only the major 3.
enum class StandardLibrary
{
	UNKNOWN,  //!< \brief Any compiler that we haven't added explicit
			  //!< checking for yet
	Microsoft,	//!< \brief Microsoft c++ Standard library
	LibSTDCpp,	//!< \brief GCC c++ Standard library
	LibCpp		//!< \brief Clang c++ Standard library
};

// TODO: predefine stuff

#if defined(_LIBCPP_VERSION)
#define FRAO_LIBVER_LIBCPP "LibCpp"
#elif defined(__GLIBCXX__)
#define FRAO_LIBVER_LIBSTDCPP "LibSTDCpp"
#elif defined(_CPPLIB_VER)
#define FRAO_LIBVER_MICROSOFT "Microsoft"
#else
#define FRAO_LIBVER_UNKNOWN "UNKNOWN"
#endif


//! \brief Detect the standard library used. Originally intended to
//! distinguish available functions (for instance _aligned_alloc,
//! which at the time of writing microsoft plans to not provide)
//!
//! \note The existence of a standard library in this check should NOT
//! be taken to imply that this project has been tested, or even
//! compiles, with that library.
NUCLEUS_LIB_API constexpr StandardLibrary checkStdLib() noexcept
{
	// order here (might be?) important. Clang, I believe, can define
	// multiple of these
#if defined(FRAO_LIBVER_LIBCPP)
	return StandardLibrary::LibCpp;
#elif defined(FRAO_LIBVER_LIBSTDCPP)
	return StandardLibrary::LibSTDCpp;
#elif defined(FRAO_LIBVER_MICROSOFT)
	return StandardLibrary::Microsoft;
#else
	return StandardLibrary::UNKNOWN;
#endif
}

//! \brief enum that lists potential instruction set architecture
//! targets
enum class CompileTarget
{
	Unknown,  //!< \brief Any unknown or unsupported architecture
	ARM,	  //!< \brief ARM architecture(s)
	x86,	  //!< \brief 32-bit x86 architecture
	x64		  //!< \brief 64-bit x86-64 architecture
};

//! \brief Determine, and return at compile time, the architecture
//! that the current compile is targeting
//!
//! \returns enum value describing the architecture that is being
//! targeted
NUCLEUS_LIB_API constexpr CompileTarget checkCompileTarget() noexcept
{
#if defined(_M_ARM) || defined(__arm__)
	return CompileTarget::ARM;
#else  // non-arm branch

	// size_t can hold the size of any type. Any type in c++14 or
	// later, whose SIZE cannot be held in a size_t, is ill formed
	constexpr const size_t size = sizeof(void*);

	if constexpr (size == 8)
	{
		return CompileTarget::x64;
	} else if constexpr (size == 4)
	{
		return CompileTarget::x86;
	} else
	{
		return CompileTarget::Unknown;
	}
#endif
}

//! \brief enum that Lists potential Operating System targets
enum class OSTarget
{
	Unknown,  //!< \brief Any OS other than one listed elsewhere in
			  //!< this enum
	Windows,  //!< \brief Any iteration of the windows OS
	Linux	  //!< \brief Any iteration of linux OS
};

//! \brief Determine, and return at compile time, the OS that the
//! current compile is targeting
//!
//! \returns enum value relating to the OS being currently targeted
//! for compilation
NUCLEUS_LIB_API constexpr OSTarget checkOSTarget() noexcept
{
#if defined(_WIN32)
	return OSTarget::Windows;
#elif defined(__linux__)
	return OSTarget::Linux;
#else
	return OSTarget::Unknown;
#endif
}

//! \brief enum for different compilation configurations
enum class CompileMode
{
	Debug,	 //!< \brief A standard debug build
	Release	 //!< \brief A standard release build
};

//! \brief Will get the current build configuration
//!
//! \note We will need to ensure that _DEBUG is declared,
//! appropriately, in compilers that aren't cl.exe
//!
//! \returns enum value denoting the current CompileMode. eg: debug /
//! release build
NUCLEUS_LIB_API constexpr CompileMode checkCompileMode() noexcept
{
#if defined(_DEBUG)
	return CompileMode::Debug;
#else
	return CompileMode::Release;
#endif
}

//! \brief Will determine whether asserts are currently enabled
//!
//! \returns true iff asserts are enabled
NUCLEUS_LIB_API constexpr bool assertsEnabled() noexcept
{
#if defined(NDEBUG)
	return false;
#else
	return true;
#endif
}

//! \brief determines whether the primitive char8_t type is supported
//!
//! \note Will NOT check whether, for example, std::u8string is
//! supported
//!
//! \returns a boolean whether there exists a primitive type called
//! char8_t
NUCLEUS_LIB_API constexpr bool has_char8_type() noexcept
{
#if defined(__cpp_char8_t)
	return true;
#else
	return false;
#endif
}

//! \brief determines whether the primitive char8_t type is supported
//! by the stdlib
//!
//! \returns The stdlib in use supports a primitive type called
//! char8_t
NUCLEUS_LIB_API constexpr bool has_char8_lib() noexcept
{
#if defined(__cpp_lib_char8_t)
	return true;
#else
	return false;
#endif
}

//! \brief Class for holding version information. Primarily for the
//! purposes of versioning our APIs
class NUCLEUS_LIB_API VersionInfo final
{
	//! \brief The Status component of version. Must be two ASCII
	//! characters, and array should be null-terminated. The s in
	//! a.i.s.p.d
	char m_Status[3];
	//! \brief The release patch component of version. Range [0,
	//! 65535]. p in "a.i.s.p.d"
	small_nat16 m_ReleasePatch;
	//! \brief The development build component of version. Range [0,
	//! 65535]. b in "a.i.s.p.b"
	small_nat16 m_DevBuild;
	//! \brief The minor component of version. Range [0, 1023]. i in
	//! "a.i.s.p.d"
	small_nat16 m_Minor;
	//! \brief The major component of version. Range [0, 63]. a in
	//! "a.i.s.p.d"
	small_nat8 m_Major;

   public:
	//! \brief Constructor for making a version from the version, as
	//! it would be read by a human
	//!
	//! \param[in] major Major component of version. a in "a.i.s.p.d"
	//!
	//! \param[in] minor Minor component of version. i in "a.i.s.p.d"
	//!
	//! \param[in] patch Release patch component of version. p in
	//! "a.i.s.p.d"
	//!
	//! \param[in] build Development build component of version. d in
	//! "a.i.s.p.d"
	//!
	//! \param[in] status Status component of version. s in
	//! "a.i.s.p.d"
	constexpr VersionInfo(small_nat8 major, small_nat16 minor,
						  small_nat16 patch, small_nat16 build,
						  const char* status) noexcept
		: m_Status{0x0, 0x0, 0x0},
		  m_ReleasePatch(patch),
		  m_DevBuild(build),
		  m_Minor(minor),
		  m_Major(major)
	{
		for (natural64 index = 0; (status != nullptr) && (index < 2)
								  && (status[index] != 0x0);
			 ++index)
		{
			m_Status[index] = status[index];
		}
	}

	//! \brief get the major version component
	//!
	//! \returns The major version component, which shall be in the
	//! range [0, 63]. Is the a in "a.i.s.p.d"
	constexpr small_nat8 getMajorComponent() const noexcept
	{
		return m_Major;
	}
	//! \brief get the minor version component
	//!
	//! \returns The minor version component, which shall be in the
	//! range [0, 1023]. Is the i in "a.i.s.p.d"
	constexpr small_nat16 getMinorComponent() const noexcept
	{
		return m_Minor;
	}
	//! \brief get the release patch version component
	//!
	//! \returns The release patch version component, which shall be
	//! in the range [0, 65535]. Is the p in "a.i.s.p.d"
	constexpr small_nat16 getReleasePatchComponent() const noexcept
	{
		return m_ReleasePatch;
	}
	//! \brief get the development build version component
	//!
	//! \returns The development build version component, which shall
	//! be in the range [0, 65535]. Is the d in "a.i.s.p.d"
	constexpr small_nat16 getDevelopmentBuildComponent()
		const noexcept
	{
		return m_DevBuild;
	}
	//! \brief get a pointer to the status version component
	//!
	//! \returns A pointer to a null-terminated array of two
	//! (meaningful) elements, describing the status version
	//! component. Each element shall be two ASCII characters. Is the
	//! s in "a.i.s.p.d"
	constexpr const char* getStatusFullComponent() const noexcept
	{
		return m_Status;
	}
	//! \brief get one of the characters of the status version
	//! component
	//!
	//! \returns The specified of the two ASCII characters. Is the s
	//! in "a.i.s.p.d"
	template<natural CharacterIndex>
	constexpr char getStatusCharacterComponent() const noexcept
	{
		static_assert(CharacterIndex < 2,
					  "Status only has two characters, so the index "
					  "must be less than 2");

		return m_Status[CharacterIndex];
	}
};

//! \brief version number, major component. a in "a.i.s.p.d"
constexpr const small_nat8 g_NucleusVersionMajor = VERSION_MAJOR;
//! \brief version number, minor component. i in "a.i.s.p.d"
constexpr const small_nat16 g_NucleusVersionMinor = VERSION_MINOR;
//! \brief version number, status component. s in "a.i.s.p.d"
constexpr const char g_NucleusVersionStatus[3] = VERSION_STATUS;
//! \brief version number, patch component. p in "a.i.s.p.d"
constexpr const small_nat16 g_NucleusVersionPatch = VERSION_PATCH;
//! \brief version number, development build component. d in
//! "a.i.s.p.d"
constexpr const small_nat16 g_NucleusVersionBuild = VERSION_DEV_BUILD;

//! \brief Get the nucleus API version, for this build
//!
//! \returns A version information class, that holds the version data,
//! for this build
NUCLEUS_LIB_API constexpr VersionInfo getNucleusVersion() noexcept
{
	return VersionInfo(g_NucleusVersionMajor, g_NucleusVersionMinor,
					   g_NucleusVersionPatch, g_NucleusVersionBuild,
					   g_NucleusVersionStatus);
}
}  // namespace Environment
}  // namespace frao

// This will undef the macros we just used, second time around
#include "NucleusVersion.h"

#endif