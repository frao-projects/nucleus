//! \file
//!
//! \brief Implementation of clock time functions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

//! \cond DOXYGEN_IGNORE

// To disable annoying macros that come with windows.h
// so that we don't get collisions on the name max()
#ifndef NOMINMAX
#define NOMINMAX
#endif

//! \endcond

#include "Time/ClockTime.hpp"

#if defined(_WIN32)	 // MSVC stdlib / Windows only

#include <Windows.h>  //for SYSTEMTIME / ::GetLocalTime

#elif defined(__linux__)

#include <ctime>  //for localtime_r etc

#endif

namespace frao
{
inline namespace Time
{
// RawTimeData functions
RawTimeData::RawTimeData() noexcept
	: m_Year(0),
	  m_MilliSeconds(0),
	  m_Month(0),
	  m_DayOfMonth(0),
	  m_DayOfWeek(0),
	  m_Hour(0),
	  m_Minute(0),
	  m_Second(0)
{
	updateTime();
}
//
void RawTimeData::updateTime() noexcept
{
#if defined(_WIN32)

	SYSTEMTIME sysTime;

	::GetLocalTime(&sysTime);

	m_Year		   = sysTime.wYear;
	m_Month		   = static_cast<natural8>(sysTime.wMonth);
	m_DayOfMonth   = static_cast<natural8>(sysTime.wDay);
	m_DayOfWeek	   = static_cast<natural8>(sysTime.wDayOfWeek);
	m_Hour		   = static_cast<natural8>(sysTime.wHour);
	m_Minute	   = static_cast<natural8>(sysTime.wMinute);
	m_Second	   = static_cast<natural8>(sysTime.wSecond);
	m_MilliSeconds = sysTime.wMilliseconds;


#elif defined(__linux__)

	// Get the raw time in seconds and nanoseconds, and then use the
	// threadsafe version of localtime to get a proper readable time
	timespec rawLocal;
	int		 resTime = clock_gettime(CLOCK_REALTIME, &rawLocal);

	std::tm	 local;
	std::tm* resLocal = localtime_r(&rawLocal.tv_sec, &local);

	if ((resTime != -1) && (resLocal != nullptr))
	{
		m_Year		 = 1900 + static_cast<natural16>(local.tm_year);
		m_Month		 = 1 + static_cast<natural8>(local.tm_mon);
		m_DayOfMonth = static_cast<natural8>(local.tm_mday);
		m_DayOfWeek	 = static_cast<natural8>(local.tm_wday);
		m_Hour		 = static_cast<natural8>(local.tm_hour);
		m_Minute	 = static_cast<natural8>(local.tm_min);
		m_Second	 = static_cast<natural8>(local.tm_sec);
		m_MilliSeconds =
			static_cast<natural16>(rawLocal.tv_nsec / (1'000'000));
	} else
	{
		m_Year = m_MilliSeconds = 0;
		m_Month = m_DayOfMonth = m_DayOfWeek = m_Hour = m_Minute =
			m_Second								  = 0;
	}

#else  // Not windows or linux, Calling function in wrong OS

	static_assert(false,
				  "Attempted to compile RawTimeData::updateTime() "
				  "function in unrecognised OS");

#endif
}
// comparison operations
bool RawTimeData::operator==(const RawTimeData& rhs) const noexcept
{
	// check if time of day is different. If so, return
	// false
	if (m_Second != rhs.m_Second)
		return false;
	else if (m_Minute != rhs.m_Minute)
		return false;
	else if (m_Hour != rhs.m_Hour)
		return false;

	// check if the day of the year is different, If so,
	// return false.
	else if (m_DayOfMonth != rhs.m_DayOfMonth)
		return false;
	else if (m_Month != rhs.m_Month)
		return false;

	// check if the year is the same. If it is, then the day
	// of the week must be the same too, because we've
	// checked the time of day, and the day of the year. We
	// don't need to check the day of the week, because that
	// information is contained in other data
	return m_Year == rhs.m_Year;
}
bool RawTimeData::operator!=(const RawTimeData& rhs) const noexcept
{
	return !(*this == rhs);
}

// ClockTime Functions
ClockTime::ClockTime() noexcept : m_Data()
{}
//
void ClockTime::updateTime() noexcept
{
	m_Data.updateTime();
}
// comparison operations
bool ClockTime::operator==(const ClockTime& rhs) const noexcept
{
	return this->m_Data == rhs.m_Data;
}
bool ClockTime::operator!=(const ClockTime& rhs) const noexcept
{
	return !(*this == rhs);
}


// TimeStamp Functions
// get functions
auto TimeStamp::getTime() const noexcept -> const decltype(m_Time)&
{
	return m_Time;
}
// comparison operations
bool TimeStamp::operator==(const TimeStamp& rhs) const noexcept
{
	return m_Time == rhs.getTime();
}
bool TimeStamp::operator!=(const TimeStamp& rhs) const noexcept
{
	return !(*this == rhs);
}

}  // namespace Time
}  // namespace frao
