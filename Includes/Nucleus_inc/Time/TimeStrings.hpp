#ifndef FRAO_NUCLEUS_TIME_TIME_STRINGS
#define FRAO_NUCLEUS_TIME_TIME_STRINGS

//! \file
//!
//! \brief file for getting strings, as corresponding to days, months
//! etc.
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <functional>		 //for std::function
#include <initializer_list>  //for constructing ParseDescriptions
#include <type_traits>		 //for std::is_same
#include <vector>			 //for holding ParseDescriptions
#include "../Environment.hpp"
#include "../Strings.hpp"

namespace frao
{
//! \brief namespace for code related to time. For instance, timers,
//! and timestamps.
inline namespace Time
{
namespace IMPL
{
//! \brief get a string literal, of the textual form of a number, in
//! ascii format
//!
//! \returns An ascii string literal, containing the textual form of a
//! number (eg: three, twelve, etc.)
//!
//! \param[in] number An index, of the number to return a string of.
//! Numbers [0, 19] are at their respective indices, but 20, 30, ...,
//! 90 are at consecutive indices, starting at 20. That is, in the
//! range [20, 27]
const charA* _numbertext(natural16 number) noexcept;

//! \brief get a string literal, of the textual form of an ordinal
//! number, in ascii format
//!
//! \returns An ascii string literal, containing the textual form of
//! an ordinal number (eg: three, twelve, etc.)
//!
//! \param[in] number An index, of the number to return a string of.
//! Numbers [0, 19] are at their respective indices, but 20, 30, ...,
//! 90 are at consecutive indices, starting at 20. That is, in the
//! range [20, 27]
const charA* _numbertextordinal(natural16 number) noexcept;

//! \brief get a string literal, of the numeric form of an ordinal
//! number, in ascii format
//!
//! \returns An ascii string literal, containing the numeric form of
//! an ordinal number (eg: 3, 12, etc.)
//!
//! \param[in] number The number to return a string of, in range [0,
//! 19]
const charA* _numberordinal(natural16 number) noexcept;

//! \brief get a string literal, of the day of the week, in ascii
//! format
//!
//! \returns An ascii string literal, containing the day of the week
//!
//! \param[in] day The day of the week, to return a string of.
//! Expected to be in [0, 6]. Numbers outside that range will be
//! normalised to it
const charA* _dayofweek(natural16 day) noexcept;

//! \brief get a string literal, of the month, in ascii format
//!
//! \returns An ascii string, containing the month
//!
//! \param[in] month The month, to return a string of. Expected to be
//! in [1, 12]. Jan=1, Feb=2, Dec=12
const charA* _month(natural16 month) noexcept;

//! \brief get a string, of the textual form of a number, in the
//! specified format
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \returns A string of the specified format, containing the textual
//! form of a number (eg: three, twelve, etc.)
//!
//! \param[in] number An index, of the number to return a string of.
//! Numbers [0, 19] are at their respective indices, but 20, 30, ...,
//! 90 are at consecutive indices, starting at 20. That is, in the
//! range [20, 27]
template<typename CharType>
Basic_String<CharType> getText(natural16 number)
{
	return Basic_String<CharType>(_numbertext(number));
}

//! \brief get a string, of the textual form of an ordinal number, in
//! the specified format
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \returns A string of the specified format, containing the textual
//! form of an ordinal number (eg: three, twelve, etc.)
//!
//! \param[in] number An index, of the number to return a string of.
//! Numbers [0, 19] are at their respective indices, but 20, 30, ...,
//! 90 are at consecutive indices, starting at 20. That is, in the
//! range [20, 27]
template<typename CharType>
Basic_String<CharType> getOrdinalText(natural16 number)
{
	return Basic_String<CharType>(_numbertextordinal(number));
}

//! \brief get a string, of the numeric form of an ordinal number, in
//! the specifed format
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \returns A string of the specified format, containing the numeric
//! form of an ordinal number (eg: 3, 12, etc.)
//!
//! \param[in] number The number to return a string of, in range [0,
//! 19]
template<typename CharType>
Basic_String<CharType> getOrdinalNumber(natural16 number)
{
	return Basic_String<CharType>(_numberordinal(number));
}

//! \brief get a string, of the day of the week, in the specified
//! format
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \returns A string of the specifed format, containing the day of
//! the week
//!
//! \param[in] day The day of the week, to return a string of.
//! Expected to be in [0, 6]. Numbers outside that range will be
//! normalised to it
template<typename CharType>
Basic_String<CharType> getDayOfWeek(natural16 day)
{
	return Basic_String<CharType>(_dayofweek(day));
}

//! \brief get a string, of the month, in the specified format
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \returns A string of the specified format, containing the month
//!
//! \param[in] month The month, to return a string of. Expected to be
//! in [1, 12]. Jan=1, Feb=2, Dec=12
template<typename CharType>
Basic_String<CharType> getMonth(natural16 month)
{
	return Basic_String<CharType>(_month(month));
}

}  // namespace IMPL

//! \brief The way that we wish to print the specified data
enum class TimePrintStyle
{
	//! \brief print the data as a number (1/23/356 etc.)
	Number,

	//! \brief print the data as an ordinal number (1st/23rd/356th
	//! etc.)

	OrdinalNumber,
	//! \brief print the data as text (one, twenty-three, three
	//! hundred and fifty-six, etc.)
	Text,

	//! \brief print the data as a textual ordinal number (first,
	//! twenty-third, three hundred and fifty-sixth, etc.)
	OrdinalText,

	//! brief This will cause the name of the month corresponding to
	//! the data to be printed
	//!
	//! \note Months are numbered such that 1=January, 2=Febuary, etc
	Month,

	//! \brief This will cause the name of the day of the week
	//! corresponding to the data to be printed
	//!
	//! \note Days are counted as days since Sunday. So Sunday=0,
	//! Monday=1 etc
	DayOfWeek,

	//! \brief This will cause the data to be the index to a
	//! user-supplied string array
	Custom
};

// So the compiler won't complain about using STL containers in the interface. Obviously
// doing this can cause problems, esp. if we use a different compiler / flags for compilation
// of dll / consumer of dll, but it should mostly work, if we can keep those straight. Which
// our build system should anyway
DISABLE_VC_WARNING(4251)

//! \brief a specification on what to print, and how to print it,
//! per segment
//!
//! \tparam CharType The underlying character type, of the string to
//! be built
template<typename CharType>
class NUCLEUS_LIB_API TimePrintSpec
{
	//! \brief A function that will be called, if it exists, before
	//! any parsed data is used
	std::function<natural16(natural16)> m_TransformFunc;

	//! \brief An array used to store a series of strings, that will
	//! be indexed into, if #m_PrintStyle has value Custom
	std::vector<Basic_String<CharType>> m_ReadArray;

	//! \brief How should we display the time data?
	//!
	//! \details Controls the formatting of the data provided. Should
	//! the number be printed textually ("four")? Numerically ("4")?
	//! Does it refer to an ordinal number ("fourth")? Is it a month
	//! ("April")?
	TimePrintStyle m_PrintStyle;

	//! \brief calculate the decimal digits that make up a 16-bit
	//! number
	//!
	//! \param[out] digitStore A reference to an array of natural16,
	//! of length 5. It shall receive the calculated digits, from the
	//! highest order in [0], to lowest in [5]
	//!
	//! \param[in] number The 16-bit number, for which we wish to
	//! calculate digits. \pre Must not be greater than 65535, even if
	//! the type used to hold it is (on many architectures it will be)
	static void getDigits(natural16 (&digitStore)[5],
						  natural16 number) noexcept
	{
		natural16 magnitude = 10000;
		for (natural index = 0; index < 5; ++index, magnitude /= 10)
		{
			digitStore[index] = number / magnitude;
			number -= digitStore[index] * magnitude;

			runtime_assert(digitStore[index] < 10,
						   "Should not be possible "
						   "to get two-digit digit");
		}
	}

	//! \brief Get a number string (1, 23, etc), from the specified
	//! number
	//!
	//! \returns A string, of the passed number
	//!
	//! \param[in] number The number, of which a string should be
	//! created
	static Basic_String<CharType> makeNumberString(natural16 number)
	{
		return Basic_String<CharType>(number);
	}
	//! \brief Get an ordinal number (1st, 23rd, etc), from the
	//! specified number
	//!
	//! \returns A string, of the passed number
	//!
	//! \param[in] number The number, of which a string should be
	//! created
	static Basic_String<CharType> makeOrdinalNumberString(
		natural16 number)
	{
		if (number < 10)
		{
			// then we have a one-digit number, so just get it from
			// the array
			return IMPL::getOrdinalNumber<CharType>(number);
		}

		// get the raw digits
		natural16 digits[5];
		getDigits(digits, number);

		// get the right suffix
		Basic_String<CharType> suffix;
		if (digits[3] == 1)
		{
			// then the last two digits are between 10 and 19, which
			// have difference suffixes, than standard
			suffix = IMPL::getOrdinalNumber<CharType>(10 + digits[4]);
		} else
		{
			suffix = Basic_String<CharType>(digits[3])
					 + IMPL::getOrdinalNumber<CharType>(digits[4]);
		}

		// Then we have a 2-digit number. ie: just a suffix
		if (number < 100)
		{
			return suffix;
		}

		// we now reconstruct the first three digits. It would be
		// better to not deconstruct them in the first place, but this
		// is simpler to code. That's because we need the current form
		// of the getDigits() function, for the make...TextString()
		// functions
		natural16 remainingDigits =
			(100 * digits[0]) + (10 * digits[1]) + digits[2];
		Basic_String<CharType> prefix;

		if (remainingDigits != 0)
		{
			prefix = Basic_String<CharType>(remainingDigits);
		}

		return prefix + suffix;
	}

	//! \brief Get a two-digit number, from supplied digits, in text
	//! format
	//!
	//! \returns A string of the supplied two-digit number, in textual
	//! format. ie: "two", "twenty-three", etc
	//!
	//! \param[in] tensDigit The digit, from 0 to 9, corresponding to
	//! the number of tens, in the number. ie, in a two digit number
	//! xy, x refers to the tens digit
	//!
	//! \param[in] unitDigit The digit, from 0 to 9, corresponding to
	//! the number of units, in the number ie, in a two digit number
	//! xy, y refers to the units digit
	static Basic_String<CharType> makeTwoDigitString(
		natural16 tensDigit, natural16 unitDigit)
	{
		runtime_assert(tensDigit < 10,
					   "tensDigit parameter invariant violated");
		runtime_assert(unitDigit < 10,
					   "unitDigit parameter invariant violated");

		Basic_String<CharType> result;

		if (tensDigit == 1)
		{
			// then the last two digits are between 10 and 19,
			// which have difference suffixes, than standard
			result = IMPL::getText<CharType>(10 + unitDigit);
		} else if (tensDigit != 0)
		{
			// if we don't have a zero in the tens column
			result = IMPL::getText<CharType>(18 + tensDigit);

			if (unitDigit != 0)
			{
				result += "-";
				result += IMPL::getText<CharType>(unitDigit);
			}
		} else
		{
			// if we have a zero in the tens column
			result = IMPL::getText<CharType>(unitDigit);
		}

		return result;
	}
	//! \brief Get a string of the thousands/hundreds section of a
	//! number, in textual format
	//!
	//! \returns A string of the supplied thousands/hundreds number,
	//! in textual format. ie: "two thousand", "twenty-three thousand,
	//! four hundred", "six hundred", etc
	//!
	//! \param[in] tensOfThousands The digit, from 0 to 9,
	//! corresponding to the number of tens of thousands, in the
	//! number. ie, in a five digit number abcde, a refers to the tens
	//! of thousands digit
	//!
	//! \param[in] thousands The digit, from 0 to 9, corresponding to
	//! the number of thousands, in the number. ie, in a five digit
	//! number abcde, b refers to the thousands digit
	//!
	//! \param[in] hundreds The digit, from 0 to 9, corresponding to
	//! the number of hundreds, in the number. ie, in a five digit
	//! number abcde, c refers to the hundreds digit
	static Basic_String<CharType> makeHundredsAndThousandsString(
		natural16 tensOfThousands, natural16 thousands,
		natural16 hundreds)
	{
		runtime_assert(
			tensOfThousands < 10,
			"tensOfThousands parameter invariant violated");
		runtime_assert(thousands < 10,
					   "thousands parameter invariant violated");
		runtime_assert(hundreds < 10,
					   "hundreds parameter invariant violated");

		// get the right text, for the remaining sections of the
		// number
		Basic_String<CharType> result;

		if ((tensOfThousands & thousands) != 0)
		{
			// then we're non-zero in thousands
			result += makeTwoDigitString(tensOfThousands, thousands)
					  + " thousand";
		}

		if (hundreds != 0)
		{
			// then we're non-zero in hundreds
			if (result.size() != 0)
			{
				// then we have a number greater than 999, so insert a
				// comma and space
				result += ", ";
			}

			result += IMPL::getText<CharType>(hundreds) + " hundred";
		}

		return result;
	}

	//! \brief Get an textual representation of a  number (one,
	//! twenty-third, etc), from the specified number
	//!
	//! \returns A string, of the passed number
	//!
	//! \param[in] number The number, of which a string should be
	//! created
	static Basic_String<CharType> makeTextString(natural16 number)
	{
		if (number == 0)
		{
			return IMPL::getText<CharType>(0);
		}

		// get the raw digits
		natural16 digits[5];
		getDigits(digits, number);

		// get the right text, for the sections of the number
		Basic_String<CharType> result =
			makeHundredsAndThousandsString(digits[0], digits[1],
										   digits[2]);

		if ((digits[3] & digits[4]) != 0)
		{
			// then we're non-zero in tens and units
			if (result.size() != 0)
			{
				// then we have a number greater than 99, so insert a
				// " and "
				result += " and ";
			}

			result += makeTwoDigitString(digits[3], digits[4]);
		}

		return result;
	}
	//! \brief Get an ordinal number (1st, 23rd, etc), from the
	//! specified number
	//!
	//! \returns A string, of the passed number
	//!
	//! \param[in] number The number, of which a string should be
	//! created
	static Basic_String<CharType> makeOrdinalTextString(
		natural16 number)
	{
		if (number == 0)
		{
			return IMPL::getOrdinalText<CharType>(0);
		}

		// get the raw digits
		natural16 digits[5];
		getDigits(digits, number);

		Basic_String<CharType> tens;

		if (digits[3] == 1)
		{
			// then the last two digits are between 10 and 19,
			// which have difference suffixes, than standard
			tens = IMPL::getOrdinalText<CharType>(10 + digits[4]);
		} else if (digits[3] != 0)
		{
			// if we don't have a zero in the tens column
			if (digits[4] != 0)
			{
				// then the last digit is non-0, in which case we can
				// use that for the th/st/nd suffix
				tens = IMPL::getText<CharType>(18 + digits[3]) + "-"
					   + IMPL::getOrdinalText<CharType>(digits[4]);
			} else
			{
				// then the last digit is 0, so we need to get the
				// th/st/nd from the tens value
				tens = IMPL::getOrdinalText<CharType>(18 + digits[3]);
			}
		} else if (digits[4] != 0)
		{
			// if we have a zero in the tens column, but not in the
			tens = IMPL::getOrdinalText<CharType>(digits[4]);
		}

		// get the right text, for the remaining sections of the
		// number
		Basic_String<CharType> result =
			makeHundredsAndThousandsString(digits[0], digits[1],
										   digits[2]);

		if (tens.size() != 0)
		{
			// then the last two digits were something other than 00,
			// so we have the th/st/nd suffix. And hence we're
			// non-zero in tens and units
			if (result.size() != 0)
			{
				// then we have a number greater than 99, so insert a
				// " and "
				result += " and ";
			}

			result += tens;
		} else
		{
			// then the last two digits were 00, so we need to add
			// "th", and then we're done
			runtime_assert(result.size() != 0,
						   "We should have had non-zero thousands or "
						   "hundreds, since we short-circuited "
						   "number == 0, at the top of the function");

			result += "th";
		}

		return result;
	}


   public:
	//! \brief Create an object detailing the style of printing, for
	//! standard format parsed numbers
	//!
	//! \param[in] style The standard format with which to parse the
	//! object. Must not be Custom. For custom format, use another
	//! constructor
	TimePrintSpec(TimePrintStyle style) noexcept
		: m_TransformFunc(), m_ReadArray(), m_PrintStyle(style)
	{
		if (m_PrintStyle == TimePrintStyle::Custom)
		{
			// cannot have Custom, without specifying array (via other
			// constructor)
			m_PrintStyle = TimePrintStyle::Number;
		}
	}
	//! \brief Create an object detailing the style of printing, for
	//! standard format parsed numbers
	//!
	//! \param[in] customArray The array to use, instead of parsing
	//! the specified numbers normally. Will be called with any
	//! numbers to parse, after any possible transformation step
	TimePrintSpec(
		std::vector<Basic_String<CharType>> customArray) noexcept
		: m_TransformFunc(),
		  m_ReadArray(std::move(customArray)),
		  m_PrintStyle(TimePrintStyle::Custom)
	{
		if (m_ReadArray.size() == 0)
		{
			// must have custom array, so turn to number array
			*this = TimePrintSpec(TimePrintStyle::Number);
		}
	}

	//! \brief Get a reference to the array we're using.
	//!
	//! \note This is only used for custom arrays
	//!
	//! \returns Reference to the passed custom array
	auto getArray() noexcept -> decltype(m_ReadArray)&
	{
		return m_ReadArray;
	}
	//! \brief Get a reference to the transformation function we're
	//! using.
	//!
	//! \returns Reference to the used transformation function
	auto getFunc() noexcept -> decltype(m_TransformFunc)&
	{
		return m_TransformFunc;
	}

	//! \brief Output a string of this segment, according to the
	//! data supplied
	//!
	//! \returns A string, formatted how specified in this classes
	//! member variables
	//!
	//! \param[in] data The data supplied to the function, to be
	//! parsed to string
	Basic_String<CharType> parse(natural16 data) const
	{
		// apply pre-call function
		natural16 realData;
		if (m_TransformFunc)
		{
			realData = m_TransformFunc(data);
		} else
		{
			realData = data;
		}

		switch (m_PrintStyle)
		{
			case TimePrintStyle::Month:
				return IMPL::getMonth<CharType>(realData);
			case TimePrintStyle::DayOfWeek:
				return IMPL::getDayOfWeek<CharType>(realData);
			case TimePrintStyle::Custom:
				// we need to make sure that we don't overrun the
				// custom array
				return m_ReadArray[realData % m_ReadArray.size()];
			case TimePrintStyle::Number:
				return makeNumberString(realData);
			case TimePrintStyle::OrdinalNumber:
				return makeOrdinalNumberString(realData);
			case TimePrintStyle::Text:
				return makeTextString(realData);
			case TimePrintStyle::OrdinalText:
				return makeOrdinalTextString(realData);
			default:
				// should not be possible
				runtime_assert(false,
							   "Parse function encountered "
							   "unexpected print style");
				return Basic_String<CharType>();
		}
	}
};

//retore disabled c4251 warning, for using non-exporting class in interface
RESTORE_VC_WARNING(4251)

}  // namespace Time
}  // namespace frao


#endif
