#ifndef FRAO_NUCLEUS_TIME
#define FRAO_NUCLEUS_TIME

//! \file
//!
//! \brief Header of all time funtions, used for recording and measuring
//! time. Simply amalgamates other headers, according to specifications of the user
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Time/TimeStrings.hpp"
#include "Time/ClockTime.hpp"
#include "Time/PerformanceTime.hpp"

#endif