
class TreeElem {
	hidden [TreeElem]$Parent
	[string]$Name

	TreeElem([TreeElem]$MyParent, [string]$MyName) {
		$this.Parent = $MyParent
		$this.Name = $MyName
	}

	[string] MyName() {
		if($this.Name) {
			return $this.Name
		}
		else {
			return ''
		}
	}
	[string] ParentPath() {
		if($this.Parent)
		{
			return $this.Parent.FullPath()
		}

		return ''
	}
	[string] FullPath() {

		[string]$local:parentName = $this.ParentPath()
		if($parentName.Length -gt 0)
		{
			$parentName += '/'
		}

		return $parentName + [System.IO.Path]::GetFileNameWithoutExtension($this.MyName())
	}
	[hashtable] GetHashtable() {
		$myName = $this.MyName()

		if($myName.Length -gt 0)
		{
			return @{
				SectionName = [System.IO.Path]::GetFileNameWithoutExtension($myName)
				HeaderExt = [System.IO.Path]::GetExtension($myName)
				SubFiles = $this.GetChildrenNames()
				ParentName = $this.ParentPath()
			}
		}

		return @{}
	}
	[string[]] GetChildrenNames() {
		return @()
	}
	[hashtable[]] GetAllHashtables() {
		return @($this.GetHashtable())
	}
	[string[]] GetPrintedTree([int]$level) {
		[string]$local:ourString = '-'*$level + $this.MyName()
		return @($ourString)
	}
	[string[]] GetPrintedTree() {
		return $this.GetPrintedTree(0)
	}
}

class FileTree : TreeElem {
	[TreeElem[]]$Children

	FileTree([string[]]$FilePaths, [TreeElem]$MyParent, [string]$MyName) :
		base($MyParent, $MyName) {
		#goal is to find common first segments, and create sub-trees for each

		#start by ordering the list, as we'll need 'NAME.ext' to be before 'NAME/'
		#if this isn't the case, file and directory names won't get collated correctly
		$FilePaths = $FilePaths | Sort-Object

		#copy the filepaths array, to the 'not-selected' array
		[string[]]$local:noArray = @()
		$noArray += $FilePaths

		while($noArray.Length -gt 0)
		{
			#get first segment 'seg1', of first member
			$local:head, $local:tail = $noArray
			$local:headSegments = $head.Split('/', 2)

			#we need to check whether we have a valid segment, and if not, ignore it
			if(($headSegments.Length -eq 0) -or ($headSegments[0].Length -eq 0))
			{
				#then the first element didn't seem to have anything
				$noArray = $tail
				continue
			}
			[string]$local:segmentName = $headSegments[0]

			if(($headSegments.Length -eq 1) -and $segmentName.Contains('.'))
			{
				$segmentName = [System.IO.Path]::GetFileNameWithoutExtension($segmentName)
			}

			#get 'yeslist', of strings with the same first segment
			[string[]]$local:yesArray = $noArray.Where({
				$local:segs = $_.Split('/', 2)

				if($segs.Length -eq 0) {
					return $false
				}

				return ($segs.Length -gt 0) -and ($segs[0] -eq $segmentName)
			})

			#update noarray, to be in correct new state
			$noArray = $tail.Where({ -not $yesArray.Contains($_) })

			#with noarray updated, now we can safely change yesarray, to be in the
			#form it now needs to be, to create a child object
			$yesArray = $yesArray | ForEach-Object {
				$segs = $_.Split('/', 2)

				if($segs.Length -gt 1)
				{
					return $segs[1]
				}

				return ''
			}
			$yesArray = $yesArray.Where({$_.Length -gt 0})

			if($yesArray.Length -gt 0)
			{
				#then we have more sub-elements below the next child
				$this.Children += [FileTree]::new($yesArray, $this, $headSegments[0])
			}
			else
			{
				#then this element is a leaf
				$this.Children += [TreeElem]::new($this, $headSegments[0])
			}
		}
	}

	[string[]] GetChildrenNames() {
		return $this.Children | ForEach-Object{$_.MyName()}
	}
	[hashtable[]] GetAllHashtables() {
		[hashtable[]]$local:returnArray= @()
		[hashtable]$local:hashtable = $this.GetHashtable()

		#check if the hashtable is empty
		if($hashtable.Count -gt 0)
		{
			$returnArray += $hashtable
		}

		foreach($local:child in $this.Children)
		{
			$returnArray += $child.GetAllHashtables()
		}

		return $returnArray
	}
	[string[]] GetPrintedTree([int]$level) {
		[string]$local:ourString = '-'*$level + $this.MyName()
		[string[]]$local:result = @($ourString)

		foreach($child in $this.Children)
		{
			$result += $child.GetPrintedTree($level + 1)
		}

		return $result
	}
}

function Get-FileList {
	param (
		#The base folder, from which to execute the command
		[Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container})]
		[string]$SourceDir,
		#The sub-folder, inside $SourceDir, that actually holds the files
		[string]$NameDir,
		#The includes (eg. normally extensions of the file types), that should be included in the list
		[string[]]$ToInclude,
		#What should be exluded from the file list
		[string[]]$ToExclude
	)

	# Setup what we should remove from the start of paths.
	# Must end in '\', or '/', depending on the OS, because files and directory
	# names can start with '.'
	$local:SepChar = [IO.Path]::DirectorySeparatorChar
	[string]$local:regex="^(\.|\$SepChar)*\$SepChar"
	if($NameDir)
	{
		$regex = "^.*($NameDir\$SepChar)"
	}

	#navigate to the source directory
	Push-Location $SourceDir

	#get the list of files, where the path of each is relative to its location in $NameDir
	$local:result = (Get-ChildItem -Path "$NameDir" -Exclude $ToExclude -Force -Recurse -File -Include $ToInclude |
				Resolve-Path -Relative) -replace "$regex", '' |
				Sort-Object

	#return to prior location
	Pop-Location

	#Make paths to be seperated by forward slashes
		$result = $result -replace "\\", '/'

	#return, ensuring we return an array
	return @($result)
}

function New-FileList {
	param (
		[Parameter(ValueFromPipeline)][array]$list
	)

	[string]$local:result
	[bool]$local:subsequentLine=$false

	$list | ForEach-Object {
		if($_.length -gt 0)
		{
			#for files after the first, we need to add commas between the file strings
			if($subsequentLine)
			{
				$result += ","
			}
			else {
				$subsequentLine=$true
			}

			#if the line in the file list isn't empty, and isn't the header, add the
			#name of the file to the list
			$result += "`r`n`t'$_'"
		}
	}

	return $result
}

function Write-NewFile {
	param (
		#The base section of the directory path, at which to write the file. This holds the
		#part of the path that already exists
		[Parameter(Mandatory=$true)]
		[ValidateScript({Test-Path -Path $_ -PathType Container})]
		[string]$ExistingDir,
		#The remaining section of the file path. This golds thr part of the path that can
		#be new, and which should be created by the operation
		[Parameter(Mandatory=$true)]
		[ValidateScript({$_.Length -gt 0})]
		[string]$FilePath,
		#The data that the file should hold
		[Parameter(Mandatory=$true)]
		[string]$FileData
	)

	#get the fully qualified name, for the existing directory
	[string]$local:fullExistingPath = (Resolve-Path "$ExistingDir").Path
	[string]$local:fullWritePath = "$fullExistingPath\$FilePath"

	#write to a new file
	Set-Content -Path "$fullWritePath" -Value "$FileData"
}
