#ifndef FRAO_BASIC_API_STRINGS_WORD
#define FRAO_BASIC_API_STRINGS_WORD

//! \file
//!
//! \brief Header of the Interface of unicode word class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <type_traits>	//for std::conditional, enable_if
#include "Experimental/WordIMPL_Expr.hpp"
#include "StringDefines.hpp"  //for default string type variable
#include "Transitional/WordIMPL_Trans.hpp"

namespace frao
{
inline namespace Strings
{
template<typename CharType, bool UseExperimental,
		 template<typename, bool> class NumericalPunctuation>
class NUCLEUS_LIB_API Word;

//! \brief Class that encapsulates ascii numerical punctuation.
//! Provided in a struct, so that it can be conveniently replaced at
//! once, for alternative numeric punctuation
//!
//! \tparam CharType The underlying type of the string [charU8,
//! char16_t, char32_t, wchar_t].
//!
//! \tparam UseExperimental Should be experimental version of the
//! unicode classes be used? True if we should. Will only have an
//! effect if g_Experimental_Strings_Allowed is true, which it will be
//! if FRAO_EXPERIMENTAL_STRINGS_ALLOWED was defined for this project
template<typename CharType, bool UseExperimental>
struct NUCLEUS_LIB_API AsciiNumericalPunctuation
{
	using ClusterType = GraphemeCluster<CharType, UseExperimental>;
	using WordType	  = std::conditional_t<
		   UseExperimental,
		   Experimental::WordIMPL<
			   CharType, AsciiNumericalPunctuation<CharType, true>>,
		   Transitional::WordIMPL<
			   CharType, AsciiNumericalPunctuation<CharType, false>>>;

	//! \brief Get a null-terminated string, corresponding to Infinity
	//! (Inf)
	//!
	//! \returns A null-terminated string, holding the string
	//! representation of Infinity (Inf)
	static WordType getInfString() noexcept
	{
		return WordType{ClusterType('I'), ClusterType('n'), ClusterType('f')};
	}
	//! \brief Get a null-terminated string, corresponding to
	//! not-a-number (NaN)
	//!
	//! \returns A null-terminated string, holding the string
	//! representation of not-a-number (NaN)
	static WordType getNaNString() noexcept
	{
		return WordType{ClusterType('N'), ClusterType('a'), ClusterType('N')};
	}
	//! \brief Get a null-terminated string, containing all digits
	//!
	//! \returns A null-terminated string, holding the string
	//! representation of all digits
	static std::array<ClusterType, 10> getNumberString() noexcept
	{
		return std::array<ClusterType, 10>{
			ClusterType('0'), ClusterType('1'), ClusterType('2'),
			ClusterType('3'), ClusterType('4'), ClusterType('5'),
			ClusterType('6'), ClusterType('7'), ClusterType('8'),
			ClusterType('9')};
	}
	//! \brief Get a cluster, corresponding to the negation
	//! character (-)
	//!
	//! \returns A cluster, holding the representation of the
	//! negation character (-)
	static ClusterType getNegationCharacter() noexcept
	{
		return ClusterType('-');
	}
	//! \brief Get a cluster, corresponding to a decimal dot (.)
	//!
	//! \returns A cluster, holding the representation of a decimal
	//! dot (.)
	static ClusterType getDotCharacter() noexcept
	{
		return ClusterType('.');
	}
	//! \brief Get a character, corresponding to Eulers constant (E)
	//!
	//! \returns A character, holding the representation of Eulers
	//! constant (E)
	static ClusterType getECharacter() noexcept
	{
		return ClusterType('E');
	}

	//! \brief The character at which stream extraction shall stop.
	//! Normally the newline character
	//!
	//! \returns The stream delimiter character
	static CharType getStreamDelimiter() noexcept
	{
		// since \n is < 128 (in the ascii range), we can safely just
		// cast
		return static_cast<CharType>('\n');
	}
};

// tell the compiler that we will define these elsewhere
NUCLEUS_HEADER_EXTERN template class Transitional::WordIMPL<
	charU8, AsciiNumericalPunctuation<charU8, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::WordIMPL<
	wchar_t, AsciiNumericalPunctuation<wchar_t, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::WordIMPL<
	char16_t, AsciiNumericalPunctuation<char16_t, false>>;
NUCLEUS_HEADER_EXTERN template class Transitional::WordIMPL<
	char32_t, AsciiNumericalPunctuation<char32_t, false>>;
#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
NUCLEUS_HEADER_EXTERN template class Experimental::WordIMPL<
	charU8, AsciiNumericalPunctuation<charU8, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::WordIMPL<
	wchar_t, AsciiNumericalPunctuation<wchar_t, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::WordIMPL<
	char16_t, AsciiNumericalPunctuation<char16_t, true>>;
NUCLEUS_HEADER_EXTERN template class Experimental::WordIMPL<
	char32_t, AsciiNumericalPunctuation<char32_t, true>>;
#endif

// So the compiler won't complain about using STL containers in the
// interface. Obviously doing this can cause problems, esp. if we use
// a different compiler / flags for compilation of dll / consumer of
// dll, but it should mostly work, if we can keep those straight.
// Which our build system should anyway
DISABLE_VC_WARNING(4251)

//! \brief Class to encapsulate the Unicode Word concept. A
//! word is pretty much what you think it is, except that
//! symbolic characters (grapheme clusters) such " or ( or
//! even space(s) count as words
template<typename CharType,
		 bool UseExperimental = g_Experimental_Strings_By_Default,
		 template<typename, bool> class NumericalPunctuation =
			 AsciiNumericalPunctuation>
class NUCLEUS_LIB_API Word final
	: private std::conditional_t<
		  UseExperimental && g_Experimental_Strings_Allowed,
		  Experimental::WordIMPL<
			  CharType, NumericalPunctuation<CharType, true>>,
		  Transitional::WordIMPL<
			  CharType, NumericalPunctuation<CharType, false>>>
{
	//! \brief template for possible base classes of this class.
	//! Dispatches to experimental or transtional base class as
	//! appropriate. Used so that we don't have to whole conditional
	//! statement every time we need to refer to another possible base
	//! class
	template<typename CharType_templ>
	using BaseClass_template = std::conditional_t<
		UseExperimental && g_Experimental_Strings_Allowed,
		Experimental::WordIMPL<CharType_templ,
							   NumericalPunctuation<CharType, true>>,
		Transitional::WordIMPL<
			CharType_templ, NumericalPunctuation<CharType, false>>>;

	//! \brief using declaration for the base class of this one. Used
	//! for simplicity of referring to it.
	using BaseClass = BaseClass_template<CharType>;

   public:
	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ThisType = Word<CharType, UseExperimental>;

	//! \brief alias template, so that it is clear(er) that
	//! transitional and experimental implementations are mutually
	//! incompatible
	//!
	//! \tparam OtherType The character type of the similiar type
	template<typename OtherType>
	using SimilarType = Word<OtherType, UseExperimental>;

	//! \brief type alias, so that our intent is easier to read, and
	//! in order to save on horizontal line space
	using ClusterType = GraphemeCluster<CharType, UseExperimental>;

	//! \brief construct empty word
	Word() : BaseClass()
	{}

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 8-bit integer that should
	//! be converted to a word
	explicit Word(frao::small_nat8 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 16-bit integer that
	//! should be converted to a word
	explicit Word(frao::small_nat16 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 32-bit integer that
	//! should be converted to a word
	explicit Word(frao::small_nat32 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number unsigned 64-bit integer that
	//! should be converted to a word
	explicit Word(frao::small_nat64 number) : BaseClass(number)
	{}

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 8-bit integer that should
	//! be converted to a word
	explicit Word(frao::small_int8 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 16-bit integer that should
	//! be converted to a word
	explicit Word(frao::small_int16 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 32-bit integer that should
	//! be converted to a word
	explicit Word(frao::small_int32 number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number signed 64-bit integer that should
	//! be converted to a word
	explicit Word(frao::small_int64 number) : BaseClass(number)
	{}

	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 32-bit floating point that should
	//! be converted to a word
	explicit Word(float number) : BaseClass(number)
	{}
	//! \brief create a word that has value of the number
	//! specified (ie: convert binary representation of
	//! number to human-readable represenatation)
	//!
	//! \param[in] number 64-bit floating point that should
	//! be converted to a word
	explicit Word(double number) : BaseClass(number)
	{}

	//! \brief Construct a word, from the specified string. Will get
	//! either the first or last word in the string, depending on the
	//! value of getLastWord
	//!
	//! \param[in] string pointer to a buffer containing a string.
	//! This string has at least one word. Must not be null
	//!
	//! \param[in] buffersize unsigned 64-bit integer than contains
	//! the size of the buffer pointed to in 'string'. Must not be 0
	//!
	//! \param[in] getLastWord iff true, will scan for the last valid
	//! word in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct word to read. If null, will not be written to
	Word(const CharType* string, frao::natural64 buffersize,
		 bool getLastWord = false, natural64* readStart = nullptr)
		: BaseClass(string, buffersize, getLastWord, readStart)
	{}

	//! \brief construct word from the first word in the
	//! specified string
	//!
	//! \param[in] string pointer to a buffer containing a
	//! string. This string has at least one word
	Word(const CharType* string)
		: BaseClass(string, getBufferLength(string), false, nullptr)
	{}

	//! \brief Construct a word from an ascii string
	//!
	//! \details Will take the bottom 7 bits of the suppied 8 bit
	//! characters, in the string. This is because ascii is only 7
	//! bits, so any encoding that uses the top bit would require
	//! specialised conversion to UTF8/16/32
	//!
	//! \param[in] string The ascii string, from which to take the
	//! first word. Only bottom 7 bits of each character are
	//! meaningful, because string is ascii
	template<typename OurCharType  = CharType,
			 std::enable_if_t<!std::is_same_v<OurCharType, charA>,
							  int> = 0>
	Word(const charA* string) : BaseClass(string)
	{}

	//! \brief construct word from the first word in the specified
	//! string
	//!
	//! \param[in] clusters A list of grapheme clusters, to make into
	//! a word (note: they must actually consitute a [single] word, as
	//! far as unicode algorithms are concerned)
	Word(std::initializer_list<ClusterType> clusters)
		: BaseClass(std::move(clusters))
	{}

	//! \brief Copy construct a Word, from another Word of any
	//! underlying type that differs from that of this
	//!
	//! \tparam OtherType The type of the other Word, that we are
	//! copying from. One of [charU8, char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The Word to copy from
	template<typename OtherType>
	Word(const SimilarType<OtherType>& rhs)
		: BaseClass(
			static_cast<const BaseClass_template<OtherType>&>(rhs))
	{}

	//! \brief standard copy constructor
	//!
	//! \param[in] rhs word to copy from
	Word(const ThisType& rhs) : BaseClass(rhs)
	{}

	//! \brief standard move constructor
	//!
	//! \param[in, out] rhs word to move from. Left in a
	//! potentially invalid state
	Word(ThisType&& rhs) : BaseClass(std::move(rhs))
	{}
	~Word() override = default;

	//! \brief standard copy assignment operator
	//!
	//! \param[in] rhs word to copy from
	ThisType& operator=(const ThisType& rhs)
	{
		static_cast<BaseClass&>(*this) = rhs;

		return *this;
	}

	//! \brief standard move assignment operator
	//!
	//! \param[in, out] rhs word to move from. Left in a
	//! potentially invalid state
	ThisType& operator=(ThisType&& rhs)
	{
		static_cast<BaseClass&>(*this) = std::move(rhs);

		return *this;
	}

	//! \brief add a grapheme cluster to the end of this
	//! word
	//!
	//! \param[in] cluster to append to word. Should be a
	//! cluster that makes sense to append to this word.
	//! (ie: don't try and append a space to 'dog', for
	//! example)
	void append(const ClusterType& cluster)
	{
		this->appendIMPL(cluster);
	}
	//! \brief remove all grapheme clusters from this word,
	//! rendering it empty
	void clear()
	{
		this->clearIMPL();
	}

	//! \brief returns memory size of word, in bytes
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! bytes
	frao::natural64 byteSize() const noexcept
	{
		return this->byteSizeIMPL();
	}
	//! \brief returns logical size of word, in grapheme
	//! clusters
	//!
	//! \returns unsigned 64-bit integer indicating size in
	//! number of grapheme clusters
	frao::natural64 size() const noexcept
	{
		return this->sizeIMPL();
	}

	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A Grapheme cluster reference of the same
	//! underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	ClusterType& get(frao::natural64 index) noexcept
	{
		return this->getIMPL(index);
	}
	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A const Grapheme cluster reference of the
	//! same underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	const ClusterType& get(frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A Grapheme cluster reference of the same
	//! underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	ClusterType& operator[](frao::natural64 index) noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief get a section of this Word. Words may be
	//! composed of multiple grapheme clusters, and this
	//! funciton will return the grapheme cluster at the
	//! index in question
	//!
	//! \returns A const Grapheme cluster reference of the
	//! same underlying type as this word
	//!
	//! \param[in] index indicating the section of the word
	//! to reference
	const ClusterType& operator[](
		frao::natural64 index) const noexcept
	{
		return this->getIMPL(index);
	}

	//! \brief converts this word to a number, storing in
	//! the passed variable
	//!
	//! \returns boolean indicating the success of the
	//! operation
	//!
	//! \param[out] number reference to where the result should be
	//! held
	template<typename NumberType>
	bool toNumber(NumberType& number) const noexcept
	{
		return this->toNumberIMPL(number);
	}

	//! \brief get the ordering of two words. Stand-in for
	//! the upcoming C++20 (?) <=> spaceship operator
	//!
	//! \returns <0, 0, >0 for less-than, equal-to and
	//! greater-than, respectively
	//!
	//! \param[in] rhs the value to compare on the rhs of
	//! the order. ie: (*this <=> rhs). word passed to rhs
	//! may have any underlying type with the same implementation
	//! strategy (exp or trans)
	template<typename OtherType>
	frao::integer64 getOrder(
		const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs);
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is before rhs
	//!
	//! \returns bool which has value true iff *this is
	//! strictly before rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator<(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) < 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after rhs
	//!
	//! \returns bool which has value true iff *this is
	//! strictly after rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator>(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) > 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is before or equal to rhs
	//!
	//! \returns bool which has value true iff *this is
	//! before ot equal to rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator<=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) <= 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! is after or equal to rhs
	//!
	//! \returns bool which has value true iff *this is
	//! after or equal to rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator>=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) >= 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! and rhs are equal
	//!
	//! \returns bool which has value true iff *this is
	//! equal to rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator==(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) == 0;
	}

	//! \brief uses getOrder() function to dermine if *this
	//! and rhs are unequal
	//!
	//! \returns bool which has value true iff *this is
	//! not equal to rhs
	//!
	//! \param[in] rhs const reference to another word, of
	//! any underlying type with the same implementation strategy (exp
	//! or trans)
	template<typename OtherType>
	bool operator!=(const SimilarType<OtherType>& rhs) const noexcept
	{
		return this->getOrderIMPL(rhs) != 0;
	}

	//! \brief convert word to different underlying
	//! representation. ie: convert UTF-16 to UTF-8 (say)
	template<typename OtherType>
	explicit operator SimilarType<OtherType>() const noexcept
	{
		// should call constructor for WordIMPL
		return Word<OtherType>(*this);
	}
};

// retore disabled c4251 warning, for using non-exporting class in
// interface
RESTORE_VC_WARNING(4251)

inline namespace Operators
{
//! \brief Construct a utf8 word, from a user-defined string literal
//!
//! \note function is actually called with u8"WORD"_uword, where WORD
//! is the word to construct the object from
//!
//! \note If more than one word is supplied, the first will be taken
//!
//! \returns a constructed utf8 word object
//!
//! \param[in] str Pointer to the string, that we should create the
//! word from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Word<charU8> operator"" _uword(const charU8* str, size_t size)
{
	return Word<charU8>{str, size};
}
//! \brief Construct a wide word, from a user-defined string literal
//!
//! \note function is actually called with L"WORD"_uword, where WORD
//! is the word to construct the object from
//!
//! \note If more than one word is supplied, the first will be taken
//!
//! \returns a constructed wide word object
//!
//! \param[in] str Pointer to the string, that we should create the
//! word from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Word<wchar_t> operator"" _uword(const wchar_t* str,
									   size_t		  size)
{
	return Word<wchar_t>{str, size};
}
//! \brief Construct a utf16 word, from a user-defined string literal
//!
//! \note function is actually called with u"WORD"_uword, where WORD
//! is the word to construct the object from
//!
//! \note If more than one word is supplied, the first will be taken
//!
//! \returns a constructed utf16 word object
//!
//! \param[in] str Pointer to the string, that we should create the
//! word from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Word<char16_t> operator"" _uword(const char16_t* str,
										size_t			size)
{
	return Word<char16_t>{str, size};
}
//! \brief Construct a utf32 word, from a user-defined string literal
//!
//! \note function is actually called with U"WORD"_uword, where WORD
//! is the word to construct the object from
//!
//! \note If more than one word is supplied, the first will be taken
//!
//! \returns a constructed utf32 word object
//!
//! \param[in] str Pointer to the string, that we should create the
//! word from. Not explicitly passed by the user, as this is a
//! user-defined string literal
//!
//! \param[in] size The size of the passed string. Not explicitly
//! passed by the user, as this is a user-defined string literal
inline Word<char32_t> operator"" _uword(const char32_t* str,
										size_t			size)
{
	return Word<char32_t>{str, size};
}
}  // namespace Operators

// instantiate normal Word versions, for API boundary
NUCLEUS_HEADER_EXTERN template class frao::Strings::Word<
	frao::charU8>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Word<char16_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Word<wchar_t>;
NUCLEUS_HEADER_EXTERN template class frao::Strings::Word<char32_t>;

//! \brief Type used to represent UTF8-encoded words
typedef Word<charU8> utf8word;

//! \brief Type used to represent UTF16-encoded words
typedef Word<char16_t> utf16word;

//! \brief Type used to represent UTF32-encoded words
typedef Word<char32_t> utf32word;

//! \brief Type used to represent windows-encoded words
//! (windows basically uses UTF16. This class is
//! distinguished from the UTF16 version because it uses
//! wchar rather than char16. nb: wchar is platform
//! dependant!). In future this class may be used for
//! platform-native wchar type (on any platform?)
typedef Word<wchar_t> wideword;
}  // namespace Strings
}  // namespace frao

#endif	// !FRAO_BASIC_API_STRINGS_WORD
