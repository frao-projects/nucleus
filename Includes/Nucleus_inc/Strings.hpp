#ifndef FRAO_NUCLEUS_STRINGS
#define FRAO_NUCLEUS_STRINGS

//! \file
//!
//! \brief Header of all defintions, functions, and classes, relating
//! to the unicode string handling, of the software
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/BasicString.hpp"
#include "Strings/CodePoint.hpp"
#include "Strings/GraphemeCluster.hpp"
#include "Strings/StringDefines.hpp"
#include "Strings/Word.hpp"

#endif  // !FRAO_BASIC_STRINGS_HEADER
