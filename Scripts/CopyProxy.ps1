param(
	[Parameter(Mandatory=$true)]
	[string]$Target,
	[Parameter(Mandatory=$true)]
	[ValidateScript({Test-Path -Path $_ -PathType Leaf})]
	[string]$FilePath
)

Copy-Item -Path "$FilePath" -Destination "$Target"