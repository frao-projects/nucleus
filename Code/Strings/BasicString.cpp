//! \file
//!
//! \brief Implementation of the frao unicode string class(es)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Strings/BasicString.hpp"


// If we're not exporting or importing a dll interface, then we need
// to instantiate the templates
#if defined(NUCLEUS_HEADER_ONLY)
// frao::Strings::Transitional::StringIMPL instantiations
template class frao::Strings::Transitional::StringIMPL<
	frao::charU8,
	frao::Strings::AsciiNumericalPunctuation<frao::charU8, false>>;
template class frao::Strings::Transitional::StringIMPL<
	wchar_t,
	frao::Strings::AsciiNumericalPunctuation<wchar_t, false>>;
template class frao::Strings::Transitional::StringIMPL<
	char16_t,
	frao::Strings::AsciiNumericalPunctuation<char16_t, false>>;
template class frao::Strings::Transitional::StringIMPL<
	char32_t,
	frao::Strings::AsciiNumericalPunctuation<char32_t, false>>;

#ifdef FRAO_EXPERIMENTAL_STRINGS_ALLOWED
template class frao::Strings::Experimental::StringIMPL<
	frao::charU8,
	frao::Strings::AsciiNumericalPunctuation<frao::charU8, true>>;
template class frao::Strings::Experimental::StringIMPL<
	wchar_t, frao::Strings::AsciiNumericalPunctuation<wchar_t, true>>;
template class frao::Strings::Experimental::StringIMPL<
	char16_t,
	frao::Strings::AsciiNumericalPunctuation<char16_t, true>>;
template class frao::Strings::Experimental::StringIMPL<
	char32_t,
	frao::Strings::AsciiNumericalPunctuation<char32_t, true>>;
#endif
#endif

//! \class frao::Strings::Transitional::StringIMPL
//!
//! \todo We need a statically-allocated version of this,
//! that doesn't put string on the heap. Failing that, we
//! need a way of telling it to use a specific memory
//! location (ie: we need to be able to pass it a memory
//! buffer that was allocated outside this class). We need
//! this so that we can guarantee that Error types (which
//! use these strings) will work when the process is out of
//! memory, by using some memory that is kept around just
//! for critical failures
//!
//! \todo String class should have capability to sub-string
//! in terms of all of code-points / grapheme clusters /
//! words
//!
//! \todo We need to add size() functions to string the
//! return the size on terms of words / grapheme clusters /
//! code-points, so that we can index through those things
//! effectively
//!
//! \todo add reserve functions in terms of code-points /
//! grapheme clusters / words?
//!
//! \todo add 'shrink to fit' functions, as a counterpoint
//! to reserve

// If we're not exporting or importing a dll interface, then we need
// to instantiate the templates
#if defined(NUCLEUS_HEADER_ONLY)

template class frao::Strings::Basic_String<frao::charU8>;
template class frao::Strings::Basic_String<char16_t>;
template class frao::Strings::Basic_String<wchar_t>;
template class frao::Strings::Basic_String<char32_t>;

#endif