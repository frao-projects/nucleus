using Module .\BuildFunctions.psm1

param(
	# The type of task to perform
	[Parameter(Mandatory=$true)]
	[string]$CommandName,
	#Extra inputs for some commands
    [parameter(ValueFromRemainingArguments = $true)]
	[string[]]$ExtraInputs
)

[string]$FraoDir = $PSScriptRoot
[string]$OSString = Read-OSString

[string]$MesonPathsJsonFile = "$FraoDir/${OSString}_meson_paths.json"
Write-Host "Consequent *_meson_paths.json: $MesonPathsJsonFile"

[string]$ScriptPath

if(Test-Path -Path "$MesonPathsJsonFile" -PathType Leaf) {
	$local:MesonPathsFileData = (Get-Content -Path "$MesonPathsJsonFile" -Raw |
		ConvertFrom-Json)

	Write-Host "In meson_paths reader"

	$ScriptPath = $MesonPathsFileData.scripts_path
} else {
	throw "Could not determine frao scripts path: file '$MesonPathsJsonFile' did not exist"
}

Write-Host "Using:"
Write-Host "Scripts directory = '$ScriptPath'"

if($CommandName -ne "Echo") {
. "$ScriptPath/TaskController.ps1" -CommandName "$CommandName" -FraoDir "$FraoDir" -ExtraInputs $ExtraInputs
}