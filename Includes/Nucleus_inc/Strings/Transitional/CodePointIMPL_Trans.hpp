#ifndef FRAO_NUCLEUS_STRINGS_CODEPOINT_IMPLEMENTATION_TRANSITIONAL
#define FRAO_NUCLEUS_STRINGS_CODEPOINT_IMPLEMENTATION_TRANSITIONAL

//! \file
//!
//! \author Freya Rhiannon Mayger
//!
//! \brief Header of the transitional implementation of unicode
//! codepoint class(es)
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <climits>		//for MB_LEN_MAX
#include <cuchar>		//for conversion to/from char16_t and char32_t
#include <string>		//for returning the name of the std::locale
#include <type_traits>	// for is_same
#include "../../Environment.hpp"

namespace frao
{
inline namespace Strings
{
//! \namespace frao::Strings::Transitional
//!
//! \brief Namespace that is used for the transition (partial unicode)
//! strings code
namespace Transitional
{
namespace IMPL
{
//! \brief Attempt to set our current locale, and the global c++
//! locale, to be the one specified
//!
//! \returns The value true iff setting the locale was successful
//!
//! \param[in] localeName Null-terminated string of the locale name
bool setOurLocale(const charA* localeName) noexcept;
//! \brief Get the name of the currently specified locale
//!
//! \returns A std::string with the name of our currently set locale
const std::string getOurLocaleName() noexcept;

//! \brief Convert a character to its UTF8 representation
//!
//! \tparam CharType The type of character to convert to UTF8. Must be
//! char16_t, char32_t, or wchar_t (for the specialisation)
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] input The character to convert to UTF8
//!
//! \param[out] outputBuffer A pointer to a charU8 buffer, which shall
//! receive the output, if any. Length of buffer must be at least
//! MB_LEN_MAX
//!
//! \param[out] writtenBytes The number of bytes actually written to
//! the buffer
template<typename CharType>
bool convToUTF8(CharType input, charU8* outputBuffer,
				natural64& writtenBytes) noexcept
{
	static_assert(
		std::is_same_v<
			CharType, char16_t> || std::is_same_v<CharType, char32_t>,
		"We need to have a UTF16/32 type as input!");

	std::mbstate_t state = std::mbstate_t{};
	size_t		   returnCode;

	if constexpr (std::is_same_v<CharType, char16_t>)
	{
		returnCode = std::c16rtomb(
			reinterpret_cast<char*>(outputBuffer), input, &state);
	} else
	{
		returnCode = std::c32rtomb(
			reinterpret_cast<char*>(outputBuffer), input, &state);
	}

	switch (returnCode)
	{
		//-1, converted to size_t, is the failure cases of
		// c16rtomb()
		case static_cast<size_t>(-1):
			return false;
		case 0:
			outputBuffer[0] = '\0';
			return true;
		default:
			writtenBytes = returnCode;
			return true;
	}
}

//! \brief Convert a character to its UTF8 representation. A
//! specialisation for wchar_t, which routes to the correct (UTF16/32)
//! implementation
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] input The character to convert to UTF8
//!
//! \param[out] outputBuffer A pointer to a charU8 buffer, which shall
//! receive the output, if any. Length of buffer must be at least
//! MB_LEN_MAX
//!
//! \param[out] writtenBytes The number of bytes actually written to
//! the buffer
template<>
inline bool convToUTF8<wchar_t>(wchar_t input, charU8* outputBuffer,
								natural64& writtenBytes) noexcept
{
	if constexpr (g_WChar_Large)
	{
		// then we have 4 byte wchar
		char32_t realInput = *reinterpret_cast<char32_t*>(&input);

		return convToUTF8(realInput, outputBuffer, writtenBytes);
	} else
	{
		// then we have 2 byte wchar
		char16_t realInput = *reinterpret_cast<char16_t*>(&input);

		return convToUTF8(realInput, outputBuffer, writtenBytes);
	}
}

//! \brief Convert a utf8 codepoint to a wide one
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to convert, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
//!
//! \param[out] output Where to put the resultant wide character
bool convToWCHAR(const charU8* utf8Buffer, natural64 bufferLength,
				 wchar_t& output) noexcept;
//! \brief Convert a utf16 codepoint to a wide one
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a wide character
//!
//! \param[out] output Where to put the resultant wide character
bool convToWCHAR(char16_t character, wchar_t& output) noexcept;
//! \brief Convert a utf32 codepoint to a wide one
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a wide character
//!
//! \param[out] output Where to put the resultant wide character
bool convToWCHAR(char32_t character, wchar_t& output) noexcept;

//! \brief Convert a utf8 codepoint to a utf16
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to convert, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
//!
//! \param[out] output Where to put the resultant utf16 character
bool convToUTF(const charU8* utf8Buffer, natural64 bufferLength,
			   char16_t& output) noexcept;
//! \brief Convert a wide codepoint to a utf16
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a utf16 character
//!
//! \param[out] output Where to put the resultant utf16 character
bool convToUTF(wchar_t character, char16_t& output) noexcept;
//! \brief Convert a utf32 codepoint to a utf16
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a utf16 character
//!
//! \param[out] output Where to put the resultant utf16 character
bool convToUTF(char32_t character, char16_t& output) noexcept;
//! \brief Convert a utf8 codepoint to a utf32
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to convert, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
//!
//! \param[out] output Where to put the resultant utf32 character
bool convToUTF(const charU8* utf8Buffer, natural64 bufferLength,
			   char32_t& output) noexcept;
//! \brief Convert a wide codepoint to a utf32
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a utf32 character
//!
//! \param[out] output Where to put the resultant utf32 character
bool convToUTF(wchar_t character, char32_t& output) noexcept;
//! \brief Convert a utf16 codepoint to a utf32
//!
//! \returns true iff the conversion was successful
//!
//! \param[in] character The character to convert to a utf32 character
//!
//! \param[out] output Where to put the resultant utf32 character
bool convToUTF(char16_t character, char32_t& output) noexcept;

//! \brief Get the length of the next UTF8 codepoint, in bytes
//!
//! \returns true if we successfully got the length
//!
//! \param[in] inputData A pointer to the buffer containing the
//! codepoint, that we wish to get the length of
//!
//! \param[in] inputSize The length, in bytes, of the buffer pointed
//! to by inputData
//!
//! \param[out] calculatedSize The number of bytes, of the next UTF8
//! codepoint, if valid
bool lengthNextUTF8(const charU8* inputData, natural64 inputSize,
					natural64& calculatedSize) noexcept;
//! \brief Write the next valid codepoint, and only the next
//! codepoint, into the buffer. Will walk string, until either a valid
//! codepoint or the end of string is found
//!
//! \returns true if we successfully got the codepoint
//!
//! \param[in] inputData A pointer to the buffer containing the
//! codepoint, that we wish to get
//!
//! \param[in] inputSize The length, in bytes, of the buffer pointed
//! to by inputData
//!
//! \param[out] outputBuffer A pointer to a char buffer, which shall
//! receive the output, if any. Length of buffer must be at least
//! MB_LEN_MAX
//!
//! \param[out] startPosition The position of the acquired codepoint,
//! in the inputData string
//!
//! \param[out] writtenBytes The number of bytes actually written to
//! the buffer
bool acquireNextUTF8(const charU8* inputData, natural64 inputSize,
					 charU8* outputBuffer, natural64& startPosition,
					 natural64& writtenBytes) noexcept;
//! \brief Write the last codepoint in the input data, and only the
//! last codepoint, into the buffer
//!
//! \returns true if we successfully got the codepoint
//!
//! \param[in] inputData A pointer to the buffer containing the
//! codepoint, that we wish to get
//!
//! \param[in] inputSize The length, in bytes, of the buffer pointed
//! to by inputData
//!
//! \param[out] outputBuffer A pointer to a char buffer, which shall
//! receive the output, if any. Length of buffer must be at least
//! MB_LEN_MAX
//!
//! \param[out] startPosition The position of the acquired codepoint,
//! in the inputData string
//!
//! \param[out] writtenBytes The number of bytes actually written to
//! the buffer
bool acquireLastUTF8(const charU8* inputData, natural64 inputSize,
					 charU8* outputBuffer, natural64& startPosition,
					 natural64& writtenBytes) noexcept;

//! \brief Determine the ordering of two UTF8 codepoints. Conceptually
//! identical to lhs_codepoint <=> rhs_codepoint
//!
//! \returns A value less than 0, if lhs is ordered before rhs. A
//! value more than 0, if rhs is ordered before lhs. Precisely 0, if
//! the two are equal or equivalent
//!
//! \param[in] lhsBuffer A pointer to the buffer containing the
//! codepoint, that we wish to examine on the left hand side
//!
//! \param[in] lhsLength The length, in bytes, of the buffer pointed
//! to by lhsBuffer
//!
//! \param[in] rhsBuffer A pointer to the buffer containing the
//! codepoint, that we wish to examine on the right hand side
//!
//! \param[in] rhsLength The length, in bytes, of the buffer pointed
//! to by rhsBuffer
integer64 orderCodepointsUTF8Only(const charU8* lhsBuffer,
								  natural64		lhsLength,
								  const charU8* rhsBuffer,
								  natural64		rhsLength) noexcept;

//! \brief Determine the ordering of a UTF8 codepoint, with a non-UTF8
//! codepoint. Conceptually identical to utf8_codepoint <=>
//! non_utf8_codepoint
//!
//! \tparam CharType The type of the variable used to hold the
//! non-utf8 codepoint
//!
//! \returns A value less than 0, if the utf8 codepoint is ordered
//! before the non-utf8 codepoint. A value more than 0, if the
//! non-utf8 codepoint is ordered before the utf8codepoint. Precisely
//! 0, if the two are equal or equivalent
//!
//! \param[in] lhsBuffer A pointer to the buffer containing the utf8
//! codepoint, that we wish to examine on the left hand side
//!
//! \param[in] lhsLength The length, in bytes, of the buffer pointed
//! to by lhsBuffer
//!
//! \param[in] rhs The non-utf8 codepoint, that we wish to compare to
//! the utf8 codepoint, on the rhs
template<typename CharType>
integer64 orderCodepointsUTF8Partially(const charU8* lhsBuffer,
									   natural64	 lhsLength,
									   CharType		 rhs) noexcept
{
	static_assert(sizeof(CharType) != sizeof(charU8),
				  "This function should not be used with a UTF8 "
				  "type, on the rhs");

	charU8	  rhsBuffer[MB_LEN_MAX];
	natural64 rhsLength;

	if (!convToUTF8(rhs, rhsBuffer, rhsLength))
	{
		// then we can't get a rhs to compare. Sort the invalid AFTER
		// the valid
		return -1;
	}

	return orderCodepointsUTF8Only(lhsBuffer, lhsLength, rhsBuffer,
								   rhsLength);
}
//! \brief Determine the ordering of non-UTF8 codepoints. Conceptually
//! identical to codepoint <=> codepoint
//!
//! \tparam LHSCharType The type of the variable used to hold the left
//! hand side, non-utf8 codepoint
//!
//! \tparam RHSCharType The type of the variable used to hold the
//! right hand side, non-utf8 codepoint
//!
//! \returns A value less than 0, if the lhs is ordered before the
//! rhs. A value more than 0, if the rhs is ordered before the lhs.
//! Precisely 0, if the two are equal or equivalent
//!
//! \param[in] lhs The non-utf8 codepoint, that we wish to compare on
//! the lhs
//!
//! \param[in] rhs The non-utf8 codepoint, that we wish to compare on
//! the rhs
template<typename LHSCharType, typename RHSCharType>
integer64 orderCodepointsNoUTF8(LHSCharType lhs,
								RHSCharType rhs) noexcept
{
	static_assert(
		sizeof(LHSCharType) != sizeof(charU8),
		"This function should not be used with a UTF8 type");
	static_assert(
		sizeof(RHSCharType) != sizeof(charU8),
		"This function should not be used with a UTF8 type");

	// we can just cast everything to a larger type. Specifically,
	// a 64-bit int, which can hold any of the otehr possible
	// values, and then just take the difference, to find the
	// order
	integer64 thisChar	= static_cast<integer64>(lhs),
			  otherChar = static_cast<integer64>(rhs);

	return thisChar - otherChar;
}

//! \brief Check whether a wide character is whitespace
//!
//! \returns true iff the character is whitespace
//!
//! \param[in] character The character that we wish to ascertain the
//! whitespace-ness of
bool isWhitespace_WCHAR(wchar_t character) noexcept;
//! \brief Check whether a utf8 character is whitespace
//!
//! \returns true iff the character is whitespace
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to check, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
bool isWhitespace_UTF8(const charU8* utf8Buffer,
					   natural64	 bufferLength) noexcept;
//! \brief Check whether a utf16 character is whitespace
//!
//! \returns true iff the character is whitespace
//!
//! \param[in] character The character that we wish to ascertain the
//! whitespace-ness of
bool isWhitespace_UTF16(char16_t character) noexcept;
//! \brief Check whether a utf32 character is whitespace
//!
//! \returns true iff the character is whitespace
//!
//! \param[in] character The character that we wish to ascertain the
//! whitespace-ness of
bool isWhitespace_UTF32(char32_t character) noexcept;

//! \brief Check whether a wide character is blank
//!
//! \returns true iff the character is blank
//!
//! \param[in] character The character that we wish to ascertain the
//! blank-ness of
bool isBlank_WCHAR(wchar_t character) noexcept;
//! \brief Check whether a utf8 character is blank
//!
//! \returns true iff the character is blank
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to check, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
bool isBlank_UTF8(const charU8* utf8Buffer,
				  natural64		bufferLength) noexcept;
//! \brief Check whether a utf16 character is blank
//!
//! \returns true iff the character is blank
//!
//! \param[in] character The character that we wish to ascertain the
//! blank-ness of
bool isBlank_UTF16(char16_t character) noexcept;
//! \brief Check whether a utf32 character is blank
//!
//! \returns true iff the character is blank
//!
//! \param[in] character The character that we wish to ascertain the
//! blank-ness of
bool isBlank_UTF32(char32_t character) noexcept;

//! \brief Check whether a wide character is a newline
//!
//! \returns true iff the character is a newline
//!
//! \param[in] character The character that we wish to ascertain the
//! newline-ness of
bool isNewline_WCHAR(wchar_t character) noexcept;
//! \brief Check whether a utf8 character is a newline
//!
//! \returns true iff the character is a newline
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to check, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
bool isNewline_UTF8(const charU8* utf8Buffer,
					natural64	  bufferLength) noexcept;
//! \brief Check whether a utf16 character is a newline
//!
//! \returns true iff the character is a newline
//!
//! \param[in] character The character that we wish to ascertain the
//! newline-ness of
bool isNewline_UTF16(char16_t character) noexcept;
//! \brief Check whether a utf32 character is a newline
//!
//! \returns true iff the character is a newline
//!
//! \param[in] character The character that we wish to ascertain the
//! newline-ness of
bool isNewline_UTF32(char32_t character) noexcept;

//! \brief Check whether a wide character is numeric
//!
//! \returns true iff the character is a number
//!
//! \param[in] character The character that we wish to ascertain the
//! numeric-ness of
bool isNumeric_WCHAR(wchar_t character) noexcept;
//! \brief Check whether a utf8 character is numeric
//!
//! \returns true iff the character is a number
//!
//! \param[in] utf8Buffer A buffer, which contains at least one utf8
//! codepoint to check, at the start
//!
//! \param[in] bufferLength The size of the utf8Buffer, in bytes
bool isNumeric_UTF8(const charU8* utf8Buffer,
					natural64	  bufferLength) noexcept;
//! \brief Check whether a utf16 character is numeric
//!
//! \returns true iff the character is a number
//!
//! \param[in] character The character that we wish to ascertain the
//! numeric-ness of
bool isNumeric_UTF16(char16_t character) noexcept;
//! \brief Check whether a utf32 character is numeric
//!
//! \returns true iff the character is a number
//!
//! \param[in] character The character that we wish to ascertain the
//! numeric-ness of
bool isNumeric_UTF32(char32_t character) noexcept;
}  // namespace IMPL

//! \brief Transitional implementation class, for codepoints
//!
//! \tparam CharType The underlying type of the codepoint [char16_t,
//! char32_t, wchar_t]. There is a specicalisation for charU8
template<typename CharType>
class CodePointIMPL;

//! \brief Specialisation of CodePointIMPL, for multiple byte UTF8,
//! which must be stored differently
template<>
class NUCLEUS_LIB_API CodePointIMPL<charU8>
{
	//! \brief c array, of maximum possible utf8 codepoint length,
	//! which holds the actual utf8 codepoint
	charU8 m_Character[MB_LEN_MAX];
	//! \brief The actual length of the utf8 codepoint stored in
	//! m_Character. Used for convenience
	natural64 m_ActualLength;

   public:
	template<typename>
	friend class CodePointIMPL;

	//! \brief Construct null codepoint
	CodePointIMPL()
		: m_Character{0x0, 0x0, 0x0, 0x0, 0x0}, m_ActualLength(0)
	{}
	//! \brief Construct a codepoint, from the specified string. Will
	//! get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \param[in] codepoint String that contains the
	//! code-point
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the last
	//! valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct codepoint to read. If null, will not be written to
	CodePointIMPL(const charU8* codepoint, frao::natural64 buffersize,
				  bool getLastCodepoint, natural64* readStart)
		: m_Character{0x0, 0x0, 0x0, 0x0, 0x0}, m_ActualLength(0)
	{
		natural64 readBytes, startPos;

		if (getLastCodepoint)
		{
			if (IMPL::acquireLastUTF8(codepoint, buffersize,
									  m_Character, startPos,
									  readBytes))
			{
				runtime_assert(readBytes <= MB_LEN_MAX);
				m_ActualLength = static_cast<natural8>(readBytes);

				if (readStart != nullptr)
				{
					*readStart = startPos;
				}
			} else
			{
				throw invalid_input();
			}
		} else
		{
			if (IMPL::acquireNextUTF8(codepoint, buffersize,
									  m_Character, startPos,
									  readBytes))
			{
				runtime_assert(readBytes <= MB_LEN_MAX);
				m_ActualLength = static_cast<natural8>(readBytes);

				if (readStart != nullptr)
				{
					*readStart = startPos;
				}
			} else
			{
				throw invalid_input();
			}
		}
	}

	//! brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF8
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePointIMPL(charA asciiChar)
		: m_Character{static_cast<charU8>(asciiChar & 0x7F), 0x0, 0x0,
					  0x0, 0x0},
		  m_ActualLength(1)
	{}

	//! \brief Copy construct a codepoint, from another codepoint of
	//! any underlying type, except charU8
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from. One of [char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	CodePointIMPL(const CodePointIMPL<OtherType>& rhs)
		: m_Character(), m_ActualLength(0)
	{
		if (!IMPL::convToUTF8(rhs.m_Character, &m_Character[0],
							  m_ActualLength))
		{
			throw conversion_failure();
		}
	}
	//! \brief Copy construct a codepoint, from a utf8 codepoint
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL(const CodePointIMPL<charU8>& rhs) = default;
	//! \brief Move construct a codepoint, from a utf8 codepoint
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL(CodePointIMPL<charU8>&& rhs) = default;
	//! \brief destroy the codepoint, and call any derived destructors
	virtual ~CodePointIMPL() = default;

	//! \brief Copy assign a codepoint, from a utf8 codepoint
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL<charU8>& operator		  =(
		   const CodePointIMPL<charU8>& rhs) = default;
	//! \brief Move assign a codepoint, from a utf8 codepoint
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL<charU8>& operator=(CodePointIMPL<charU8>&& rhs) =
		default;

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return sizeof(charU8) * m_ActualLength;
	}
	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 sizeIMPL() const noexcept
	{
		return m_ActualLength;
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	charU8& getIMPL(frao::natural64 index) noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Character[index];
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const charU8& getIMPL(frao::natural64 index) const noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Character[index];
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepointIMPL() const noexcept
	{
		return static_cast<char32_t>(m_Character[0]);
	}
};

template<typename CharType>
class NUCLEUS_LIB_API CodePointIMPL
{
	//! \brief stored codepoint
	CharType m_Character;

   public:
	template<typename>
	friend class CodePointIMPL;

	//! \brief Construct null codepoint
	CodePointIMPL() : m_Character(0x0)
	{}
	//! \brief Construct a codepoint, from the specified string. Will
	//! get either the first or last codepoint in the string,
	//! depending on the value of getLastCodepoint
	//!
	//! \param[in] codepoint String that contains the
	//! code-point. Must not be null
	//!
	//! \param[in] buffersize Size of buffer that holds
	//! string. Must not be zero
	//!
	//! \param[in] getLastCodepoint iff true, will scan for the last
	//! valid codepoint in the string, instead of the first
	//!
	//! \param[out] readStart a pointer to a variable, that will
	//! receive the location, that was determined as the start of the
	//! correct codepoint to read. If null, will not be written to
	CodePointIMPL(const CharType* codepoint,
				  frao::natural64 buffersize, bool getLastCodepoint,
				  natural64* readStart)
		: m_Character(getLastCodepoint ? codepoint[buffersize - 1]
									   : codepoint[0])
	{
		runtime_assert(codepoint != nullptr);
		runtime_assert(buffersize != 0);

		if (readStart != nullptr)
		{
			*readStart = getLastCodepoint ? buffersize - 1 : 0;
		}
	}

	//! brief Construct a codepoint, from an ascii character
	//!
	//! \details Will take the bottom 7 bits of the suppied 8bit
	//! character. This is because ascii is only 7 bits, so any
	//! encoding that uses the top bit would require specialised
	//! conversion to UTF16/32
	//!
	//! \param[in] asciiChar The ascii character to store in the
	//! codepoint. The bottom 7 bits will be taken, and the top bit
	//! ignored, since ascii does not use the top bit
	CodePointIMPL(charA asciiChar) : m_Character()
	{
		// get the bottom 7 bits of the supplied character, since
		// ascii only occupies the bottom 7 bits, and those are the
		// bits that utf8 also has (which means we can use utf8 ->
		// other converters)
		charU8 retreivedChar = static_cast<charU8>(asciiChar & 0x7F);

		if constexpr (std::is_same_v<CharType, wchar_t>)
		{
			// we have to use a different funciton for wchar_t
			// instantiations
			if (!IMPL::convToWCHAR(&retreivedChar, 1, m_Character))
			{
				throw conversion_failure();
			}
		} else
		{
			if (!IMPL::convToUTF(&retreivedChar, 1, m_Character))
			{
				throw conversion_failure();
			}
		}
	}

	//! \brief Copy construct a codepoint, from another codepoint of
	//! any underlying type, except charU8
	//!
	//! \tparam OtherType The type of the other codepoint, that we are
	//! copying from. One of [char16_t, wchar_t, char32_t]
	//!
	//! \param[in] rhs The codepoint to copy from
	template<typename OtherType>
	explicit CodePointIMPL(const CodePointIMPL<OtherType>& rhs)
		: m_Character()
	{
		if constexpr (std::is_same_v<CharType, wchar_t>)
		{
			// we have to use a different funciton for wchar_t
			// instantiations
			if (!IMPL::convToWCHAR(rhs.m_Character, m_Character))
			{
				throw conversion_failure();
			}
		} else
		{
			if (!IMPL::convToUTF(rhs.m_Character, m_Character))
			{
				throw conversion_failure();
			}
		}
	}
	//! \brief Copy construct a codepoint, from a utf8 codepoint
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL(const CodePointIMPL<charU8>& rhs) : m_Character()
	{
		if constexpr (std::is_same_v<CharType, wchar_t>)
		{
			// we have to use a different funciton for wchar_t
			// instantiations
			if (!IMPL::convToWCHAR(rhs.m_Character,
								   rhs.m_ActualLength, m_Character))
			{
				throw conversion_failure();
			}
		} else
		{
			if (!IMPL::convToUTF(rhs.m_Character, rhs.m_ActualLength,
								 m_Character))
			{
				throw conversion_failure();
			}
		}
	}
	//! \brief Copy construct a codepoint, from another codepoint, of
	//! the same type
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL(const CodePointIMPL<CharType>& rhs)
		: m_Character(rhs.m_Character)
	{}
	//! \brief move construct a codepoint, from another codepoint, of
	//! the same type
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL(CodePointIMPL<CharType>&& rhs)
		: m_Character(std::move(rhs.m_Character))
	{}
	//! \brief destroy the codepoint, and call any derived destructors
	virtual ~CodePointIMPL() = default;

	//! \brief Copy assign a codepoint, from a non-utf8 codepoint
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL<CharType>& operator		=(
		   const CodePointIMPL<CharType>& rhs) = default;
	//! \brief Copy assign a codepoint, from a utf8 codepoint
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to copy from
	CodePointIMPL<CharType>& operator=(
		const CodePointIMPL<charU8>& rhs)
	{
		if constexpr (std::is_same_v<CharType, wchar_t>)
		{
			// we have to use a different funciton for wchar_t
			// instantiations
			if (!IMPL::convToWCHAR(rhs.m_Character,
								   rhs.m_ActualLength, m_Character))
			{
				throw conversion_failure();
			}
		} else
		{
			if (!IMPL::convToUTF(rhs.m_Character, rhs.m_ActualLength,
								 m_Character))
			{
				throw conversion_failure();
			}
		}

		return *this;
	}

	//! \brief Move assign a codepoint, from another codepoint, of the
	//! same type
	//!
	//! \returns A reference to this, so that the operations can be
	//! chained
	//!
	//! \param[in] rhs The codepoint to move from
	CodePointIMPL<CharType>& operator  =(
		  CodePointIMPL<CharType>&& rhs) = default;

	//! \brief gets actual memory size (in bytes) of this
	//! code-point
	//!
	//! \returns unsigned 64-bit integer
	frao::natural64 byteSizeIMPL() const noexcept
	{
		return sizeof(CharType);
	}
	//! \brief gets logical size of this code-point, in
	//! terms of the underlying character type used. ie: a 4
	//! byte UTF-8 code-point would return 4. But all UTF-32
	//! code-points would return 1
	//!
	//! \returns unsigned 64-bit integer with the logical
	//! size of this code point
	frao::natural64 sizeIMPL() const noexcept
	{
		return 1;
	}

	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	CharType& getIMPL(frao::natural64 index) noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Character;
	}
	//! \brief get a section of this code-point. useful for
	//! UTF-8 2+ byte code-points, and UTF-16 surrogate
	//! pairs (need to check that one?). If code-point is
	//! not one of those, only an index of 0 may be used
	//!
	//! \returns Underlying character reference to that
	//! section of the code-point
	//!
	//! \param[in] index unsigned 64-bit integer indicating
	//! position in code-point
	const CharType& getIMPL(frao::natural64 index) const noexcept
	{
		runtime_assert(index < sizeIMPL());

		return m_Character;
	}

	//! \brief Get the full codepoint as UTF32
	//!
	//! \note Used for situations where we need to check for specific
	//! codepoints, without creating a series of codepoint objects for
	//! comparison, or dealing with details of the specific encoding
	//!
	//! \returns The full UTF32 codepoint
	char32_t getFullCodepointIMPL() const noexcept
	{
		return static_cast<char32_t>(m_Character);
	}
};

}  // namespace Transitional
}  // namespace Strings
}  // namespace frao

#endif