#ifndef FRAO_NUCLEUS_TOPINCL
#define FRAO_NUCLEUS_TOPINCL

//! \file
//!
//! \brief Header of entire Nucleus api
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Environment.hpp"
#include "BitUtilities.hpp"
#include "Allocate.hpp"
#include "Strings.hpp"
#include "Time.hpp"
#include "Error.hpp"

#endif